// Prior2DView.cpp : implementation of the CPrior2DView class
//

#include "stdafx.h"
#include "Prior2D.h"

#include "Prior2DDoc.h"
#include "Prior2DView.h"
#include "Prior2DModule.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPrior2DView


IMPLEMENT_DYNCREATE(CPrior2DView, CFormView)

BEGIN_MESSAGE_MAP(CPrior2DView, CFormView)
	ON_COMMAND(ID_FILE_PRINT, &CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_CHECK_NOURGENT, &CPrior2DView::OnBnClickedCheckNourgent)
	ON_BN_CLICKED(IDC_CHECK_URGENT, &CPrior2DView::OnBnClickedCheckUrgent)
//	ON_COMMAND(ID_FILE_PRINT, &CFormView::OnPrint)
END_MESSAGE_MAP()

// CPrior2DView construction/destruction

CPrior2DView::CPrior2DView()
	: CFormView(CPrior2DView::IDD)
{
	// TODO: add construction code here

}

CPrior2DView::~CPrior2DView()
{
}

void CPrior2DView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CPrior2DView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CPrior2DView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

}


// CPrior2DView diagnostics

#ifdef _DEBUG
void CPrior2DView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPrior2DView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CPrior2DDoc* CPrior2DView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPrior2DDoc)));
	return (CPrior2DDoc*)m_pDocument;
}
#endif //_DEBUG


// CPrior2DView message handlers

BOOL CPrior2DView::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;
	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));

	return DoPreparePrinting(pInfo);
}

void CPrior2DView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void CPrior2DView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void CPrior2DView::OnFilePrint(CDC* pDC, CPrintInfo* pInfo)
{
}


WCHAR* char2wchar(char* inStr)
{
	WCHAR* wstr = new WCHAR[strlen(inStr)+1];
	for(int i = strlen(inStr)-1; i >= 0 ; i--) wstr[i] = inStr[i];
	wstr[strlen(inStr)] = 0;
	return wstr;
}

const GUID guidBmp = { 0x557cf400, 0x1a04, 0x11d3, { 0x9a, 0x73, 0x00, 0x00, 0xf8, 0x1e, 0xf3, 0x2e } };

void CPrior2DView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	Prior2DModule p2dPrinter;

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i/%.2i/%i"), time.wDay, time.wMonth, time.wYear);
	p2dPrinter.setDate(cstr);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED,
		 bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	p2dPrinter.setUrgent(cstr);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	p2dPrinter.setOrdinary(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCurrency(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentPurpose(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN1));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN2));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN3));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUNP_Third(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBudgetPaymentCode(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ORDER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentOrder(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCodeBankaCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSchetBankCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCorrespondentA(cstr);

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	bool bPL = cPL->GetCheck()==BST_CHECKED;
	cstr.Format(bPL?_T("X"):_T(" "));
	p2dPrinter.setPL(cstr);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	bool bBN = cBN->GetCheck()==BST_CHECKED;
	cstr.Format(bBN?_T("X"):_T(" "));
	p2dPrinter.setPL(cstr);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	bool bPL_BN = cPL_BN->GetCheck()==BST_CHECKED;
	cstr.Format(bPL_BN?_T("X"):_T(" "));
	p2dPrinter.setPL_BN(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMISSION_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setComissiaInvoice(cstr);

	int ibw = 2480;
	int ibh = 3508;
	CDC dc;
	CBitmap bmp;
	p2dPrinter.BuildBarcode(&dc, &bmp, false, ibw, ibh);
	pDC->StretchBlt(pInfo->m_rectDraw.left, pInfo->m_rectDraw.top, pInfo->m_rectDraw.Width(), pInfo->m_rectDraw.Height(), &dc, 0, 0, ibw, ibh, SRCCOPY);
}

void CPrior2DView::OnBnClickedCheckNourgent()
{
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void CPrior2DView::OnBnClickedCheckUrgent()
{
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}
