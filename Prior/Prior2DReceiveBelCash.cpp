#include "stdafx.h"
#include "Prior2D.h"
#include "Prior2DReceiveBelCash.h"
#include "TemplateName.h"
#include "..\pp2dlib\Prior2DModule.h"

extern CString csLang[32];
extern bool bGlobalPrintBarcode;

IMPLEMENT_DYNCREATE(Prior2DReceiveBelCash, CFormView)

Prior2DReceiveBelCash::Prior2DReceiveBelCash()
: CFormView(Prior2DReceiveBelCash::IDD)
{
	numberOfPurposes = 1;
	
	codes.SetAt(_T("2040"), _T("������ �������� ����� �� ���������� ����� � �� ������ ������� � �������, �� ���������� � ������� ����� ���������� �����"));
	codes.SetAt(_T("2044"), _T("������ �� ������� ��������� �������� ����� ������������ ������, �� ������������� ���������������, ��������������� �����������������"));	
	codes.SetAt(_T("2053"), _T("������ �������� ����� �� ������ ����"));

	addIndividualBusinessmanPurpose(_T("������ �����"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("���������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("������, �������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("��������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("������������ ������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("���������������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("����� ��������������� ���������������"), _T("2040"));
	addIndividualBusinessmanPurpose(_T("��������� �������� ��� �������� � ��. ������, ��; ���������� �������� ���. ���"), _T("2044"));
	addIndividualBusinessmanPurpose(_T("���������"), _T("2053"));
	addIndividualBusinessmanPurpose(_T("������� �� �������� �����"), _T("2053"));
	addIndividualBusinessmanPurpose(_T("������ �� �������� ������"), _T("2053"));
	addIndividualBusinessmanPurpose(_T("��, ���  - ������� ��������� �������� �� ���������� �����"), _T("2053"));
	addIndividualBusinessmanPurpose(_T("���� ����"), _T("2053"));

	addLegalClientPurpose(_T("������ �����"), _T("2040"));
	addLegalClientPurpose(_T("���������"), _T("2040"));
	addLegalClientPurpose(_T("������"), _T("2040"));
	addLegalClientPurpose(_T("������, �������"), _T("2040"));
	addLegalClientPurpose(_T("��������"), _T("2040"));
	addLegalClientPurpose(_T("������������ ������"), _T("2040"));
	addLegalClientPurpose(_T("���������������"), _T("2040"));	
	addLegalClientPurpose(_T("��������� �������� ��� �������� � ��. ������, ��; ���������� �������� ���. ���"), _T("2044"));
	addLegalClientPurpose(_T("���������"), _T("2053"));
	addLegalClientPurpose(_T("������� �� �������� �����"), _T("2053"));
	addLegalClientPurpose(_T("������ �� �������� ������"), _T("2053"));
	addLegalClientPurpose(_T("��, ���  - ������� ��������� �������� �� ���������� �����"), _T("2053"));
	addLegalClientPurpose(_T("���� ����"), _T("2053"));
}

void Prior2DReceiveBelCash::addIndividualBusinessmanPurpose(CString purpose, CString code)
{
	_individualBusinessmenPurposesCodes.SetAt(purpose, code);
	_individualBusinessmenPurposesOrder.push_back(purpose);
}

void Prior2DReceiveBelCash::addLegalClientPurpose(CString purpose, CString code)
{
	_legalClientsPurposesCodes.SetAt(purpose, code);
	_legalClientsPurposesOrder.push_back(purpose);
}

Prior2DReceiveBelCash::~Prior2DReceiveBelCash()
{
}

void Prior2DReceiveBelCash::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Prior2DReceiveBelCash, CFormView)
	ON_EN_CHANGE(IDC_EDIT_SUM, &Prior2DReceiveBelCash::OnEnChangeEditSummdigits)
	ON_COMMAND(ID_SAVE_TEMPLATE, &Prior2DReceiveBelCash::OnSaveTemplate)	
	ON_COMMAND(ID_FILE_PRINT, &Prior2DReceiveBelCash::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DReceiveBelCash::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)	
	ON_EN_CHANGE(IDC_EDIT_ACCOUNT, &Prior2DReceiveBelCash::OnEnChangeEditAccount)
	ON_CBN_SELCHANGE(IDC_COMBO_PURPOSE1, &Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose1)	
	ON_BN_CLICKED(IDC_BUTTON_ADD_PURPOSE2, &Prior2DReceiveBelCash::OnBnClickedButtonAddPurpose2)
	ON_BN_CLICKED(IDC_BUTTON_ADD_PURPOSE3, &Prior2DReceiveBelCash::OnBnClickedButtonAddPurpose3)
	ON_BN_CLICKED(IDC_BUTTON_ADD_PURPOSE4, &Prior2DReceiveBelCash::OnBnClickedButtonAddPurpose4)
	ON_CBN_SELCHANGE(IDC_COMBO_PURPOSE2, &Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose2)
	ON_CBN_SELCHANGE(IDC_COMBO_PURPOSE3, &Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose3)
	ON_CBN_SELCHANGE(IDC_COMBO_PURPOSE4, &Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose4)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER_DOC_ISSUE_DATE, &Prior2DReceiveBelCash::OnDtnDatetimechangeDatetimepickerDocIssueDate)
END_MESSAGE_MAP()

void Prior2DReceiveBelCash::OnInitialUpdate()
{
	CString cstmp;
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_APPLICATION_NUMBER));	
	cEdit->SetLimitText(6);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NAME));
	cEdit->SetLimitText(128);
		
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ACCOUNT));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REPRESENTATIVE_NAME));
	cEdit->SetLimitText(128);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_TYPE));
	cEdit->SetLimitText(28);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_SERIA));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_NUMBER));
	cEdit->SetLimitText(9);
	
	CDateTimeCtrl* cDtCtrl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DOC_ISSUE_DATE));		
    cDtCtrl->SetFormat(_T(" "));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_ISSUE_COMPANY));
	cEdit->SetLimitText(64);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX_PAGECOUNT));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYMENT_PRIORITY));
	cEdit->SetLimitText(5);
	cEdit->SetWindowTextW(_T("22"));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_YEAR));
	cEdit->SetLimitText(4);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_YEAR));
	cEdit->SetLimitText(4);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_YEAR));
	cEdit->SetLimitText(4);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_YEAR));
	cEdit->SetLimitText(4);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_SUM));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_SUM));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_SUM));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_SUM));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_CLARIFICATION));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_CLARIFICATION));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_CLARIFICATION));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_CLARIFICATION));
	cEdit->SetLimitText(32);

	CComboBox* cBox;

	int purposeDroppedWidth = 450;
	
	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1));
	cBox->SetDroppedWidth(purposeDroppedWidth);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2));
	cBox->SetDroppedWidth(purposeDroppedWidth);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3));
	cBox->SetDroppedWidth(purposeDroppedWidth);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4));
	cBox->SetDroppedWidth(purposeDroppedWidth);

	int purposeCodeDroppedWidth = 810;

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_CODE));
	cBox->SetDroppedWidth(purposeCodeDroppedWidth);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_CODE));
	cBox->SetDroppedWidth(purposeCodeDroppedWidth);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_CODE));
	cBox->SetDroppedWidth(purposeCodeDroppedWidth);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_CODE));
	cBox->SetDroppedWidth(purposeCodeDroppedWidth);


	fillPurposeTypeCBox(IDC_COMBO_PURPOSE1_TYPE);
	fillPurposeTypeCBox(IDC_COMBO_PURPOSE2_TYPE);
	fillPurposeTypeCBox(IDC_COMBO_PURPOSE3_TYPE);
	fillPurposeTypeCBox(IDC_COMBO_PURPOSE4_TYPE);

	fillPurposeMonthsCBox(IDC_COMBO_PURPOSE1_MONTH);
	fillPurposeMonthsCBox(IDC_COMBO_PURPOSE2_MONTH);
	fillPurposeMonthsCBox(IDC_COMBO_PURPOSE3_MONTH);
	fillPurposeMonthsCBox(IDC_COMBO_PURPOSE4_MONTH);
}


BOOL Prior2DReceiveBelCash::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;

	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	return DoPreparePrinting(pInfo);	
}

void Prior2DReceiveBelCash::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DReceiveBelCash::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	delete m_FLarge;
}

void Prior2DReceiveBelCash::SaveTemplate(CString strSection)
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = strSection;
	tmptltName.setPassword = true;

	while(tmptltName.DoModal() == IDOK)
	{
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DReceiveBelCash::SaveConfigContent(CString strSection, CString password)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (password != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	iniReader->setKey(password,_T("PASSWORD"),strSection);	
	iniReader->setKey(_T("RECEIVEBELCASH"),_T("TYPE"),strSection);

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_APPLICATION_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NUMBER"), strSection);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_APPLICATION_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	iniReader->setKey(cstr, _T("DATE"), strSection);


	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAME"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNT"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REPRESENTATIVE_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("REPRESENTATIVENAME"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_TYPE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("DOCTYPE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_SERIA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("DOCSERIA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("DOCNUMBER"), strSection);

	cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DOC_ISSUE_DATE));
	cDTControl->GetWindowTextW(cstr);
	if (cstr.Trim() != _T(""))
	{
		cDTControl->GetTime(&time);
		cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
		iniReader->setKey(cstr, _T("DOCISSUEDATE"), strSection);
	}
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_ISSUE_COMPANY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("DOCISSUECOMPANY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ANNEX"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX_PAGECOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ANNEXPAGECOUNT"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYMENT_PRIORITY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PAYMENTPRIORITY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMDIGITS"), strSection);

	char buf[32];
	cstr = CString(itoa(numberOfPurposes, buf, 10));
	iniReader->setKey(cstr, _T("PURPOSESCOUNT"), strSection);

	//1
	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_CODE));
	cCmb->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("CODE1"), strSection);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_SUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUM1"), strSection);
	
	cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1));
	cCmb->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PURPOSE1"), strSection);
	
	if(cstr == _T("������ �����"))
	{
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_TYPE));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSETYPE1"), strSection);
		
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_MONTH));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSEMONTH1"), strSection);
		
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_YEAR));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSEYEAR1"), strSection);	

		iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION1"), strSection);
	}
	else if (cstr == _T("���� ����"))
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_CLARIFICATION));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSECLARIFICATION1"), strSection);

		iniReader->setKey(_T(""), _T("PURPOSETYPE1"), strSection);
		iniReader->setKey(_T(""), _T("PURPOSEMONTH1"), strSection);
		iniReader->setKey(_T(""), _T("PURPOSEYEAR1"), strSection);
	}
	else
	{
		iniReader->setKey(_T(""), _T("PURPOSETYPE1"), strSection);
		iniReader->setKey(_T(""), _T("PURPOSEMONTH1"), strSection);
		iniReader->setKey(_T(""), _T("PURPOSEYEAR1"), strSection);
		iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION1"), strSection);
	}

	if (numberOfPurposes > 1)
	{
		//2
		CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_CODE));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CODE2"), strSection);
	
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_SUM));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("SUM2"), strSection);
	
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSE2"), strSection);
	
		if(cstr == _T("������ �����"))
		{
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_TYPE));
			cCmb->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSETYPE2"), strSection);
		
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_MONTH));
			cCmb->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSEMONTH2"), strSection);
		
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_YEAR));
			cEdit->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSEYEAR2"), strSection);

			iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION2"), strSection);
		}
		else if (cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_CLARIFICATION));
			cEdit->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSECLARIFICATION2"), strSection);

			iniReader->setKey(_T(""), _T("PURPOSETYPE2"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEMONTH2"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEYEAR2"), strSection);
		}
		else
		{
			iniReader->setKey(_T(""), _T("PURPOSETYPE2"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEMONTH2"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEYEAR2"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION2"), strSection);
		}
	}

	if (numberOfPurposes > 2)
	{
		//3
		CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_CODE));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CODE3"), strSection);
	
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_SUM));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("SUM3"), strSection);
	
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSE3"), strSection);
	
		if(cstr == _T("������ �����"))
		{
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_TYPE));
			cCmb->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSETYPE3"), strSection);
		
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_MONTH));
			cCmb->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSEMONTH3"), strSection);
		
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_YEAR));
			cEdit->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSEYEAR3"), strSection);

			iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION3"), strSection);
		}
		else if (cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_CLARIFICATION));
			cEdit->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSECLARIFICATION3"), strSection);

			iniReader->setKey(_T(""), _T("PURPOSETYPE3"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEMONTH3"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEYEAR3"), strSection);
		}
		else
		{
			iniReader->setKey(_T(""), _T("PURPOSETYPE3"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEMONTH3"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEYEAR3"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION3"), strSection);
		}
	}

	if (numberOfPurposes > 3)
	{
		//4
		CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_CODE));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CODE4"), strSection);
	
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_SUM));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("SUM4"), strSection);
	
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4));
		cCmb->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PURPOSE4"), strSection);
	
		if(cstr == _T("������ �����"))
		{
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_TYPE));
			cCmb->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSETYPE4"), strSection);
		
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_MONTH));
			cCmb->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSEMONTH4"), strSection);
		
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_YEAR));
			cEdit->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSEYEAR4"), strSection);

			iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION4"), strSection);
		}
		else if (cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_CLARIFICATION));
			cEdit->GetWindowTextW(cstr);
			iniReader->setKey(cstr, _T("PURPOSECLARIFICATION4"), strSection);

			iniReader->setKey(_T(""), _T("PURPOSETYPE4"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEMONTH4"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEYEAR4"), strSection);
		}
		else
		{
			iniReader->setKey(_T(""), _T("PURPOSETYPE4"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEMONTH4"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSEYEAR4"), strSection);
			iniReader->setKey(_T(""), _T("PURPOSECLARIFICATION4"), strSection);
		}
	}

	iniReader->NeedEncryption = false;
}

void Prior2DReceiveBelCash::LoadConfigContent(CString strSection)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (iniReader->getKeyValue(_T("PASSWORD"), strSection) != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_APPLICATION_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NUMBER"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAME"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ACCOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNT"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REPRESENTATIVE_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("REPRESENTATIVENAME"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_TYPE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DOCTYPE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_SERIA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DOCSERIA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DOCNUMBER"), strSection));

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DOC_ISSUE_DATE));
	SYSTEMTIME  time;	
	CString issueDate = iniReader->getKeyValue(_T("DOCISSUEDATE"), strSection);
	if (issueDate.Trim() != _T(""))
	{
		time.wDay = _ttoi(issueDate.Left(2));
		time.wMonth = _ttoi(issueDate.Mid(3, 2));
		time.wYear = _ttoi(issueDate.Right(4));
		cDTControl->SetTime(&time);
		cDTControl->SetFormat(DTS_SHORTDATEFORMAT);
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_ISSUE_COMPANY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DOCISSUECOMPANY"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ANNEX"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX_PAGECOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ANNEXPAGECOUNT"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYMENT_PRIORITY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTPRIORITY"), strSection));
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMDIGITS"), strSection));

	CString cstr;

	cstr.Format(iniReader->getKeyValue(_T("PURPOSESCOUNT"), strSection));
	if (cstr != _T(""))
		numberOfPurposes = _ttoi(cstr);
	
	CComboBox* cCmb;
	//1	
	cstr = iniReader->getKeyValue(_T("PURPOSE1"), strSection);
	cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1));
	cCmb->SelectString(0, cstr);
	OnCbnSelchangeComboPurpose1();
			
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_SUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUM1"), strSection));
	
	if(cstr == _T("������ �����"))
	{
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_TYPE));
		cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSETYPE1"), strSection));		
		
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_MONTH));
		cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSEMONTH1"), strSection));		
		
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_YEAR));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSEYEAR1"), strSection));		
	}
	if(cstr == _T("���� ����"))
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_CLARIFICATION));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSECLARIFICATION1"), strSection));		
	}

	if (numberOfPurposes > 1)
	{		
		//2
		cstr = iniReader->getKeyValue(_T("PURPOSE2"), strSection);
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2));
		cCmb->SelectString(0, cstr);
		OnCbnSelchangeComboPurpose2();
				
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_SUM));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUM2"), strSection));
	
		if(cstr == _T("������ �����"))
		{
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_TYPE));
			cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSETYPE2"), strSection));		
		
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_MONTH));
			cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSEMONTH2"), strSection));		
		
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_YEAR));
			cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSEYEAR2"), strSection));		
		}
		if(cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_CLARIFICATION));
			cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSECLARIFICATION2"), strSection));		
		}
		ShowHidePurpose2(SW_SHOW);
	}

	if (numberOfPurposes > 2)
	{		
		//2
		cstr = iniReader->getKeyValue(_T("PURPOSE3"), strSection);
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3));
		cCmb->SelectString(0, cstr);
		OnCbnSelchangeComboPurpose3();
				
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_SUM));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUM3"), strSection));
	
		if(cstr == _T("������ �����"))
		{
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_TYPE));
			cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSETYPE3"), strSection));		
		
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_MONTH));
			cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSEMONTH3"), strSection));		
		
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_YEAR));
			cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSEYEAR3"), strSection));		
		}
		if(cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_CLARIFICATION));
			cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSECLARIFICATION3"), strSection));		
		}
		ShowHidePurpose3(SW_SHOW);
	}

	if (numberOfPurposes > 3)
	{
		//4
		cstr = iniReader->getKeyValue(_T("PURPOSE4"), strSection);
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4));
		cCmb->SelectString(0, cstr);
		OnCbnSelchangeComboPurpose4();
				
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_SUM));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUM4"), strSection));
	
		if(cstr == _T("������ �����"))
		{
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_TYPE));
			cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSETYPE4"), strSection));		
		
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_MONTH));
			cCmb->SelectString(0, iniReader->getKeyValue(_T("PURPOSEMONTH4"), strSection));		
		
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_YEAR));
			cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSEYEAR4"), strSection));		
		}
		if(cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_CLARIFICATION));
			cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PURPOSECLARIFICATION4"), strSection));		
		}
		ShowHidePurpose4(SW_SHOW);
	}

	iniReader->NeedEncryption = false;

	if (!CIniReader::CheckPasswordExists(strSection))
	{
		int res = ::MessageBoxA(this->m_hWnd, "������ ��� ������� ������� �� ����������. ����������?", 
				"�������� ������ �������", MB_YESNO);	
		if (res == IDYES)
		{
			SaveTemplate(strSection);
		}
	}	
}

void Prior2DReceiveBelCash::OnSaveTemplate()
{
	
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = _T("");
	tmptltName.setPassword = false;
	while(tmptltName.DoModal() == IDOK)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		if(iniReader->sectionExists(tmptltName.cstrTemplateName))
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ����� ������ ��� ����������, ������������ ���?", 
				"���������� �������", MB_YESNOCANCEL);
			if(res == IDCANCEL) return;
			if(res == IDNO) continue;
			if (!CIniReader::CheckPassword(tmptltName.cstrTemplateName))
				continue;
		}		
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DReceiveBelCash::OnEnChangeEditSummdigits()
{
	int i = 0;
	CString cstr1, cstr2;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->GetWindowTextW(cstr1);
	if (cstr1.GetLength() == 0)
	{
		(reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM_SPELLED)))->SetWindowText(_T(""));
		return;
	}
	if (_ttoi(cstr1.Trim()) == 0)
	{
		(reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM_SPELLED)))->SetWindowText(_T("����"));
		return;
	}
	cstr2 = cstr1;
	int len = cstr1.GetLength();
	char* str = new char[len+4];
	while(i < len){
		if (cstr1[i] >= '0' && cstr1[i] <= '9')
		{
			str[i] = cstr1[i];
			i++;
		}
		else if (i < len)
		{
				CString::CopyChars(cstr1.GetBuffer()+i,cstr1.GetBuffer()+i+1,len-i+1);
				len--;
		}
	}
	str[len]=0;
	__int64 ival = _atoi64(str);

	_i64toa( ival, str, 10 ); 
	cstr1.Format(L"%lld",ival);
	if (len > 0 && cstr2 != cstr1) cEdit->SetWindowTextW(cstr1);
	wchar_t * cs = (wchar_t*)malloc(1024);
	Utilites::SummToStringS(ival, cs);
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM_SPELLED));
	cEdit->SetWindowText(cs);
	free(cs);
	delete []str;
}

void Prior2DReceiveBelCash::OnFilePrint()
{
	CString cstr;
	CEdit* cEdit;
	

	if (!CheckNonEmpty(IDC_EDIT_APPLICATION_NUMBER, _T("����� ���������:�������� ������"), 
		_T("���� \"����� ���������\" ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_NAME, _T("������������:�������� ������"), 
		_T("���� \"������������\" ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_ACCOUNT, _T("����� �����:�������� ������"), 
		_T("���� \"����� �����\" ������ ���� ���������.")))
		return;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=13)
	{
		MessageBox(_T("���� \"����� �����\" ������ ��������� 13 ����."),
			_T("����� �����:�������� ������"),MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}else {
		if(!Utilites::CheckAccount(cstr, _T("749")) &&
		   !Utilites::CheckAccount(cstr, _T("999")))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	if (!CheckNonEmpty(IDC_EDIT_REPRESENTATIVE_NAME, _T("�������������:�������� ������"), 
		_T("���� \"���\" ������������� ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_DOC_TYPE, _T("��� ���������:�������� ������"), 
		_T("���� \"��� ���������\" ������������� ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_DOC_NUMBER, _T("����� ���������:�������� ������"), 
		_T("���� \"����� ���������\" ������������� ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_DATETIMEPICKER_DOC_ISSUE_DATE, _T("���� ������ ���������:�������� ������"), 
		_T("���� \"���� ������ ���������\" ������������� ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_DOC_ISSUE_COMPANY, _T("�����, �������� ��������:�������� ������"), 
		_T("���� \"�����, �������� ��������\" ������������� ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_DOC_ISSUE_COMPANY, _T("�����, �������� ��������:�������� ������"), 
		_T("���� \"�����, �������� ��������\" ������������� ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_PAYMENT_PRIORITY, _T("����������� �������:�������� ������"), 
		_T("���� \"����������� �������\" ������ ���� ���������.")))
		return;

	if (!CheckNonEmpty(IDC_EDIT_SUM, _T("�����:�������� ������"), 
		_T("���� \"�����\" ������ ���� ���������.")))
		return;

	if (!CheckPurpose1Filled())
		return;

	if (numberOfPurposes > 1)
		if (!CheckPurpose2Filled())
			return;

	if (numberOfPurposes > 2)
		if (!CheckPurpose3Filled())
			return;

	if (numberOfPurposes > 3)
		if (!CheckPurpose4Filled())
			return;

	//����� �� ������������� �������� � �� �������� ��������� �������� �� ������ ���� ������ ��� ��������� 100 ������� ������� 
	__int64 sumToControl = 0;
	__int64 totalSum = 0;
	__int64 partialSum;
	CString partialSumString;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_COMBO_PURPOSE1));
	cEdit->GetWindowTextW(cstr);
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_SUM));
	cEdit->GetWindowTextW(partialSumString);
	partialSum = _ttoi64(partialSumString);
	totalSum += partialSum;
	if (cstr == _T("��������� �������� ��� �������� � ��. ������, ��; ���������� �������� ���. ���"))
	{
		sumToControl += partialSum;
	}

	if (numberOfPurposes > 1)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_COMBO_PURPOSE2));
		cEdit->GetWindowTextW(cstr);
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_SUM));
		cEdit->GetWindowTextW(partialSumString);
		partialSum = _ttoi64(partialSumString);
		totalSum += partialSum;
		if (cstr == _T("��������� �������� ��� �������� � ��. ������, ��; ���������� �������� ���. ���"))
		{
			sumToControl += partialSum;
		}
	}

	if (numberOfPurposes > 2)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_COMBO_PURPOSE3));
		cEdit->GetWindowTextW(cstr);
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_SUM));
		cEdit->GetWindowTextW(partialSumString);
		partialSum = _ttoi64(partialSumString);
		totalSum += partialSum;
		if (cstr == _T("��������� �������� ��� �������� � ��. ������, ��; ���������� �������� ���. ���"))
		{
			sumToControl += partialSum;
		}
	}

	if (numberOfPurposes > 3)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_COMBO_PURPOSE4));
		cEdit->GetWindowTextW(cstr);
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_SUM));
		cEdit->GetWindowTextW(partialSumString);
		partialSum = _ttoi64(partialSumString);
		totalSum += partialSum;
		if (cstr == _T("��������� �������� ��� �������� � ��. ������, ��; ���������� �������� ���. ���"))
		{
			sumToControl += partialSum;
		}
	}

	if (sumToControl >= 100*GetBaseAmount())
	{
		MessageBox(_T("����� �� ������������� �������� � �� �������� ��������� �������� �� ������ ���� ������ ��� ��������� 100 ������� �������."),
			_T("�����:�������� ��������"),MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->GetWindowTextW(cstr);
	if (totalSum != _ttoi64(cstr))
	{
		MessageBox(_T("����� ��������� ������ ���� ����� ����� ������ ���������."),
			_T("�����:�������� ��������"),MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	CFormView::OnFilePrint();
}
void Prior2DReceiveBelCash::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	//TODO TO DO
	Prior2DReceiveBelCashModule printer;

	CString cstr;
	CEdit* cEdit;
	CDateTimeCtrl* cDTControl;
	SYSTEMTIME  time;
	CComboBox* cCmb;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_APPLICATION_NUMBER));
	cEdit->GetWindowTextW(cstr);
	printer.setApplicationNumber(cstr);

	cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_APPLICATION_DATE));	
	cDTControl->GetTime(&time);
	printer.setApplicationDate(time);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NAME));
	cEdit->GetWindowTextW(cstr);
	printer.setName(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->GetWindowTextW(cstr);
	printer.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM_SPELLED));
	cEdit->GetWindowTextW(cstr);
	printer.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYMENT_PRIORITY));
	cEdit->GetWindowTextW(cstr);
	printer.setPriority(cstr);	

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	printer.setAccount(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REPRESENTATIVE_NAME));
	cEdit->GetWindowTextW(cstr);
	printer.setRepresentative(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_TYPE));
	cEdit->GetWindowTextW(cstr);
	printer.setDocType(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_SERIA));
	cEdit->GetWindowTextW(cstr);
	printer.setDocSeria(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_NUMBER));
	cEdit->GetWindowTextW(cstr);
	printer.setDocNumber(cstr);

	cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DOC_ISSUE_DATE));	
	DWORD timeFlags = cDTControl->GetTime(&time);
	if (timeFlags == GDT_NONE)
	{
		SYSTEMTIME fakeTime;
		fakeTime.wYear = 0;
		printer.setDocIssueDate(fakeTime);
	}
	else
		printer.setDocIssueDate(time);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_DOC_ISSUE_COMPANY));
	cEdit->GetWindowTextW(cstr);
	printer.setDocIssueCompany(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX));
	cEdit->GetWindowTextW(cstr);
	printer.setAnnex(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ANNEX_PAGECOUNT));
	cEdit->GetWindowTextW(cstr);
	printer.setAnnexPageCount(cstr);

	printer.setPurposesCount(numberOfPurposes);

	//1
	cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_CODE));
	cCmb->GetWindowTextW(cstr);
	printer.setCode1(cstr.Left(4));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_SUM));
	cEdit->GetWindowTextW(cstr);
	printer.setSum1(cstr);

	cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1));
	cCmb->GetWindowTextW(cstr);
	
	if(cstr == _T("������ �����"))
	{
		CString purpose = _T("������ �����. ");
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_TYPE));
		cCmb->GetWindowTextW(cstr);
		purpose += cstr;
		purpose += _T(" �� ");

		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE1_MONTH));
		cCmb->GetWindowTextW(cstr);
		purpose += cstr;
		purpose += _T(" ");

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_YEAR));
		cEdit->GetWindowTextW(cstr);
		purpose += cstr;
		//purpose += _T("�");
		printer.setPurpose1(purpose);
	}
	else if (cstr == _T("���� ����"))
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE1_CLARIFICATION));
		cEdit->GetWindowTextW(cstr);		
		printer.setPurpose1(cstr);
	}
	else
	{
		printer.setPurpose1(cstr);
	}

	if (numberOfPurposes > 1)
	{
		//2
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_CODE));
		cCmb->GetWindowTextW(cstr);
		printer.setCode2(cstr.Left(4));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_SUM));
		cEdit->GetWindowTextW(cstr);
		printer.setSum2(cstr);

		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2));
		cCmb->GetWindowTextW(cstr);
		if(cstr == _T("������ �����"))
		{
			CString purpose = _T("������ �����. ");
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_TYPE));
			cCmb->GetWindowTextW(cstr);
			purpose += cstr;
			purpose += _T(" �� ");

			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE2_MONTH));
			cCmb->GetWindowTextW(cstr);
			purpose += cstr;
			purpose += _T(" ");

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_YEAR));
			cEdit->GetWindowTextW(cstr);
			purpose += cstr;
			//purpose += _T("�");
			printer.setPurpose2(purpose);
		}
		else if (cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE2_CLARIFICATION));
			cEdit->GetWindowTextW(cstr);		
			printer.setPurpose2(cstr);
		}
		else
		{
			printer.setPurpose2(cstr);
		}
	}

	if (numberOfPurposes > 2)
	{
		//3
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_CODE));
		cCmb->GetWindowTextW(cstr);
		printer.setCode3(cstr.Left(4));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_SUM));
		cEdit->GetWindowTextW(cstr);
		printer.setSum3(cstr);

		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3));
		cCmb->GetWindowTextW(cstr);
		if(cstr == _T("������ �����"))
		{
			CString purpose = _T("������ �����. ");
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_TYPE));
			cCmb->GetWindowTextW(cstr);
			purpose += cstr;
			purpose += _T(" �� ");

			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE3_MONTH));
			cCmb->GetWindowTextW(cstr);
			purpose += cstr;
			purpose += _T(" ");

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_YEAR));
			cEdit->GetWindowTextW(cstr);
			purpose += cstr;
			//purpose += _T("�");
			printer.setPurpose3(purpose);
		}
		else if (cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE3_CLARIFICATION));
			cEdit->GetWindowTextW(cstr);		
			printer.setPurpose3(cstr);
		}
		else
		{
			printer.setPurpose3(cstr);
		}
	}

	if (numberOfPurposes > 3)
	{
		//4
		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_CODE));
		cCmb->GetWindowTextW(cstr);
		printer.setCode4(cstr.Left(4));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_SUM));
		cEdit->GetWindowTextW(cstr);
		printer.setSum4(cstr);

		cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4));
		cCmb->GetWindowTextW(cstr);
		if(cstr == _T("������ �����"))
		{
			CString purpose = _T("������ �����. ");
			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_TYPE));
			cCmb->GetWindowTextW(cstr);
			purpose += cstr;
			purpose += _T(" �� ");

			cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PURPOSE4_MONTH));
			cCmb->GetWindowTextW(cstr);
			purpose += cstr;
			purpose += _T(" ");

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_YEAR));
			cEdit->GetWindowTextW(cstr);
			purpose += cstr;
			//purpose += _T("�");
			printer.setPurpose4(purpose);
		}
		else if (cstr == _T("���� ����"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE4_CLARIFICATION));
			cEdit->GetWindowTextW(cstr);		
			printer.setPurpose4(cstr);
		}
		else
		{
			printer.setPurpose4(cstr);
		}
	}

	int ibw = 2480;
	int ibh = 3558;

	CDC dc;
	CBitmap bmp;
	printer.BuildBarcode(&dc, &bmp, ibw, ibh, bGlobalPrintBarcode);
	pDC->StretchBlt(pInfo->m_rectDraw.left, pInfo->m_rectDraw.top, pInfo->m_rectDraw.Width(), pInfo->m_rectDraw.Height(), &dc, 0, 0, ibw, ibh, SRCCOPY);	
}

int Prior2DReceiveBelCash::GetBaseAmount()
{
	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	WCHAR *sDir = new WCHAR[sDirLen];
	::GetCurrentDirectory(sDirLen, sDir);
	CIniReader *cIR = new CIniReader(CString(sDir)+L"\\language.ini");
	
		if (cIR->sectionExists(L"BaseAmount"))
		{
			return _ttoi(cIR->getKeyValue(L"BaseAmount", L"BaseAmount"));
		}
		else 
		{
			return 0;
		}
}

bool Prior2DReceiveBelCash::CheckBigSum()
{
	int baseAmount = GetBaseAmount();

	CString cstr;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUM));
	cEdit->GetWindowTextW(cstr);
	__int64 ival = _ttoi64(cstr);

	return ival >= baseAmount*100;
}

bool Prior2DReceiveBelCash::is3013()
{
	CString cstr;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	if (cstr.Left(4) == _T("3013"))
		return true;
	return false;
}

void Prior2DReceiveBelCash::OnEnChangeEditAccount()
{
	if (is3013())
	{
		fillIndividualBusinessmanValues(IDC_COMBO_PURPOSE1);
		fillIndividualBusinessmanValues(IDC_COMBO_PURPOSE2);
		fillIndividualBusinessmanValues(IDC_COMBO_PURPOSE3);
		fillIndividualBusinessmanValues(IDC_COMBO_PURPOSE4);
		OnCbnSelchangeComboPurpose1();
		OnCbnSelchangeComboPurpose2();
		OnCbnSelchangeComboPurpose3();
		OnCbnSelchangeComboPurpose4();
	}
	else
	{
		fillDefaultValues(IDC_COMBO_PURPOSE1);
		fillDefaultValues(IDC_COMBO_PURPOSE2);
		fillDefaultValues(IDC_COMBO_PURPOSE3);
		fillDefaultValues(IDC_COMBO_PURPOSE4);
		OnCbnSelchangeComboPurpose1();
		OnCbnSelchangeComboPurpose2();
		OnCbnSelchangeComboPurpose3();
		OnCbnSelchangeComboPurpose4();
	}
}

void Prior2DReceiveBelCash::fillIndividualBusinessmanValues(int purposeCbox)
{
	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeCbox));
	CString curritem;
	cBox->GetWindowTextW(curritem);
	cBox->ResetContent();
	for (unsigned int i = 0; i < _individualBusinessmenPurposesOrder.size(); i++)
	{
		cBox->AddString(_individualBusinessmenPurposesOrder[i]);
	}	
	cBox->SelectString(0, curritem);	
}

void Prior2DReceiveBelCash::fillDefaultValues(int purposeCbox)
{
	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeCbox));
	CString curritem;
	cBox->GetWindowTextW(curritem);
	cBox->ResetContent();	
	for (unsigned int i = 0; i < _legalClientsPurposesOrder.size(); i++)
	{
		cBox->AddString(_legalClientsPurposesOrder[i]);
	}	
	cBox->SelectString(0, curritem);
}


void Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose1()
{
	onPurposeChanged(IDC_COMBO_PURPOSE1, IDC_COMBO_PURPOSE1_CODE, IDC_COMBO_PURPOSE1_TYPE, IDC_STATIC_MONTH_TEXT1, IDC_COMBO_PURPOSE1_MONTH, IDC_STATIC_YEAR_TEXT1, IDC_EDIT_PURPOSE1_YEAR, IDC_STATIC_PURPOSE1_CLARIFICATION, IDC_EDIT_PURPOSE1_CLARIFICATION);
}

void Prior2DReceiveBelCash::addCodeWithDesc(CString key, CComboBox *cBox)
{
	CString description;
	codes.Lookup(key, description);
	cBox->AddString(key + _T(" - ") + description);
}

void Prior2DReceiveBelCash::fillCBoxWithSingleCode(CString key, CComboBox *cBox)
{
	cBox->ResetContent();
	addCodeWithDesc(key, cBox);
	cBox->SetCurSel(0);
}

void Prior2DReceiveBelCash::ShowHidePurposeDetailsGeneral(int nCmdShow, int purposeType, int monthText, int monthCBox, int yearText, int yearTBox)
{
	CEdit* cEdit;
	CComboBox* cBox;

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeType));
	cBox->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(monthText));
	cEdit->ShowWindow(nCmdShow);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(monthCBox));
	cBox->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearText));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearTBox));
	cEdit->ShowWindow(nCmdShow);
}

void Prior2DReceiveBelCash::ShowHidePurposeDetails1(int nCmdShow)
{
	ShowHidePurposeDetailsGeneral(nCmdShow, IDC_COMBO_PURPOSE1_TYPE, IDC_STATIC_MONTH_TEXT1, IDC_COMBO_PURPOSE1_MONTH, IDC_STATIC_YEAR_TEXT1, IDC_EDIT_PURPOSE1_YEAR);
}

void Prior2DReceiveBelCash::fillPurposeTypeCBox(int cbox)
{
	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(cbox));
	cBox->ResetContent();
	cBox->AddString(_T("��������"));
	cBox->AddString(_T("�����"));
}

void Prior2DReceiveBelCash::fillPurposeMonthsCBox(int cbox)
{
	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(cbox));
	cBox->ResetContent();
	cBox->AddString(_T("������"));
	cBox->AddString(_T("�������"));
	cBox->AddString(_T("����"));
	cBox->AddString(_T("������"));
	cBox->AddString(_T("���"));
	cBox->AddString(_T("����"));
	cBox->AddString(_T("����"));
	cBox->AddString(_T("������"));
	cBox->AddString(_T("��������"));
	cBox->AddString(_T("�������"));
	cBox->AddString(_T("������"));
	cBox->AddString(_T("�������"));
}

void Prior2DReceiveBelCash::onPurposeChanged(int purposeCbox, int codeCbox, int purposeType, int monthText, int monthCBox, int yearText, int yearTBox, int clarificationText, int clarificationEdit)
{
	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeCbox));
	CString purpose;
	cBox->GetWindowTextW(purpose);
	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(codeCbox));

	if (purpose == _T(""))
	{
		cBox->ResetContent();
		return;
	}

	CString code;
	if (is3013())
		_individualBusinessmenPurposesCodes.Lookup(purpose, code);
	else
		_legalClientsPurposesCodes.Lookup(purpose, code);
	
	fillCBoxWithSingleCode(code, cBox);

	if (purpose == _T("������ �����"))
		{
			ShowHidePurposeDetailsGeneral(SW_SHOW, purposeType, monthText, monthCBox, yearText, yearTBox);
			ShowHidePurposeClarificationGeneral(SW_HIDE, clarificationText, clarificationEdit);
		}
	else if (purpose == _T("���� ����"))
		{			
			ShowHidePurposeDetailsGeneral(SW_HIDE, purposeType, monthText, monthCBox, yearText, yearTBox);
			ShowHidePurposeClarificationGeneral(SW_SHOW, clarificationText, clarificationEdit);
		}
	else
		{
			ShowHidePurposeDetailsGeneral(SW_HIDE, purposeType, monthText, monthCBox, yearText, yearTBox);
			ShowHidePurposeClarificationGeneral(SW_HIDE, clarificationText, clarificationEdit);
		}
}

void Prior2DReceiveBelCash::OnBnClickedButtonAddPurpose2()
{
	if (numberOfPurposes == 1)
	{
		numberOfPurposes++;
		ShowHidePurpose2(SW_SHOW);
	}
	else
	{
		numberOfPurposes--;
		ShowHidePurpose2(SW_HIDE);		
	}
}

void Prior2DReceiveBelCash::OnBnClickedButtonAddPurpose3()
{	
	if (numberOfPurposes == 2)
	{
		numberOfPurposes++;
		ShowHidePurpose3(SW_SHOW);
	}
	else
	{
		numberOfPurposes--;
		ShowHidePurpose3(SW_HIDE);
	}
}

void Prior2DReceiveBelCash::OnBnClickedButtonAddPurpose4()
{
	if (numberOfPurposes == 3)
	{
		numberOfPurposes++;
		ShowHidePurpose4(SW_SHOW);
	}
	else
	{
		numberOfPurposes--;
		ShowHidePurpose4(SW_HIDE);
	}
}

void Prior2DReceiveBelCash::ShowHidePurpose2(int nCmdShow)
{
	CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE2));
	CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE3));
	if (numberOfPurposes > 1)
	{		
		cBut1->SetWindowTextW(_T("������� ����"));
		cBut2->ShowWindow(SW_SHOW);
		cBut2->SetWindowTextW(L"�������� ����");
	}
	else
	{		
		cBut1->SetWindowTextW(_T("�������� ����"));
		cBut2->ShowWindow(SW_HIDE);
		cBut2->SetWindowTextW(L"������� ����");
	}
	ShowHidePurposeGeneral(nCmdShow, IDC_STATIC_PURPOSE2_FRAME, IDC_STATIC_PURPOSE2, IDC_COMBO_PURPOSE2,
		IDC_STATIC_PURPOSE2_CODE, IDC_COMBO_PURPOSE2_CODE, IDC_STATIC_PURPOSE2_SUM, IDC_EDIT_PURPOSE2_SUM,
		IDC_COMBO_PURPOSE2_TYPE, IDC_STATIC_PURPOSE2_MONTH, IDC_COMBO_PURPOSE2_MONTH, IDC_STATIC_PURPOSE2_YEAR,
		IDC_EDIT_PURPOSE2_YEAR, IDC_STATIC_PURPOSE2_CLARIFICATION, IDC_EDIT_PURPOSE2_CLARIFICATION);
}

void Prior2DReceiveBelCash::ShowHidePurpose3(int nCmdShow)
{
	CButton* cBut0 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE2));
	CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE3));
	CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE4));
	if (numberOfPurposes > 2)
	{
		cBut0->EnableWindow(FALSE);	
		cBut1->SetWindowTextW(_T("������� ����"));
		cBut2->ShowWindow(SW_SHOW);
		cBut2->SetWindowTextW(L"�������� ����");
	}
	else
	{
		cBut0->EnableWindow(TRUE);	
		cBut1->SetWindowTextW(_T("�������� ����"));
		cBut2->ShowWindow(SW_HIDE);
		cBut2->SetWindowTextW(L"������� ����");
	}

	ShowHidePurposeGeneral(nCmdShow, IDC_STATIC_PURPOSE3_FRAME, IDC_STATIC_PURPOSE3, IDC_COMBO_PURPOSE3,
		IDC_STATIC_PURPOSE3_CODE, IDC_COMBO_PURPOSE3_CODE, IDC_STATIC_PURPOSE3_SUM, IDC_EDIT_PURPOSE3_SUM,
		IDC_COMBO_PURPOSE3_TYPE, IDC_STATIC_PURPOSE3_MONTH, IDC_COMBO_PURPOSE3_MONTH, IDC_STATIC_PURPOSE3_YEAR,
		IDC_EDIT_PURPOSE3_YEAR, IDC_STATIC_PURPOSE3_CLARIFICATION, IDC_EDIT_PURPOSE3_CLARIFICATION);
}

void Prior2DReceiveBelCash::ShowHidePurpose4(int nCmdShow)
{
	CButton* cBut0 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE3));
	CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_ADD_PURPOSE4));
	if (numberOfPurposes > 3)
	{
		cBut0->EnableWindow(FALSE);
		cBut1->SetWindowTextW(_T("������� ����"));		
	}
	else
	{	
		cBut0->EnableWindow(TRUE);	
		cBut1->SetWindowTextW(_T("�������� ����"));		
	}

	ShowHidePurposeGeneral(nCmdShow, IDC_STATIC_PURPOSE4_FRAME, IDC_STATIC_PURPOSE4, IDC_COMBO_PURPOSE4,
		IDC_STATIC_PURPOSE4_CODE, IDC_COMBO_PURPOSE4_CODE, IDC_STATIC_PURPOSE4_SUM, IDC_EDIT_PURPOSE4_SUM,
		IDC_COMBO_PURPOSE4_TYPE, IDC_STATIC_PURPOSE4_MONTH, IDC_COMBO_PURPOSE4_MONTH, IDC_STATIC_PURPOSE4_YEAR,
		IDC_EDIT_PURPOSE4_YEAR, IDC_STATIC_PURPOSE4_CLARIFICATION, IDC_EDIT_PURPOSE4_CLARIFICATION);
}

void Prior2DReceiveBelCash::ShowHidePurposeGeneral(int nCmdShow, int frame, int purposeText, int purposeCBox, 
												   int codeText, int codeCBox, int sumText, int sumEdit, 
												   int typeCBox, int monthText, int monthCBox, int yearText, 
												   int yearEdit, int clarificationText, int clarificationEdit)
{
	CEdit* cEdit;
	CComboBox* cBox;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(frame));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(purposeText));
	cEdit->ShowWindow(nCmdShow);
	
	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeCBox));
	cBox->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(codeText));
	cEdit->ShowWindow(nCmdShow);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(codeCBox));
	cBox->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(sumText));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(sumEdit));
	cEdit->ShowWindow(nCmdShow);

	if (nCmdShow == SW_SHOW)
	{
		CString cstr;
		cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeCBox));
		cBox->GetWindowTextW(cstr);
		if(cstr == _T("������ �����"))
		{
			cBox = reinterpret_cast<CComboBox*>(GetDlgItem(typeCBox));
			cBox->ShowWindow(SW_SHOW);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(monthText));
			cEdit->ShowWindow(SW_SHOW);

			cBox = reinterpret_cast<CComboBox*>(GetDlgItem(monthCBox));
			cBox->ShowWindow(SW_SHOW);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearText));
			cEdit->ShowWindow(SW_SHOW);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearEdit));
			cEdit->ShowWindow(SW_SHOW);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationText));
			cEdit->ShowWindow(SW_HIDE);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationEdit));
			cEdit->ShowWindow(SW_HIDE);
			return;
		}

		if(cstr == _T("���� ����"))
		{
			cBox = reinterpret_cast<CComboBox*>(GetDlgItem(typeCBox));
			cBox->ShowWindow(SW_HIDE);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(monthText));
			cEdit->ShowWindow(SW_HIDE);

			cBox = reinterpret_cast<CComboBox*>(GetDlgItem(monthCBox));
			cBox->ShowWindow(SW_HIDE);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearText));
			cEdit->ShowWindow(SW_HIDE);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearEdit));
			cEdit->ShowWindow(SW_HIDE);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationText));
			cEdit->ShowWindow(SW_SHOW);

			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationEdit));
			cEdit->ShowWindow(SW_SHOW);
			return;
		}
	}

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(typeCBox));
	cBox->ShowWindow(SW_HIDE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(monthText));
	cEdit->ShowWindow(SW_HIDE);

	cBox = reinterpret_cast<CComboBox*>(GetDlgItem(monthCBox));
	cBox->ShowWindow(SW_HIDE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearText));
	cEdit->ShowWindow(SW_HIDE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(yearEdit));
	cEdit->ShowWindow(SW_HIDE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationText));
	cEdit->ShowWindow(SW_HIDE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationEdit));
	cEdit->ShowWindow(SW_HIDE);
}

void Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose2()
{
	onPurposeChanged(IDC_COMBO_PURPOSE2, IDC_COMBO_PURPOSE2_CODE, IDC_COMBO_PURPOSE2_TYPE, IDC_STATIC_PURPOSE2_MONTH, IDC_COMBO_PURPOSE2_MONTH, IDC_STATIC_PURPOSE2_YEAR, IDC_EDIT_PURPOSE2_YEAR, IDC_STATIC_PURPOSE2_CLARIFICATION, IDC_EDIT_PURPOSE2_CLARIFICATION);
}

void Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose3()
{
	onPurposeChanged(IDC_COMBO_PURPOSE3, IDC_COMBO_PURPOSE3_CODE, IDC_COMBO_PURPOSE3_TYPE, IDC_STATIC_PURPOSE3_MONTH, IDC_COMBO_PURPOSE3_MONTH, IDC_STATIC_PURPOSE3_YEAR, IDC_EDIT_PURPOSE3_YEAR, IDC_STATIC_PURPOSE3_CLARIFICATION, IDC_EDIT_PURPOSE3_CLARIFICATION);
}

void Prior2DReceiveBelCash::OnCbnSelchangeComboPurpose4()
{
	onPurposeChanged(IDC_COMBO_PURPOSE4, IDC_COMBO_PURPOSE4_CODE, IDC_COMBO_PURPOSE4_TYPE, IDC_STATIC_PURPOSE4_MONTH, IDC_COMBO_PURPOSE4_MONTH, IDC_STATIC_PURPOSE4_YEAR, IDC_EDIT_PURPOSE4_YEAR, IDC_STATIC_PURPOSE4_CLARIFICATION, IDC_EDIT_PURPOSE4_CLARIFICATION);
}

bool Prior2DReceiveBelCash::CheckNonEmpty(int editId, CString captionText, CString errorText)
{
	CString cstr;
	CEdit* cEdit;
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(editId));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim().GetLength()==0)
	{
		MessageBox(errorText, captionText, MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return false;
	}
	return true;
}
bool Prior2DReceiveBelCash::CheckPurpose1Filled()
{
	return CheckPurposeFilled(_T("������ ����"), IDC_COMBO_PURPOSE1, IDC_COMBO_PURPOSE1_CODE, IDC_EDIT_PURPOSE1_SUM,
		IDC_COMBO_PURPOSE1_TYPE, IDC_COMBO_PURPOSE1_MONTH, IDC_EDIT_PURPOSE1_YEAR, IDC_EDIT_PURPOSE1_CLARIFICATION);
}
bool Prior2DReceiveBelCash::CheckPurpose2Filled()
{
	return CheckPurposeFilled(_T("������ ����"), IDC_COMBO_PURPOSE2, IDC_COMBO_PURPOSE2_CODE, IDC_EDIT_PURPOSE2_SUM,
		IDC_COMBO_PURPOSE2_TYPE, IDC_COMBO_PURPOSE2_MONTH, IDC_EDIT_PURPOSE2_YEAR, IDC_EDIT_PURPOSE2_CLARIFICATION);
}
bool Prior2DReceiveBelCash::CheckPurpose3Filled()
{
	return CheckPurposeFilled(_T("������� ����"), IDC_COMBO_PURPOSE3, IDC_COMBO_PURPOSE3_CODE, IDC_EDIT_PURPOSE3_SUM,
		IDC_COMBO_PURPOSE3_TYPE, IDC_COMBO_PURPOSE3_MONTH, IDC_EDIT_PURPOSE3_YEAR, IDC_EDIT_PURPOSE3_CLARIFICATION);
}
bool Prior2DReceiveBelCash::CheckPurpose4Filled()
{
	return CheckPurposeFilled(_T("��������� ����"), IDC_COMBO_PURPOSE4, IDC_COMBO_PURPOSE4_CODE, IDC_EDIT_PURPOSE4_SUM,
		IDC_COMBO_PURPOSE4_TYPE, IDC_COMBO_PURPOSE4_MONTH, IDC_EDIT_PURPOSE4_YEAR, IDC_EDIT_PURPOSE4_CLARIFICATION);
}
bool Prior2DReceiveBelCash::CheckPurposeFilled(CString purposeText, int purposeCBox, int codeCbox, int sumEdit, int typeCBox, int monthCBox, int yearEdit, int otherPurposeName)
{
	if (!CheckNonEmpty(purposeCBox, _T("����:�������� ������"), 
		_T("���� \"����\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;

	if (!CheckNonEmpty(codeCbox, _T("���:�������� ������"), 
		_T("���� \"���\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;

	if (!CheckNonEmpty(sumEdit, _T("�����:�������� ������"), 
		_T("���� \"�����\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;

	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(purposeCBox));
	CString cstr;
	cBox->GetWindowTextW(cstr);

	if (cstr == _T("������ �����"))
	{
		if (!CheckNonEmpty(typeCBox, _T("��� ������ �����:�������� ������"), 
		_T("���� \"��� ������ �����\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;

		if (!CheckNonEmpty(monthCBox, _T("����� ������ �����:�������� ������"), 
		_T("���� \"����� ������ �����\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;

		if (!CheckNonEmpty(yearEdit, _T("��� ������ �����:�������� ������"), 
		_T("���� \"��� ������ �����\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;

		if (!CheckYearValid(yearEdit, _T("��� ������ �����:�������� ������"), 
		_T("���� \"��� ������ �����\" ") + purposeText + _T(" ������ ����� ������ 'YYYY'.")))
		return false;
	}

	if (cstr == _T("���� ����"))
	{
		if (!CheckNonEmpty(otherPurposeName, _T("��������� ����:�������� ������"), 
			_T("���� \"��������� ����\" ") + purposeText + _T(" ������ ���� ���������.")))
		return false;
	}

	return true;
}

bool Prior2DReceiveBelCash::CheckYearValid(int editYearId, CString captionText, CString errorText)
{
	CString cstr;
	CEdit* cEdit;
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(editYearId));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=4)
	{
		MessageBox(errorText, captionText, MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return false;
	}
	return true;
}

void Prior2DReceiveBelCash::ShowHidePurposeClarificationGeneral(int nCmdShow, int clarificationText, int clarificationEdit)
{
	CEdit* cEdit;
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationText));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(clarificationEdit));
	cEdit->ShowWindow(nCmdShow);
}

void Prior2DReceiveBelCash::OnDtnDatetimechangeDatetimepickerDocIssueDate(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	// TODO: Add your control notification handler code here
	CDateTimeCtrl* cDtCtrl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DOC_ISSUE_DATE));
	cDtCtrl->SetFormat(DTS_SHORTDATEFORMAT);
	*pResult = 0;
}
