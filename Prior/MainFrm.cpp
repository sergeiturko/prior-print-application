#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame
CString csLang[32];
HICON hGI;

bool g_Encrypt;

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString cst;
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	CButton* cButton = reinterpret_cast<CButton*>(GetDlgItem(ID_FILE_OPEN));
	//cButton->

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	WCHAR *sDir = new WCHAR[sDirLen];
	::GetCurrentDirectory(sDirLen, sDir);
	//MessageBox(sDir,L"����������� ������",MB_OK);
	CIniReader *cIR = new CIniReader(CString(sDir)+L"\\language.ini");
	CString csSect = L"BankAttributes";
	CString csEnc;
	
	if (cIR->sectionExists(csSect))
	{
		csLang[0] = cIR->getKeyValue(L"BankName",csSect);
		csLang[1] = cIR->getKeyValue(L"BankType",csSect);
		csLang[2] = cIR->getKeyValue(L"BankCode",csSect);
		csLang[3] = cIR->getKeyValue(L"BankAddr",csSect);
		csLang[4] = cIR->getKeyValue(L"BankSpec",csSect);

		csSect = L"Encryption";
		csEnc = cIR->getKeyValue(L"Encrypt",csSect);
		csEnc.MakeLower();
		if (csEnc == "1" || csEnc == "true") g_Encrypt = true;
		else g_Encrypt = false;
	}
	else
	{
		MessageBox(L"��� ����� ��������",L"����������� ������",MB_OK);
		return -1;
	}
	this->SetWindowTextW(L"���������� ������ ��������� ���������� "+csLang[0]);
	hGI = (HICON)LoadImage(0,CString(sDir)+L"\\main.ico",IMAGE_ICON,0,0,LR_LOADFROMFILE);
	if (hGI)
	{
		this->SetIcon(hGI,TRUE);
		this->SetIcon(hGI,FALSE);
	}
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.cx = 800;
	cs.cy = 600;

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers



