// Prior2DFeeCash.cpp : implementation file
// ���������� �� ����� ���������

#include "stdafx.h"
#include "Prior2D.h"
#include "Prior2DFeeCash.h"
#include "TemplateName.h"
#include "..\pp2dlib\Prior2DModule.h"

// Prior2DFeeCash

extern CString csLang[32];

IMPLEMENT_DYNCREATE(Prior2DFeeCash, CFormView)

Prior2DFeeCash::Prior2DFeeCash()
	: CFormView(Prior2DFeeCash::IDD)
{

}

Prior2DFeeCash::~Prior2DFeeCash()
{
}

void Prior2DFeeCash::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Prior2DFeeCash, CFormView)
	ON_EN_CHANGE(IDC_EDIT_FEE_SUMMDIGITS, &Prior2DFeeCash::OnEnChangeEditFeeCashSummdigits)
	ON_COMMAND(ID_SAVE_TEMPLATE, &Prior2DFeeCash::OnSaveTemplate)
	ON_COMMAND(ID_EDIT_COPY, &Prior2DFeeCash::OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, &Prior2DFeeCash::OnEditCut)
	ON_COMMAND(ID_EDIT_PASTE, &Prior2DFeeCash::OnEditPaste)
	ON_COMMAND(ID_FILE_PRINT, &Prior2DFeeCash::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DFeeCash::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_EN_CHANGE(IDC_EDIT_FEE_ACCOUNTB, &Prior2DFeeCash::OnEnChangeEditFeeAccountb)
	ON_CBN_SELCHANGE(IDC_CBOX_FEE_REPORTCODE, &Prior2DFeeCash::OnCbnSelchangeCboxFeeReportcode)
	ON_CBN_EDITUPDATE(IDC_CBOX_FEE_REPORTCODE, &Prior2DFeeCash::OnCbnEditupdateCboxFeeReportcode)	
END_MESSAGE_MAP()


// Prior2DFeeCash diagnostics

#ifdef _DEBUG
void Prior2DFeeCash::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void Prior2DFeeCash::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void Prior2DFeeCash::OnInitialUpdate()
{
	CString cstmp;
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB));
	cEdit->SetWindowTextW(_T("\"���������\" ���"));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB_CODE));
	cEdit->SetWindowTextW(_T("749"));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NUMBER));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_UNP));
	cEdit->SetLimitText(9);
	cEdit->EnableWindow(false);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NAMEB));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_PAYMENTPURPOSE));
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ADDITIONAL_INFO));
	cEdit->SetLimitText(180);

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_FEE_REPORTCODE));
	cCmbBox->AddString(_T("2004"));
	cCmbBox->SelectString(0, _T("2004"));
	OnCbnSelchangeCboxFeeReportcode();
}


BOOL Prior2DFeeCash::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;

	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	return DoPreparePrinting(pInfo);	
}

void Prior2DFeeCash::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DFeeCash::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	delete m_FLarge;
}

void Prior2DFeeCash::SaveTemplate(CString strSection)
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = strSection;
	tmptltName.setPassword = true;

	while(tmptltName.DoModal() == IDOK)
	{
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DFeeCash::SaveConfigContent(CString strSection, CString password)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (password != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	iniReader->setKey(password,_T("PASSWORD"),strSection);	
	iniReader->setKey(_T("FEECASH"),_T("TYPE"),strSection);

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NUMBER"), strSection);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_FEE_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	iniReader->setKey(cstr, _T("DATE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMSPELLED"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMDIGITS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCODEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NAMEB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PAYMENTPURPOUSE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_UNP));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNP"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ADDITIONAL_INFO));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ADDITIONALINFO"), strSection);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_FEE_REPORTCODE));
	cCmb->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("REPORTCODE"), strSection);

	iniReader->NeedEncryption = false;
}

void Prior2DFeeCash::LoadConfigContent(CString strSection)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (iniReader->getKeyValue(_T("PASSWORD"), strSection) != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NUMBER"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMSPELLED"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMDIGITS"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NAMEB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_PAYMENTPURPOSE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTPURPOUSE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_UNP));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNP"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ADDITIONAL_INFO));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ADDITIONALINFO"), strSection));
	
	iniReader->NeedEncryption = false;

	if (!CIniReader::CheckPasswordExists(strSection))
	{
		int res = ::MessageBoxA(this->m_hWnd, "������ ��� ������� ������� �� ����������. ����������?", 
				"�������� ������ �������", MB_YESNO);	
		if (res == IDYES)
		{
			SaveTemplate(strSection);
		}
	}
	
}

void Prior2DFeeCash::OnSaveTemplate()
{
	
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = _T("");
	tmptltName.setPassword = false;
	while(tmptltName.DoModal() == IDOK)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		if(iniReader->sectionExists(tmptltName.cstrTemplateName))
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ����� ������ ��� ����������, ������������ ���?", 
				"���������� �������", MB_YESNOCANCEL);
			if(res == IDCANCEL) return;
			if(res == IDNO) continue;
			if (!CIniReader::CheckPassword(tmptltName.cstrTemplateName))
				continue;
		}		
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DFeeCash::OnEditCopy()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_COPY);
}

void Prior2DFeeCash::OnEditCut()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_CUT);
}

void Prior2DFeeCash::OnEditPaste()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_PASTE);
}

void Prior2DFeeCash::OnEnChangeEditFeeCashSummdigits()
{
	int i = 0;
	CString cstr1, cstr2;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr1);
	if (cstr1.GetLength() == 0)
	{
		(reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED)))->SetWindowText(_T(""));
		return;
	}
	if (_ttoi(cstr1.Trim()) == 0)
	{
		(reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED)))->SetWindowText(_T("����"));
		return;
	}
	cstr2 = cstr1;
	int len = cstr1.GetLength();
	char* str = new char[len+4];
	while(i < len){
		if (cstr1[i] >= '0' && cstr1[i] <= '9')
		{
			str[i] = cstr1[i];
			i++;
		}
		else if (i < len)
		{
				CString::CopyChars(cstr1.GetBuffer()+i,cstr1.GetBuffer()+i+1,len-i+1);
				len--;
		}
	}
	str[len]=0;
	__int64 ival = _atoi64(str);

	_i64toa( ival, str, 10 ); 
	cstr1.Format(L"%lld",ival);
	if (len > 0 && cstr2 != cstr1) cEdit->SetWindowTextW(cstr1);
	wchar_t * cs = (wchar_t*)malloc(1024);
	Utilites::SummToStringWithoutBelRub(ival, cs);
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED));
	cEdit->SetWindowText(cs);
	free(cs);
	delete []str;
}
void Prior2DFeeCash::OnFilePrint()
{
	CString cstr;
	CEdit* cEdit;
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NAMEB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"���������� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ���������� ������ ���� ���������.",
			"���������� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �������\" ������ ���� ���������.",
			"����� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength() != 0)
	{
		_tsetlocale(LC_ALL, _T("Russian_Russia.1251"));
		CString cstrTmp(_T(" "));
		cstr.Trim();
		cstrTmp.SetAt(0,cstr.GetAt(0));
		cstrTmp.MakeUpper();
		cstr.SetAt(0,cstrTmp.GetAt(0));
		cEdit->SetWindowTextW(cstr);
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� ������\" ������ ���� ���������.",
			"���������� ������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� ����������\" ������ ���� ���������.",
			"���� ����������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB_CODE));
	CString bcode;
	cEdit->GetWindowTextW(bcode);
	if(bcode.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����\" �����-���������� ������ ���� ���������.",
			"��� �����-����������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ������ ���� ���������.",
			"����� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	if(cstr.GetLength()!=13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ������ ��������� 13 ����.",
			"����� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_FEE_REPORTCODE));
	cCmbBox->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� ��������� �������\" ������ ���� ���������.",
			"��� ��������� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	if (CheckBigSum())
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ADDITIONAL_INFO));
		cEdit->GetWindowTextW(cstr);
		if (cstr.Trim(_T(" ")).GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "�������������� ���������� ������ ���� ���������.",
				"�������������� ����������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
		cEdit->GetWindowTextW(cstr);
		if (cstr.Left(4) == _T("3013"))
		{
			cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_UNP));
			cEdit->GetWindowTextW(cstr);
			if (cstr.GetLength()==0)
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "��� ������ ���� ��������.",
					"���:�������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}
		}
	}

	CFormView::OnFilePrint();
}
void Prior2DFeeCash::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	Prior2DFeeCashModule printer;

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NUMBER));
	cEdit->GetWindowTextW(cstr);
	printer.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_FEE_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("\"%i\" %s %i �."), time.wDay, Utilites::NumserOfMonthToString(time.wMonth), time.wYear);
	printer.setDate(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	printer.setAccountB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB));
	cEdit->GetWindowTextW(cstr);
	printer.setBankNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	printer.setBankCodeB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_NAMEB));
	cEdit->GetWindowTextW(cstr);
	printer.setNameB(cstr);
	printer.setNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	printer.setPaymentPurpose(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	printer.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	printer.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_UNP));
	cEdit->GetWindowTextW(cstr);
	printer.setUnp(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ADDITIONAL_INFO));
	cEdit->GetWindowTextW(cstr);
	printer.setAdditionalInfo(cstr);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_FEE_REPORTCODE));
	cCmb->GetWindowTextW(cstr);
	printer.setReportCode(cstr);

	int ibw = 2480;
	int ibh = 3558;

	CDC dc;
	CBitmap bmp;
	printer.BuildBarcode(&dc, &bmp, ibw, ibh);
	pDC->StretchBlt(pInfo->m_rectDraw.left, pInfo->m_rectDraw.top, pInfo->m_rectDraw.Width(), pInfo->m_rectDraw.Height(), &dc, 0, 0, ibw, ibh, SRCCOPY);	
}


void Prior2DFeeCash::OnEnChangeEditFeeAccountb()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_ACCOUNTB));
	CEdit* unpEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_UNP));
	CString cstr;
	cEdit->GetWindowTextW(cstr);
	
	if (cstr.Left(4) == _T("3013"))
	{
		unpEdit->EnableWindow(true);			
	}
	else
	{
		unpEdit->EnableWindow(false);			
	}		
}

void Prior2DFeeCash::OnCbnSelchangeCboxFeeReportcode()
{
	CStatic* cStatic = reinterpret_cast<CStatic*>(GetDlgItem(IDC_STATIC_CODEDESC));
	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_FEE_REPORTCODE));
	CString cstr;
	cCmbBox->GetWindowTextW(cstr);
	cStatic->SetWindowTextW(GetDescriptionByCode(cstr));	
}

void Prior2DFeeCash::OnCbnEditupdateCboxFeeReportcode()
{
}

CString Prior2DFeeCash::GetDescriptionByCode(CString code)
{
	if (code == L"2004")
	{
		return L"����������� �������� ����� �� ����������� ���, �������������� ����������������, ���������� ���.";
	};	

	return L"";
}

int Prior2DFeeCash::GetBaseAmount()
{
	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	WCHAR *sDir = new WCHAR[sDirLen];
	::GetCurrentDirectory(sDirLen, sDir);
	CIniReader *cIR = new CIniReader(CString(sDir)+L"\\language.ini");
	
		if (cIR->sectionExists(L"BaseAmount"))
		{
			return _ttoi(cIR->getKeyValue(L"BaseAmount", L"BaseAmount"));
		}
		else 
		{
			return 0;
		}
}

bool Prior2DFeeCash::CheckBigSum()
{
	int baseAmount = GetBaseAmount();

	CString cstr;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_FEE_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);	
	__int64 ival = _ttoi64(cstr);

	return ival >= baseAmount*100;
}
