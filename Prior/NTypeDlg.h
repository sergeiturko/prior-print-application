#pragma once


// CNTypeDlg dialog

class CNTypeDlg : public CDialog
{
	DECLARE_DYNAMIC(CNTypeDlg)

public:
	CNTypeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNTypeDlg();
public:
	CDocTemplate *m_pSelectedTemplate;
	CNTypeDlg(CPtrList*);

// Dialog Data
	enum { IDD = IDD_DTEMPLATES };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog(); // called first time after construct
	bool m_bOpen;
	CPtrList *m_CDPL;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnLbnDblclkListtmpl();
};
