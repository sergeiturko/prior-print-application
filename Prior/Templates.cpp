#include "stdafx.h"
#include "Templates.h"


// CTemplates dialog
IMPLEMENT_DYNAMIC(CTemplates, CDialog)

CTemplates::CTemplates(bool bOpen, CWnd* pParent /*=NULL*/)
	: CDialog(CTemplates::IDD, pParent)
{
	m_bOpen = bOpen;
	Action = TMPLT_ACTION_NONE;
}

BOOL CTemplates::OnInitDialog()
{
	CIniReader* iniReader =  theApp.GetINIHandler();
	CStringList* strl = iniReader->getSectionNames();

	CListBox* cListBox = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LIST_TEMPLATES));
	int iCount = strl->GetCount();
	POSITION pos = strl->GetHeadPosition();
	for(int i = 0; i < iCount; i++)
	{
		CString cstrTmplName = strl->GetNext(pos);
		CString cstrType = iniReader->getKeyValue(_T("TYPE"),cstrTmplName);
		if(cstrType==_T("PPSBEL"))cstrType=_T("����. �����., BYR(974), ����� 36");
		else if(cstrType==_T("PPBEL"))cstrType=_T("����. �����., BYR(974), ����� 31");
		//Changed 21.09.2011
		else if(cstrType==_T("PPFULL"))cstrType=_T("����. �����., ������, ����� 31");
		else if(cstrType==_T("FEECASH"))cstrType=_T("���������� �� ����� ���������");
		else if(cstrType==_T("CHECKREG"))cstrType=_T("���������-������ �����");
		else if(cstrType==_T("RECEIVEBELCASH"))cstrType=_T("��������� �� ��������� �������� ����������� ������");
		cListBox->AddString(cstrTmplName+_T("     (")+cstrType+_T(")"));
	}

	if(!m_bOpen)
	{
		CButton* cButton = reinterpret_cast<CButton*>(GetDlgItem(ID_TMPL_OPEN));
		cButton->ShowWindow(SW_HIDE);
	}
	return TRUE;
}

CTemplates::~CTemplates()
{
}

void CTemplates::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTemplates, CDialog)
	ON_BN_CLICKED(ID_TMPL_DEL, &CTemplates::OnBnClickedTmplDel)
	ON_BN_CLICKED(IDOK, &CTemplates::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CTemplates::OnBnClickedOk)
	ON_BN_CLICKED(ID_TMPL_OPEN, &CTemplates::OnBnClickedTmplOpen)
	ON_LBN_DBLCLK(IDC_LIST_TEMPLATES, &CTemplates::OnLbnDblclkListTemplates)
END_MESSAGE_MAP()


// CTemplates message handlers

void CTemplates::OnBnClickedTmplDel()
{
	CListBox* cListBox = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LIST_TEMPLATES));
	int iIndex = cListBox->GetCurSel();
	// ��� ��������� � ������
	if(iIndex == -1) return;
	if(MessageBoxA(this->m_hWnd, "�� ������������� ������ ������� ���������� ������?",
		"�������� �������", MB_YESNO) == IDYES)
	{
		CString cstrTmplt;
		cListBox->GetText(iIndex,cstrTmplt);
		int ind = cstrTmplt.Find(_T("(����. �����.,"));
		if(ind!=-1) 
		{
			cstrTmplt = cstrTmplt.Left(ind-5);
		}
		else
		{
			ind = cstrTmplt.Find(_T("(���������� �� ����� ���������"));
			if (ind != -1)
			{
				cstrTmplt = cstrTmplt.Left(ind-5);
			}
			else
			{
				ind = cstrTmplt.Find(_T("(���������-������ �����)"));
				if (ind != -1)
				{
					cstrTmplt = cstrTmplt.Left(ind-5);
				}
				else
				{
					ind = cstrTmplt.Find(_T("(��������� �� ��������� �������� ����������� ������)"));
					cstrTmplt = cstrTmplt.Left(ind-5);
				}				
			}
		}
		if (!CheckPassword(cstrTmplt))
			return;
		cListBox->DeleteString(iIndex);
		CIniReader* iniReader = theApp.GetINIHandler();
		if(iniReader->sectionExists(cstrTmplt))
		{
			WCHAR *buffer = new WCHAR[100];
			SHGetSpecialFolderPath(0, buffer, CSIDL_APPDATA, false);
			
			CString sDir = CString(buffer) + CString("\\ITSoft\\PriorVznos");

			CIniReader tmp_iniReader;
			tmp_iniReader.setINIFileName(CString(sDir)+CString("\\~config.ini~.tmp"));

			CIniReader* iniReader = theApp.GetINIHandler();
			CStringList* strl = iniReader->getSectionNames();

			int iCount = strl->GetCount();
			POSITION pos = strl->GetHeadPosition();
			for(int i = 0; i < iCount; i++)
			{
				CString cSection = strl->GetNext(pos);
				if(cSection==_T("") || cSection==cstrTmplt) continue;
				CStringList* strlFields = iniReader->getSectionData(cSection);
				int iCountFields = strlFields->GetCount();
				POSITION posField = strlFields->GetHeadPosition();
				for(int ii = 0; ii < iCountFields; ii++)
				{
					CString cField = strlFields->GetNext(posField);
					int iIndex = cField.Find(_T("="));
					if(iIndex!=-1)
						tmp_iniReader.setKey(cField.Right(cField.GetLength()-iIndex-1), cField.Left(iIndex), cSection);
				}
			}
			try
			{
				BOOL bRes = DeleteFileW(CString(sDir)+CString("\\config.ini"));
				bRes = CopyFileW(CString(sDir)+CString("\\~config.ini~.tmp"), 
					CString(sDir)+CString("\\config.ini"), FALSE);
				bRes = DeleteFileW(CString(sDir)+CString("\\~config.ini~.tmp"));
				int gg = 0;
			}
			catch(...)
			{
				int gg = 0;
			}

			//delete[] sDir;
		}
	}
}

void CTemplates::OnBnClickedOk()
{
	OnOK();
}

void CTemplates::OnBnClickedTmplOpen()
{
	CListBox* cListBox = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LIST_TEMPLATES));
	int iIndex = cListBox->GetCurSel();
	if(iIndex != -1)
	{
		cListBox->GetText(iIndex,cstrTemplateName);
		
		int ind = cstrTemplateName.Find(_T("(����. �����.,"));
		if(ind!=-1) 
		{
			cstrTemplateName = cstrTemplateName.Left(ind-5);
		}
		else
		{
			ind = cstrTemplateName.Find(_T("(���������� �� ����� ���������"));
			if (ind != -1)
			{
				cstrTemplateName = cstrTemplateName.Left(ind-5);
			}
			else
			{
				ind = cstrTemplateName.Find(_T("(���������-������ �����)"));
				if (ind != -1)
				{
					cstrTemplateName = cstrTemplateName.Left(ind-5);
				}
				else
				{
					ind = cstrTemplateName.Find(_T("(��������� �� ��������� �������� ����������� ������)"));
					cstrTemplateName = cstrTemplateName.Left(ind-5);
				}
			}
		}
		if (m_bOpen)
			if (CheckPassword(cstrTemplateName))
				Action = TMPLT_ACTION_OPEN;
			else return;
	}
	OnOK();
}

void CTemplates::OnLbnDblclkListTemplates()
{
	OnBnClickedTmplOpen();
}

bool CTemplates::CheckPassword(CString cstrTemplateName)
{
	return CIniReader::CheckPassword(cstrTemplateName);
}
