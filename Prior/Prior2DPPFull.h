#pragma once



// Prior2DPPFull form view

class Prior2DPPFull : public CFormView
{
	DECLARE_DYNCREATE(Prior2DPPFull)

protected:
	Prior2DPPFull();           // protected constructor used by dynamic creation
	virtual ~Prior2DPPFull();

public:
	enum { IDD = IDD_PRIOR2DPPFULL };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	virtual void SaveConfigContent(CString strSection, CString password);
	virtual void LoadConfigContent(CString strSection);
	virtual void OnEditCopy();
	virtual void OnEditCut();
	virtual void OnEditPaste();
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnFilePrint();
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	CFont* m_FLarge;

	DECLARE_MESSAGE_MAP()
private:
	CString EditSummcypher;

	void SaveTemplate(CString strSection);
	bool CheckSummDigits(CString sumDigits);
public:
	afx_msg void OnBnClickedCheckPpfUrgent();
	afx_msg void OnBnClickedCheckPpfNonurgent();
	afx_msg void OnBnClickedCheckPpfPl();
	afx_msg void OnBnClickedCheckPpfBn();
	afx_msg void OnBnClickedCheckPpfPlBn();
	afx_msg void OnEnChangeEditPpfNumber();
	afx_msg void OnSaveTemplate();
	afx_msg void OnEnChangeEditPpfSummdigits();
};


