#pragma once



// Prior2DPTBEL form view

class Prior2DPTBEL : public CFormView
{
	DECLARE_DYNCREATE(Prior2DPTBEL)

protected:
	Prior2DPTBEL();           // protected constructor used by dynamic creation
	virtual ~Prior2DPTBEL();

public:
//	virtual void SaveConfigContent(CString strSection);
	// enum { IDD = IDD_PRIOR2DPTBEL };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnFilePrint();
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditPtSummspelled();
private:
	CFont* m_FLarge;
	CFont* m_FSmall;
	afx_msg void OnBnClickedRadioPtbPa();
	afx_msg void OnBnClickedRadioPtbLa();
	afx_msg void OnBnClickedRadioPtbNa();
	afx_msg void OnEnChangeEditPtbSummdigits();
};


