// Prior2D.h : main header file for the Prior2D application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

#include "INI.h"

// CPrior2DApp:
// See Prior2D.cpp for the implementation of this class
//

class CPrior2DApp : public CWinApp
{
public:
	CPrior2DApp();

// Overrides
public:
	virtual BOOL InitInstance();
	CIniReader* GetINIHandler();
	void AddDocTemplate(CDocTemplate*);

// Implementation
	afx_msg void OnFilePrint(CDC* pDC, CPrintInfo* pInfo);
	afx_msg void OnAppAbout();
	afx_msg void OnAbout();
//	afx_msg void OnFileNew();
	DECLARE_MESSAGE_MAP()
private:
	CIniReader m_iniReader;
	afx_msg void OnLoadTemplate();
	afx_msg void OnTemplates();
	afx_msg void OnToggleBarcode();


	CMultiDocTemplate* pDocTemplatePPS;
	CMultiDocTemplate* pDocTemplatePP;
	CMultiDocTemplate* pDocTemplatePPF;
	
	CMultiDocTemplate* pDocTemplateFEECASH;
	
	CMultiDocTemplate* pDocTemplateRECEIVEBELCASH;
};
extern CPrior2DApp theApp;