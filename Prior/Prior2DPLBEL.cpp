//
// 0401600031
//
#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Prior2DPPBEL
extern CString csLang[32];
extern bool bGlobalPrintBarcode;

IMPLEMENT_DYNCREATE(Prior2DPPBEL, CFormView)

BEGIN_MESSAGE_MAP(Prior2DPPBEL, CFormView)
	ON_COMMAND(ID_FILE_PRINT, &Prior2DPPBEL::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DPPBEL::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_CHECK_NOURGENT, &Prior2DPPBEL::OnBnClickedCheckNourgent)
	ON_BN_CLICKED(IDC_CHECK_URGENT, &Prior2DPPBEL::OnBnClickedCheckUrgent)
	ON_BN_CLICKED(IDC_CHECK_PL, &Prior2DPPBEL::OnBnClickedCheckPl)
	ON_BN_CLICKED(IDC_CHECK_BN, &Prior2DPPBEL::OnBnClickedCheckBn)
	ON_BN_CLICKED(IDC_CHECK_PL_BN, &Prior2DPPBEL::OnBnClickedCheckPlBn)
	ON_EN_CHANGE(IDC_EDIT_RECEIVER_ACCOUNT, &Prior2DPPBEL::OnEnChangeEditReceiverAccount)
	ON_EN_CHANGE(IDC_EDIT_SUMMCYPHER, &Prior2DPPBEL::OnEnChangeEditSummcypher)
	ON_EN_CHANGE(IDC_EDIT_NUMBER, &Prior2DPPBEL::OnEnChangeEditNumber)
	ON_COMMAND(ID_SAVE_TEMPLATE, &Prior2DPPBEL::OnSaveTemplate)
	ON_COMMAND(ID_EDIT_COPY, &Prior2DPPBEL::OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, &Prior2DPPBEL::OnEditCut)
	ON_COMMAND(ID_EDIT_PASTE, &Prior2DPPBEL::OnEditPaste)
	ON_EN_CHANGE(IDC_EDIT_UNN1, &Prior2DPPBEL::OnEnChangeEditUnn1)
	ON_EN_CHANGE(IDC_EDIT_UNN2, &Prior2DPPBEL::OnEnChangeEditUnn2)
	ON_EN_CHANGE(IDC_EDIT_UNN3, &Prior2DPPBEL::OnEnChangeEditUnn3)
	ON_EN_CHANGE(IDC_EDIT_PAYERACCOUNT, &Prior2DPPBEL::OnEnChangeEditPayeraccount)
	ON_EN_CHANGE(IDC_EDIT_BANK_A_KOD, &Prior2DPPBEL::OnEnChangeEditBankAKod)
	ON_EN_CHANGE(IDC_EDIT_BANK_B_KOD, &Prior2DPPBEL::OnEnChangeEditBankBKod)
	ON_EN_CHANGE(IDC_EDIT_BANKCORR_KOD, &Prior2DPPBEL::OnEnChangeEditBankcorrKod)
	ON_EN_CHANGE(IDC_EDIT_BANKCORR_ACCOUNT, &Prior2DPPBEL::OnEnChangeEditBankcorrAccount)
END_MESSAGE_MAP()

// Prior2DPPBEL construction/destruction

Prior2DPPBEL::Prior2DPPBEL()
	: CFormView(Prior2DPPBEL::IDD)
{
	// TODO: add construction code here

}

Prior2DPPBEL::~Prior2DPPBEL()
{
}

void Prior2DPPBEL::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL Prior2DPPBEL::PreCreateWindow(CREATESTRUCT& cs)
{
	return CFormView::PreCreateWindow(cs);
}

void Prior2DPPBEL::OnInitialUpdate()
{
	CString cstmp;
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_CURRENCY));
	cEdit->SetWindowTextW(_T("974"));
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_NAME));
	cstmp = L"\""+csLang[0]+L"\" "+csLang[1]+L", ���������� ��������";
	cEdit->SetWindowTextW(cstmp);
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_KOD));
	cstmp = csLang[2];
	cEdit->SetWindowTextW(cstmp);
	cEdit->SetLimitText(9);
	cEdit->EnableWindow(FALSE);

	CButton* cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	cButton->SetCheck(BST_CHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->EnableWindow(TRUE);

	cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	cButton->SetCheck(BST_CHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_FULL2));
	cstmp = csLang[3];
	cEdit->SetWindowTextW(cstmp);
	cEdit->SetLimitText(30);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_CURRENCY));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->SetLimitText(12);	

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->SetLimitText(220);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
	cEdit->SetLimitText(150);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_NAME));
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_CBU));
	cEdit->SetLimitText(3);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_NAME));
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_KOD));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CBU));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CITY));
	cEdit->SetLimitText(30);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_NAME));
	cEdit->SetLimitText(150);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->SetLimitText(13);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE));
	cEdit->SetLimitText(290);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN1));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN2));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN3));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->SetLimitText(5);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ORDER));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_NAME));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_ACCOUNT));
	cEdit->SetLimitText(20);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_KOD));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMISSION_ACCOUNT));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TRADE_DATE_PASS));
	cEdit->SetLimitText(60);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PP_DETAILS));
	cEdit->SetLimitText(120);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->SetLimitText(6);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->SetLimitText(62);

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmbBox->AddString(_T("01 - ������� ���������� ���� ������� 1974 �."));
	cCmbBox->AddString(_T("02 - ������� ���������� �� ������� 1993 �."));
	cCmbBox->AddString(_T("03 - ������� ���������� �� ������� 1996 �."));
	cCmbBox->AddString(_T("04 - ������� ����� � 9"));
	cCmbBox->AddString(_T("05 - ������������� � �������� (��� ���, �� ��������� 16-������� ��������)"));
	cCmbBox->AddString(_T("06 - ��� �� ���������� � ��, �������� ������������� �������� ��"));
	cCmbBox->AddString(_T("07 - ������������� �������, �������� � ������������� ������� �� ���������� ��"));
	cCmbBox->AddString(_T("08 - ������������� � ����������� ����������� � �������������� ������� ������� ��� �������������� ������ � ��"));
	cCmbBox->AddString(_T("09 - �������������� ������������ ������� ���������� ������������ ����������� ��� ��������, ��� ����������"));
	cCmbBox->AddString(_T("10 - ��������, �������������� �������� ���� ��� �����������, �������� ������������� �������� ����������� ����������� ����������"));
	cCmbBox->AddString(_T("11 - ������� ����� ��������������� ������� ������"));
	cCmbBox->AddString(_T("12 - ������������ �������������"));
	cCmbBox->AddString(_T("13 - ������� ������ (������������� �������� ������)"));
	cCmbBox->AddString(_T("14 - ������������� � �������������� �������������� ������ � ��"));	
	cCmbBox->AddString(_T("99 - ���� ��������"));
	cCmbBox->SetDroppedWidth(730);

	ShowHideAdditionalInfo();
}


// Prior2DPPBEL diagnostics

#ifdef _DEBUG
void Prior2DPPBEL::AssertValid() const
{
	CFormView::AssertValid();
}

void Prior2DPPBEL::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CPrior2DDoc* Prior2DPPBEL::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPrior2DDoc)));
	return (CPrior2DDoc*)m_pDocument;
}
#endif //_DEBUG
// Prior2DPPBEL message handlers

BOOL Prior2DPPBEL::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;
	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));

	return DoPreparePrinting(pInfo);
}

void Prior2DPPBEL::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DPPBEL::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	delete m_FLarge;
}

void Prior2DPPBEL::SaveTemplate(CString strSection)
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = strSection;
	tmptltName.setPassword = true;

	while(tmptltName.DoModal() == IDOK)
	{
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DPPBEL::SaveConfigContent(CString strSection, CString password)
{
	CIniReader* iniReader = theApp.GetINIHandler();
	CString cstr;

	if (password != _T(""))
	{
		iniReader->NeedEncryption = true;
	}
	
	iniReader->setKey(password,_T("PASSWORD"),strSection);
	iniReader->setKey(_T("PPBEL"),_T("TYPE"),strSection);	

	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("NUMBER"),strSection);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	iniReader->setKey(cstr,_T("DATE"),strSection);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED,
		 bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	iniReader->setKey(cstr,_T("URGENT"),strSection);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	iniReader->setKey(cstr,_T("ORDINARY"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("SUMMSPELLED"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("CURRENCY"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("SUMMDIGITS"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("NAMEA"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("ACCOUNTA"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKA"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_KOD));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKCODEA"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_CBU));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKCBUA"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKB"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_KOD));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKCODEB"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CBU));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKCBUB"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("NAMEB"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CITY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKBCITY"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("ACCOUNTB"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("PAYMENTPURPOUSE"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN1));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("UNNA"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN2));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("UNNB"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN3));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("UNN3"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("PAYMENTCODE"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ORDER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("ORDER"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_KOD));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKCODEC"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("ACCOUNTC"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("BANKC"),strSection);

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	bool bPL = cPL->GetCheck()==BST_CHECKED;
	cstr.Format(bPL?_T("X"):_T(" "));
	iniReader->setKey(cstr,_T("PL"),strSection);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	bool bBN = cBN->GetCheck()==BST_CHECKED;
	cstr.Format(bBN?_T("X"):_T(" "));
	iniReader->setKey(cstr,_T("BN"),strSection);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	bool bPL_BN = cPL_BN->GetCheck()==BST_CHECKED;
	cstr.Format(bPL_BN?_T("X"):_T(" "));
	iniReader->setKey(cstr,_T("PL_BN"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMISSION_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("COMMISION"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TRADE_DATE_PASS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("DATE_PASSPORT"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PP_DETAILS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("DETAILES"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_FULL2));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr,_T("COUNTRY"),strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMPANYFIRSTMAN"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMPANYSECONDMAN"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SERIANUM"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("LIVEADRESS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ISSUEDATE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ISSUECOMPANY"), strSection);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->GetWindowTextW(cstr);
	iniReader->setKey(cstr.Mid(0, 2), _T("DOCCODE"), strSection);
}

void Prior2DPPBEL::LoadConfigContent(CString strSection)
{
	CIniReader* iniReader = theApp.GetINIHandler();
	CString cstr;

	if (iniReader->getKeyValue(_T("PASSWORD"), strSection) != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NUMBER"),strSection));

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));

	if(iniReader->getKeyValue(_T("URGENT"),strSection) == "X") cBtnUrgent->SetCheck(BST_CHECKED);
	else cBtnUrgent->SetCheck(BST_UNCHECKED);

	if(iniReader->getKeyValue(_T("ORDINARY"),strSection) == "X") cBtnNOUrgent->SetCheck(BST_CHECKED);
	else cBtnNOUrgent->SetCheck(BST_UNCHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMSPELLED"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_CURRENCY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CURRENCY"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMDIGITS"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKA"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_KOD));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEA"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_CBU));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCBUA"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKB"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_KOD));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEB"),strSection));
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CBU));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCBUB"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEB"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CITY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKBCITY"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTB"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTCODE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE));
	CString* s = new CString(iniReader->getKeyValue(_T("PAYMENTPURPOUSE"), strSection));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTPURPOUSE"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN1));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNNA"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN2));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNNB"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN3));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNN3"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTCODE"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ORDER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ORDER"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_KOD));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEC"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_ACCOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTC"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKC"),strSection));

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	if(iniReader->getKeyValue(_T("PL"),strSection)==_T("X")) cPL->SetCheck(BST_CHECKED);
	else cPL->SetCheck(BST_UNCHECKED);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	if(iniReader->getKeyValue(_T("BN"),strSection)==_T("X")) cBN->SetCheck(BST_CHECKED);
	else cBN->SetCheck(BST_UNCHECKED);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	if(iniReader->getKeyValue(_T("PL_BN"),strSection)==_T("X")) cPL_BN->SetCheck(BST_CHECKED);
	else cPL_BN->SetCheck(BST_UNCHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMISSION_ACCOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMMISION"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TRADE_DATE_PASS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DATE_PASSPORT"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PP_DETAILS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DETAILES"),strSection));
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_FULL2));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COUNTRY"),strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMPANYFIRSTMAN"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMPANYSECONDMAN"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SERIANUM"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("LIVEADRESS"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ISSUEDATE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ISSUECOMPANY"), strSection));

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->SelectString(0, iniReader->getKeyValue(_T("DOCCODE"), strSection));

	ShowHideAdditionalInfo();

	iniReader->NeedEncryption = false;

	if (!CIniReader::CheckPasswordExists(strSection))
	{
		int res = ::MessageBoxA(this->m_hWnd, "������ ��� ������� ������� �� ����������. ����������?", 
				"�������� ������ �������", MB_YESNO);	
		if (res == IDYES)
		{
			SaveTemplate(strSection);
		}
	}
}

void Prior2DPPBEL::OnFilePrint()
{
	CString cstr;

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ���������\" ������ ���� ���������.",
			"����� ���������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_FULL2));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �����\" ������ ���� ���������.",
			"���������� �����: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �������\" ������ ���� ���������.",
			"����� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ������ ���� ���������.",
			"����� ��������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	else
	{
		_tsetlocale(LC_ALL, _T("Russian_Russia.1251"));
		CString cstrTmp(_T(" "));
		cstr.Trim();
		cstrTmp.SetAt(0,cstr.GetAt(0));
		cstrTmp.MakeUpper();
		cstr.SetAt(0,cstrTmp.GetAt(0));
		cEdit->SetWindowTextW(cstr);
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_KOD));
	CString acode;
	cEdit->GetWindowTextW(acode);
	acode.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(acode);
	if(acode.GetLength() != 3 && acode.GetLength() != 9) {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, 
			"���� \"��� �����\" ��� �����-����������� ������ ��������� 3 ��� 9 ����.",
			"��� �����-�����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_KOD));
	CString bcode;
	cEdit->GetWindowTextW(bcode);
	bcode.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(bcode);
	if(bcode.GetLength() != 3 && bcode.GetLength() != 9) {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, 
			"���� \"��� �����\" ��� �����-���������� ������ ��������� 3 ��� 9 ����.",
			"��� �����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	
	if(cstr.GetLength() != 13) {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, 
			"���� \"����� �����\" ����������� ������ ��������� 13 ����.",
			"���� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	} else {
		if(!Utilites::CheckAccount(cstr, acode, _T("749")) &&
		   !Utilites::CheckAccount(cstr, acode, _T("999")))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()!=13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ���������� ������ ��������� 13 ����.",
			"���� ����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	} else {
		if(!Utilites::CheckAccount(cstr, bcode, NULL)) {
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, 
				"���� \"����� �����\" ���������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	bool bUNNBlankAcceptable = cstr[0]=='3'&&cstr[1]=='0'&&cstr[2]=='2'&& (cstr[3]>='1'&&cstr[3]<='4');

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_CBU));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	bool bHasErrors = cstr.GetLength()>0 && cstr.GetLength()!=3;
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-����������� ������ ��������� 3 �����.",
			"��� �����-�����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_NAME));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����-����������\" ������ ���� ���������.",
			"����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CBU));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	bHasErrors = cstr.GetLength()>0 && cstr.GetLength()!=3;
	if(!bHasErrors)
		for(int i = 0; i < cstr.GetLength(); i++) bHasErrors |=  cstr[i]<'0' || cstr[i]>'9';
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-���������� ������ ��������� 3 ����� ��� ���� �����������.",
			"��� �����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CITY));
	cEdit->GetWindowTextW(cstr);	
	if(cstr.Trim().GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �����\" �����-���������� ������ ���� ���������.",
			"���������� �����: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_NAME));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	CString csPayerAcc = cstr;
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength() != 13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� ������ ��������� 13 ����.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	} else {
		if(!Utilites::CheckAccount(cstr, bcode, NULL)) {
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}
	bool bUNNBlankNotAcceptable = cstr[0]=='3'&&cstr[1]=='6';
	{
		if(!Utilites::CheckAccount(cstr, bcode, NULL))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()==0){
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �������\" ������ ���� ���������.",
			"���������� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN1));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if((!bUNNBlankAcceptable || cstr != "") && !Utilites::CheckUNN(cstr))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����������\" ������� - ������ ������� �� ���������� ��������.",
			"��� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN2));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);

	if((bUNNBlankNotAcceptable || cstr != "") && !Utilites::CheckUNN(cstr))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����������\" ������� - ������ ������� �� ���������� ��������.",
			"��� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	
		int ilen = 0;
		cEdit->GetWindowTextW(cstr);
		cstr.Trim(L"\0x20\0x09");
		cEdit->SetWindowTextW(cstr);
		if (csPayerAcc[0] == '3' && csPayerAcc[1] == '6')
		{
			if (!((csPayerAcc[2] == '0') && (csPayerAcc[3] == '5')))
			{
				ilen = cstr.GetLength();
				if (ilen == 5) bHasErrors = false;
				else bHasErrors = true;
				if(!bHasErrors)
					for(int i = 0; i < cstr.GetLength(); i++) bHasErrors |=  cstr[i]<'0' || cstr[i]>'9';
				if(bHasErrors)
				{
					MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �������\" ������ ��������� 5 ����.", "��� �������: �������� ������",MB_OK|MB_ICONWARNING);
					cEdit->SetFocus();
					return;
				}
			}
		}
	

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ORDER));
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	if(cstr.GetLength()!=2 || cstr[0]<'0' || cstr[0]>'9' || cstr[1]<'0' || cstr[1]>'9')
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�������\" ������ ��������� 2 �����.", "�������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	CString cdate;
	CDateTimeCtrl *cDT = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DATE));
	SYSTEMTIME st;
	cDT->GetTime(&st);
	if(st.wYear >= 2010 && cstr != "01" && cstr != "02" && cstr != "03" && cstr != "04" && cstr != "05" && 
		cstr != "11" && cstr != "12" && cstr != "13" && 
		cstr != "21" && cstr != "22" && 
		cstr != "50" &&  cstr != "51")
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�������\" ������ ��������� ���� �� ����� ���� (01,02,03,04,05,11,12,13,21,22,50,51).", "�������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim(_T(" ")).GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"������� � �������� ������� ���� �����������\" ������ ���� ���������.",
			"������� � �������� ������� ���� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	if (CheckShowHideAdditionalInfo())
	{
		CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
		cCmbBox->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� ���������\" ������ ���� ���������.",
				"��� ���������: �������� ������",MB_OK|MB_ICONWARNING);
			cCmbBox->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
		cEdit->GetWindowTextW(cstr);
		if(cstr.Trim().GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� ����������\" ������ ���� ���������.",
				"����� ����� ����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		//105 �������� ��� ������ � �����������
		int homeAdressLen = cstr.GetLength();

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength() + homeAdressLen > 105)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� ����������\" � ���� \"����������\" � ����� ������ ��������� �� ����� 105 ��������.",
				"�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
		cEdit->GetWindowTextW(cstr);
		if(cstr.Trim().GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" ������ ���� ���������.",
				"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
		if(cstr.GetLength()>10)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" �� ����� ����� ������ 10 ��������.",
				"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		CString seriaNum = CString(cstr);
		seriaNum.Remove(' ');

		cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
		cCmbBox->GetWindowTextW(cstr);
		if(cstr.Mid(0, 2)==_T("03"))
		{
			if (seriaNum.GetLength() != 9)
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" ������ ��������� 9 ��������.",
					"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}

			if (!CheckLatin(seriaNum.Mid(0, 2)))
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "������ ��� ������� � ���� \"����� � �����\" ������ ���� ���������� �������.",
					"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}

			if (!CheckNumber(seriaNum.Mid(2, 7)))
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" ������ ��������� 7 ���� ����� �����.",
					"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()!=6)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� ������\" ������ ��������� 6 ��������.",
				"���� ������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
		if(!CIniReader::CheckDate(cstr))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� ������\" �������. ���� ����� �������� ������.",
				"���� ������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�����, �������� ��������\" ������ ���� ���������.",
				"�����, �������� ��������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	CFormView::OnFilePrint();
}

void Prior2DPPBEL::OnPrint(CDC* pDC, CPrintInfo* pInfo){
	Prior2DPPModule p2dPrinter;

	CString cstr;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	p2dPrinter.setDate(cstr);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED, bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	p2dPrinter.setUrgent(cstr);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	p2dPrinter.setOrdinary(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCurrency(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERNAME));
	cEdit->GetWindowTextW(cstr);
	cstr.Replace(_T("\r\n"), _T(" "));
	p2dPrinter.setNameA(cstr.Trim());

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_NAME));
	cEdit->GetWindowTextW(cstr);
	CString country;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_FULL2));
	cEdit->GetWindowTextW(country);
	p2dPrinter.setBankNameA(cstr + _T(" ") + country);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_A_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_NAME));
	cEdit->GetWindowTextW(cstr);

	CString bankBCity;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CITY));
	cEdit->GetWindowTextW(bankBCity);
	p2dPrinter.setBankNameB(cstr + _T(" ") + bankBCity);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANK_B_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_RECEIVER_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PURPOSE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentPurpose(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN1));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN2));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_UNN3));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUNP_Third(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_KOD));
	cEdit->GetWindowTextW(cstr);	
	p2dPrinter.setBudgetPaymentCode(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ORDER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentOrder(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCodeBankaCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSchetBankCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_BANKCORR_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCorrespondentA(cstr);

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	bool bPL = cPL->GetCheck()==BST_CHECKED;
	cstr.Format(bPL?_T("X"):_T(" "));
	p2dPrinter.setPL(cstr);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	bool bBN = cBN->GetCheck()==BST_CHECKED;
	cstr.Format(bBN?_T("X"):_T(" "));
	p2dPrinter.setBN(cstr);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	bool bPL_BN = cPL_BN->GetCheck()==BST_CHECKED;
	cstr.Format(bPL_BN?_T("X"):_T(" "));
	p2dPrinter.setPL_BN(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMISSION_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setComissiaInvoice(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TRADE_DATE_PASS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setDataNomerPasport(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PP_DETAILS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setDetaliPlatezha(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCompanyFirstMan(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCompanySecondMan(cstr);

	if (CheckShowHideAdditionalInfo())
	{
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->GetWindowTextW(cstr);
	cstr.Remove(' ');
	p2dPrinter.setSeriaNum(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setLiveAdress(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setIssueDate(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setIssueCompany(cstr);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->GetWindowTextW(cstr);
	p2dPrinter.setDocCode(cstr.Mid(0, 2));
	}
	else
	{
		p2dPrinter.setSeriaNum(CString(""));
		p2dPrinter.setLiveAdress(CString(""));
		p2dPrinter.setIssueDate(CString(""));
		p2dPrinter.setIssueCompany(CString(""));
		p2dPrinter.setDocCode(CString(""));
	}

	int ibw = 2480;
	int ibh = 3508;

	CDC dc;
	CBitmap bmp;
	p2dPrinter.BuildBarcode(&dc, &bmp, true, ibw, ibh, bGlobalPrintBarcode);
	
	int pHeight = ::GetDeviceCaps(pDC->m_hDC, PHYSICALHEIGHT);
	int pWidth = ::GetDeviceCaps(pDC->m_hDC, PHYSICALWIDTH);
	int pOffsetX = ::GetDeviceCaps(pDC->m_hDC, PHYSICALOFFSETX);
	int pOffsetY = ::GetDeviceCaps(pDC->m_hDC, PHYSICALOFFSETY);
	/*int horzres = ::GetDeviceCaps(pDC->m_hDC, HORZRES);
	int vertres = ::GetDeviceCaps(pDC->m_hDC, VERTRES);*/
	int fakeWidthOffset = pWidth/150;
	pDC->StretchBlt(- pOffsetX - fakeWidthOffset, - pOffsetY, pWidth, pHeight, &dc, 0, 0, ibw, ibh, SRCCOPY);	
	
}

void Prior2DPPBEL::OnBnClickedCheckNourgent()
{
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void Prior2DPPBEL::OnBnClickedCheckUrgent()
{
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_NOURGENT));
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void Prior2DPPBEL::OnBnClickedCheckPl()
{
	CButton* btnPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	CButton* btnBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	CButton* btnPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	if(btnPL->GetCheck() == BST_CHECKED)
	{
		btnBN->SetCheck(BST_UNCHECKED);
		btnPL_BN->SetCheck(BST_UNCHECKED);
	}
}

void Prior2DPPBEL::OnBnClickedCheckBn()
{
	CButton* btnPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	CButton* btnBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	CButton* btnPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	if(btnBN->GetCheck() == BST_CHECKED)
	{
		btnPL->SetCheck(BST_UNCHECKED);
		btnPL_BN->SetCheck(BST_UNCHECKED);
	}
}

void Prior2DPPBEL::OnBnClickedCheckPlBn()
{
	CButton* btnPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL));
	CButton* btnBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_BN));
	CButton* btnPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PL_BN));
	if(btnPL_BN->GetCheck() == BST_CHECKED)
	{
		btnPL->SetCheck(BST_UNCHECKED);
		btnBN->SetCheck(BST_UNCHECKED);
	}
}

void Prior2DPPBEL::OnEnChangeEditReceiverAccount()
{
	
}
void Prior2DPPBEL::OnEnChangeEditSummcypher()
{
	int i = 0;
	CString cstr1,cstr2;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->GetWindowTextW(cstr1);
	cstr2 = cstr1;
	int len = cstr1.GetLength();
	char* str = new char[len+4];
	while(i < len){
		if (cstr1[i] >= '0' && cstr1[i] <= '9')
		{
			str[i] = cstr1[i];
			i++;
		}
		else if (i < len)
		{
				CString::CopyChars(cstr1.GetBuffer()+i,cstr1.GetBuffer()+i+1,len-i+1);
				len--;
		}
	}
	str[len]=0;
	__int64 ival = _atoi64(str);

	_i64toa(ival, str, 10); 
	cstr1.Format(L"%lld",ival);
	if (len > 0 && cstr2 != cstr1) cEdit->SetWindowTextW(cstr1);
	wchar_t * cs = (wchar_t*)malloc(1024);
	Utilites::SummToStringS(ival, cs);
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMM));
	cEdit->SetWindowTextW(cs);
	free(cs);
	delete []str;

	ShowHideAdditionalInfo();
}

void Prior2DPPBEL::OnEnChangeEditNumber()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_NUMBER));
	CString cstr;
	cEdit->GetWindowTextW(cstr);
	int pos;
	if((pos = cstr.FindOneOf(_T("/\\")))!=-1)
	{
		cEdit->SetSel(pos, pos+1);
		cEdit->ReplaceSel(_T(""));
	}
}

void Prior2DPPBEL::OnSaveTemplate()
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = _T("");
	tmptltName.setPassword = false;
	while(tmptltName.DoModal() == IDOK)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		if(tmptltName.cstrTemplateName.IsEmpty())
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ c ������ ������", "������ ���������� �������", MB_OK);
			if(res == IDOK) return;
		}
		if(iniReader->sectionExists(tmptltName.cstrTemplateName))
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ����� ������ ��� ����������, ������������ ���?", 
				"���������� �������", MB_YESNOCANCEL);
			if(res == IDCANCEL) return;
			if(res == IDNO) continue;
			if (!CIniReader::CheckPassword(tmptltName.cstrTemplateName))
				continue;
		}
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DPPBEL::OnEditCopy()
{
	CEdit *cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_COPY);
}

void Prior2DPPBEL::OnEditCut()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_CUT);
}

void Prior2DPPBEL::OnEditPaste()
{
	CEdit *cEditTmp,*cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_PASTE);
}
void Prior2DPPBEL::CheckNumber(int id)
{
	int i = 0;
	CString cstr,cstr2;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(id));
	cEdit->GetWindowTextW(cstr);
	cstr2 = cstr;
	int len = cstr.GetLength();
	while(i < len){
		if (cstr[i] >= '0' && cstr[i] <= '9')
		{
			i++;
		}
		else if (i < len)
		{
			CString::CopyChars(cstr.GetBuffer()+i,cstr.GetBuffer()+i+1,len-i+1);
			len--;
		}
	}
	if (cstr2 != cstr) cEdit->SetWindowTextW(cstr);
}

bool Prior2DPPBEL::CheckLatin(CString text)
{
	for (int i = 0; i < text.GetLength(); i++)
	{
		if ( !( ((text[i] >= 'A') && (text[i] <= 'Z')) ||
			((text[i] >= 'a') && (text[i] <= 'z')) ) )
			return false;
	}
	return true;
}

bool Prior2DPPBEL::CheckNumber(CString text)
{
	for (int i = 0; i < text.GetLength(); i++)
	{
		if ( !((text[i] >= '0') && (text[i] <= '9')))
			return false;
	}
	return true;
}

void Prior2DPPBEL::OnEnChangeEditUnn1()
{
	CheckNumber(IDC_EDIT_UNN1);
}

void Prior2DPPBEL::OnEnChangeEditUnn2()
{
	CheckNumber(IDC_EDIT_UNN2);
}

void Prior2DPPBEL::OnEnChangeEditUnn3()
{
	CheckNumber(IDC_EDIT_UNN3);
}

void Prior2DPPBEL::OnEnChangeEditPayeraccount()
{
	CheckNumber(IDC_EDIT_PAYERACCOUNT);
	ShowHideAdditionalInfo();
}

void Prior2DPPBEL::OnEnChangeEditBankAKod()
{
	CheckNumber(IDC_EDIT_BANK_A_KOD);
}

void Prior2DPPBEL::OnEnChangeEditBankBKod()
{
	CheckNumber(IDC_EDIT_BANK_B_KOD);
}

void Prior2DPPBEL::OnEnChangeEditBankcorrKod()
{
	CheckNumber(IDC_EDIT_BANKCORR_KOD);
}

void Prior2DPPBEL::OnEnChangeEditBankcorrAccount()
{
	CheckNumber(IDC_EDIT_BANKCORR_ACCOUNT);
}

int Prior2DPPBEL::GetBaseAmount()
{
	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	WCHAR *sDir = new WCHAR[sDirLen];
	::GetCurrentDirectory(sDirLen, sDir);
	CIniReader *cIR = new CIniReader(CString(sDir)+L"\\language.ini");
	
		if (cIR->sectionExists(L"BaseAmount"))
		{
			return _ttoi(cIR->getKeyValue(L"BaseAmount", L"BaseAmount"));
		}
		else 
		{
			return 0;
		}
}

bool Prior2DPPBEL::CheckShowHideAdditionalInfo()
{
	int baseAmount = GetBaseAmount();

	int i = 0;
	CString cstr;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SUMMCYPHER));
	cEdit->GetWindowTextW(cstr);	
	__int64 ival = _ttoi64(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PAYERACCOUNT));
	cEdit->GetWindowTextW(cstr);

	if ((ival >= baseAmount*100) && (cstr.Left(4) == _T("3013")))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Prior2DPPBEL::ShowHideAdditionalInfo()
{
	int nCmdShow;
	if (CheckShowHideAdditionalInfo())
		nCmdShow = SW_SHOW;
	else
		nCmdShow = SW_HIDE;

	CEdit* cEdit;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_ADDITIONAL_INFO));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_LIVEADRESS));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_DOCCODE));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_SERIANUM));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_ISSUEDATE));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_ISSUECOMPANY));
	cEdit->ShowWindow(nCmdShow);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->ShowWindow(nCmdShow);
}
