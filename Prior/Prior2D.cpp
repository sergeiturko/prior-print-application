#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment (linker, "/ENTRY:WinMainCRTStartup")
#pragma comment (linker, "/SUBSYSTEM:WINDOWS")

// CPrior2DApp
extern CString csLang[32];

CMainFrame*			pMainFrame = NULL;
bool				bGlobalPrintBarcode = true;

BEGIN_MESSAGE_MAP(CPrior2DApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CPrior2DApp::OnAppAbout)
	ON_COMMAND(ID_ABOUT, &CPrior2DApp::OnAbout)
	ON_COMMAND(ID_FILE_NEW, &CPrior2DApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_LOAD_TEMPLATE, &CPrior2DApp::OnLoadTemplate)
	ON_COMMAND(ID_MENU_TOGGLEBARCODEGENERATION, &CPrior2DApp::OnToggleBarcode)
//	ON_COMMAND(ID_SAVE_TEMPLATE, &CPrior2DApp::OnSaveTemplate)
	ON_COMMAND(ID_TEMPLATES, &CPrior2DApp::OnTemplates)
END_MESSAGE_MAP()

class CMyDocManager : public CDocManager
{
    public:
    virtual void OnFileNew();
};
void CMyDocManager::OnFileNew(){
    if (m_templateList.IsEmpty()){
        TRACE0("Error: no document templates registered with CWinApp.\n");
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }
    // ���� ����� ���� ������������������ ������ ���������,
    // �� �� �������� ������ ������, � ������� �������� ����� ����. 
    CDocTemplate* pTemplate = (CDocTemplate*)m_templateList.GetHead();
    if (m_templateList.GetCount() > 1){
        // more than one document template to choose from
        // bring up dialog prompting user
        CNTypeDlg dlg(&m_templateList);
        int nID = dlg.DoModal();
        if (nID == IDOK){
            // ����� ��������� �� ��������� ������ ���������.
            pTemplate = dlg.m_pSelectedTemplate;
		    CDocument* pDoc = pTemplate->OpenDocumentFile(NULL);
        }
	    // ������� ����� �������� �� ����� �������.
    }
}

CPrior2DApp::CPrior2DApp()
{
	WCHAR *sDir = new WCHAR[100];
	SHGetSpecialFolderPath(0, sDir, CSIDL_APPDATA, false);

	CreateDirectory(sDir + CString("\\ITSoft"), NULL);
	CreateDirectory(sDir + CString("\\ITSoft\\PriorVznos"), NULL);
	CString iniFileName = CString(sDir)+CString("\\ITSoft\\PriorVznos\\config.ini");
	m_iniReader.setINIFileName(iniFileName);
	HANDLE hFile = CreateFile(iniFileName,
         GENERIC_WRITE, FILE_SHARE_READ,
         NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	//CFile file(hFile);
	//file.Close();
}

CPrior2DApp theApp;

// CPrior2DApp initialization

CIniReader* CPrior2DApp::GetINIHandler()
{
	return &m_iniReader;
}
void CPrior2DApp::AddDocTemplate(CDocTemplate* pTemplate){
    if (m_pDocManager == NULL)
        m_pDocManager = new CMyDocManager;
    m_pDocManager->AddDocTemplate(pTemplate);
}
BOOL CPrior2DApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	pDocTemplatePPS = new CMultiDocTemplate(IDR_Prior2D_PPSBLR,
		RUNTIME_CLASS(CPrior2DDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(Prior2DPPSBEL));
	if (!pDocTemplatePPS) return FALSE;
	AddDocTemplate(pDocTemplatePPS);

	pDocTemplatePP = new CMultiDocTemplate(IDR_Prior2D_PPBLR,
		RUNTIME_CLASS(CPrior2DDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(Prior2DPPBEL));
	if (!pDocTemplatePP) return FALSE;
	AddDocTemplate(pDocTemplatePP);

	pDocTemplatePPF = new CMultiDocTemplate(IDR_Prior2D_PPFULL,
		RUNTIME_CLASS(CPrior2DDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(Prior2DPPFull));
	if (!pDocTemplatePPF) return FALSE;
	AddDocTemplate(pDocTemplatePPF);

	//21.09.2011
	pDocTemplateFEECASH = new CMultiDocTemplate(IDR_PRIOR2D_FEECASH,
		RUNTIME_CLASS(CPrior2DDoc),
		RUNTIME_CLASS(CChildFrame),
		RUNTIME_CLASS(Prior2DFeeCash));
	if (!pDocTemplateFEECASH) return FALSE;
	AddDocTemplate(pDocTemplateFEECASH);
	
	pDocTemplateRECEIVEBELCASH = new CMultiDocTemplate(IDD_PRIOR2D_RECEIVEBELCASH,
		RUNTIME_CLASS(CPrior2DDoc),
		RUNTIME_CLASS(CChildFrame),
		RUNTIME_CLASS(Prior2DReceiveBelCash));
	if (!pDocTemplateRECEIVEBELCASH) return FALSE;
	AddDocTemplate(pDocTemplateRECEIVEBELCASH);

	// create main MDI Frame window
	pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;
	// call DragAcceptFiles only if there's a suffix
	//  In an MDI app, this should occur immediately after setting m_pMainWnd


	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	// if (!ProcessShellCommand(cmdInfo)) return FALSE;
	if (cmdInfo.m_strFileName.GetLength() > 2)
		m_iniReader.setINIFileName(CString(cmdInfo.m_strFileName));
	// The main window has been initialized, so show and update it
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	pMainFrame->m_wndToolBar.SetButtonStyle(11, TBBS_CHECKBOX);

	CMenu* menu = pMainFrame->GetMenu();
	UINT state = menu->EnableMenuItem(ID_EDIT_COPY,MF_ENABLED);
	state = menu->EnableMenuItem(ID_EDIT_PASTE,MF_ENABLED);
	pMainFrame->SetMenu(menu);
	return TRUE;
}
// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	CAboutDlg();

	afx_msg void OnEnChangeEditAbout();
	afx_msg void OnEnUpdateEditAbout();
	CString csAboutText;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, csAboutText(_T(""))
{
	csAboutText = csLang[0]+L", ���������� ������ ��������� ����������.";
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ABOUT, csAboutText);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CPrior2DApp::OnAppAbout()
{
	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	WCHAR *sDir = new WCHAR[MAX_PATH];
	::GetCurrentDirectory(MAX_PATH, sDir);
	wcscat(sDir, L"\\help.chm");
	ShellExecute(NULL,
		NULL,
		sDir,
		NULL,
		NULL,
		SW_SHOW);
	delete sDir;
}


void CPrior2DApp::OnAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}
void CPrior2DApp::OnFilePrint(CDC* pDC, CPrintInfo* pInfo)
{
	CRect cr(100, 100, 200, 150);
	int res = pDC->DrawText(_T("Test"), -1, &cr, DT_NOCLIP);
	res = res;
}

// CPrior2DApp message handlers
void CPrior2DApp::OnToggleBarcode()
{
	UINT uiStyle;
	UINT nID;
	int iImage;
	try {
		pMainFrame->m_wndToolBar.GetButtonInfo(11, nID, uiStyle, iImage);
	} catch(...) {
	}
	if(uiStyle & 0x00010000) {
		bGlobalPrintBarcode = false;
		pMainFrame->m_wndToolBar.LoadBitmapW(IDR_MAINFRAMEDIS);
	} else {
		bGlobalPrintBarcode = true;
		pMainFrame->m_wndToolBar.LoadBitmapW(IDR_MAINFRAMEENA);
	}
}
void CPrior2DApp::OnLoadTemplate()
{
	CTemplates tmplts(true);
	tmplts.DoModal();
	if(tmplts.Action == CTemplates::TMPLT_ACTION_OPEN)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		CString cstr = iniReader->getKeyValue(_T("TYPE"), tmplts.cstrTemplateName);
		if(cstr == _T("PPBEL"))
		{
			CDocument* doc = pDocTemplatePP->OpenDocumentFile(NULL);
			POSITION pos = doc->GetFirstViewPosition();
			Prior2DPPBEL* pView = (Prior2DPPBEL*)doc->GetNextView(pos);
			pView->LoadConfigContent(tmplts.cstrTemplateName);
		}
		if(cstr == _T("PPSBEL"))
		{
			CDocument* doc = pDocTemplatePPS->OpenDocumentFile(NULL);
			POSITION pos = doc->GetFirstViewPosition();
			Prior2DPPBEL* pView = (Prior2DPPBEL*)doc->GetNextView(pos);
			pView->LoadConfigContent(tmplts.cstrTemplateName);
		}
		if(cstr == _T("PPFULL"))
		{
			CDocument* doc = pDocTemplatePPF->OpenDocumentFile(NULL);
			POSITION pos = doc->GetFirstViewPosition();
			Prior2DPPBEL* pView = (Prior2DPPBEL*)doc->GetNextView(pos);
			pView->LoadConfigContent(tmplts.cstrTemplateName);
		}
		if(cstr == _T("FEECASH"))
		{
			CDocument* doc = pDocTemplateFEECASH->OpenDocumentFile(NULL);
			doc->SetTitle(doc->GetTitle() + _T(" (") + tmplts.cstrTemplateName + _T(")"));
			POSITION pos = doc->GetFirstViewPosition();
			Prior2DFeeCash* pView = (Prior2DFeeCash*)doc->GetNextView(pos);			
			pView->LoadConfigContent(tmplts.cstrTemplateName);			
		}
		if(cstr == _T("CHECKREG"))
		{
			AfxMessageBox(_T("��������� ������ ����� �� ��������������"), MB_ICONERROR | MB_OK);
		}
		if(cstr == _T("RECEIVEBELCASH"))
		{
			CDocument* doc = pDocTemplateRECEIVEBELCASH->OpenDocumentFile(NULL);
			doc->SetTitle(doc->GetTitle() + _T(" (") + tmplts.cstrTemplateName + _T(")"));
			POSITION pos = doc->GetFirstViewPosition();
			Prior2DReceiveBelCash* pView = (Prior2DReceiveBelCash*)doc->GetNextView(pos);
			pView->LoadConfigContent(tmplts.cstrTemplateName);			
		}
	}
}

void CPrior2DApp::OnTemplates()
{
	CTemplates tmpltsDlg(false);
	tmpltsDlg.DoModal();
}


