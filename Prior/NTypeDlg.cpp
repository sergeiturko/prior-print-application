#include "stdafx.h"

// CNTypeDlg dialog
IMPLEMENT_DYNAMIC(CNTypeDlg, CDialog)

CNTypeDlg::CNTypeDlg(CWnd* pParent /*=NULL*/)
: CDialog(CNTypeDlg::IDD, pParent){
	m_pSelectedTemplate = NULL;
}
CNTypeDlg::CNTypeDlg(CPtrList *p)
: CDialog(CNTypeDlg::IDD, NULL){
	m_pSelectedTemplate = NULL;
	if (p->GetCount() > 0){
		m_CDPL = p;
		m_pSelectedTemplate = reinterpret_cast<CDocTemplate*>(p->GetHead());
	}
}
BOOL CNTypeDlg::OnInitDialog(){
	CListBox* cListBox = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LISTTMPL));
	cListBox->InsertString(0,L"����. �����., BYR(974), ����� 36");
	cListBox->InsertString(1,L"����. �����., BYR(974), ����� 31");
	cListBox->InsertString(2,L"����. �����., ������, ����� 31");
	
	cListBox->InsertString(3,L"���������� �� ����� ���������");
		
	cListBox->InsertString(4,L"��������� �� ��������� �������� ����������� ������");
	cListBox->SetCurSel(0);
	return TRUE;
}
CNTypeDlg::~CNTypeDlg()
{
}

void CNTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNTypeDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CNTypeDlg::OnBnClickedOk)
	ON_LBN_DBLCLK(IDC_LISTTMPL, &CNTypeDlg::OnLbnDblclkListtmpl)
END_MESSAGE_MAP()


// CNTypeDlg message handlers

void CNTypeDlg::OnBnClickedOk()
{
	CListBox* cListBox = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LISTTMPL));
	int iIndex = cListBox->GetCurSel();
	if(iIndex != -1)
	{
		POSITION pos = m_CDPL->FindIndex(iIndex);
		if (pos != NULL){
			m_pSelectedTemplate = reinterpret_cast<CDocTemplate*>(m_CDPL->GetAt(pos));
		}
	}
	OnOK();
}

void CNTypeDlg::OnLbnDblclkListtmpl()
{
	CListBox* cListBox = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LISTTMPL));
	int iIndex = cListBox->GetCurSel();
	if(iIndex != -1)
	{
		POSITION pos = m_CDPL->FindIndex(iIndex);
		if (pos != NULL){
			m_pSelectedTemplate = reinterpret_cast<CDocTemplate*>(m_CDPL->GetAt(pos));
		}
	}
	OnOK();
}
