#pragma once


// CTemplates dialog

class CTemplates : public CDialog
{
	DECLARE_DYNAMIC(CTemplates)

public:
	CTemplates(bool bOpen, CWnd* pParent = NULL);   // standard constructor
	virtual ~CTemplates();
	afx_msg void OnBnClickedTmplDel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnLbnDblclkListTemplates();

// Dialog Data
	enum { IDD = IDD_TEMPLATES };

	enum TmpltAction{ TMPLT_ACTION_OPEN = 1, TMPLT_ACTION_NONE = 0 };
	TmpltAction Action;
	CString cstrTemplateName;
	afx_msg void OnBnClickedTmplOpen();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog(); // called first time after construct
	bool m_bOpen;
	bool CheckPassword(CString cstrTemplateName);
	DECLARE_MESSAGE_MAP()
};
