//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Prior2D.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_PRIOR2D_PLBEL               101
#define IDD_PRIOR2DPLBEL                101
#define IDD_PRIOR2DPPSBEL               104
#define IDD_PRIOR2DPPFULL               105
#define IDD_TEMPLATES                   106
#define IDD_TEMPLATENAME                107
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAMEENA                128
#define IDR_Prior2DTYPE                 129
#define IDR_Prior2D_PPBLR               129
#define IDR_Prior2D_PTBLR               130
#define IDD_DTEMPLATES                  130
#define IDR_Prior2D_PPSBLR              131
#define IDR_Prior2DTYPE2                131
#define IDR_Prior2D_PPFULL              132
#define IDR_Prior2DTYPE1                132
#define ID_MENU_TOGGLEBARCODEGENERATION 133
#define IDR_MAINFRAMEDIS                134
#define IDR_PRIOR2D_FEECASH             135
#define IDD_PRIOR2D_CHECKREG            137
#define IDD_PASSWORD                    138
#define IDD_PRIOR2D_RECEIVEBELCASH      140
#define IDC_EDIT_SUMMCYPHER             1000
#define IDC_RADIO_PT_1                  1000
#define IDC_STATIC_SUMMCYPHER           1001
#define IDC_RADIO_PT_2                  1001
#define IDC_STATIC_SUMM                 1002
#define IDC_RADIO_PT_3                  1002
#define IDC_EDIT_SUMM                   1003
#define IDC_EDIT_NUMBER                 1004
#define IDC_STATIC_SUMMCYPHER2          1005
#define IDC_STATIC_SUMMCYPHER3          1006
#define IDC_DATETIMEPICKER_DATE         1007
#define IDC_EDIT_CURRENCY               1008
#define IDC_CHECK_URGENT                1009
#define IDC_EDIT_PT_SUMMDIGITS          1009
#define IDC_CHECK_NOURGENT              1010
#define IDC_EDIT_PT_CURRENCY            1010
#define IDC_EDIT_PAYERACCOUNT           1011
#define IDC_EDIT_PT_SUMMSPELLED         1011
#define IDC_STATIC_SUMMCYPHER4          1012
#define IDC_EDIT_PAYERNAME              1013
#define IDC_EDIT_RECEIVER_ACCOUNT       1014
#define IDC_EDIT_PT_ACCOUNTA            1014
#define IDC_STATIC_SUMMCYPHER5          1015
#define IDC_EDIT_PT_NUMBER              1015
#define IDC_EDIT_RECEIVER_NAME          1016
#define IDC_DATETIMEPICKER_PT_DATE      1016
#define IDC_EDIT_BANK_A_NAME            1017
#define IDC_EDIT_PT_NAMEA               1017
#define IDC_EDIT_BANK_A_KOD             1018
#define IDC_EDIT_BANKA                  1018
#define IDC_EDIT_BANK_A_CBU             1019
#define IDC_EDIT_BANK_A_KOD2            1019
#define IDC_EDIT_BANKA_CODE             1019
#define IDC_STATIC_SUMM2                1020
#define IDC_STATIC_SUMM3                1021
#define IDC_STATIC_SUMMCYPHER6          1022
#define IDC_STATIC_SUMMCYPHER7          1023
#define IDC_EDIT_BANK_A_CBU2            1023
#define IDC_EDIT_PTB_BANKA_CBU2         1023
#define IDC_STATIC_SUMMCYPHER8          1024
#define IDC_EDIT_BANK_B_NAME            1025
#define IDC_EDIT_BANK_B_KOD             1026
#define IDC_EDIT_BANK_B_CBU             1027
#define IDC_STATIC_SUMM4                1028
#define IDC_STATIC_SUMM5                1029
#define IDC_EDIT_PURPOSE                1030
#define IDC_EDIT_UNN1                   1031
#define IDC_EDIT_UNN2                   1032
#define IDC_EDIT_UNN3                   1033
#define IDC_EDIT_BANKCORR_NAME          1034
#define IDC_EDIT_PPS_NUMBER             1034
#define IDC_EDIT_BANKCORR_KOD           1035
#define IDC_DATETIMEPICKER_PPS_DATE     1035
#define IDC_EDIT_KOD                    1036
#define IDC_EDIT_ORDER                  1037
#define IDC_EDIT_BANKCORR_ACCOUNT       1038
#define IDC_CHECK_PPS_URGENT            1038
#define IDC_CHECK_PPS_NONURGENT         1039
#define IDC_EDIT_COUNTRY_FULL2          1039
#define IDC_CHECK_PL                    1040
#define IDC_EDIT_PPS_SUMMDIGITS         1040
#define IDC_CHECK_BN                    1041
#define IDC_EDIT_PPS_SUMMSPELLED        1041
#define IDC_CHECK_PL_BN                 1042
#define IDC_EDIT_PPS_NAMEA              1042
#define IDC_EDIT_COMISSION_ACCOUNT      1043
#define IDC_EDIT_PPS_ACCOUNTA           1043
#define IDC_STATIC_SUMMCYPHER9          1044
#define IDC_EDIT_PPS_BANKA              1044
#define IDC_EDIT_                       1045
#define IDC_EDIT_TRADE_DATE_PASS        1045
#define IDC_EDIT_PPS_BANKCODEA          1045
#define IDC_EDIT_PPS_BANKA_CODE         1045
#define IDC_EDIT_NAME                   1045
#define IDC_EDIT_SUMMCYPHER18           1046
#define IDC_EDIT_PP_DETAILS             1046
#define IDC_EDIT_PPS_BANKA_CBU          1046
#define IDC_EDIT_PPS_BANKB              1047
#define IDC_EDIT_PPS_BANKB_CODE         1048
#define IDC_EDIT_PPS_BANKB_CBU          1049
#define IDC_EDIT_PPS_NAMEB              1050
#define IDC_EDIT_PPS_ACCOUNTB           1051
#define IDC_EDIT_PPS_PAYMENTPURPOSE     1052
#define IDC_EDIT_PPS_UNNA               1053
#define IDC_EDIT_PPS_UNNB               1054
#define IDC_EDIT_PPS_UNN3               1055
#define IDC_EDIT_PPS_CODE               1056
#define IDC_EDIT_PPS_ORDER              1057
#define IDC_EDIT_PPS_CURRENCY           1058
#define IDC_EDIT_PPF_NUMBER             1059
#define IDC_EDIT_COUNTRY                1059
#define IDC_DATETIMEPICKER_PPF_DATE     1060
#define IDC_CHECK_PPF_URGENT            1061
#define IDC_CHECK_PPF_NONURGENT         1062
#define IDC_EDIT_PPF_SUMMDIGITS         1063
#define IDC_EDIT_PPF_CURRENCY           1064
#define IDC_EDIT_PPF_SUMMSPELLED        1065
#define IDC_EDIT_PPF_ACCOUNTA           1066
#define IDC_EDIT_PPF_NAMEA              1067
#define IDC_EDIT_PPF_BANKA              1068
#define IDC_EDIT_PPF_BANKA_CODE         1069
#define IDC_EDIT_PPF_BANKA_CBU          1070
#define IDC_EDIT_PPF_BANKB              1071
#define IDC_EDIT_PPF_BANKB_CODE         1072
#define IDC_EDIT_PPF_BANKB_CBU          1073
#define IDC_EDIT_PPF_NAMEB              1074
#define IDC_EDIT_PPF_ACCOUNTB           1075
#define IDC_EDIT_PPF_PAYMENTPURPOSE     1076
#define IDC_EDIT_PPF_UNNA               1077
#define IDC_EDIT_PPF_UNNB               1078
#define IDC_EDIT_PPF_UNN3               1079
#define IDC_EDIT_PPF_CODE               1080
#define IDC_EDIT_PPF_ORDER              1081
#define IDC_EDIT_PPF_BANKCORR_NAME      1082
#define IDC_EDIT_PPF_BANKCORR_KOD       1083
#define IDC_EDIT_PPF_BANKCORR_ACCOUNT   1084
#define IDC_CHECK_PPF_PL                1085
#define IDC_CHECK_PPF_BN                1086
#define IDC_CHECK_PPF_PL_BN             1087
#define IDC_EDIT_PPF_ACCOUNTC           1088
#define IDC_EDIT_PPF_PASSPORT           1089
#define IDC_EDIT_PPF_DETAILS            1090
#define IDC_LIST_TEMPLATES              1091
#define IDC_EDIT_COUNTRY2               1091
#define IDC_EDIT_COUNTRY_VALL           1091
#define IDC_EDIT1                       1092
#define IDC_EDIT_TEMPLATE_NAME          1092
#define IDC_EDIT_ABOUT                  1092
#define IDC_EDIT_REG_FIO                1092
#define IDC_EDIT_COMPANYFIRSTMAN        1092
#define IDC_EDIT_PASSWORD               1092
#define IDC_EDIT_UNP                    1092
#define IDC_EDIT_FEE_UNP                1092
#define IDC_EDIT_APPLICATION_NUMBER     1092
#define IDC_STATIC_CONTRY               1093
#define IDC_EDIT_REG_FIO1               1093
#define IDC_STATIC_COUNTRY_FULL         1094
#define IDC_EDIT_REG_FIO2               1094
#define IDC_STATIC_CONTRY2              1094
#define IDC_EDIT_REG_FIO3               1095
#define IDC_LIST2                       1097
#define IDC_LISTTMPL                    1097
#define IDC_PROGRESS1                   1098
#define IDC_EDIT_FEE_NUMBER             1099
#define IDC_DATETIMEPICKER_FEE_DATE     1100
#define IDC_EDIT_FEE_SUMMDIGITS         1101
#define IDC_EDIT_FEE_SUMMSPELLED        1102
#define IDC_EDIT_FEE_NAMEA              1103
#define IDC_STATIC_CODE                 1103
#define IDC_STATIC_CODEDESC             1103
#define IDC_EDIT_REG_BANKNAMEA          1103
#define IDC_EDIT_FEE_BANKB              1104
#define IDC_EDIT_REG_ACCOUNTA           1104
#define IDC_EDIT_FEE_BANKB_CODE         1105
#define IDC_EDIT_REG_BANKNAMEB          1105
#define IDC_EDIT_REG_ACCOUNTB           1106
#define IDC_EDIT_FEE_PAYMENTPURPOSE     1107
#define IDC_EDIT_REG_NUMBER             1107
#define IDC_DATETIMEPICKER_REG_DATE     1108
#define IDC_CBOX_FEE_REPORTCODE         1109
#define IDC_EDIT_REG_NAMEB              1109
#define IDC_EDIT_FEE_NAMEB              1110
#define IDC_CBOX_FEE_REPORTCODE2        1110
#define IDC_CBOX_REG_CODE               1110
#define IDC_EDIT_FEE_ACCOUNTB           1111
#define IDC_EDIT_FEE_NAMEB2             1111
#define IDC_EDIT_REG_NAMEA              1111
#define IDC_EDIT_REG_SERIAPAS           1111
#define IDC_EDIT_REG_BIKA               1112
#define IDC_EDIT_REG_BIKB               1113
#define IDC_EDIT_REG_PASSNUM            1114
#define IDC_EDIT_REG_PASSISSUE          1115
#define IDC_EDIT_REG_CHECKNUM           1116
#define IDC_EDIT_REG_SERIAPAS1          1117
#define IDC_EDIT_REG_CHECKSUM           1118
#define IDC_EDIT_REG_PASSNUM1           1119
#define IDC_BUTTON_REG_ADD1             1120
#define IDC_EDIT_REG_PASSISSUE1         1121
#define IDC_EDIT_REG_CHECKNUM1          1122
#define IDC_EDIT_REG_ACCOUNTA1          1123
#define IDC_EDIT_REG_CHECKSUM1          1124
#define IDC_STATIC_REG_NAMEA1           1125
#define IDC_STATIC_REG_PAS1             1126
#define IDC_STATIC_REG_FIO1             1127
#define IDC_STATIC_REG_SERIAPAS1        1128
#define IDC_STATIC_REG_PASNUM1          1129
#define IDC_STATIC_REG_PASISSUE1        1130
#define IDC_STATIC_REG_CHECKNUM1        1131
#define IDC_STATIC_REG_ACCOUNTA1        1132
#define IDC_STATIC_REG_CHECKSUM1        1133
#define IDC_BUTTON_REG_ADD2             1134
#define IDC_STATIC_REG_FIO2             1135
#define IDC_STATIC_REG_PAS2             1136
#define IDC_STATIC_REG_SERIAPAS2        1137
#define IDC_EDIT_REG_SERIAPAS2          1138
#define IDC_STATIC_REG_PASNUM2          1139
#define IDC_EDIT_REG_PASSNUM2           1140
#define IDC_STATIC_REG_PASISSUE2        1141
#define IDC_EDIT_REG_PASSISSUE2         1142
#define IDC_STATIC_REG_CHECKNUM2        1143
#define IDC_EDIT_REG_CHECKNUM2          1144
#define IDC_STATIC_REG_ACCOUNTA2        1145
#define IDC_EDIT_REG_ACCOUNTA2          1146
#define IDC_STATIC_REG_CHECKSUM2        1147
#define IDC_EDIT_REG_CHECKSUM2          1148
#define IDC_STATIC_REG_NAMEA2           1149
#define IDC_BUTTON_REG_ADD3             1150
#define IDC_STATIC_REG_FIO3             1151
#define IDC_STATIC_REG_PAS3             1152
#define IDC_STATIC_REG_SERIAPAS3        1153
#define IDC_EDIT_REG_SERIAPAS3          1154
#define IDC_STATIC_REG_PASNUM3          1155
#define IDC_EDIT_REG_PASSNUM3           1156
#define IDC_STATIC_REG_PASISSUE3        1157
#define IDC_EDIT_REG_PASSISSUE3         1158
#define IDC_STATIC_REG_CHECKNUM3        1159
#define IDC_EDIT_REG_CHECKNUM3          1160
#define IDC_STATIC_REG_ACCOUNTA3        1161
#define IDC_EDIT_REG_ACCOUNTA3          1162
#define IDC_STATIC_REG_CHECKSUM3        1163
#define IDC_STATIC_REG_NAMEA3           1164
#define IDC_COMBO1                      1165
#define IDC_COMBO_DOCCODE               1165
#define IDC_COMBO_PURPOSE1              1165
#define IDC_EDIT_REG_CHECKSUM3          1166
#define IDC_STATIC_COMPANYFIRSTMAN      1166
#define IDC_COMBO_PURPOSE2              1166
#define IDC_EDIT_COMPANYSECONDMAN       1167
#define IDC_COMBO_PURPOSE3              1167
#define IDC_STATIC_COMPANYHEAD          1168
#define IDC_COMBO_PURPOSE4              1168
#define IDC_STATIC_ADDITIONAL_INFO      1169
#define IDC_EDIT_LIVEADRESS             1170
#define IDC_EDIT_SERIANUM               1171
#define IDC_EDIT_ISSUEDATE              1172
#define IDC_EDIT6                       1173
#define IDC_EDIT_ISSUECOMPANY           1173
#define IDC_EDIT_DOC_SERIA              1173
#define IDC_STATIC_LIVEADRESS           1174
#define IDC_STATIC_DOCCODE              1175
#define IDC_STATIC_SERIANUM             1176
#define IDC_STATIC_ISSUEDATE            1177
#define IDC_STATIC_ISSUECOMPANY         1178
#define IDC_EDIT_TEMPLATE_PASS          1179
#define IDC_BUTTON1                     1180
#define IDC_BUTTON_SET_PASSWORD         1180
#define IDC_BUTTON_ADD_PURPOSE2         1180
#define IDC_EDIT_BANK_B_CITY            1181
#define IDC_BUTTON_ADD_PURPOSE3         1181
#define IDC_EDIT_PPS_BANK_B_CITY        1182
#define IDC_BUTTON_ADD_PURPOSE4         1182
#define IDC_EDIT2                       1183
#define IDC_EDIT_ADDITIONAL_INFO        1183
#define IDC_EDIT_FEE_ADDITIONAL_INFO    1183
#define IDC_EDIT_PURPOSE1_SUM           1183
#define IDC_STATIC_APPLICATION_NUMBER   1184
#define IDC_STATIC_APPLICATION_DATE     1185
#define IDC_DATETIMEPICKER_APPLICATION_DATE 1186
#define IDC_STATIC_NAME                 1187
#define IDC_EDIT_ACCOUNT                1188
#define IDC_STATIC_ACCOUNT              1189
#define IDC_STATIC_REPRESENTATIVE       1190
#define IDC_EDIT_REPRESENTATIVE_NAME    1191
#define IDC_STATIC_REPRESENTATIVE_NAME  1192
#define IDC_EDIT_DOC_TYPE               1193
#define IDC_STATIC_DOC_TYPE             1194
#define IDC_STATIC_DOC_SERIA            1195
#define IDC_EDIT_DOC_NUMBER             1196
#define IDC_STATIC_DOC_NUMBER           1197
#define IDC_DATETIMEPICKER_DOC_ISSUE_DATE 1198
#define IDC_EDIT_DOC_ISSUE_COMPANY      1199
#define IDC_STATIC_DOC_ISSUE_DATE       1200
#define IDC_STATIC_DOC_ISSUE_COMPANY    1201
#define IDC_EDIT_ANNEX                  1202
#define IDC_EDIT_ANNEX_PAGECOUNT        1203
#define IDC_STATIC_ANNEX                1204
#define IDC_STATIC_ANNEX_PAGECOUNT      1205
#define IDC_EDIT_SUM                    1206
#define IDC_EDIT12                      1207
#define IDC_EDIT_SUM_SPELLED            1207
#define IDC_EDIT_PAYMENT_PRIORITY       1208
#define IDC_STATIC_PAYMENT_PRIORITY     1209
#define IDC_STATIC_SUM                  1210
#define IDC_STATIC_SUM_SPELLED          1211
#define IDC_COMBO_PURPOSE1_CODE         1212
#define IDC_COMBO_PURPOSE1_TYPE         1213
#define IDC_COMBO_PURPOSE1_MONTH        1214
#define IDC_EDIT_PURPOSE1_YEAR          1215
#define IDC_STATIC_MONTH_TEXT1          1216
#define IDC_STATIC_YEAR_TEXT1           1217
#define IDC_COMBO_PURPOSE2_CODE         1218
#define IDC_EDIT_PURPOSE2_SUM           1219
#define IDC_COMBO_PURPOSE2_TYPE         1220
#define IDC_STATIC_PURPOSE2_MONTH       1221
#define IDC_COMBO_PURPOSE2_MONTH        1222
#define IDC_STATIC_PURPOSE2_YEAR        1223
#define IDC_EDIT_PURPOSE2_YEAR          1224
#define IDC_STATIC_PURPOSE2_FRAME       1225
#define IDC_STATIC_PURPOSE2             1226
#define IDC_STATIC_PURPOSE2_CODE        1227
#define IDC_STATIC_PURPOSE2_SUM         1228
#define IDC_STATIC_PURPOSE3_CODE        1229
#define IDC_COMBO_PURPOSE3_CODE         1230
#define IDC_STATIC_PURPOSE3_SUM         1231
#define IDC_EDIT_PURPOSE3_SUM           1232
#define IDC_COMBO_PURPOSE3_TYPE         1233
#define IDC_STATIC_PURPOSE3_MONTH       1234
#define IDC_COMBO_PURPOSE3_MONTH        1235
#define IDC_STATIC_PURPOSE3_YEAR        1236
#define IDC_EDIT_PURPOSE3_YEAR          1237
#define IDC_STATIC_PURPOSE4_CODE        1238
#define IDC_STATIC_PURPOSE3_FRAME       1239
#define IDC_STATIC_PURPOSE3             1240
#define IDC_STATIC_PURPOSE4_FRAME       1241
#define IDC_STATIC_PURPOSE4             1242
#define IDC_COMBO_PURPOSE4_CODE         1243
#define IDC_STATIC_PURPOSE4_SUM         1244
#define IDC_EDIT_PURPOSE4_SUM           1245
#define IDC_COMBO_PURPOSE4_TYPE         1246
#define IDC_STATIC_PURPOSE4_MONTH       1247
#define IDC_COMBO_PURPOSE4_MONTH        1248
#define IDC_STATIC_PURPOSE4_YEAR        1249
#define IDC_EDIT_PURPOSE3_YEAR2         1250
#define IDC_EDIT_PURPOSE4_YEAR          1250
#define IDC_EDIT_PURPOSE1_CLARIFICATION 1251
#define IDC_STATIC_PURPOSE1_CLARIFICATION 1252
#define IDC_EDIT_PURPOSE2_CLARIFICATION 1253
#define IDC_STATIC_PURPOSE2_CLARIFICATION 1254
#define IDC_EDIT_PURPOSE3_CLARIFICATION 1255
#define IDC_STATIC_PURPOSE3_CLARIFICATION 1256
#define IDC_EDIT_PURPOSE4_CLARIFICATION 1257
#define IDC_STATIC_PURPOSE1_CLARIFICATION4 1258
#define IDC_STATIC_PURPOSE4_CLARIFICATION 1258
#define IDD_DLGPROGRESS                 30722
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_LOAD_TEMPLATE                32777
#define ID_SAVE_TEMPLATE                32778
#define ID_TEMPLATES                    32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_32782                        32782
#define ID_32783                        32783
#define ID_UNDO                         32784
#define ID_CUT                          32785
#define ID_32786                        32786
#define ID_32787                        32787
#define ID_ABOUT                        32788
#define ID_TMPL_DEL                     33002
#define ID_TMPL_DEL2                    33003
#define ID_TMPL_OPEN                    33003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         1253
#define _APS_NEXT_SYMED_VALUE           108
#endif
#endif
