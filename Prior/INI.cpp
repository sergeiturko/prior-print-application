#include "stdafx.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
// Used to retrieve a value give the section and key
CString CIniReader::getKeyValue(CString strKey,CString strSection)
{
	WCHAR ac_Result[400];

	// Get the info from the .ini file	
	m_lRetValue = GetPrivateProfileString((LPCTSTR)strSection,(LPCTSTR)strKey,
		(LPCTSTR)_T(""),ac_Result, 400, (LPCTSTR)m_strFileName);	

	CString strResult(ac_Result);
	if (strKey == _T("TYPE") || !NeedEncryption)
		return strResult;
	else
		return Decode(strResult);
}

// Used to add or set a key value pair to a section
long CIniReader::setKey(CString strValue, CString strKey, CString strSection)
{

	if (strKey == _T("TYPE") || !NeedEncryption)
		m_lRetValue = WritePrivateProfileString (strSection, strKey, 
                             strValue, m_strFileName);
	else
		m_lRetValue = WritePrivateProfileString (strSection, strKey, 
                             Encode(strValue), m_strFileName);
	
	return m_lRetValue;
}

// Used to find out if a given section exists
BOOL CIniReader::sectionExists(CString strSection)
{
	WCHAR ac_Result[100];	
	CString csAux;
	// Get the info from the .ini file	
	m_lRetValue = GetPrivateProfileString((LPCTSTR)strSection,NULL,
		_T(""),ac_Result, 90, (LPCTSTR)m_strFileName);
	// Return if we could retrieve any info from that section
	return (m_lRetValue > 0);
}

// Used to retrieve all of the  section names in the ini file
CStringList* CIniReader::getSectionNames()  //returns collection of section names
{
	WCHAR *ac_Result = new WCHAR[32768];
	m_sectionList->RemoveAll();
	
	m_lRetValue = GetPrivateProfileSectionNames(ac_Result,32768,(LPCTSTR)m_strFileName);
	
	CString strSectionName;
	for(int i=0; i<m_lRetValue; i++)
	{
		if(ac_Result[i] != '\0') {
			strSectionName = strSectionName + ac_Result[i];
		} else {
			if(strSectionName != "") {
				m_sectionList->InsertAfter(m_sectionList->GetTailPosition(),strSectionName);
			}
			strSectionName = "";
		}
	}
	delete ac_Result;
	return m_sectionList;
}

// Used to retrieve all key/value pairs of a given section.  
CStringList* CIniReader::getSectionData(CString strSection)  
{
	WCHAR ac_Result[2000];  //change size depending on needs
	m_sectionDataList->RemoveAll();
	m_lRetValue = GetPrivateProfileSection((LPCTSTR)strSection, ac_Result, 2000, (LPCTSTR)m_strFileName);

	CString strSectionData;
	for(int i=0; i<m_lRetValue; i++)
	{
		if(ac_Result[i] != '\0') {
			strSectionData = strSectionData + ac_Result[i];
		} else {
			if(strSectionData != "") {
				m_sectionDataList->InsertAfter(m_sectionDataList->GetTailPosition(),strSectionData);
			}
			strSectionData = "";
		}
	}

	return m_sectionDataList;
}

void CIniReader::setINIFileName(CString strINIFile)
{
	m_strFileName = strINIFile;
}

CString CIniReader::Encode(CString value)
{
	for (int i=0; i<value.GetLength(); i++)
	{
		if (32 <= value.GetAt(i) && value.GetAt(i) <= 127)
			value.SetAt(i, 127 - value.GetAt(i) + 32);
		else if (1040 <= value.GetAt(i) && value.GetAt(i) <= 1103)
			value.SetAt(i, 1103 - value.GetAt(i) + 1040);
	}
	/*for (int i=0; i<value.GetLength(); i++)
	{
		int m = value.GetAt(i);
	}*/
	return value;
}

CString CIniReader::Decode(CString value)
{	
	for (int i=0; i<value.GetLength(); i++)
	{
		if (32 <= value.GetAt(i) && value.GetAt(i) <= 127)
			value.SetAt(i, 127 - value.GetAt(i) + 32);
		else if (1040 <= value.GetAt(i) && value.GetAt(i) <= 1103)
			value.SetAt(i, 1103 - value.GetAt(i) + 1040);
	}
	return value;
}

BOOL CIniReader::CheckPassword(CString cstrTemplateName)
{
	CIniReader* iniReader =  theApp.GetINIHandler();
	CStringList* strl = iniReader->getSectionNames();

	int iCount = strl->GetCount();
	POSITION pos = strl->GetHeadPosition();

	CString cstrPass;

	for(int i = 0; i < iCount; i++)
	{
		CString cstrTmplName = strl->GetNext(pos);
		if (cstrTmplName != cstrTemplateName)
			continue;
		cstrPass = iniReader->getKeyValue(_T("PASSWORD"),cstrTemplateName);
		if (cstrPass == _T(""))
			return true; //������ ���, �������� ������ �� ���������
	}

	Password passWnd(cstrTemplateName, cstrPass);	
	if (passWnd.DoModal() == IDOK)
		return true;
	return false;
}

BOOL CIniReader::CheckPasswordExists(CString cstrTemplateName)
{
	CIniReader* iniReader =  theApp.GetINIHandler();
	CStringList* strl = iniReader->getSectionNames();

	int iCount = strl->GetCount();
	POSITION pos = strl->GetHeadPosition();

	CString cstrPass;

	for(int i = 0; i < iCount; i++)
	{
		CString cstrTmplName = strl->GetNext(pos);
		if (cstrTmplName != cstrTemplateName)
			continue;

		if (iniReader->getKeyValue(_T("PASSWORD"),cstrTemplateName) == _T(""))
			return false;
	}
	return true;
}

BOOL CIniReader::CheckDate(CString value)
{
	int day = _ttoi(value.Left(2));
	int month = _ttoi(value.Mid(2, 2));
	int year = _ttoi(value.Right(2));

	/*if ( (year < 1900) || (year > 2020) )
		return false;*/

	if ((month < 1) || (month > 12))
		return false;

	if ((day > 31) || (day < 1))
		return false;

	if ( (day==31) && (month==2 || month==4 || month==6 || month==9 || month==11) )
		return false;
	if ( (day==30) && (month==2) )
		 return false;
	if ( (month==2) && (day==29) && (year%4!=0) )
		 return false;
	if ( (month==2) && (day==29) && (year==0) )
		 return true;
	/*if ( (month==2) && (day==29) && (year%400==0) )
		 return true;*/
	/*if ( (month==2) && (day==29) && (year%100==0) )
		 return false;*/
	
	return true;
}


