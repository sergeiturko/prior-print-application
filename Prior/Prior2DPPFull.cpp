//
// ������
//
#include "stdafx.h"
#include "Prior2DPPFull.h"

// Prior2DPPFull
extern CString csLang[32];
extern bool bGlobalPrintBarcode;

IMPLEMENT_DYNCREATE(Prior2DPPFull, CFormView)

Prior2DPPFull::Prior2DPPFull()
	: CFormView(Prior2DPPFull::IDD)
{

}

Prior2DPPFull::~Prior2DPPFull()
{
}

void Prior2DPPFull::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Prior2DPPFull, CFormView)
	ON_COMMAND(ID_FILE_PRINT, &Prior2DPPFull::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DPPFull::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_CHECK_PPF_URGENT, &Prior2DPPFull::OnBnClickedCheckPpfUrgent)
	ON_BN_CLICKED(IDC_CHECK_PPF_NONURGENT, &Prior2DPPFull::OnBnClickedCheckPpfNonurgent)
	ON_BN_CLICKED(IDC_CHECK_PPF_PL, &Prior2DPPFull::OnBnClickedCheckPpfPl)
	ON_BN_CLICKED(IDC_CHECK_PPF_BN, &Prior2DPPFull::OnBnClickedCheckPpfBn)
	ON_BN_CLICKED(IDC_CHECK_PPF_PL_BN, &Prior2DPPFull::OnBnClickedCheckPpfPlBn)
	ON_EN_CHANGE(IDC_EDIT_PPF_NUMBER, &Prior2DPPFull::OnEnChangeEditPpfNumber)
	ON_COMMAND(ID_SAVE_TEMPLATE, &Prior2DPPFull::OnSaveTemplate)
	ON_COMMAND(ID_EDIT_COPY, &Prior2DPPFull::OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, &Prior2DPPFull::OnEditCut)
	ON_COMMAND(ID_EDIT_PASTE, &Prior2DPPFull::OnEditPaste)
	ON_EN_CHANGE(IDC_EDIT_PPF_SUMMDIGITS, &Prior2DPPFull::OnEnChangeEditPpfSummdigits)
END_MESSAGE_MAP()


// Prior2DPPFull diagnostics

#ifdef _DEBUG
void Prior2DPPFull::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void Prior2DPPFull::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void Prior2DPPFull::OnInitialUpdate()
{
	CString cstmp;	
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA));
	cstmp = L"\""+csLang[0]+L"\" "+csLang[1]+L", ���������� ��������";
	cEdit->SetWindowTextW(cstmp);
	cEdit->EnableWindow(TRUE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CODE));
	cstmp = csLang[2];
	cEdit->SetWindowTextW(cstmp);
	cEdit->SetLimitText(9);
	cEdit->EnableWindow(TRUE);

	CButton* cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_NONURGENT));
	cButton->SetCheck(BST_CHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CODE));
	cEdit->EnableWindow(TRUE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_VALL));
	cstmp = csLang[3];
	cEdit->SetWindowTextW(cstmp);
	cEdit->SetLimitText(30);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NUMBER));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CURRENCY));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMDIGITS));
	cEdit->SetLimitText(28);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMSPELLED));
	cEdit->SetLimitText(220);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEA));
	cEdit->SetLimitText(150);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTA));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA));
	cEdit->SetLimitText(105);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CODE));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CBU));
	cEdit->SetLimitText(3);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB));
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CODE));
	cEdit->SetLimitText(11);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CBU));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEB));
	cEdit->SetLimitText(150);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTB));
	cEdit->SetLimitText(32);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PAYMENTPURPOSE));
	cEdit->SetLimitText(290);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNA));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNN3));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CODE));
	cEdit->SetLimitText(5);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ORDER));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_NAME));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_ACCOUNT));
	cEdit->SetLimitText(20);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_KOD));
	cEdit->SetLimitText(11);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTC));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->SetLimitText(32);
}

BOOL Prior2DPPFull::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;
	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));

	return DoPreparePrinting(pInfo);
}

void Prior2DPPFull::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DPPFull::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	delete m_FLarge;
}

void Prior2DPPFull::SaveTemplate(CString strSection)
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = strSection;
	tmptltName.setPassword = true;

	while(tmptltName.DoModal() == IDOK)
	{
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DPPFull::OnFilePrint()
{
	CString cstr;

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NUMBER));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ���������\" ������ ���� ���������.",
			"����� ���������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim(_T(" ")).GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �������\" ������ ���� ���������.",
			"����� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	if(!CheckSummDigits(cstr.Trim(_T(" "))))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �������\" ����� �������� ������.",
			"����� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim(_T(" ")).GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� ������\" ������ ���� ���������.",
			"��� ������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ������ ���� ���������.",
			"����� ��������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	else
	{
		_tsetlocale(LC_ALL, _T("Russian_Russia.1251"));
		CString cstrTmp(_T(" "));
		cstr.Trim();
		cstrTmp.SetAt(0,cstr.GetAt(0));
		cstrTmp.MakeUpper();
		cstr.SetAt(0,cstrTmp.GetAt(0));
		cEdit->SetWindowTextW(cstr);
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_VALL));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �����\" ������ ���� ���������.",
			"���������� �����: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CODE));
	CString acode;
	cEdit->GetWindowTextW(acode);
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� ������ ��������� 13 ����.",
			"���� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	else
	{
		if(!Utilites::CheckAccount(cstr, acode, _T("749")) &&
		   !Utilites::CheckAccount(cstr, acode, _T("999")))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}
	bool bUNNBlankAcceptable = cstr.GetLength()==13 && cstr[0]=='3'&&cstr[1]=='0'&&cstr[2]=='2'&&
		(cstr[3]>='1'&&cstr[3]<='4');

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	bool bHasErrors = cstr.GetLength()>0 && cstr.GetLength()!=3;
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-����������� ������ ��������� 3 �����.",
			"��� �����-�����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����-����������\" ������ ���� ���������.",
			"����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CODE));
	int ilen = 0;
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	ilen = cstr.GetLength();

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CODE));
	CString bcode;
	cEdit->GetWindowTextW(bcode);
	if(bcode.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����\" ��� �����-���������� ������ ���� ���������.",
			"��� �����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	bHasErrors = cstr.GetLength()>0 && cstr.GetLength()!=3;
	if(!bHasErrors)
		for(int i = 0; i < cstr.GetLength(); i++) bHasErrors |=  cstr[i]<'0' || cstr[i]>'9';
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-���������� ������ ��������� 3 ����� ��� ���� �����������.",
			"��� �����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	} 

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �������\" ������ ���� ���������.",
			"���������� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNA));
	cEdit->GetWindowTextW(cstr);
	if((!bUNNBlankAcceptable || cstr != "") && !Utilites::CheckUNN(cstr))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����������\" ������� - ������ ������� �� ���������� ��������.",
			"��� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim(_T(" ")).GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"������� � �������� ������� ���� �����������\" ������ ���� ���������.",
			"������� � �������� ������� ���� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	CFormView::OnFilePrint();
}

void Prior2DPPFull::SaveConfigContent(CString strSection, CString password)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (password != _T(""))
	{
		iniReader->NeedEncryption = true;
	}
	
	iniReader->setKey(password,_T("PASSWORD"),strSection);		
	iniReader->setKey(_T("PPFULL"),_T("TYPE"),strSection);

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NUMBER"), strSection);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_PPF_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	iniReader->setKey(cstr, _T("DATE"), strSection);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_NONURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED,
		 bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("URGENT"), strSection);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("ORDINARY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMSPELLED"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("CURRENCY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMDIGITS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCODEA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCBUA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCODEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCBUB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PAYMENTPURPOUSE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNNA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNNB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNN3));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNN3"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PAYMENTCODE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ORDER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ORDER"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_KOD));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCODEC"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTC"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_NAME));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEC"), strSection);

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL));
	bool bPL = cPL->GetCheck()==BST_CHECKED;
	cstr.Format(bPL?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("PL"), strSection);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_BN));
	bool bBN = cBN->GetCheck()==BST_CHECKED;
	cstr.Format(bBN?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("BN"), strSection);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL_BN));
	bool bPL_BN = cPL_BN->GetCheck()==BST_CHECKED;
	cstr.Format(bPL_BN?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("PL_BN"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTC));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMISSION"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PASSPORT));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("DATEPASSPORT"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_DETAILS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("DETAILS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_VALL));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COUNTRY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMPANYFIRSTMAN"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMPANYSECONDMAN"), strSection);

	iniReader->NeedEncryption = false;
}
void Prior2DPPFull::LoadConfigContent(CString strSection)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (iniReader->getKeyValue(_T("PASSWORD"), strSection) != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NUMBER"), strSection));

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_NONURGENT));
	if(iniReader->getKeyValue(_T("URGENT"), strSection)==_T("X")) cBtnUrgent->SetCheck(BST_CHECKED);
	else cBtnUrgent->SetCheck(BST_UNCHECKED);

	if(iniReader->getKeyValue(_T("ORDINARY"), strSection)==_T("X")) cBtnNOUrgent->SetCheck(BST_CHECKED);
	else cBtnNOUrgent->SetCheck(BST_UNCHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMSPELLED));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMSPELLED"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CURRENCY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CURRENCY"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMDIGITS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMDIGITS"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CBU));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCBUA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CBU));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCBUB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PAYMENTPURPOSE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTPURPOUSE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNNA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNNB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNN3));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNN3"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTCODE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ORDER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ORDER"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_KOD));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEC"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_ACCOUNT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTC"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_NAME));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEC"), strSection));

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL));
	if(iniReader->getKeyValue(_T("PL"), strSection) == _T("X")) cPL->SetCheck(BST_CHECKED);
	else cPL->SetCheck(BST_UNCHECKED);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_BN));
	if(iniReader->getKeyValue(_T("BN"), strSection) == _T("X")) cBN->SetCheck(BST_CHECKED);
	else cBN->SetCheck(BST_UNCHECKED);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL_BN));
	if(iniReader->getKeyValue(_T("PL_BN"), strSection) == _T("X")) cPL_BN->SetCheck(BST_CHECKED);
	else cPL_BN->SetCheck(BST_UNCHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTC));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMISSION"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PASSPORT));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DATEPASSPORT"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_DETAILS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("DETAILS"), strSection));
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_VALL));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COUNTRY"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMPANYFIRSTMAN"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMPANYSECONDMAN"), strSection));

	iniReader->NeedEncryption = false;

	if (!CIniReader::CheckPasswordExists(strSection))
	{
		int res = ::MessageBoxA(this->m_hWnd, "������ ��� ������� ������� �� ����������. ����������?", 
				"�������� ������ �������", MB_YESNO);	
		if (res == IDYES)
		{
			SaveTemplate(strSection);
		}
	}
}
void Prior2DPPFull::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	Prior2DPPModule p2dPrinter;

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NUMBER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_PPF_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	p2dPrinter.setDate(cstr);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_NONURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED,
		 bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	p2dPrinter.setUrgent(cstr);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	p2dPrinter.setOrdinary(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCurrency(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountA(cstr);

	/////////////////
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA));
	cEdit->GetWindowTextW(cstr);
	
	CString country;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY_VALL));
	cEdit->GetWindowTextW(country);

	p2dPrinter.setBankNameA(cstr + _T(" ") + country);
	////////////////////

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NAMEB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentPurpose(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNNB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_UNN3));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUNP_Third(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBudgetPaymentCode(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ORDER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentOrder(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_KOD));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCodeBankaCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSchetBankCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_BANKCORR_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCorrespondentA(cstr);

	CButton* cPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL));
	bool bPL = cPL->GetCheck()==BST_CHECKED;
	cstr.Format(bPL?_T("X"):_T(" "));
	p2dPrinter.setPL(cstr);

	CButton* cBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_BN));
	bool bBN = cBN->GetCheck()==BST_CHECKED;
	cstr.Format(bBN?_T("X"):_T(" "));
	p2dPrinter.setBN(cstr);

	CButton* cPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL_BN));
	bool bPL_BN = cPL_BN->GetCheck()==BST_CHECKED;
	cstr.Format(bPL_BN?_T("X"):_T(" "));
	p2dPrinter.setPL_BN(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_ACCOUNTC));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setComissiaInvoice(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_PASSPORT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setDataNomerPasport(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_DETAILS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setDetaliPlatezha(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCompanyFirstMan(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCompanySecondMan(cstr);

	p2dPrinter.setSeriaNum(CString(""));
	p2dPrinter.setLiveAdress(CString(""));
	p2dPrinter.setIssueDate(CString(""));
	p2dPrinter.setIssueCompany(CString(""));
	p2dPrinter.setDocCode(CString(""));
	

	int ibw = 2480;
	// int ibh = 3508;
	int ibh = 3558;

	CDC dc;
	CBitmap bmp;
	p2dPrinter.BuildBarcode(&dc, &bmp, true, ibw, ibh, bGlobalPrintBarcode);
	
	int pHeight = ::GetDeviceCaps(pDC->m_hDC, PHYSICALHEIGHT);
	int pWidth = ::GetDeviceCaps(pDC->m_hDC, PHYSICALWIDTH);
	int pOffsetX = ::GetDeviceCaps(pDC->m_hDC, PHYSICALOFFSETX);
	int pOffsetY = ::GetDeviceCaps(pDC->m_hDC, PHYSICALOFFSETY);
	/*int horzres = ::GetDeviceCaps(pDC->m_hDC, HORZRES);
	int vertres = ::GetDeviceCaps(pDC->m_hDC, VERTRES);*/
	int fakeWidthOffset = pWidth/150;
	pDC->StretchBlt(- pOffsetX - fakeWidthOffset, - pOffsetY, pWidth, pHeight, &dc, 0, 0, ibw, ibh, SRCCOPY);
}

void Prior2DPPFull::OnBnClickedCheckPpfUrgent()
{
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_NONURGENT));
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void Prior2DPPFull::OnBnClickedCheckPpfNonurgent()
{
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_NONURGENT));
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void Prior2DPPFull::OnBnClickedCheckPpfPl()
{
	CButton* btnPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL));
	CButton* btnBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_BN));
	CButton* btnPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL_BN));
	if(btnPL->GetCheck() == BST_CHECKED)
	{
		btnBN->SetCheck(BST_UNCHECKED);
		btnPL_BN->SetCheck(BST_UNCHECKED);
	}
}

void Prior2DPPFull::OnBnClickedCheckPpfBn()
{
	CButton* btnPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL));
	CButton* btnBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_BN));
	CButton* btnPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL_BN));
	if(btnBN->GetCheck() == BST_CHECKED)
	{
		btnPL->SetCheck(BST_UNCHECKED);
		btnPL_BN->SetCheck(BST_UNCHECKED);
	}
}

void Prior2DPPFull::OnBnClickedCheckPpfPlBn()
{
	CButton* btnPL = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL));
	CButton* btnBN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_BN));
	CButton* btnPL_BN = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPF_PL_BN));
	if(btnPL_BN->GetCheck() == BST_CHECKED)
	{
		btnPL->SetCheck(BST_UNCHECKED);
		btnBN->SetCheck(BST_UNCHECKED);
	}
}

void Prior2DPPFull::OnEnChangeEditPpfNumber()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPF_NUMBER));
	CString cstr;
	cEdit->GetWindowTextW(cstr);
	int pos;
	if((pos = cstr.FindOneOf(_T("/\\")))!=-1)
	{
		cEdit->SetSel(pos, pos+1);
		cEdit->ReplaceSel(_T(""));
	}
}

void Prior2DPPFull::OnSaveTemplate()
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = _T("");
	tmptltName.setPassword = false;
	while(tmptltName.DoModal() == IDOK)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		if(tmptltName.cstrTemplateName.IsEmpty())
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ������ ������", "������ ���������� �������", MB_OK);
			if(res == IDOK) return;
		}
		if(iniReader->sectionExists(tmptltName.cstrTemplateName))
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ����� ������ ��� ����������, ������������ ���?", 
				"���������� �������", MB_YESNOCANCEL);
			if(res == IDCANCEL) return;
			if(res == IDNO) continue;
			if (!CIniReader::CheckPassword(tmptltName.cstrTemplateName))
				continue;
		}
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DPPFull::OnEditCopy()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_COPY);
}

void Prior2DPPFull::OnEditCut()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_CUT);
}

void Prior2DPPFull::OnEditPaste()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_PASTE);
}

void Prior2DPPFull::OnEnChangeEditPpfSummdigits()
{
}

bool Prior2DPPFull::CheckSummDigits(CString sumDigits)
{
	int i = 0;
	bool checkPoint = false;
	int pointPos;
	while (i < sumDigits.GetLength())
	{		
		if ((sumDigits[i] >= '0') && (sumDigits[i] <= '9'))
		{
			i++;
			continue;
		}
		else if ((sumDigits[i] == ',') || (sumDigits[i] == '.'))
		{
			if (checkPoint)
				return false;
			else
			{
				checkPoint = true;
				pointPos = i;
				i++;
				continue;
			}
		}
		else return false;
	}

	if (checkPoint)
	{
		if (!(pointPos == sumDigits.GetLength() - 3))
			return false;
	}

	return true;
}
