//
// 0401600036
//
#include "stdafx.h"
#include "Prior2DPPSBEL.h"

// Prior2DPPSBEL
extern CString csLang[32];
extern bool bGlobalPrintBarcode;

IMPLEMENT_DYNCREATE(Prior2DPPSBEL, CFormView)

Prior2DPPSBEL::Prior2DPPSBEL()
	: CFormView(Prior2DPPSBEL::IDD)
	, UNPPayer(_T(""))
{
}

Prior2DPPSBEL::~Prior2DPPSBEL()
{
}

void Prior2DPPSBEL::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PPS_UNNA, UNPPayer);
}

void Prior2DPPSBEL::OnInitialUpdate()
{
	CString cstmp;
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	this->SetWindowTextW(L"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CURRENCY));
	cEdit->SetWindowTextW(_T("974"));
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA));
	cstmp = L"\""+csLang[0]+L"\" "+csLang[1]+L", ���������� ��������";
	cEdit->SetWindowTextW(cstmp);
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CODE));
	cstmp = csLang[2];
	cEdit->SetWindowTextW(cstmp);
	cEdit->SetLimitText(9);
	cEdit->EnableWindow(FALSE);

	CButton* cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_NONURGENT));
	cButton->SetCheck(BST_CHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
	cEdit->SetLimitText(5);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY));
	cstmp = csLang[3];
	cEdit->SetWindowTextW(cstmp);
	cEdit->SetLimitText(30);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NUMBER));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CURRENCY));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->SetLimitText(12);	

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMSPELLED));
	cEdit->SetLimitText(220);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEA));
	cEdit->SetLimitText(150);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTA));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA));
	cEdit->SetLimitText(105);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CODE));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CBU));
	cEdit->SetLimitText(3);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB));	
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CODE));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CBU));
	cEdit->SetLimitText(3);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANK_B_CITY));	
	cEdit->SetLimitText(30);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEB));	
	cEdit->SetLimitText(150);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
	cEdit->SetLimitText(13);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_PAYMENTPURPOSE));	
	cEdit->SetLimitText(290);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNA));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNB));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNN3));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ORDER));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->SetLimitText(32);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->SetLimitText(105);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->SetLimitText(6);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->SetLimitText(62);

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmbBox->AddString(_T("01 - ������� ���������� ���� ������� 1974 �."));
	cCmbBox->AddString(_T("02 - ������� ���������� �� ������� 1993 �."));
	cCmbBox->AddString(_T("03 - ������� ���������� �� ������� 1996 �."));
	cCmbBox->AddString(_T("04 - ������� ����� � 9"));
	cCmbBox->AddString(_T("05 - ������������� � �������� (��� ���, �� ��������� 16-������� ��������)"));
	cCmbBox->AddString(_T("06 - ��� �� ���������� � ��, �������� ������������� �������� ��"));
	cCmbBox->AddString(_T("07 - ������������� �������, �������� � ������������� ������� �� ���������� ��"));
	cCmbBox->AddString(_T("08 - ������������� � ����������� ����������� � �������������� ������� ������� ��� �������������� ������ � ��"));
	cCmbBox->AddString(_T("09 - �������������� ������������ ������� ���������� ������������ ����������� ��� ��������, ��� ����������"));
	cCmbBox->AddString(_T("10 - ��������, �������������� �������� ���� ��� �����������, �������� ������������� �������� ����������� ����������� ����������"));
	cCmbBox->AddString(_T("11 - ������� ����� ��������������� ������� ������"));
	cCmbBox->AddString(_T("12 - ������������ �������������"));
	cCmbBox->AddString(_T("13 - ������� ������ (������������� �������� ������)"));
	cCmbBox->AddString(_T("14 - ������������� � �������������� �������������� ������ � ��"));	
	cCmbBox->AddString(_T("99 - ���� ��������"));
	cCmbBox->SetDroppedWidth(730);

	ShowHideAdditionalInfo();
}

BEGIN_MESSAGE_MAP(Prior2DPPSBEL, CFormView)
	ON_COMMAND(ID_FILE_PRINT, &Prior2DPPSBEL::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DPPSBEL::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_EN_CHANGE(IDC_EDIT_PPS_SUMMDIGITS, &Prior2DPPSBEL::OnEnChangeEditPpsSummdigits)
	ON_EN_CHANGE(IDC_EDIT_PPS_ACCOUNTB, &Prior2DPPSBEL::OnEnChangeEditPpsAccountb)
	ON_EN_CHANGE(IDC_EDIT_PPS_ACCOUNTA, &Prior2DPPSBEL::OnEnChangeEditPpsAccounta)
	ON_BN_CLICKED(IDC_CHECK_PPS_URGENT, &Prior2DPPSBEL::OnBnClickedCheckPpsUrgent)
	ON_BN_CLICKED(IDC_CHECK_PPS_NONURGENT, &Prior2DPPSBEL::OnBnClickedCheckPpsNonurgent)
	ON_EN_CHANGE(IDC_EDIT_PPS_NUMBER, &Prior2DPPSBEL::OnEnChangeEditPpsNumber)
	ON_COMMAND(ID_SAVE_TEMPLATE, &Prior2DPPSBEL::OnSaveTemplate)
	ON_COMMAND(ID_EDIT_COPY, &Prior2DPPSBEL::OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, &Prior2DPPSBEL::OnEditCut)
	ON_COMMAND(ID_EDIT_PASTE, &Prior2DPPSBEL::OnEditPaste)
	ON_EN_CHANGE(IDC_EDIT_PPS_UNNA, &Prior2DPPSBEL::OnEnChangeEditPpsUnna)
	ON_EN_CHANGE(IDC_EDIT_PPS_UNNB, &Prior2DPPSBEL::OnEnChangeEditPpsUnnb)
	ON_EN_CHANGE(IDC_EDIT_PPS_UNN3, &Prior2DPPSBEL::OnEnChangeEditPpsUnn3)
	ON_CBN_SELCHANGE(IDC_COMBO_DOCCODE, &Prior2DPPSBEL::OnCbnSelchangeComboDoccode)
END_MESSAGE_MAP()


// Prior2DPPSBEL diagnostics

#ifdef _DEBUG
void Prior2DPPSBEL::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void Prior2DPPSBEL::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


BOOL Prior2DPPSBEL::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;
	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));

	return DoPreparePrinting(pInfo);
}

void Prior2DPPSBEL::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DPPSBEL::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	delete m_FLarge;
}

void Prior2DPPSBEL::SaveTemplate(CString strSection)
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = strSection;
	tmptltName.setPassword = true;

	while(tmptltName.DoModal() == IDOK)
	{
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DPPSBEL::SaveConfigContent(CString strSection, CString password)
{
	CIniReader* iniReader = theApp.GetINIHandler();	

	if (password != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	iniReader->setKey(password,_T("PASSWORD"),strSection);			
	iniReader->setKey(_T("PPSBEL"),_T("TYPE"),strSection);

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NUMBER"), strSection);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_PPS_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	iniReader->setKey(cstr, _T("DATE"), strSection);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_NONURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED,
		 bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("URGENT"), strSection);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	iniReader->setKey(cstr, _T("ORDINARY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMSPELLED"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("CURRENCY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SUMMDIGITS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCODEA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCBUA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCODEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKCBUB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANK_B_CITY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKBCITY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PAYMENTPURPOUSE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNNA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNNB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNN3));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("UNN3"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PAYMENTCODE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ORDER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ORDER"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COUNTRY"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMPANYFIRSTMAN"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("COMPANYSECONDMAN"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SERIANUM"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("LIVEADRESS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ISSUEDATE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ISSUECOMPANY"), strSection);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->GetWindowTextW(cstr);
	iniReader->setKey(cstr.Mid(0, 2), _T("DOCCODE"), strSection);

	iniReader->NeedEncryption = false;
}

void Prior2DPPSBEL::LoadConfigContent(CString strSection)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (iniReader->getKeyValue(_T("PASSWORD"), strSection) != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NUMBER"), strSection));

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_NONURGENT));

	if(iniReader->getKeyValue(_T("URGENT"), strSection)==_T("X")) cBtnUrgent->SetCheck(BST_CHECKED);
	else cBtnUrgent->SetCheck(BST_UNCHECKED);

	if(iniReader->getKeyValue(_T("ORDINARY"), strSection)==_T("X")) cBtnNOUrgent->SetCheck(BST_CHECKED);
	else cBtnNOUrgent->SetCheck(BST_UNCHECKED);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMSPELLED));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMSPELLED"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CURRENCY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CURRENCY"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SUMMDIGITS"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CBU));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCBUA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCODEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CBU));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKCBUB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANK_B_CITY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKBCITY"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_PAYMENTPURPOSE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTPURPOUSE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNNA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNNB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNN3));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("UNN3"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PAYMENTCODE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ORDER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ORDER"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COUNTRY"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMPANYFIRSTMAN"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("COMPANYSECONDMAN"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SERIANUM"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("LIVEADRESS"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ISSUEDATE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ISSUECOMPANY"), strSection));

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->SelectString(0, iniReader->getKeyValue(_T("DOCCODE"), strSection));

	ShowHideAdditionalInfo();

	iniReader->NeedEncryption = false;

	if (!CIniReader::CheckPasswordExists(strSection))
	{
		int res = ::MessageBoxA(this->m_hWnd, "������ ��� ������� ������� �� ����������. ����������?", 
				"�������� ������ �������", MB_YESNO);	
		if (res == IDYES)
		{
			SaveTemplate(strSection);
		}
	}
}

void Prior2DPPSBEL::OnFilePrint()
{
	CString cstr;

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NUMBER));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ���������\" ������ ���� ���������.",
			"����� ���������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �����\" ������ ���� ���������.",
			"���������� �����: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �������\" ������ ���� ���������.",
			"����� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength() != 0)
	{
		_tsetlocale(LC_ALL, _T("Russian_Russia.1251"));
		CString cstrTmp(_T(" "));
		cstr.Trim();
		cstrTmp.SetAt(0,cstr.GetAt(0));
		cstrTmp.MakeUpper();
		cstr.SetAt(0,cstrTmp.GetAt(0));
		cEdit->SetWindowTextW(cstr);
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CODE));
	CString acode;
	cEdit->GetWindowTextW(acode);
	if(acode.GetLength() == 9 || acode.GetLength() == 3) {
	} else {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����\" ��� �����-����������� ������ ��������� 3 ��� 9 ����.",
			"��� �����-�����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� ������ ��������� 13 ����.",
			"���� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	else
	{
		if(!Utilites::CheckAccount(cstr, acode, _T("749")) &&
		   !Utilites::CheckAccount(cstr, acode, _T("999")))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}
	bool bUNNBlankAcceptable = cstr[0]=='3'&&cstr[1]=='0'&&cstr[2]=='2'&&
		(cstr[3]>='1'&&cstr[3]<='4');

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	bool bHasErrors = cstr.GetLength()>0 && cstr.GetLength()!=3;
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-����������� ������ ��������� 3 �����.",
			"��� �����-�����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����-����������\" ������ ���� ���������.",
			"����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CODE));
	CString bcode;
	cEdit->GetWindowTextW(bcode);
	if(bcode.GetLength() == 9 || bcode.GetLength() == 3) {
	} else {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����\" ��� �����-���������� ������ ��������� 3 ��� 9 ����.",
			"��� �����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	bHasErrors = cstr.GetLength()>0 && cstr.GetLength()!=3;
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-���������� ������ ��������� 3 ����� ��� ���� �����������.",
			"��� �����-����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANK_B_CITY));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim().GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �����\" �����-���������� ������ ���� ���������.",
			"���������� �����: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����������\" ������ ���� ���������.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	CString csPayerAcc = cstr;
	if(cstr.GetLength() != 13) {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� ������ ��������� 13 ����.",
			"����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	} else {
		if(!Utilites::CheckAccount(cstr, bcode, NULL))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� �����\" ����������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	//����� ����� ����������� ���������� � 36
	bool bUNNBlankNotAcceptable = cstr[0]=='3'&&cstr[1]=='6';

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���������� �������\" ������ ���� ���������.",
			"���������� �������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNA));
	cEdit->GetWindowTextW(cstr);
	if((!bUNNBlankAcceptable || cstr != "") && !Utilites::CheckUNN(cstr)) {
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����������\" ������� - ������ ������� �� ���������� ��������.",
			"��� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNB));
	cEdit->GetWindowTextW(cstr);
	if((bUNNBlankNotAcceptable||cstr != "") && !Utilites::CheckUNN(cstr))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �����������\" ������� - ������ ������� �� ���������� ��������.",
			"��� ����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
	
	int ilen = 0;
	cEdit->GetWindowTextW(cstr);
	cstr.Trim(L"\0x20\0x09");
	cEdit->SetWindowTextW(cstr);
	ilen = cstr.GetLength();
	
	if (csPayerAcc[0] == '3' && csPayerAcc[1] == '6')
	{
		if (!((csPayerAcc[2] == '0') && (csPayerAcc[3] == '5')))
		{
			if (ilen == 5) bHasErrors = false;
			else bHasErrors = true;
			if(!bHasErrors)
				for(int i = 0; i < cstr.GetLength(); i++) bHasErrors |=  cstr[i]<'0' || cstr[i]>'9';
			if(bHasErrors)
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� �������\" ������ ��������� 5 ����.",
					"��� �������: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}
		}
	}	

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ORDER));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=2 || cstr[0]<'0' || cstr[0]>'9' ||
		cstr[1]<'0' || cstr[1]>'9')
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�������\" ������ ��������� 2 �����.", "�������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	CString cdate;
	CDateTimeCtrl *cDT = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_PPS_DATE));
	SYSTEMTIME st;
	cDT->GetTime(&st);
	if(st.wYear >= 2010 && cstr != "01" && cstr != "02" && cstr != "03" && cstr != "04" && cstr != "05" && 
		cstr != "11" && cstr != "12" && cstr != "13" && 
		cstr != "21" && cstr != "22" && 
		cstr != "50" &&  cstr != "51")
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�������\" ������ ��������� ���� �� ����� ���� (01,02,03,04,05,11,12,13,21,22,50,51).", "�������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	if(cstr.Trim(_T(" ")).GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"������� � �������� ������� ���� �����������\" ������ ���� ���������.",
			"������� � �������� ������� ���� �����������: �������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	if (CheckShowHideAdditionalInfo())
	{
		CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
		cCmbBox->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� ���������\" ������ ���� ���������.",
				"��� ���������:�������� ������",MB_OK|MB_ICONWARNING);
			cCmbBox->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
		cEdit->GetWindowTextW(cstr);
		if(cstr.Trim().GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� ����������\" ������ ���� ���������.",
				"����� ����� ����������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		//75 �������� ��� ������ � �����������
		int homeAdressLen = cstr.GetLength();

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEA));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength() + homeAdressLen > 105)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� ����������\" � ���� \"����������\" � ����� ������ ��������� �� ����� 105 ��������.",
				"�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
		cEdit->GetWindowTextW(cstr);
		if(cstr.Trim().GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" ������ ���� ���������.",
				"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
		if(cstr.GetLength()>10)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" �� ����� ����� ������ 10 ��������.",
				"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		CString seriaNum = CString(cstr);
		seriaNum.Remove(' ');

		cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
		cCmbBox->GetWindowTextW(cstr);
		if(cstr.Mid(0, 2)==_T("03"))
		{
			if (seriaNum.GetLength() != 9)
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" ������ ��������� 9 ��������.",
					"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}

			if (!CheckLatin(seriaNum.Mid(0, 2)))
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "������ ��� ������� � ���� \"����� � �����\" ������ ���� ���������� �������.",
					"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}

			if (!CheckNumber(seriaNum.Mid(2, 7)))
			{
				MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � �����\" ������ ��������� 7 ���� ����� �����.",
					"����� � �����: �������� ������",MB_OK|MB_ICONWARNING);
				cEdit->SetFocus();
				return;
			}
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()!=6)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� ������\" ������ ��������� 6 ��������.",
				"���� ������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		if(!CIniReader::CheckDate(cstr))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� ������\" �������. ���� ����� �������� ������.",
				"���� ������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�����, �������� ��������\" ������ ���� ���������.",
				"�����, �������� ��������: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	/*cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	CEdit* cEditK = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
	cEditK->EnableWindow(TRUE);

	if(cstr.GetLength()==13 && (cstr[0]=='3' && cstr[1]=='6' && cstr[2]=='0' && (cstr[3]=='2'||cstr[3]=='0'))){
		cEditK->EnableWindow(TRUE);
	}
	else
	{
		cEditK->SetWindowTextW(L"");
		cEditK->EnableWindow(FALSE);
	}*/

	CFormView::OnFilePrint();
}

void Prior2DPPSBEL::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	Prior2DPPModule p2dPrinter;

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NUMBER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_PPS_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	p2dPrinter.setDate(cstr);

	CButton* cBtnUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_URGENT));
	CButton* cBtnNOUrgent = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_NONURGENT));
	bool bUrgent = cBtnUrgent->GetCheck() == BST_CHECKED,
		 bNOUrgent = cBtnNOUrgent->GetCheck() == BST_CHECKED;

	cstr.Format(bUrgent?_T("X"):_T(" "));
	p2dPrinter.setUrgent(cstr);

	cstr.Format(bNOUrgent?_T("X"):_T(" "));
	p2dPrinter.setOrdinary(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCurrency(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEA));
	cEdit->GetWindowTextW(cstr);
	cstr.Replace(_T("\r\n"), _T(" "));
	p2dPrinter.setNameA(cstr.Trim());

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameA(cstr);

	/////////////////
	CString country;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COUNTRY));
	cEdit->GetWindowTextW(country);
	p2dPrinter.setBankNameA(cstr +_T(" ") + country);
	/////////////////

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionA(cstr);

	CString city;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANK_B_CITY));
	cEdit->GetWindowTextW(city);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameB(cstr + _T(" ") + city);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NAMEB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentPurpose(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNNB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_UNN3));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUNP_Third(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
	cEdit->GetWindowTextW(cstr);	
	p2dPrinter.setBudgetPaymentCode(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ORDER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentOrder(cstr);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYFIRSTMAN));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCompanyFirstMan(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_COMPANYSECONDMAN));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCompanySecondMan(cstr);

	if (CheckShowHideAdditionalInfo())
	{
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->GetWindowTextW(cstr);
	cstr.Remove(' ');
	p2dPrinter.setSeriaNum(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setLiveAdress(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setIssueDate(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setIssueCompany(cstr);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->GetWindowTextW(cstr);
	p2dPrinter.setDocCode(cstr.Mid(0, 2));
	}
	else
	{
		p2dPrinter.setSeriaNum(CString(""));
		p2dPrinter.setLiveAdress(CString(""));
		p2dPrinter.setIssueDate(CString(""));
		p2dPrinter.setIssueCompany(CString(""));
		p2dPrinter.setDocCode(CString(""));
	}

	int ibw = 2480;
	int ibh = 3508;
	//int ibh = 3558;
	
	CDC dc;
	CBitmap bmp;
	p2dPrinter.BuildBarcode(&dc, &bmp, false, ibw, ibh, bGlobalPrintBarcode);
	
	int pHeight = ::GetDeviceCaps(pDC->m_hDC, PHYSICALHEIGHT);
	int pWidth = ::GetDeviceCaps(pDC->m_hDC, PHYSICALWIDTH);
	int pOffsetX = ::GetDeviceCaps(pDC->m_hDC, PHYSICALOFFSETX);
	int pOffsetY = ::GetDeviceCaps(pDC->m_hDC, PHYSICALOFFSETY);
	/*int horzres = ::GetDeviceCaps(pDC->m_hDC, HORZRES);
	int vertres = ::GetDeviceCaps(pDC->m_hDC, VERTRES);*/
	int fakeWidthOffset = pWidth/150;
	pDC->StretchBlt(- pOffsetX - fakeWidthOffset, - pOffsetY, pWidth, pHeight, &dc, 0, 0, ibw, ibh, SRCCOPY);
}
// Prior2DPPSBEL message handlers

void Prior2DPPSBEL::OnEnChangeEditPpsSummdigits()
{	
	int i = 0;
	CString cstr1, cstr2;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr1);
	cstr2 = cstr1;
	int len = cstr1.GetLength();
	char* str = new char[len+4];
	while(i < len){
		if (cstr1[i] >= '0' && cstr1[i] <= '9')
		{
			str[i] = cstr1[i];
			i++;
		}
		else if (i < len)
		{
				CString::CopyChars(cstr1.GetBuffer()+i,cstr1.GetBuffer()+i+1,len-i+1);
				len--;
		}
	}
	str[len]=0;
	__int64 ival = _atoi64(str);

	_i64toa( ival, str, 10 ); 
	cstr1.Format(L"%lld",ival);
	if (len > 0 && cstr2 != cstr1) cEdit->SetWindowTextW(cstr1);
	wchar_t * cs = (wchar_t*)malloc(1024);
	Utilites::SummToStringS(ival, cs);
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMSPELLED));
	cEdit->SetWindowText(cs);
	free(cs);
	delete []str;

	ShowHideAdditionalInfo();
}

void Prior2DPPSBEL::OnEnChangeEditPpsAccountb()
{
//	CString cstr;
//	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTB));
//	cEdit->GetWindowTextW(cstr);
//	CEdit* cEditK = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_CODE));
////	cEditK->EnableWindow(TRUE);
//	if(cstr.GetLength()==13 && (cstr[0]=='3' && cstr[1]=='6' && cstr[2]=='0' && (cstr[3]=='2'||cstr[3]=='0')))
//	{
//		cEditK->EnableWindow(TRUE);
//	}
//	else
//	{
//		cEditK->EnableWindow(FALSE);
//	}
}

void Prior2DPPSBEL::OnEnChangeEditPpsAccounta()
{
	ShowHideAdditionalInfo();
}

void Prior2DPPSBEL::OnBnClickedCheckPpsUrgent()
{
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_NONURGENT));
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void Prior2DPPSBEL::OnBnClickedCheckPpsNonurgent()
{
	CButton* btn = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_NONURGENT));
	CButton* btn1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_CHECK_PPS_URGENT));
	switch(btn->GetCheck())
	{
	case BST_UNCHECKED:
		btn1->SetCheck(BST_CHECKED);
		break;
	case BST_CHECKED:
		btn1->SetCheck(BST_UNCHECKED);
		break;
	case BST_INDETERMINATE:
		btn->SetCheck(BST_UNCHECKED);
		btn1->SetCheck(BST_CHECKED);
		break;
	}
}

void Prior2DPPSBEL::OnEnChangeEditPpsNumber()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_NUMBER));
	CString cstr;
	cEdit->GetWindowTextW(cstr);
	int pos;
	if((pos = cstr.FindOneOf(_T("/\\")))!=-1)
	{
		cEdit->SetSel(pos, pos+1);
		cEdit->ReplaceSel(_T(""));
	}
}

void Prior2DPPSBEL::OnSaveTemplate()
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = _T("");
	tmptltName.setPassword = false;

	while(tmptltName.DoModal() == IDOK)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		if(tmptltName.cstrTemplateName.IsEmpty())
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ������ ������", "������ ���������� �������", MB_OK);
			if(res == IDOK) return;
		}
		if(iniReader->sectionExists(tmptltName.cstrTemplateName))
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ����� ������ ��� ����������, ������������ ���?", 
				"���������� �������", MB_YESNOCANCEL);
			if(res == IDCANCEL) return;
			if(res == IDNO) continue;
			if (!CIniReader::CheckPassword(tmptltName.cstrTemplateName))
				continue;
		}
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DPPSBEL::OnEditCopy()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_COPY);
}

void Prior2DPPSBEL::OnEditCut()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_CUT);
}

void Prior2DPPSBEL::OnEditPaste()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_PASTE);
}
void Prior2DPPSBEL::CheckNumber(int id){
	int i = 0;
	CString cstr,cstr2;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(id));
	cEdit->GetWindowTextW(cstr);
	cstr2 = cstr;
	int len = cstr.GetLength();
	while(i < len){
		if (cstr[i] >= '0' && cstr[i] <= '9')
		{
			i++;
		}
		else if (i < len)
		{
			CString::CopyChars(cstr.GetBuffer()+i,cstr.GetBuffer()+i+1,len-i+1);
			len--;
		}
	}
	if (cstr2 != cstr) cEdit->SetWindowTextW(cstr);
}

bool Prior2DPPSBEL::CheckLatin(CString text)
{
	for (int i = 0; i < text.GetLength(); i++)
	{
		if ( !( ((text[i] >= 'A') && (text[i] <= 'Z')) ||
			((text[i] >= 'a') && (text[i] <= 'z')) ) )
			return false;
	}
	return true;
}

bool Prior2DPPSBEL::CheckNumber(CString text)
{
	for (int i = 0; i < text.GetLength(); i++)
	{
		if ( !((text[i] >= '0') && (text[i] <= '9')))
			return false;
	}
	return true;
}

void Prior2DPPSBEL::OnEnChangeEditPpsUnna()
{
	CheckNumber(IDC_EDIT_PPS_UNNA);
}

void Prior2DPPSBEL::OnEnChangeEditPpsUnnb()
{
	CheckNumber(IDC_EDIT_PPS_UNNB);
}

void Prior2DPPSBEL::OnEnChangeEditPpsUnn3()
{
	CheckNumber(IDC_EDIT_PPS_UNN3);
}

void Prior2DPPSBEL::OnCbnSelchangeComboDoccode()
{
	// TODO: Add your control notification handler code here
}

int Prior2DPPSBEL::GetBaseAmount()
{
	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	WCHAR *sDir = new WCHAR[sDirLen];
	::GetCurrentDirectory(sDirLen, sDir);
	CIniReader *cIR = new CIniReader(CString(sDir)+L"\\language.ini");
	
		if (cIR->sectionExists(L"BaseAmount"))
		{
			return _ttoi(cIR->getKeyValue(L"BaseAmount", L"BaseAmount"));
		}
		else 
		{
			return 0;
		}
}

bool Prior2DPPSBEL::CheckShowHideAdditionalInfo()
{
	int baseAmount = GetBaseAmount();

	int i = 0;
	CString cstr;
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);	
	__int64 ival = _ttoi64(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PPS_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);

	if ((ival >= baseAmount*100) && (cstr.Left(4) == _T("3013")))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Prior2DPPSBEL::ShowHideAdditionalInfo()
{
	int nCmdShow;
	if (CheckShowHideAdditionalInfo())
		nCmdShow = SW_SHOW;
	else
		nCmdShow = SW_HIDE;

	CEdit* cEdit;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_SERIANUM));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_LIVEADRESS));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUEDATE));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_ISSUECOMPANY));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_ADDITIONAL_INFO));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_LIVEADRESS));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_DOCCODE));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_SERIANUM));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_ISSUEDATE));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_ISSUECOMPANY));
	cEdit->ShowWindow(nCmdShow);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_COMBO_DOCCODE));
	cCmb->ShowWindow(nCmdShow);
}
