// Prior2DCheckReg.cpp : implementation file
//

#include "stdafx.h"
#include "Prior2D.h"
#include "Prior2DCheckReg.h"
#include "TemplateName.h"
#include "..\pp2dlib\Prior2DModule.h"

// Prior2DCheckReg

extern CString csLang[32];

IMPLEMENT_DYNCREATE(Prior2DCheckReg, CFormView)

Prior2DCheckReg::Prior2DCheckReg()
	: CFormView(Prior2DCheckReg::IDD)
{
	numberOfIssuers = 1;
}

Prior2DCheckReg::~Prior2DCheckReg()
{
}

void Prior2DCheckReg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Prior2DCheckReg, CFormView)
	ON_COMMAND(ID_SAVE_TEMPLATE, &Prior2DCheckReg::OnSaveTemplate)
	ON_COMMAND(ID_EDIT_COPY, &Prior2DCheckReg::OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, &Prior2DCheckReg::OnEditCut)
	ON_COMMAND(ID_EDIT_PASTE, &Prior2DCheckReg::OnEditPaste)
	ON_COMMAND(ID_FILE_PRINT, &Prior2DCheckReg::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DCheckReg::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_BUTTON_REG_ADD1, &Prior2DCheckReg::OnBnClickedButtonRegAdd1)
	ON_BN_CLICKED(IDC_BUTTON_REG_ADD2, &Prior2DCheckReg::OnBnClickedButtonRegAdd2)
	ON_BN_CLICKED(IDC_BUTTON_REG_ADD3, &Prior2DCheckReg::OnBnClickedButtonRegAdd3)
	ON_EN_CHANGE(IDC_EDIT_REG_CHECKSUM, &Prior2DCheckReg::OnEnChangeEditRegChecksum)
	ON_EN_CHANGE(IDC_EDIT_REG_CHECKSUM1, &Prior2DCheckReg::OnEnChangeEditRegChecksum1)
	ON_EN_CHANGE(IDC_EDIT_REG_CHECKSUM2, &Prior2DCheckReg::OnEnChangeEditRegChecksum2)
	ON_EN_CHANGE(IDC_EDIT_REG_CHECKSUM3, &Prior2DCheckReg::OnEnChangeEditRegChecksum3)
END_MESSAGE_MAP()


// Prior2DCheckReg diagnostics

#ifdef _DEBUG
void Prior2DCheckReg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void Prior2DCheckReg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void Prior2DCheckReg::OnInitialUpdate()
{
	CString cstmp;
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEA));
	cEdit->SetLimitText(100);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEB));
	cEdit->SetLimitText(100);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NUMBER));
	cEdit->SetLimitText(10);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM));
	cEdit->SetLimitText(7);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE));
	cEdit->SetLimitText(50);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM));
	cEdit->SetLimitText(12);

	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NAMEB));
	cEdit->SetLimitText(100);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cEdit->SetLimitText(9);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cEdit->SetLimitText(9);
	cEdit->SetWindowTextW(_T("153001749"));

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_REG_CODE));
	cCmbBox->AddString(_T("2074"));
	cCmbBox->AddString(_T("2076"));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO1));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS1));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM1));
	cEdit->SetLimitText(7);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE1));
	cEdit->SetLimitText(50);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM1));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM1));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO2));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS2));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM2));
	cEdit->SetLimitText(7);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE2));
	cEdit->SetLimitText(50);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM2));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM2));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
	cEdit->SetLimitText(13);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO3));
	cEdit->SetLimitText(80);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS3));
	cEdit->SetLimitText(2);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM3));
	cEdit->SetLimitText(7);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE3));
	cEdit->SetLimitText(50);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM3));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM3));
	cEdit->SetLimitText(12);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
	cEdit->SetLimitText(13);
}


BOOL Prior2DCheckReg::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;

	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	bool b = DoPreparePrinting(pInfo);
	return b;
}

void Prior2DCheckReg::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DCheckReg::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	delete m_FLarge;
}


void Prior2DCheckReg::SaveConfigContent(CString strSection, CString password)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (password != _T(""))
	{
		iniReader->NeedEncryption = true;
	}
	
	iniReader->setKey(password,_T("PASSWORD"),strSection);			
	iniReader->setKey(_T("CHECKREG"),_T("TYPE"),strSection);

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NUMBER));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NUMBER"), strSection);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_REG_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i.%.2i.%i"), time.wDay, time.wMonth, time.wYear);
	iniReader->setKey(cstr, _T("DATE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NAMEB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("NAMEB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKA"), strSection);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BANKB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BIKA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("BIKB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTA"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("ACCOUNTB"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("SERIAPAS"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PASSNUM"), strSection);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("PASSISSUE"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("CHECKNUM"), strSection);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM));
	cEdit->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("CHECKSUM"), strSection);

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_REG_CODE));
	cCmbBox->GetWindowTextW(cstr);
	iniReader->setKey(cstr, _T("CODE"), strSection);

	char buf[32];
	cstr = CString(itoa(numberOfIssuers, buf, 10));
	iniReader->setKey(cstr, _T("COUNT"), strSection);

	if (numberOfIssuers > 1)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("NAMEA1"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("ACCOUNTA1"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("SERIAPAS1"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PASSNUM1"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PASSISSUE1"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CHECKNUM1"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM1));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CHECKSUM1"), strSection);
	}

	if (numberOfIssuers > 2)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("NAMEA2"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("ACCOUNTA2"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("SERIAPAS2"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PASSNUM2"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PASSISSUE2"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CHECKNUM2"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM2));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CHECKSUM2"), strSection);
	}

	if (numberOfIssuers > 3)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("NAMEA3"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("ACCOUNTA3"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("SERIAPAS3"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PASSNUM3"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("PASSISSUE3"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CHECKNUM3"), strSection);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM3));
		cEdit->GetWindowTextW(cstr);
		iniReader->setKey(cstr, _T("CHECKSUM3"), strSection);
	}

	iniReader->NeedEncryption = false;
}

void Prior2DCheckReg::LoadConfigContent(CString strSection)
{
	CIniReader* iniReader = theApp.GetINIHandler();

	if (iniReader->getKeyValue(_T("PASSWORD"), strSection) != _T(""))
	{
		iniReader->NeedEncryption = true;
	}

	CString cstr;

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NUMBER));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NUMBER"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NAMEB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKA"), strSection));
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BANKB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BIKA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("BIKB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTB"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SERIAPAS"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSNUM"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSISSUE"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKNUM"), strSection));

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM));
	cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKSUM"), strSection));

	CComboBox* cCmbBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_REG_CODE));
	cCmbBox->SelectString(0, iniReader->getKeyValue(_T("CODE"), strSection));	

	numberOfIssuers = _ttoi(iniReader->getKeyValue(_T("COUNT"), strSection));

	
	if (numberOfIssuers > 1)
	{
		CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD1));
		CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD2));
		ShowHideIssuer1(SW_SHOW);
		cBut1->SetWindowTextW(_T("������� ����������"));
		cBut2->ShowWindow(SW_SHOW);
		cBut2->SetWindowTextW(L"�������� ����������");

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA1"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA1"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SERIAPAS1"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSNUM1"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSISSUE1"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKNUM1"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM1));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKSUM1"), strSection));
	}

	if (numberOfIssuers > 2)
	{
		CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD1));
		CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD2));
		CButton* cBut3 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD3));
		ShowHideIssuer2(SW_SHOW);
		cBut1->EnableWindow(FALSE);
		cBut2->SetWindowTextW(L"������� ����������");
		cBut3->ShowWindow(SW_SHOW);
		cBut3->SetWindowTextW(L"�������� ����������");

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA2"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA2"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SERIAPAS2"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSNUM2"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSISSUE2"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKNUM2"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM2));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKSUM2"), strSection));
	}

	if (numberOfIssuers > 3)
	{
		
		CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD2));
		CButton* cBut3 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD3));
		ShowHideIssuer3(SW_SHOW);
		cBut2->EnableWindow(FALSE);
		cBut3->SetWindowTextW(L"������� ����������");

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("NAMEA3"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("ACCOUNTA3"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("SERIAPAS3"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSNUM3"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("PASSISSUE3"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKNUM3"), strSection));

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM3));
		cEdit->SetWindowTextW(iniReader->getKeyValue(_T("CHECKSUM3"), strSection));
	}

	iniReader->NeedEncryption = false;

	if (!CIniReader::CheckPasswordExists(strSection))
	{
		int res = ::MessageBoxA(this->m_hWnd, "������ ��� ������� ������� �� ����������. ����������?", 
				"�������� ������ �������", MB_YESNO);	
		if (res == IDYES)
		{
			SaveTemplate(strSection);
		}
	}
}

void Prior2DCheckReg::SaveTemplate(CString strSection)
{
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = strSection;
	tmptltName.setPassword = true;

	while(tmptltName.DoModal() == IDOK)
	{
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DCheckReg::OnSaveTemplate()
{	
	CTemplateName tmptltName;
	tmptltName.cstrTemplateName = _T("");
	tmptltName.setPassword = false;
	while(tmptltName.DoModal() == IDOK)
	{
		CIniReader* iniReader = theApp.GetINIHandler();
		if(iniReader->sectionExists(tmptltName.cstrTemplateName))
		{
			int res = ::MessageBoxA(this->m_hWnd, "������ � ����� ������ ��� ����������, ������������ ���?", 
				"���������� �������", MB_YESNOCANCEL);
			if(res == IDCANCEL) return;
			if(res == IDNO) continue;
			if (!CIniReader::CheckPassword(tmptltName.cstrTemplateName))
				continue;
		}
		SaveConfigContent(tmptltName.cstrTemplateName, tmptltName.cstrTemplatePassword);
		return;
	}
}

void Prior2DCheckReg::OnEditCopy()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_COPY);
}

void Prior2DCheckReg::OnEditCut()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_CUT);
}

void Prior2DCheckReg::OnEditPaste()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(this->GetFocus());
	if(!cEdit) return;
	cEdit->PostMessageW(WM_PASTE);
}

void Prior2DCheckReg::OnFilePrint()
{
	CString cstr;
	CEdit* cEdit;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� ����������\" ������ ���� ���������.",
			"���� ����������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	
	CString bcode;
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cEdit->GetWindowTextW(bcode);
	if(bcode.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" ����� ���������� ������ ���� ���������.",
			"���:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=9)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" ���������� ������ ��������� 9 ����.",
			"���:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS));
	cEdit->GetWindowTextW(cstr);
	if((cstr.GetLength()!=2) && (cstr.GetLength()!=0))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� ������ ��������� 2 �����.",
			"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM));
	cEdit->GetWindowTextW(cstr);
	if((cstr.GetLength()!=7) && (cstr.GetLength()!=0))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� ������ ��������� 7 ����.",
			"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � ����� ����\" ������ ���� ���������.",
			"����� � ����� ����:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � ����� ����\" ������ ���� ���������.",
			"����� � ����� ����:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� ������ ���� ���������.",
			"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� ������ ��������� 13 ����.",
			"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	CString code;
	CEdit* cCode = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cCode->GetWindowTextW(code);
	if (!Utilites::CheckAccount(cstr, code))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"����� ����� �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�����\" ���������� ������ ���� ���������.",
			"����� ����:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NAMEB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�������������\" ������ ���� ���������.",
			"�������������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���� �������������\" ������ ���� ���������.",
			"���� �������������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cEdit->GetWindowTextW(bcode);
	if(bcode.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" ����� ������������� ������ ���� ���������.",
			"��� ����� �������������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=9)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" ������������� ������ ��������� 9 ����.",
			"���:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"������ ����� �����\" ������������� ������ ���� ���������.",			
			"������ ����� ����� �������������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()!=13)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"������ ����� �����\" ������������� ������ ��������� 13 ����.",
			"������ ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	cCode = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cCode->GetWindowTextW(code);
	if (!Utilites::CheckAccount(cstr, code))
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"������ ����� �����\" ������������� - �������� ��������, �� ��������� ������� ������������ �������.",
				"������ ����� �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
	}

	CComboBox* cBox = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_REG_CODE));
	cBox->GetWindowTextW(cstr);
	if (cstr.GetLength() == 0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"��� ��������� �������\" ������ ���� ���������.",
			"��� ��������� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cBox->SetFocus();
		return;
	}
	
	//�������������� ����������
	if (numberOfIssuers > 1)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS1));
		cEdit->GetWindowTextW(cstr);
		if((cstr.GetLength()!=2) && (cstr.GetLength()!=0))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� 2 ������ ��������� 2 �����.",
				"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM1));
		cEdit->GetWindowTextW(cstr);
		if((cstr.GetLength()!=7) && (cstr.GetLength()!=0))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� 2 ������ ��������� 7 ����.",
				"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM1));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � ����� ����\" ���������� 2 ������ ���� ���������.",
				"����� � ����� ����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}


		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 2 ������ ���� ���������.",
				"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()!=13)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 2 ������ ��������� 13 ����.",
				"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
		cEdit->GetWindowTextW(cstr);
		CEdit* cCode = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
		cCode->GetWindowTextW(code);
		if (!Utilites::CheckAccount(cstr, code))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 2 - �������� ��������, �� ��������� ������� ������������ �������.",
				"����� ����� �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM1));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�����\" ���������� 2 ������ ���� ���������.",
				"����� ����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	if (numberOfIssuers > 2)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS2));
		cEdit->GetWindowTextW(cstr);
		if((cstr.GetLength()!=2) && (cstr.GetLength()!=0))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� 3 ������ ��������� 2 �����.",
				"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM2));
		cEdit->GetWindowTextW(cstr);
		if((cstr.GetLength()!=7) && (cstr.GetLength()!=0))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� 3 ������ ��������� 7 ����.",
				"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM2));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � ����� ����\" ���������� 3 ������ ���� ���������.",
				"����� � ����� ����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}


		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 3 ������ ���� ���������.",
				"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()!=13)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 3 ������ ��������� 13 ����.",
				"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
		cEdit->GetWindowTextW(cstr);
		CEdit* cCode = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
		cCode->GetWindowTextW(code);
		if (!Utilites::CheckAccount(cstr, code))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 3 - �������� ��������, �� ��������� ������� ������������ �������.",
				"����� ����� �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM2));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�����\" ���������� 3 ������ ���� ���������.",
				"����� ����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	if (numberOfIssuers > 3)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS3));
		cEdit->GetWindowTextW(cstr);
		if((cstr.GetLength()!=2) && (cstr.GetLength()!=0))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� 4 ������ ��������� 2 �����.",
				"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM3));
		cEdit->GetWindowTextW(cstr);
		if((cstr.GetLength()!=7) && (cstr.GetLength()!=0))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ��������\" ���������� 4 ������ ��������� 7 ����.",
				"����� ��������:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM3));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� � ����� ����\" ���������� 4 ������ ���� ���������.",
				"����� � ����� ����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}


		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 4 ������ ���� ���������.",
				"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()!=13)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 4 ������ ��������� 13 ����.",
				"����� ����� �����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
		cEdit->GetWindowTextW(cstr);
		CEdit* cCode = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
		cCode->GetWindowTextW(code);
		if (!Utilites::CheckAccount(cstr, code))
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"����� ����� �����\" ���������� 4 - �������� ��������, �� ��������� ������� ������������ �������.",
				"����� ����� �����: �������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM3));
		cEdit->GetWindowTextW(cstr);
		if(cstr.GetLength()==0)
		{
			MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"�����\" ���������� 4 ������ ���� ���������.",
				"����� ����:�������� ������",MB_OK|MB_ICONWARNING);
			cEdit->SetFocus();
			return;
		}
	}

	CFormView::OnFilePrint();
}
void Prior2DCheckReg::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	Prior2DCheckRegModule printer;


	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NUMBER));
	cEdit->GetWindowTextW(cstr);
	printer.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_REG_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("\"%i\" %s %i �."), time.wDay, Utilites::NumserOfMonthToString(time.wMonth), time.wYear);
	printer.setDate(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	printer.setAccountA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	printer.setAccountB(cstr);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEA));
	cEdit->GetWindowTextW(cstr);
	printer.setBankNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BANKNAMEB));
	cEdit->GetWindowTextW(cstr);
	printer.setBankNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKA));
	cEdit->GetWindowTextW(cstr);
	printer.setBikA(cstr);
		
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_BIKB));
	cEdit->GetWindowTextW(cstr);
	printer.setBikB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO));
	cEdit->GetWindowTextW(cstr);
	printer.setNameA(cstr);
	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_NAMEB));
	cEdit->GetWindowTextW(cstr);
	printer.setNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS));
	cEdit->GetWindowTextW(cstr);
	printer.setPassSeria(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM));
	cEdit->GetWindowTextW(cstr);
	printer.setPassNum(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE));
	cEdit->GetWindowTextW(cstr);
	printer.setPassIssue(cstr);

	CComboBox* cCmb = reinterpret_cast<CComboBox*>(GetDlgItem(IDC_CBOX_REG_CODE));
	cCmb->GetWindowTextW(cstr);
	printer.setCode(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM));
	cEdit->GetWindowTextW(cstr);
	printer.setCheckNum(cstr);

	
	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM));
	cEdit->GetWindowTextW(cstr);
	printer.setCheckSum(cstr);

	__int64 totalSum = 0;
	totalSum += _ttoi64(cstr);
	
	printer.setCount(numberOfIssuers);
	
	if (numberOfIssuers > 1)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO1));
		cEdit->GetWindowTextW(cstr);
		printer.setNameA1(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
		cEdit->GetWindowTextW(cstr);
		printer.setAccountA1(cstr);
		
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS1));
		cEdit->GetWindowTextW(cstr);
		printer.setPassSeria1(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM1));
		cEdit->GetWindowTextW(cstr);
		printer.setPassNum1(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE1));
		cEdit->GetWindowTextW(cstr);
		printer.setPassIssue1(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM1));
		cEdit->GetWindowTextW(cstr);
		printer.setCheckNum1(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM1));
		cEdit->GetWindowTextW(cstr);
		printer.setCheckSum1(cstr);

		totalSum += _ttoi64(cstr);
	}

	if (numberOfIssuers > 2)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO2));
		cEdit->GetWindowTextW(cstr);
		printer.setNameA2(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
		cEdit->GetWindowTextW(cstr);
		printer.setAccountA2(cstr);
		
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS2));
		cEdit->GetWindowTextW(cstr);
		printer.setPassSeria2(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM2));
		cEdit->GetWindowTextW(cstr);
		printer.setPassNum2(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE2));
		cEdit->GetWindowTextW(cstr);
		printer.setPassIssue2(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM2));
		cEdit->GetWindowTextW(cstr);
		printer.setCheckNum2(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM2));
		cEdit->GetWindowTextW(cstr);
		printer.setCheckSum2(cstr);

		totalSum += _ttoi64(cstr);
	}

	if (numberOfIssuers > 3)
	{
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO3));
		cEdit->GetWindowTextW(cstr);
		printer.setNameA3(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
		cEdit->GetWindowTextW(cstr);
		printer.setAccountA3(cstr);
		
		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS3));
		cEdit->GetWindowTextW(cstr);
		printer.setPassSeria3(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM3));
		cEdit->GetWindowTextW(cstr);
		printer.setPassNum3(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE3));
		cEdit->GetWindowTextW(cstr);
		printer.setPassIssue3(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM3));
		cEdit->GetWindowTextW(cstr);
		printer.setCheckNum3(cstr);

		cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM3));
		cEdit->GetWindowTextW(cstr);
		printer.setCheckSum3(cstr);

		totalSum += _ttoi64(cstr);
	}
	
	printer.setTotalSum(totalSum);

	int ibw = 2480;
	int ibh = 3558;

	CDC dc;
	CBitmap bmp;
	printer.BuildBarcode(&dc, &bmp, ibw, ibh);
	pDC->StretchBlt(pInfo->m_rectDraw.left, pInfo->m_rectDraw.top, pInfo->m_rectDraw.Width(), pInfo->m_rectDraw.Height(), &dc, 0, 0, ibw, ibh, SRCCOPY);
	
}



void Prior2DCheckReg::OnBnClickedButtonRegAdd1()
{
	CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD1));
	CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD2));
	if (numberOfIssuers == 1)
	{
		numberOfIssuers++;
		ShowHideIssuer1(SW_SHOW);
		cBut1->SetWindowTextW(_T("������� ����������"));
		cBut2->ShowWindow(SW_SHOW);
		cBut2->SetWindowTextW(L"�������� ����������");
	}
	else
	{
		numberOfIssuers--;
		ShowHideIssuer1(SW_HIDE);
		cBut1->SetWindowTextW(_T("�������� ����������"));
		cBut2->ShowWindow(SW_HIDE);
		cBut2->SetWindowTextW(L"������� ����������");
	}
}

void Prior2DCheckReg::ShowHideIssuer1(int nCmdShow)
{
	CEdit* cEdit;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_ACCOUNTA1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_CHECKNUM1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_CHECKSUM1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_FIO1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_NAMEA1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PAS1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PASISSUE1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PASNUM1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_SERIAPAS1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS1));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM1));
	cEdit->ShowWindow(nCmdShow);
}

void Prior2DCheckReg::ShowHideIssuer2(int nCmdShow)
{
	CEdit* cEdit;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_ACCOUNTA2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_CHECKNUM2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_CHECKSUM2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_FIO2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_NAMEA2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PAS2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PASISSUE2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PASNUM2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_SERIAPAS2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS2));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM2));
	cEdit->ShowWindow(nCmdShow);
}

void Prior2DCheckReg::ShowHideIssuer3(int nCmdShow)
{
	CEdit* cEdit;

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_ACCOUNTA3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_CHECKNUM3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_CHECKSUM3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_FIO3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_NAMEA3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PAS3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PASISSUE3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_PASNUM3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_STATIC_REG_SERIAPAS3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_FIO3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_ACCOUNTA3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKNUM3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_CHECKSUM3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSISSUE3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_SERIAPAS3));
	cEdit->ShowWindow(nCmdShow);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_REG_PASSNUM3));
	cEdit->ShowWindow(nCmdShow);
}



void Prior2DCheckReg::OnBnClickedButtonRegAdd2()
{
	CButton* cBut1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD1));
	CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD2));
	CButton* cBut3 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD3));
	if (numberOfIssuers == 2)
	{
		numberOfIssuers++;
		ShowHideIssuer2(SW_SHOW);
		cBut1->EnableWindow(FALSE);
		cBut2->SetWindowTextW(L"������� ����������");
		cBut3->ShowWindow(SW_SHOW);
		cBut3->SetWindowTextW(L"�������� ����������");
	}
	else
	{
		numberOfIssuers--;
		ShowHideIssuer2(SW_HIDE);
		cBut1->EnableWindow(TRUE);
		cBut2->SetWindowTextW(L"�������� ����������");
		cBut3->ShowWindow(SW_HIDE);
		cBut3->SetWindowTextW(L"�������� ����������");
	}
}



void Prior2DCheckReg::OnBnClickedButtonRegAdd3()
{
	CButton* cBut2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD2));
	CButton* cBut3 = reinterpret_cast<CButton*>(GetDlgItem(IDC_BUTTON_REG_ADD3));
	if (numberOfIssuers == 3)
	{
		numberOfIssuers++;
		ShowHideIssuer3(SW_SHOW);
		cBut2->EnableWindow(FALSE);
		cBut3->SetWindowTextW(L"������� ����������");		
	}
	else
	{
		numberOfIssuers--;
		ShowHideIssuer3(SW_HIDE);
		cBut2->EnableWindow(TRUE);
		cBut3->SetWindowTextW(L"�������� ����������");		
	}
}

/*
BOOL Prior2DCheckReg::isNumeric(CString text)
{
	BOOL flag = true;
	for (int a = 0; a<text.GetLength();a++)
	{
		if (!isdigit(text.GetAt(a)))
		{
				flag = false;
		}
	}
	return flag;
	}*/
void Prior2DCheckReg::OnEnChangeEditRegChecksum()
{
	avoidZero(IDC_EDIT_REG_CHECKSUM);
}

void Prior2DCheckReg::OnEnChangeEditRegChecksum1()
{
	avoidZero(IDC_EDIT_REG_CHECKSUM1);
}



void Prior2DCheckReg::OnEnChangeEditRegChecksum2()
{
	avoidZero(IDC_EDIT_REG_CHECKSUM2);
}

void Prior2DCheckReg::OnEnChangeEditRegChecksum3()
{
	avoidZero(IDC_EDIT_REG_CHECKSUM3);
}

void Prior2DCheckReg::avoidZero(int desc)
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(desc));
	CString cstr;
	cEdit->GetWindowTextW(cstr);
	if (cstr == L"0")
	{
		cEdit->SetWindowTextW(L"");
	}
}
