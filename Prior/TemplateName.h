#pragma once


// CTemplateName dialog

class CTemplateName : public CDialog
{
	DECLARE_DYNAMIC(CTemplateName)

public:
	CTemplateName(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTemplateName();

// Dialog Data
	enum { IDD = IDD_TEMPLATENAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	static CString cstrTemplateName;
	static CString cstrTemplatePassword;
	static BOOL setPassword;

public:
	afx_msg void OnBnClickedCancel();
};
