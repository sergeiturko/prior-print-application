// Prior2DPTBEL.cpp : implementation file
//

#include "stdafx.h"
#include "Prior2D.h"
#include "Prior2DPTBEL.h"
#include "Prior2DModule.h"


// Prior2DPTBEL

IMPLEMENT_DYNCREATE(Prior2DPTBEL, CFormView)

Prior2DPTBEL::Prior2DPTBEL()
	: CFormView(Prior2DPTBEL::IDD)
{
}

Prior2DPTBEL::~Prior2DPTBEL()
{
}

void Prior2DPTBEL::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

void Prior2DPTBEL::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PT_CURRENCY));
	cEdit->SetWindowTextW(_T("764"));
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKB));
	cEdit->SetWindowTextW(_T("\"���������\" ���"));
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKB_CODE));
	cEdit->SetWindowTextW(_T("153001749"));
	cEdit->EnableWindow(FALSE);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_SUMMSPELLED));
	cEdit->EnableWindow(FALSE);
}

BEGIN_MESSAGE_MAP(Prior2DPTBEL, CFormView)
	ON_EN_CHANGE(IDC_EDIT_PT_SUMMSPELLED, &Prior2DPTBEL::OnEnChangeEditPtSummspelled)
	ON_COMMAND(ID_FILE_PRINT, &Prior2DPTBEL::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &Prior2DPTBEL::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFormView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_RADIO_PTB_PA, &Prior2DPTBEL::OnBnClickedRadioPtbPa)
	ON_BN_CLICKED(IDC_RADIO_PTB_LA, &Prior2DPTBEL::OnBnClickedRadioPtbLa)
	ON_BN_CLICKED(IDC_RADIO_PTB_NA, &Prior2DPTBEL::OnBnClickedRadioPtbNa)
	ON_EN_CHANGE(IDC_EDIT_PTB_SUMMDIGITS, &Prior2DPTBEL::OnEnChangeEditPtbSummdigits)
END_MESSAGE_MAP()


// Prior2DPTBEL diagnostics

#ifdef _DEBUG
void Prior2DPTBEL::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void Prior2DPTBEL::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// Prior2DPTBEL message handlers

void Prior2DPTBEL::OnEnChangeEditPtSummspelled()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}

BOOL Prior2DPTBEL::OnPreparePrinting(CPrintInfo* pInfo)
{
	CPrintDialog *dlg = new CPrintDialog(FALSE, PD_PAGENUMS);
	pInfo->m_pPD = dlg;
	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(1);

	m_FLarge = new CFont();
	m_FLarge->CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));

	return DoPreparePrinting(pInfo);
}

void Prior2DPTBEL::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DPTBEL::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void Prior2DPTBEL::OnFilePrint()
{
	CButton *bt1 = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_PA));
	CButton *bt2 = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_LA));
	CButton *bt3 = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_NA));

	if(bt1->GetCheck() != BST_CHECKED &&
		bt2->GetCheck() != BST_CHECKED &&
		bt3->GetCheck() != BST_CHECKED)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd,"�������� ���� �� ����� �������!", "�� ������� ����� �������", MB_OK|MB_ICONWARNING); 
		return;
	}

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	if(cstr.GetLength()==0)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-���������� ������ ���� ���������!",
			"��� �����-����������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	bool bHasErrors = cstr.GetLength()!=3;
	if(!bHasErrors)
		for(int i = 0; i < cstr.GetLength(); i++) bHasErrors |=  cstr[i]<'0' || cstr[i]>'9';
	if(bHasErrors)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "���� \"���\" �����-���������� ������ ��������� 3 �����!.",
			"��� �����-����������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	CFormView::OnFilePrint();
}

void Prior2DPTBEL::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	Prior2DPTModule p2dPrinter;

	CString cstr;
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_NUMBER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNumber(cstr);

	CDateTimeCtrl* cDTControl = reinterpret_cast<CDateTimeCtrl*>(GetDlgItem(IDC_DATETIMEPICKER_PTB_DATE));
	SYSTEMTIME  time;
	cDTControl->GetTime(&time);
	cstr.Format(_T("%.2i/%.2i/%i"), time.wDay, time.wMonth, time.wYear);
	p2dPrinter.setDate(cstr);

	CButton* cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_PA));
	cstr.Format(_T("%s"), cButton->GetCheck()==BST_CHECKED?_T("X"):_T(" "));
	p2dPrinter.setPredAccept(cstr);

	cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_LA));
	cstr.Format(_T("%s"), cButton->GetCheck()==BST_CHECKED?_T("X"):_T(" "));
	p2dPrinter.setDelayedAccept(cstr);

	cButton = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_NA));
	cstr.Format(_T("%s"), cButton->GetCheck()==BST_CHECKED?_T("X"):_T(" "));
	p2dPrinter.setNoAccept(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumDigits(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_CURRENCY));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCurrency(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_SUMMSPELLED));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSumSpelled(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_NAMEA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_ACCOUNTA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKA_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKA_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKBCORR_NAME));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCorrespondentA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKBCORR_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setCodeBankaCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKBCORR_ACCOUNT));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setSchetBankCor(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKB_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankCodeB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_BANKB_CBU));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBankDivisionB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_NAMEB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setNameB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_ACCOUNTB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setAccountB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_PAYMENTPURPOSE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentPurpose(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_UNNA));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnA(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_UNNB));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUnnB(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_UNN3));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setUNP_Third(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_CODE));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setBudgetPaymentCode(cstr);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_ORDER));
	cEdit->GetWindowTextW(cstr);
	p2dPrinter.setPaymentOrder(cstr);

	int ibw = 2480;
	int ibh = 3508;
	CDC dc;
	CBitmap bmp;
	p2dPrinter.BuildBarcode(&dc, &bmp, ibw, ibh);
	pDC->StretchBlt(pInfo->m_rectDraw.left, pInfo->m_rectDraw.top, pInfo->m_rectDraw.Width(), pInfo->m_rectDraw.Height(), &dc, 0, 0, ibw, ibh, SRCCOPY);
}


void Prior2DPTBEL::OnBnClickedRadioPtbPa()
{
	CButton* btnPA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_PA));
	CButton* btnLA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_LA));
	CButton* btnNA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_NA));
	btnPA->SetCheck(BST_CHECKED);
	btnLA->SetCheck(BST_UNCHECKED);
	btnNA->SetCheck(BST_UNCHECKED);
}

void Prior2DPTBEL::OnBnClickedRadioPtbLa()
{
	CButton* btnPA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_PA));
	CButton* btnLA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_LA));
	CButton* btnNA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_NA));
	btnPA->SetCheck(BST_UNCHECKED);
	btnLA->SetCheck(BST_CHECKED);
	btnNA->SetCheck(BST_UNCHECKED);
}

void Prior2DPTBEL::OnBnClickedRadioPtbNa()
{
	CButton* btnPA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_PA));
	CButton* btnLA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_LA));
	CButton* btnNA = reinterpret_cast<CButton*>(GetDlgItem(IDC_RADIO_PTB_NA));
	btnPA->SetCheck(BST_UNCHECKED);
	btnLA->SetCheck(BST_UNCHECKED);
	btnNA->SetCheck(BST_CHECKED);
}

void Prior2DPTBEL::OnEnChangeEditPtbSummdigits()
{
	CString cstr1, cstr2=_T("");
	CEdit *cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_SUMMDIGITS));
	cEdit->GetWindowTextW(cstr1);

	char* str = new char[cstr1.GetLength()+1];
	for(int i = 0; i < cstr1.GetLength(); i++)
		str[i] = cstr1[i];
	str[cstr1.GetLength()]=0;
	__int64 ival = _atoi64(str);
	delete []str;

	if(ival>=0xFFFFFFFF)
	{
		MessageBoxA(theApp.GetMainWnd()->m_hWnd, "����� ������� �������� ������� ������� ��������.",
			"����� �������:�������� ������",MB_OK|MB_ICONWARNING);
		cEdit->SetFocus();
		return;
	}

	CString cstrSumm(Utilites::SummToStringS(ival).c_str());

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PTB_SUMMSPELLED));
	cEdit->SetWindowTextW(cstrSumm);
}
