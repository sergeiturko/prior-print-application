#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPrior2DDoc
extern CString csLang[32];

IMPLEMENT_DYNCREATE(CPrior2DDoc, CDocument)

BEGIN_MESSAGE_MAP(CPrior2DDoc, CDocument)
END_MESSAGE_MAP()


// CPrior2DDoc construction/destruction

CPrior2DDoc::CPrior2DDoc()
{
}

CPrior2DDoc::~CPrior2DDoc()
{
}

BOOL CPrior2DDoc::OnNewDocument()
{
	CString cst;
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	cst = this->GetTitle();
	this->SetTitle(csLang[0] + cst);

	return TRUE;
}




// CPrior2DDoc serialization

void CPrior2DDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CPrior2DDoc diagnostics

#ifdef _DEBUG
void CPrior2DDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPrior2DDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CPrior2DDoc commands
