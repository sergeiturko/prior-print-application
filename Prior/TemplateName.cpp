#include "stdafx.h"

// CTemplateName dialog
IMPLEMENT_DYNAMIC(CTemplateName, CDialog)

CTemplateName::CTemplateName(CWnd* pParent /*=NULL*/)
	: CDialog(CTemplateName::IDD, pParent)
{
	
}

CTemplateName::~CTemplateName()
{
}

void CTemplateName::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTemplateName, CDialog)
	ON_BN_CLICKED(IDOK, &CTemplateName::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CTemplateName::OnBnClickedCancel)
END_MESSAGE_MAP()


CString CTemplateName::cstrTemplateName=_T("");
CString CTemplateName::cstrTemplatePassword=_T("");
BOOL CTemplateName::setPassword = false;

BOOL CTemplateName::OnInitDialog()
{
	BOOL result = CDialog::OnInitDialog();
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TEMPLATE_NAME));
	cEdit->SetLimitText(128);
	if (setPassword)
	{
		cEdit->SetWindowTextW(cstrTemplateName);
		cEdit->EnableWindow(false);
	}

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TEMPLATE_PASS));
	cEdit->SetLimitText(32);

	return result;
}

void CTemplateName::OnBnClickedOk()
{
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TEMPLATE_NAME));
	cEdit->GetWindowTextW(cstrTemplateName);

	cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_TEMPLATE_PASS));
	cEdit->GetWindowTextW(cstrTemplatePassword);

	if (cstrTemplateName.Trim() == _T(""))
	{
		MessageBox(_T("������� �������� �������."));
		return;
	}
	OnOK();
}

void CTemplateName::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}
