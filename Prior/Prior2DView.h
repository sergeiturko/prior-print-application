// Prior2DView.h : interface of the CPrior2DView class
//


#pragma once


class CPrior2DView : public CFormView
{
protected: // create from serialization only
	CPrior2DView();
	DECLARE_DYNCREATE(CPrior2DView)

public:
	enum{ IDD = IDD_PRIOR2D_FORM };

// Attributes
public:
	CPrior2DDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnFilePrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CPrior2DView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckNourgent();
public:
	afx_msg void OnBnClickedCheckUrgent();
private:
	CFont* m_FLarge;
	CFont* m_FSmall;
};

#ifndef _DEBUG  // debug version in Prior2DView.cpp
inline CPrior2DDoc* CPrior2DView::GetDocument() const
   { return reinterpret_cast<CPrior2DDoc*>(m_pDocument); }
#endif

