// Password.cpp : implementation file
//

#include "stdafx.h"
#include "Password.h"

// Password dialog

IMPLEMENT_DYNAMIC(Password, CDialog)

Password::Password(CString cstrTemplateName, CString cstrPassword, CWnd* pParent)
	: CDialog(Password::IDD, pParent)
{
	templateName = cstrTemplateName;
	templatePassword = cstrPassword;
	Action = TMPLT_ACTION_NONE;
}

Password::~Password()
{
}

void Password::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Password, CDialog)
	ON_BN_CLICKED(IDOK, &Password::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &Password::OnBnClickedCancel)
END_MESSAGE_MAP()

BOOL Password::OnInitDialog()
{
	BOOL result = CDialog::OnInitDialog();
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PASSWORD));
	cEdit->SetLimitText(32);
	cEdit->SetFocus();
	return false;
}

void Password::OnBnClickedOk()
{	
	CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PASSWORD));
	CString password;
	cEdit->GetWindowTextW(password);

	if (templatePassword == _T(""))
	{
		// no password means set password
		if (password == _T(""))
		{
			AfxMessageBox(_T("�������� ������"));
			CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PASSWORD));
			cEdit->SetFocus();
		}
		else
			templatePassword = CIniReader::Encode(password);
	}
	else if (password == Decode(templatePassword))
	{
		Action = TMPLT_ACTION_OPEN;
		OnOK();
	}
	else
	{
		AfxMessageBox(_T("�������� ������"));
		CEdit* cEdit = reinterpret_cast<CEdit*>(GetDlgItem(IDC_EDIT_PASSWORD));
		cEdit->SetFocus();
	}
}

void Password::OnBnClickedCancel()
{
	OnCancel();
}

CString Password::Decode(CString value)
{
	for (int i=0; i<value.GetLength(); i++)
	{
		if (32 <= value.GetAt(i) && value.GetAt(i) <= 127)
			value.SetAt(i, 127 - value.GetAt(i) + 32);
		else if (1040 <= value.GetAt(i) && value.GetAt(i) <= 1103)
			value.SetAt(i, 1103 - value.GetAt(i) + 1040);
	}
	return value;
}
