#pragma once


#include "afxole.h"
#include "afxadv.h"

// Prior2DPPSBEL form view

class Prior2DPPSBEL : public CFormView
{
	DECLARE_DYNCREATE(Prior2DPPSBEL)

protected:
	Prior2DPPSBEL();           // protected constructor used by dynamic creation
	virtual ~Prior2DPPSBEL();

public:
	enum { IDD = IDD_PRIOR2DPPSBEL };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	virtual void SaveConfigContent(CString strSection, CString password);
	virtual void LoadConfigContent(CString strSection);
	virtual void OnEditCopy();
	virtual void OnEditCut();
	virtual void OnEditPaste();

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnFilePrint();
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

	CFont* m_FLarge;
	DECLARE_MESSAGE_MAP()
private:
	CString EditSummcypher;
	void CheckNumber(int);

	int GetBaseAmount();
	bool CheckShowHideAdditionalInfo();
	bool CheckLatin(CString text);
	bool CheckNumber(CString text);
	void ShowHideAdditionalInfo();

	void SaveTemplate(CString strSection);

public:
	afx_msg void OnEnChangeEditPpsSummdigits();
	afx_msg void OnEnChangeEditPpsAccountb();
	afx_msg void OnBnClickedCheckPpsUrgent();
	afx_msg void OnBnClickedCheckPpsNonurgent();
	afx_msg void OnEnChangeEditPpsNumber();
	afx_msg void OnSaveTemplate();
public:
	afx_msg void OnEnChangeEditPpsUnna();
public:
	afx_msg void OnEnChangeEditPpsUnnb();
public:
	afx_msg void OnEnChangeEditPpsUnn3();
public:
	CString UNPPayer;
	afx_msg void OnEnChangeEditPpsAccounta();
	afx_msg void OnEnChangeEditPpsBankaCode();
	afx_msg void OnEnChangeEditPpsBankbCode();
	afx_msg void OnEnChangeEditBankAKod();
	afx_msg void OnEnChangeEditBankBKod();
	afx_msg void OnEnChangeEditReceiverAccount();
	afx_msg void OnEnChangeEditBankcorrKod();
	afx_msg void OnEnChangeEditBankcorrAccount();
	afx_msg void OnCbnSelchangeComboDoccode();
};


