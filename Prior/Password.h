#pragma once


// Password dialog

class Password : public CDialog
{
	DECLARE_DYNAMIC(Password)

public:
	Password(CString templateName, CString pass, CWnd* pParent = NULL);   // standard constructor
	virtual ~Password();
	enum TmpltAction{ TMPLT_ACTION_OPEN = 1, TMPLT_ACTION_NONE = 0 };
	TmpltAction Action;

// Dialog Data
	enum { IDD = IDD_PASSWORD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CString templateName;
	CString templatePassword;
	CString Decode(CString value);
	virtual BOOL OnInitDialog(); 
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
