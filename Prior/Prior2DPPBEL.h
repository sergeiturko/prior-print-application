//
//
//
#pragma once

#ifndef _PRIOR2DPPBEL_H_
#define _PRIOR2DPPBEL_H_

class Prior2DPPBEL : public CFormView
{
protected: // create from serialization only
	DECLARE_DYNCREATE(Prior2DPPBEL)

public:
	Prior2DPPBEL();
	enum{ IDD = IDD_PRIOR2D_PLBEL };

// Attributes
public:
	CPrior2DDoc* GetDocument() const;

// Operations
public:
	virtual void SaveConfigContent(CString strSection, CString password);
	virtual void LoadConfigContent(CString strSection);
	virtual void OnEditCopy();
	virtual void OnEditCut();
	virtual void OnEditPaste();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnFilePrint();
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~Prior2DPPBEL();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckNourgent();
	afx_msg void OnBnClickedCheckUrgent();
private:
	CFont* m_FLarge;
	CFont* m_FSmall;
	CString EditSummcypher;
	void CheckNumber(int);

	int GetBaseAmount();
	bool CheckShowHideAdditionalInfo();
	bool CheckLatin(CString text);
	bool CheckNumber(CString text);
	void ShowHideAdditionalInfo();

	void SaveTemplate(CString strSection);
public:
	afx_msg void OnBnClickedCheckPl();
	afx_msg void OnBnClickedCheckBn();
	afx_msg void OnBnClickedCheckPlBn();
	afx_msg void OnEnChangeEditReceiverAccount();
	afx_msg void OnEnChangeEditSummcypher();
	afx_msg void OnEnChangeEditNumber();
	afx_msg void OnSaveTemplate();
public:
	afx_msg void OnEnChangeEditUnn1();
public:
	afx_msg void OnEnChangeEditUnn2();
public:
	afx_msg void OnEnChangeEditUnn3();
	afx_msg void OnEnChangeEditPayeraccount();
	afx_msg void OnEnChangeEditBankAKod();
	afx_msg void OnEnChangeEditBankBKod();
	afx_msg void OnEnChangeEditBankcorrKod();
	afx_msg void OnEnChangeEditBankcorrAccount();
};

#ifndef _DEBUG  // debug version in Prior2DView.cpp
inline CPrior2DDoc* Prior2DPPBEL::GetDocument() const
   { return reinterpret_cast<CPrior2DDoc*>(m_pDocument); }
#endif

#endif // _PRIOR2DPPBEL_H_