// Prior2DDoc.h : interface of the CPrior2DDoc class
//


#pragma once


class CPrior2DDoc : public CDocument
{
protected: // create from serialization only
	DECLARE_DYNCREATE(CPrior2DDoc)

// Attributes
public:
	CPrior2DDoc();

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CPrior2DDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


