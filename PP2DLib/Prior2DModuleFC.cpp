#pragma once;
#include "stdafx.h"

Prior2DFeeCashModule* Prior2DFeeCashModule::_fcModule = NULL;

/***************************
---===����� ���������===---
****************************/

void Prior2DFeeCashModule::setAccountB(CString &val){AccountB = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setBankCodeB(CString &val){BankCodeB = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setBankNameB(CString &val){BankNameB = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setDate(CString &val){Date = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setNameA(CString &val){NameA = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setNameB(CString &val){NameB = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setNumber(CString &val){Number = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setPaymentPurpose(CString &val)
{
	val.Replace(_T("\n\r"),_T(" "));
	val.Replace(_T("\r\n"),_T(" "));
	val.Replace(_T("\n"),_T(" "));
	val.Replace(_T("\r"),_T(" "));
	PaymentPurpose=val.Trim(_T(" "));
}
void Prior2DFeeCashModule::setSumDigits(CString &val){SumDigits = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setSumSpelled(CString &val){SumSpelled = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setReportCode(CString &val){ReportCode = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setUnp(CString &val){Unp = val.Trim(_T(" "));}
void Prior2DFeeCashModule::setAdditionalInfo(CString &val)
{
	val.Replace(_T("\n\r"),_T(" "));
	val.Replace(_T("\r\n"),_T(" "));
	val.Replace(_T("\n"),_T(" "));
	val.Replace(_T("\r"),_T(" "));
	AdditionalInfo = val.Trim(_T(" "));
}

CString Prior2DFeeCashModule::getAccountB() {return AccountB;}
CString Prior2DFeeCashModule::getBankCodeB() {return BankCodeB;}
CString Prior2DFeeCashModule::getBankNameB() {return BankNameB;}
CString Prior2DFeeCashModule::getDate() {return Date;}
CString Prior2DFeeCashModule::getNameA() {return NameA;}
CString Prior2DFeeCashModule::getNameB() {return NameB;}
CString Prior2DFeeCashModule::getNumber() {return Number;}
CString Prior2DFeeCashModule::getPaymentPurpose() {return PaymentPurpose;}
CString Prior2DFeeCashModule::getSumDigits() {return SumDigits;}
CString Prior2DFeeCashModule::getSumSpelled() {return SumSpelled;}
CString Prior2DFeeCashModule::getReportCode() {return ReportCode;}
CString Prior2DFeeCashModule::getUnp() {return Unp;}
CString Prior2DFeeCashModule::getAdditionalInfo() {return AdditionalInfo;}

CBitmap* Prior2DFeeCashModule::BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw, int ibh)
{
	
	//����� ��������� ������� ����������������� ���� 
	int symLen = 20;//����� �������
	int symHeight = 36;//������ �������
	int xGap = symLen;		//������������ ��� 
	int yGap = symHeight/2;	//������� MoveTo
	int stride;
	char *buf;
	//�������
	int topIndent = 0.008*ibh;		//�������
	int leftIndent = 0.119*ibw;		//�����
	int rightIndent = 0.0476*ibw;	//������
	int bottomIndent = 0.056*ibh;	//������

	int billOffset = (ibh - topIndent - bottomIndent)/3; //������ ��� ���������
	int orderOffset = 2*(ibh - topIndent - bottomIndent)/3; //������ ��� ������

	CBitmap *oldBmp = Utilites::InitializeForPrinting(pDC, pBmp, ibw, ibh, &buf, &stride);
	
	CFont simpleFont;
	simpleFont.CreateFontW(36, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Courier New"));
	CFont underlinedFont;
	underlinedFont.CreateFontW(36, 0, 0, 0, 500, 0, true, 0, 0, 1, 2, 2, 34,_T("Courier New"));
	CFont italicFont;
	italicFont.CreateFontW(28, 0, 0, 0, 500, true, 0, 0, 0, 1, 2, 2, 34,_T("Courier New"));
	CFont miniFont;
	miniFont.CreateFontW(24, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Courier New"));
	
	pDC->SelectObject(new CPen(PS_SOLID, 3, RGB(0,0,0)));
	pDC->SelectObject(&simpleFont);

	RECT r; 
	r.top=0; r.left=0; r.right=ibw; r.bottom=ibh;//2480 3558
	CBrush whiteBrush(RGB(255,255,255));
	pDC->FillRect(&r,&whiteBrush);
/*
	//For debugging only: shows indents on printing document
	pDC->MoveTo(leftIndent - 10, topIndent - 10);
	pDC->LineTo(ibw-rightIndent + 10, topIndent - 10);
	pDC->LineTo(ibw-rightIndent + 10, ibh-bottomIndent + 10);
	pDC->LineTo(leftIndent - 10, ibh-bottomIndent + 10);
	pDC->LineTo(leftIndent - 10, topIndent - 10);
	//******************************************************
*/
	CRect cr = new CRect();
	CString cstr;

	//�������������� �����
	cr.bottom = billOffset + yGap;
	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = billOffset - yGap;
	pDC->MoveTo(leftIndent, billOffset);
	cstr.Format(_T("----------------------------------------------------------------------------------"));
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP);

	cr.bottom = orderOffset + yGap;
	cr.top = orderOffset - yGap;
	pDC->MoveTo(leftIndent, orderOffset);
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP);
	//--------------------------------------------------

	billOffset += symHeight;	//�� ����� ��������� �������
	orderOffset += symHeight;	//����� �������������� ����� !

	//0402510001
	cr.bottom = topIndent + 3*yGap;
	cr.top = topIndent + yGap;
	cr.left = leftIndent + xGap*60;
	cr.right = rightIndent;
	cstr.Format(_T("0402510001"));
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP);

	cr.bottom = billOffset + 3*yGap;
	cr.top = billOffset+yGap;
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP);

	cr.bottom = orderOffset + 3*yGap;
	cr.top = orderOffset + yGap;
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP);
	//----------------------------------------------------

	//����������/���������/����� � ������� � �����
	cr.left = leftIndent;
	cr.right = leftIndent + 36*xGap;
	cr.top = topIndent + 3*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("���������� � %s"), Number);
	pDC->DrawText(cstr, &cr, DT_CENTER);

	cr.top = billOffset + 3*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("��������� � %s"), Number);
	pDC->DrawText(cstr, &cr, DT_CENTER);

	cr.top = orderOffset + 3*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("����� � %s"), Number);
	pDC->DrawText(cstr, &cr, DT_CENTER);

	cr.top = topIndent + 5*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("�� ����� ���������"));
	pDC->DrawText(cstr, &cr, DT_CENTER);

	cr.top = topIndent + 7*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("%s"), Date);
	pDC->DrawText(cstr, &cr, DT_CENTER);

	cr.top = billOffset + 5*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("%s"), Date);
	pDC->DrawText(cstr, &cr, DT_CENTER);

	cr.top = orderOffset + 5*yGap;
	cr.bottom = cr.top + symHeight;
	cstr.Format(_T("%s"), Date);
	pDC->DrawText(cstr, &cr, DT_CENTER);
	//--------------------------------------------

	//��������� �����
	//����������
	pDC->MoveTo(ibw - rightIndent - 30*xGap, topIndent + 8*yGap);
	pDC->LineTo(ibw - rightIndent, topIndent + 8*yGap);
	pDC->LineTo(ibw - rightIndent, topIndent + 24*yGap);
	pDC->LineTo(ibw - rightIndent - 30*xGap, topIndent + 24*yGap);
	pDC->LineTo(ibw - rightIndent - 30*xGap, topIndent + 8*yGap);
	pDC->MoveTo(ibw - rightIndent - 30*xGap, topIndent + 12*yGap);
	pDC->LineTo(ibw - rightIndent, topIndent + 12*yGap);
	
	pDC->MoveTo(leftIndent+ 6*xGap, topIndent + 11*yGap);
	pDC->LineTo(leftIndent+ 20*xGap, topIndent + 11*yGap);
	pDC->LineTo(leftIndent+ 20*xGap, topIndent + 15*yGap);
	pDC->LineTo(leftIndent+ 6*xGap, topIndent + 15*yGap);
	pDC->LineTo(leftIndent+ 6*xGap, topIndent + 11*yGap);

	//���������
	pDC->MoveTo(ibw - rightIndent - 30*xGap, billOffset + 6*yGap);
	pDC->LineTo(ibw - rightIndent, billOffset + 6*yGap);
	pDC->LineTo(ibw - rightIndent, billOffset + 20*yGap);
	pDC->LineTo(ibw - rightIndent - 30*xGap, billOffset + 20*yGap);
	pDC->LineTo(ibw - rightIndent - 30*xGap, billOffset + 6*yGap);
	pDC->MoveTo(ibw - rightIndent - 30*xGap, billOffset + 10*yGap);
	pDC->LineTo(ibw - rightIndent, billOffset + 10*yGap);

	pDC->MoveTo(leftIndent+ 6*xGap, billOffset + 9*yGap);
	pDC->LineTo(leftIndent+ 20*xGap, billOffset + 9*yGap);
	pDC->LineTo(leftIndent+ 20*xGap, billOffset + 13*yGap);
	pDC->LineTo(leftIndent+ 6*xGap, billOffset + 13*yGap);
	pDC->LineTo(leftIndent+ 6*xGap, billOffset + 9*yGap);

	//����� ��������������
	pDC->MoveTo(ibw - rightIndent - 46*xGap, orderOffset + 14*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 14*yGap);
	pDC->MoveTo(ibw - rightIndent - 46*xGap, orderOffset + 18*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 18*yGap);
	pDC->MoveTo(ibw - rightIndent - 58*xGap, orderOffset + 22*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 22*yGap);
	pDC->MoveTo(ibw - rightIndent - 58*xGap, orderOffset + 26*yGap);
	pDC->LineTo(ibw - rightIndent - 46*xGap, orderOffset + 26*yGap);
	pDC->MoveTo(ibw - rightIndent - 23*xGap, orderOffset + 26*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 26*yGap);
	pDC->MoveTo(ibw - rightIndent - 23*xGap, orderOffset + 30*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 30*yGap);
	pDC->MoveTo(ibw - rightIndent - 46*xGap, orderOffset + 34*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 34*yGap);
	pDC->MoveTo(ibw - rightIndent - 23*xGap, orderOffset + 40*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 40*yGap);

	//����� ������������
	pDC->MoveTo(ibw - rightIndent - 58*xGap, orderOffset + 22*yGap);
	pDC->LineTo(ibw - rightIndent - 58*xGap, orderOffset + 26*yGap);
	pDC->MoveTo(ibw - rightIndent - 46*xGap, orderOffset + 14*yGap);
	pDC->LineTo(ibw - rightIndent - 46*xGap, orderOffset + 18*yGap);
	pDC->MoveTo(ibw - rightIndent - 46*xGap, orderOffset + 22*yGap);
	pDC->LineTo(ibw - rightIndent - 46*xGap, orderOffset + 34*yGap);
	pDC->MoveTo(ibw - rightIndent - 23*xGap, orderOffset + 14*yGap);
	pDC->LineTo(ibw - rightIndent - 23*xGap, orderOffset + 40*yGap);
	pDC->MoveTo(ibw - rightIndent - 6*xGap, orderOffset + 18*yGap);
	pDC->LineTo(ibw - rightIndent - 6*xGap, orderOffset + 40*yGap);
	pDC->MoveTo(ibw - rightIndent, orderOffset + 14*yGap);
	pDC->LineTo(ibw - rightIndent, orderOffset + 40*yGap);

	//����� ���
	pDC->MoveTo(leftIndent+ 6*xGap, orderOffset + 9*yGap);
	pDC->LineTo(leftIndent+ 20*xGap, orderOffset + 9*yGap);
	pDC->LineTo(leftIndent+ 20*xGap, orderOffset + 13*yGap);
	pDC->LineTo(leftIndent+ 6*xGap, orderOffset + 13*yGap);
	pDC->LineTo(leftIndent+ 6*xGap, orderOffset + 9*yGap);

	//������
	pDC->MoveTo(leftIndent, ibh - bottomIndent);
	pDC->LineTo(leftIndent+ 20*xGap, ibh - bottomIndent);
	//---------------
	
	//���������� ������ �����
	cr.left = ibw - rightIndent - 29*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = topIndent + 9*yGap;
	cr.bottom = topIndent + 11*yGap;
	pDC->DrawText(AccountB, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 29*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = billOffset + 7*yGap;
	cr.bottom = billOffset + 9*yGap;
	pDC->DrawText(AccountB, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 45*xGap;
	cr.right = ibw - rightIndent - 24*xGap;
	cr.top = orderOffset + 27*yGap;
	cr.bottom = orderOffset + 29*yGap;
	pDC->DrawText(AccountB, &cr, DT_NOCLIP);
	//-----------------------

	//���������� ����� �������
	cr.left = ibw - rightIndent - 29*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = topIndent + 15*yGap;
	cr.bottom = topIndent + 17*yGap;
	cstr.Format(_T("%s="), SumDigits);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 29*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = billOffset + 13*yGap;
	cr.bottom = billOffset + 15*yGap;
	cstr.Format(_T("%s="), SumDigits);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 22*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = orderOffset + 15*yGap;
	cr.bottom = orderOffset + 17*yGap;
	cstr.Format(_T("����� %s="), SumDigits);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);
	//------------------------

	//���������� �������� � ������ "����������"
	cr.left = ibw - rightIndent - 46*xGap;
	cr.right = ibw - rightIndent - 31*xGap;
	cr.top = topIndent + 8*yGap;
	cr.bottom = topIndent + 12*yGap;
	pDC->DrawText(_T("��� ���������� �� ���� �"), &cr, DT_WORDBREAK|DT_RIGHT);

	cr.left = leftIndent;
	cr.right = leftIndent + 4*xGap;
	cr.top = topIndent + 12*yGap;
	cr.bottom = topIndent + 14*yGap;
	pDC->DrawText(_T("���*"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 7*xGap;
	cr.top = topIndent + 16*yGap;
	cr.bottom = topIndent + 18*yGap;
	pDC->DrawText(_T("�� ����"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 15*xGap;
	cr.top = topIndent + 20*yGap;
	cr.bottom = topIndent + 22*yGap;
	pDC->DrawText(_T("���� ����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 10*xGap;
	cr.top = topIndent + 22*yGap;
	cr.bottom = topIndent + 24*yGap;
	pDC->DrawText(_T("����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 9*xGap;
	cr.top = topIndent + 26*yGap;
	cr.bottom = topIndent + 30*yGap;
	pDC->DrawText(_T("����� ��������"), &cr, DT_WORDBREAK);

	cr.left = leftIndent;
	cr.right = leftIndent + 11*xGap;
	cr.top = topIndent + 32*yGap;
	cr.bottom = topIndent + 36*yGap;
	pDC->DrawText(_T("���������� ������"), &cr, DT_WORDBREAK);

	cr.left = ibw - rightIndent - 25*xGap;
	cr.right = ibw - rightIndent - 6*xGap;
	cr.top = topIndent + 33*yGap;
	cr.bottom = topIndent + 35*yGap;
	pDC->DrawText(_T("(��� ��������� �������)"), &cr, DT_NOCLIP);
	
	cr.left = leftIndent;
	cr.right = leftIndent + 32*xGap;
	cr.top = topIndent + 47*yGap;
	cr.bottom = topIndent + 49*yGap;
	pDC->DrawText(_T("������� ��������� ______________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 36*xGap;
	cr.right = leftIndent + 76*xGap;
	cr.top = topIndent + 47*yGap;
	cr.bottom = topIndent + 49*yGap;
	pDC->DrawText(_T("������������� ����������� ______________"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 42*xGap;
	cr.right = ibw - rightIndent;
	cr.top = topIndent + 51*yGap;
	cr.bottom = topIndent + 53*yGap;
	pDC->DrawText(_T("������ ������ ������ ________________"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 29*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = topIndent + 21*yGap;
	cr.bottom = topIndent + 23*yGap;
	pDC->DrawText(_T("����� �������"), &cr, DT_CENTER);

	//-----------------------------------------

	//���������� �������� � ������ "���������"
	cr.left = ibw - rightIndent - 46*xGap;
	cr.right = ibw - rightIndent - 31*xGap;
	cr.top = billOffset + 6*yGap;
	cr.bottom = billOffset + 10*yGap;
	pDC->DrawText(_T("��� ���������� �� ���� �"), &cr, DT_WORDBREAK|DT_RIGHT);

	cr.left = leftIndent;
	cr.right = leftIndent + 4*xGap;
	cr.top = billOffset + 10*yGap;
	cr.bottom = billOffset + 12*yGap;
	pDC->DrawText(_T("���*"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 7*xGap;
	cr.top = billOffset + 14*yGap;
	cr.bottom = billOffset + 16*yGap;
	pDC->DrawText(_T("�� ����"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 15*xGap;
	cr.top = billOffset + 18*yGap;
	cr.bottom = billOffset + 20*yGap;
	pDC->DrawText(_T("���� ����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 10*xGap;
	cr.top = billOffset + 20*yGap;
	cr.bottom = billOffset + 22*yGap;
	pDC->DrawText(_T("����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 9*xGap;
	cr.top = billOffset + 24*yGap;
	cr.bottom = billOffset + 28*yGap;
	pDC->DrawText(_T("����� ��������"), &cr, DT_WORDBREAK);

	cr.left = leftIndent;
	cr.right = leftIndent + 11*xGap;
	cr.top = billOffset + 30*yGap;
	cr.bottom = billOffset + 34*yGap;
	pDC->DrawText(_T("���������� ������"), &cr, DT_WORDBREAK);

	cr.left = ibw - rightIndent - 25*xGap;
	cr.right = ibw - rightIndent - 6*xGap;
	cr.top = billOffset + 33*yGap;
	cr.bottom = billOffset + 35*yGap;
	pDC->DrawText(_T("(��� ��������� �������)"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 5*xGap;
	cr.top = billOffset + 49*yGap;
	cr.bottom = billOffset + 51*yGap;
	pDC->DrawText(_T("�.�."), &cr, DT_NOCLIP);

	cr.left = leftIndent + 12*xGap;
	cr.right = leftIndent + 57*xGap;
	pDC->DrawText(_T("������������� ����������� ___________________"), &cr, DT_NOCLIP);
	
	cr.top = billOffset + 53*yGap;
	cr.bottom = billOffset + 55*yGap;
	pDC->DrawText(_T("������ ������ ������ ________________________"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 29*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cr.top = billOffset + 17*yGap;
	cr.bottom = billOffset + 19*yGap;
	pDC->DrawText(_T("����� �������"), &cr, DT_CENTER);
	//----------------------------------------

	//���������� �������� � ������ "�����"
	cr.left = leftIndent;
	cr.right = leftIndent + 4*xGap;
	cr.top = orderOffset + 10*yGap;
	cr.bottom = orderOffset + 12*yGap;
	pDC->DrawText(_T("���*"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 7*xGap;
	cr.top = orderOffset + 14*yGap;
	cr.bottom = orderOffset + 16*yGap;
	pDC->DrawText(_T("�� ����"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 15*xGap;
	cr.top = orderOffset + 21*yGap;
	cr.bottom = orderOffset + 23*yGap;
	pDC->DrawText(_T("���� ����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 10*xGap;
	cr.top = orderOffset + 27*yGap;
	cr.bottom = orderOffset + 29*yGap;
	pDC->DrawText(_T("����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 11*xGap;
	cr.top = orderOffset + 35*yGap;
	cr.bottom = orderOffset + 39*yGap;
	pDC->DrawText(_T("���������� ������"), &cr, DT_WORDBREAK);

	cr.left = ibw - rightIndent - 46*xGap;
	cr.right = ibw - rightIndent - 23*xGap;
	cr.top = orderOffset + 11*yGap;
	cr.bottom = orderOffset + 13*yGap;
	pDC->DrawText(_T("�����"), &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 23*xGap;
	cr.right = ibw - rightIndent - 6*xGap;
	pDC->DrawText(_T("�����"), &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 45*xGap;
	cr.right = ibw - rightIndent - 23*xGap;
	cr.top = orderOffset + 15*yGap;
	cr.bottom = orderOffset + 17*yGap;
	pDC->DrawText(_T("���� �"), &cr, DT_NOCLIP);

	cr.top = orderOffset + 19*yGap;
	cr.bottom = orderOffset + 21*yGap;
	pDC->DrawText(_T("������"), &cr, DT_CENTER);

	cr.top = orderOffset + 31*yGap;
	cr.bottom = orderOffset + 33*yGap;
	pDC->DrawText(_T("���� �"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 22*xGap;
	cr.right = ibw - rightIndent - 6*xGap;
	cr.top = orderOffset + 19*yGap;
	cr.bottom = orderOffset + 21*yGap;
	pDC->DrawText(_T("�������"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 5*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	pDC->DrawText(_T("���"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 22*xGap;
	cr.right = ibw - rightIndent - 6*xGap;
	cr.top = orderOffset + 35*yGap;
	cr.bottom = orderOffset + 39*yGap;
	pDC->DrawText(_T("���������� �������"), &cr, DT_WORDBREAK);

	cr.left = leftIndent;
	cr.right = leftIndent + 42*xGap;
	cr.top = orderOffset + 57*yGap;
	cr.bottom = orderOffset + 59*yGap;
	pDC->DrawText(_T("������������� ����������� ______________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 46*xGap;
	cr.right = leftIndent + 68*xGap;
	cr.top = orderOffset + 57*yGap;
	cr.bottom = orderOffset + 59*yGap;
	pDC->DrawText(_T("������ ______________"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 43*xGap;
	cr.right = ibw - rightIndent - 24*xGap;
	cr.top = orderOffset + 41*yGap;
	cr.bottom = orderOffset + 43*yGap;
	pDC->DrawText(_T("(��� ��������� �������)"), &cr, DT_NOCLIP);
	//------------------------------------
	
	//��� ����� � ������
	cr.left = ibw - rightIndent - 57*xGap;
	cr.right = ibw - rightIndent - 47*xGap;
	cr.top = orderOffset + 23*yGap;
	cr.bottom = orderOffset + 25*yGap;
	cstr.Format(_T("��� %s"), BankCodeB);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);
	//------------------

	//���. __���.
	cr.left = leftIndent+ 54*xGap;
	cr.right = leftIndent + 67*xGap;
	cr.top = topIndent + 30*yGap;
	cr.bottom = topIndent + 32*yGap;
	pDC->DrawText(_T("���. __ ���."), &cr, DT_NOCLIP);

	cr.left = leftIndent+ 54*xGap;
	cr.right = leftIndent + 67*xGap;
	cr.top = billOffset + 28*yGap;
	cr.bottom = billOffset + 30*yGap;
	pDC->DrawText(_T("���. __ ���."), &cr, DT_NOCLIP);
	//-----------

	//���������� ���
	cr.left = leftIndent + 8*xGap;
	cr.right = leftIndent + 18*xGap;
	cr.top = topIndent + 12*yGap;
	cr.bottom = topIndent + 14*yGap;		
	pDC->DrawText(Unp, &cr, DT_NOCLIP);

	cr.left = leftIndent + 8*xGap;
	cr.right = leftIndent + 18*xGap;;
	cr.top = billOffset + 10*yGap;
	cr.bottom = billOffset + 12*yGap;		
	pDC->DrawText(Unp, &cr, DT_NOCLIP);

	cr.left = leftIndent + 8*xGap;
	cr.right = leftIndent + 18*xGap;;
	cr.top = orderOffset + 10*yGap;
	cr.bottom = orderOffset + 12*yGap;		
	pDC->DrawText(Unp, &cr, DT_NOCLIP);
	//---------------

	//���������� "�� ����"
	pDC->SelectObject(&underlinedFont);
	cr.left = leftIndent+ 8*xGap;
	cr.right = leftIndent + 56*xGap;
	cr.top = topIndent + 16*yGap;
	cr.bottom = topIndent + 20*yGap;
	pDC->DrawText(NameA, &cr, DT_WORDBREAK);

	cr.left = leftIndent+ 8*xGap;
	cr.right = leftIndent + 56*xGap;
	cr.top = billOffset + 14*yGap;
	cr.bottom = billOffset + 18*yGap;
	pDC->DrawText(NameA, &cr, DT_WORDBREAK);

	cr.left = leftIndent+ 8*xGap;
	cr.right = leftIndent + 40*xGap;
	cr.top = orderOffset + 14*yGap;
	cr.bottom = orderOffset + 24*yGap;
	pDC->DrawText(NameA, &cr, DT_WORDBREAK);
	//--------------------

	//���������� "���� ����������"
	cr.left = leftIndent+ 16*xGap;
	cr.right = leftIndent + 50*xGap;
	cr.top = topIndent + 20*yGap;
	cr.bottom = topIndent + 22*yGap;
	pDC->DrawText(BankNameB, &cr, DT_NOCLIP);

	cr.left = leftIndent+ 16*xGap;
	cr.right = leftIndent + 50*xGap;
	cr.top = billOffset + 18*yGap;
	cr.bottom = billOffset + 20*yGap;
	pDC->DrawText(BankNameB, &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 26*xGap;
	cr.top = orderOffset + 23*yGap;
	cr.bottom = orderOffset + 27*yGap;
	pDC->DrawText(BankNameB, &cr, DT_CENTER);
	//--------------------

	//���������� "����������"
	cr.left = leftIndent+ 11*xGap;
	cr.right = leftIndent + 56*xGap;
	cr.top = topIndent + 22*yGap;
	cr.bottom = topIndent + 26*yGap;
	pDC->DrawText(NameB, &cr, DT_WORDBREAK);

	cr.left = leftIndent+ 11*xGap;
	cr.right = leftIndent + 56*xGap;
	cr.top = billOffset + 20*yGap;
	cr.bottom = billOffset + 24*yGap;
	pDC->DrawText(NameB, &cr, DT_WORDBREAK);

	cr.left = leftIndent + 11*xGap;
	cr.right = leftIndent + 39*xGap;
	cr.top = orderOffset + 27*yGap;
	cr.bottom = orderOffset + 35*yGap;
	pDC->DrawText(NameB, &cr, DT_WORDBREAK);
	//--------------------

	//���������� "����� ��������"
	CString SumSpelledFilled = CString(SumSpelled);

	for (int i = 0; i < 132 - SumSpelled.GetLength(); i += 2)
		{
			SumSpelledFilled += _T(" -");
		}

	cr.left = leftIndent+ 9*xGap;
	cr.right = leftIndent + 54*xGap;
	cr.top = topIndent + 26*yGap;
	cr.bottom = topIndent + 32*yGap;
	pDC->DrawText(SumSpelledFilled, &cr, DT_WORDBREAK|DT_EDITCONTROL);

	cr.left = leftIndent+ 9*xGap;
	cr.right = leftIndent + 54*xGap;
	cr.top = billOffset + 24*yGap;
	cr.bottom = billOffset + 30*yGap;
	pDC->DrawText(SumSpelledFilled, &cr, DT_WORDBREAK|DT_EDITCONTROL);
	//--------------------

	//���������� "���������� ������"
	cr.left = leftIndent+ 11*xGap;
	cr.right = leftIndent + 56*xGap;
	cr.top = topIndent + 32*yGap;
	cr.bottom = topIndent + 38*yGap;
	pDC->DrawText(PaymentPurpose, &cr, DT_WORDBREAK);

	cr.left = leftIndent+ 11*xGap;
	cr.right = leftIndent + 56*xGap;
	cr.top = billOffset + 30*yGap;
	cr.bottom = billOffset + 36*yGap;
	pDC->DrawText(PaymentPurpose, &cr, DT_WORDBREAK);

	cr.left = leftIndent+ 11*xGap;
	cr.right = leftIndent + 46*xGap;
	cr.top = orderOffset + 35*yGap;
	cr.bottom = orderOffset + 41*yGap;
	pDC->DrawText(PaymentPurpose, &cr, DT_WORDBREAK);
	//--------------------

	//��� ��������� �������
	cr.left = ibw - rightIndent - 21*xGap;
	cr.right = ibw - rightIndent - 7*xGap;
	cr.top = topIndent + 31*yGap;
	cr.bottom = topIndent + 33*yGap;
	pDC->DrawText(ReportCode, &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 21*xGap;
	cr.right = ibw - rightIndent - 7*xGap;
	cr.top = billOffset + 31*yGap;
	cr.bottom = billOffset + 33*yGap;
	pDC->DrawText(ReportCode, &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 39*xGap;
	cr.right = ibw - rightIndent - 25*xGap;
	cr.top = orderOffset + 39*yGap;
	cr.bottom = orderOffset + 41*yGap;
	pDC->DrawText(ReportCode, &cr, DT_CENTER);
	//--------------

	//�������������� ����������	
	while(AdditionalInfo.GetLength() < 256)
		AdditionalInfo.Insert(AdditionalInfo.GetLength(), ' ');	

	Utilites::MLTextOut(pDC->GetSafeHdc(), leftIndent, topIndent + 36*yGap, ibw-leftIndent-rightIndent, 6*yGap, AdditionalInfo, 4*yGap);

	Utilites::MLTextOut(pDC->GetSafeHdc(), leftIndent, billOffset + 36*yGap, ibw-leftIndent-rightIndent, 6*yGap, AdditionalInfo, 4*yGap);

	Utilites::MLTextOut(pDC->GetSafeHdc(), leftIndent, orderOffset + 45*yGap, ibw-leftIndent-rightIndent, 6*yGap, AdditionalInfo, 4*yGap);	

	pDC->SelectObject(&italicFont);
	CString fio = _T("(�������, ����������� ���, �������� (��� �������), ����� ���������� � (���) ����� ���������� ��� ��������� ���������,");
	CString idDocument = _T("��������������� �������� ��������� (���, ����� � �����, ���� ������, �����, �������� ��������), ���� ��� ����������������� �����)**");

	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = topIndent + 38*yGap;
	cr.bottom = topIndent + 40*yGap;	
	pDC->DrawText(fio, &cr, DT_CENTER);
	cr.top = topIndent + 42*yGap;
	cr.bottom = topIndent + 44*yGap;
	pDC->DrawText(idDocument, &cr, DT_CENTER);

	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = billOffset + 38*yGap;
	cr.bottom = billOffset + 40*yGap;	
	pDC->DrawText(fio, &cr, DT_CENTER);
	cr.top = billOffset + 42*yGap;
	cr.bottom = billOffset + 44*yGap;
	pDC->DrawText(idDocument, &cr, DT_CENTER);

	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = orderOffset + 47*yGap;
	cr.bottom = orderOffset + 49*yGap;	
	pDC->DrawText(fio, &cr, DT_CENTER);
	cr.top = orderOffset + 51*yGap;
	cr.bottom = orderOffset + 53*yGap;
	pDC->DrawText(idDocument, &cr, DT_CENTER);	
	//-----------------------

	//������
	pDC->SelectObject(&miniFont);
	cr.left = leftIndent;
	cr.right = leftIndent + 2*xGap;
	cr.top = ibh - bottomIndent + yGap;
	cr.bottom = ibh - bottomIndent + 3*yGap;	
	pDC->DrawText(_T("*"), &cr, DT_RIGHT);
	cr.top = ibh - bottomIndent + 3*yGap;
	cr.bottom = ibh - bottomIndent + 5*yGap;
	pDC->DrawText(_T("**"), &cr, DT_RIGHT);

	cr.left = leftIndent + 4*xGap;
	cr.right = ibw - rightIndent;
	cr.top = ibh - bottomIndent + yGap;
	cr.bottom = ibh - bottomIndent + 3*yGap;	
	pDC->DrawText(_T("����������� ��������������� ����������������� � �������, ��������������� �����������������."), &cr, DT_NOCLIP);
	cr.top = ibh - bottomIndent + 3*yGap;
	cr.bottom = ibh - bottomIndent + 9*yGap;
	pDC->DrawText(_T("�������, ����������� ���, �������� (��� �������), ����� ���������� � (���) ����� ���������� ��� ��������� ���������, ��������������� �������� ��������� (���, ����� � �����, ���� ������, �����, �������� ��������), ���� ��� ����������������� ����� ����������� � ������� � �������, ��������������� �����������������."), &cr, DT_WORDBREAK);
	//-----------------------

	return pBmp;

}

void PP2DLIB_API FCInit()
{
	Prior2DFeeCashModule::_fcModule = new Prior2DFeeCashModule();
}
void FCFinish()
{
	delete Prior2DFeeCashModule::_fcModule;
}

void FCBuildBarcode(char* sFileName, bool bFull)
{
	int ibw = 2480;
	int ibh = 3508;
	CDC dc;
	CBitmap bmp;
	Prior2DFeeCashModule::_fcModule->BuildBarcode(&dc, &bmp, ibw, ibh);
}

void FCSetAccountB(char* val) {Prior2DFeeCashModule::_fcModule->setAccountB(CString(val));}
void FCSetBankCodeB(char* val) {Prior2DFeeCashModule::_fcModule->setBankCodeB(CString(val));}
void FCSetBankNameB(char* val) {Prior2DFeeCashModule::_fcModule->setBankNameB(CString(val));}
void FCSetDate(char* val) {Prior2DFeeCashModule::_fcModule->setDate(CString(val));}
void FCSetNameA(char* val) {Prior2DFeeCashModule::_fcModule->setNameA(CString(val));}
void FCSetNameB(char* val) {Prior2DFeeCashModule::_fcModule->setNameB(CString(val));}
void FCSetNumber(char* val) {Prior2DFeeCashModule::_fcModule->setNumber(CString(val));}
void FCSetPaymentPurpose(char* val) {Prior2DFeeCashModule::_fcModule->setPaymentPurpose(CString(val));}
void FCSetSumDigits(char* val) {Prior2DFeeCashModule::_fcModule->setSumDigits(CString(val));}
void FCSetSumSpelled(char* val) {Prior2DFeeCashModule::_fcModule->setSumSpelled(CString(val));}
void FCSetReportCode(char* val) {Prior2DFeeCashModule::_fcModule->setReportCode(CString(val));}
void FCSetUnp(char* val) {Prior2DFeeCashModule::_fcModule->setUnp(CString(val));}
void FCSetAdditionalInfo(char* val) {Prior2DFeeCashModule::_fcModule->setAdditionalInfo(CString(val));}