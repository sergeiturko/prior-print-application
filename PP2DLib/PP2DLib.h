//#define BARCODEDEBUG
//#define FORCEBINARY 0/*PDF417_FORCE_BINARY*/
#define ENCODEXML true
//#define RSAENCRYPT 

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PP2DLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PP2DLIB_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef PP2DLIB_EXPORTS
#define PP2DLIB_API __declspec(dllexport)
#else
#define PP2DLIB_API __declspec(dllimport)
#endif

#pragma once;

// This class is exported from the PP2DLib.dll
class PP2DLIB_API CPP2DLib {
public:
	CPP2DLib(void);
	// TODO: add your methods here.
};

extern PP2DLIB_API int nPP2DLib;

PP2DLIB_API int fnPP2DLib(void);
