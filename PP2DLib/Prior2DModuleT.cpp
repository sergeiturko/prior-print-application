#include "stdafx.h"
#include "Prior2DModule.h"

Prior2DPTModule* Prior2DPTModule::_ptModule=NULL;

/****************************************************************************/
/*			��������� ����������                                             */
/****************************************************************************/

void Prior2DPTModule::setAccountA(CString& val){AccountA=val;}
void Prior2DPTModule::setAccountB(CString& val){AccountB=val;}
void Prior2DPTModule::setBankCodeA(CString& val){BankCodeA=val;}
void Prior2DPTModule::setBankCodeB(CString& val){BankCodeB=val;}
void Prior2DPTModule::setBankCBUA(CString& val){BankDivisionA=val;}
void Prior2DPTModule::setBankCBUB(CString& val){BankDivisionB=val;}
void Prior2DPTModule::setBankNameA(CString& val){BankNameA=val;}
void Prior2DPTModule::setBankNameB(CString& val){BankNameB=val;}
void Prior2DPTModule::setNoAccept(CString& val){NoAccept=val;}
void Prior2DPTModule::setBudgetPaymentCode(CString& val){BudgetPaymentCode=val;}
void Prior2DPTModule::setBankCodeC(CString& val){CodeBankaCor=val;}
void Prior2DPTModule::setBankNameC(CString& val){CorrespondentA=val;}
void Prior2DPTModule::setCurrency(CString& val){Currency=val;}
void Prior2DPTModule::setDate(CString& val){Date=val;}
void Prior2DPTModule::setNameA(CString& val){NameA=val;}
void Prior2DPTModule::setNameB(CString& val){NameB=val;}
void Prior2DPTModule::setNumber(CString& val){Number=val;}
void Prior2DPTModule::setPaymentOrder(CString& val){PaymentOrder=val;}
void Prior2DPTModule::setPaymentPurpose(CString& val)
{
	val.Replace(_T("\n\r"),_T(" "));
	val.Replace(_T("\r\n"),_T(" "));
	val.Replace(_T("\n"),_T(" "));
	val.Replace(_T("\r"),_T(" "));
	PaymentPurpose=val;
}
void Prior2DPTModule::setDelayedAccept(CString& val){DelayedAccept=val;}
void Prior2DPTModule::setPredAccept(CString& val){PredAccept=val;}
void Prior2DPTModule::setAccountC(CString& val){SchetBankCor=val;}
void Prior2DPTModule::setSumDigits(CString& val){SumDigits=val;}
void Prior2DPTModule::setSumSpelled(CString& val){SumSpelled=val;}
void Prior2DPTModule::setUnpA(CString& val){UnnA=val;}
void Prior2DPTModule::setUnpB(CString& val){UnnB=val;}
void Prior2DPTModule::setUnpThird(CString& val){UNP_Third=val;}


CString Prior2DPTModule::getAccountA(){return AccountA;}
CString Prior2DPTModule::getAccountB(){return AccountB;}
CString Prior2DPTModule::getBankCodeA(){return BankCodeA;}
CString Prior2DPTModule::getBankCodeB(){return BankCodeB;}
CString Prior2DPTModule::getBankCBUA(){return BankDivisionA;}
CString Prior2DPTModule::getBankCBUB(){return BankDivisionB;}
CString Prior2DPTModule::getBankNameA(){return BankNameA;}
CString Prior2DPTModule::getBankNameB(){return BankNameB;}
CString Prior2DPTModule::getNoAccept(){return NoAccept;}
CString Prior2DPTModule::getBudgetPaymentCode(){return BudgetPaymentCode;}
CString Prior2DPTModule::getBankCodeC(){return CodeBankaCor;}
CString Prior2DPTModule::getBankNameC(){return CorrespondentA;}
CString Prior2DPTModule::getCurrency(){return Currency;}
CString Prior2DPTModule::getDate(){return Date;}
CString Prior2DPTModule::getNameA(){return NameA;}
CString Prior2DPTModule::getNameB(){return NameB;}
CString Prior2DPTModule::getNumber(){return Number;}
CString Prior2DPTModule::getPaymentOrder(){return PaymentOrder;}
CString Prior2DPTModule::getPaymentPurpose(){return PaymentPurpose;}
CString Prior2DPTModule::getDelayedAccept(){return DelayedAccept;}
CString Prior2DPTModule::getPredAccept(){return PredAccept;}
CString Prior2DPTModule::getAccountC(){return SchetBankCor;}
CString Prior2DPTModule::getSumDigits(){return SumDigits;}
CString Prior2DPTModule::getSumSpelled(){return SumSpelled;}
CString Prior2DPTModule::getUnpA(){return UnnA;}
CString Prior2DPTModule::getUnpB(){return UnnB;}
CString Prior2DPTModule::getUnpThird(){return UNP_Third;}



#pragma region Prior2DPTModule::BuildBarcode()
CBitmap* Prior2DPTModule::BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw, int ibh)
{
#pragma region XML
	CString cnt;
	CString xmlDocument;
	Utilites::formatXmlElements(&cnt, 
		_T("AccountA"), &AccountA,
		_T("AccountB"), &AccountB,
		_T("BankCodeA"), &BankCodeA,
		_T("BankCodeB"), &BankCodeB,
		_T("BankDivisionA"), &BankDivisionA,
		_T("BankDivisionB"), &BankDivisionB,
		_T("BankNameA"), &BankNameA,
		_T("BankNameB"), &BankNameB,
		_T("BezAkcepta"), &NoAccept,
		_T("BudgetPaymentCode"), &BudgetPaymentCode,
		_T("CodeBankaCor"), &CodeBankaCor,
		_T("CorrespondentA"), &CorrespondentA,
		_T("Currency"), &Currency,
		_T("Date"), &Date,
		_T("NameA"), &NameA,
		_T("NameB"), &NameB,
		_T("Number"), &Number,
		_T("PaymentOrder"), &PaymentOrder,
		_T("PaymentPurpose"), &PaymentPurpose,
		_T("PosledAkcept"), &DelayedAccept,
		_T("PredvarAkcept"), &PredAccept,
		_T("SchetBankCor"), &SchetBankCor,
		_T("SumDigits"), &SumDigits,
		_T("SumSpelled"), &SumSpelled,
		_T("UnnA"), &UnnA,
		_T("UnnB"), &UnnB,
		_T("UNP_Third"), &UNP_Third, 
		NULL
		);

	xmlDocument.Format(
		_T("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\x0A\x0D")\
		_T("<xfa:datasets xmlns:xfa=\"http://www.xfa.org/schema/xfa-data/1.0/\" >\x0A\x0D")\
		_T("<xfa:data >\x0A\x0D")\
		_T("<form1 Template=\"�������� ����������\">\x0A\x0D")\
		_T("%s")\
		_T("</form1>\x0A\x0D")\
		_T("</xfa:data>\x0A\x0D")\
		_T("</xfa:datasets>\x0A\x0D"),
		cnt
		);

#pragma endregion 
#pragma region Draw
	int iGap = 10, iXGap = 150, iYGap = 200;
	int iCurY = 0;

	int stride;
	char *buf;
	CBitmap *oldBmp = Utilites::InitializeForPrinting(pDC, pBmp, ibw, ibh, &buf, &stride);

	CFont font;
	font.CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));

	CPen* pPen = new CPen(PS_SOLID, 3, RGB(0,0,0));

	pPen = pDC->SelectObject(pPen);
	CFont* oldFont = pDC->SelectObject(&font);

	RECT r; r.top=0; r.left=0; r.right=ibw; r.bottom=ibh;
	CBrush whiteBrush(RGB(255,255,255));
	pDC->FillRect(&r,&whiteBrush);

	pDC->MoveTo(iXGap,iYGap);
	pDC->LineTo(ibw-iXGap, iYGap);

	CRect cr(iXGap+iGap,iYGap+iGap,iXGap+iGap,iYGap+iGap);
	CString cstr;
	cstr.Format(_T("��������� ����������   �  %s"),Number);
	int iht0, iht = pDC->DrawText(cstr, &cr, DT_NOCLIP);

	cr.bottom = ibh-iYGap;

	pDC->MoveTo(iXGap,iYGap+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iYGap+iht+2*iGap);

	pDC->MoveTo(1600,iYGap);
	pDC->LineTo(1600,iYGap+iht+2*iGap);

	cr.left = cr.right = iGap+1600;
	cstr.Format(_T("����    %s"), Date);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	pDC->MoveTo(2050,iYGap);
	pDC->LineTo(2050,iYGap+iht+2*iGap);

	cr.left = cr.right = iGap+2050;
	cstr.Format(_T("0401890033"));
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	iCurY = iYGap+iht+2*iGap;

	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = iXGap+iGap+500;
	cstr.Format(_T("��������������� ������"));
	iht=pDC->DrawText(cstr,cr,DT_CENTER);

	cr.left = iXGap+3*iGap+500;
	cr.right = iXGap+3*iGap+550;
	pDC->DrawText(PredAccept,cr,DT_CENTER);

	cr.left = iXGap+5*iGap+550;
	cr.right = iXGap+5*iGap+1050;
	cstr.Format(_T("����������� ������"));
	pDC->DrawText(cstr,cr,DT_CENTER);

	cr.left = iXGap+7*iGap+1050;
	cr.right = iXGap+7*iGap+1100;
	pDC->DrawText(DelayedAccept,cr,DT_CENTER);

	cr.left = iXGap+9*iGap+1100;
	cr.right = iXGap+9*iGap+1600;
	cstr.Format(_T("��� �������"));
	pDC->DrawText(cstr,cr,DT_CENTER);

	cr.left = iXGap+11*iGap+1600;
	cr.right = iXGap+11*iGap+1650;
	pDC->DrawText(NoAccept,cr,DT_CENTER);

	pDC->MoveTo(iXGap+2*iGap+500,iCurY);
	pDC->LineTo(iXGap+2*iGap+500,iCurY+2*iGap+iht);

	pDC->MoveTo(iXGap+4*iGap+550,iCurY);
	pDC->LineTo(iXGap+4*iGap+550,iCurY+2*iGap+iht);

	pDC->MoveTo(iXGap+6*iGap+1050,iCurY);
	pDC->LineTo(iXGap+6*iGap+1050,iCurY+2*iGap+iht);

	pDC->MoveTo(iXGap+8*iGap+1100,iCurY);
	pDC->LineTo(iXGap+8*iGap+1100,iCurY+2*iGap+iht);

	pDC->MoveTo(iXGap+10*iGap+1600,iCurY);
	pDC->LineTo(iXGap+10*iGap+1600,iCurY+2*iGap+iht);

	pDC->MoveTo(iXGap+12*iGap+1650,iCurY);
	pDC->LineTo(iXGap+12*iGap+1650,iCurY+2*iGap+iht);

	iCurY+=2*iGap+iht;

	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXGap,iCurY);


	cr.left = iXGap+iGap;
	cr.right = ibw-iXGap - iGap;
	cr.top = iCurY+iGap;

	cstr.Format(_T("����� � ������:     %s"), SumSpelled);
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK);

	iCurY += iht+2*iGap;

	cr.left = ibw/2;
	cr.right = ibw/2+200;
	cr.top = iCurY+iGap;
	cstr.Format(_T("��� ������"));
	iht0 = iht = pDC->DrawText(cstr,cr,DT_WORDBREAK);

	cr.left = ibw/2+200+2*iGap;
	cr.right = ibw/2+200+2*iGap+200;
	cr.top = iCurY+2*iGap;

	iht = pDC->DrawText(Currency,cr,DT_CENTER)+iGap;
	iht0 = iht>iht0?iht:iht0;

	cr.left = ibw/2+400+4*iGap;
	cr.right = ibw/2+400+2*iGap+200;
	cr.top = iCurY+iGap;
	cstr.Format(_T("����� �������"));
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK);
	iht0 = iht>iht0?iht:iht0;

	cr.left = ibw/2+600+6*iGap;
	cr.right = ibw-iXGap;
	cr.top = iCurY+2*iGap;

	iht = pDC->DrawText(SumDigits,cr,DT_CENTER)+iGap;
	iht0 = iht>iht0?iht:iht0;
	iht0 += 2*iGap;

	pDC->MoveTo(ibw/2-iGap,iCurY);
	pDC->LineTo(ibw-iXGap, iCurY);

	pDC->MoveTo(iXGap,iCurY+iht0);
	pDC->LineTo(ibw-iXGap, iCurY+iht0);

	pDC->MoveTo(ibw/2-iGap, iCurY);
	pDC->LineTo(ibw/2-iGap, iCurY+iht0);

	pDC->MoveTo(ibw/2+200+iGap, iCurY);
	pDC->LineTo(ibw/2+200+iGap, iCurY+iht0);

	pDC->MoveTo(ibw/2+400+3*iGap, iCurY);
	pDC->LineTo(ibw/2+400+3*iGap, iCurY+iht0);

	pDC->MoveTo(ibw/2+600+5*iGap, iCurY);
	pDC->LineTo(ibw/2+600+5*iGap, iCurY+iht0);

	iCurY += iht0;

	//����������

	cstr.Format(_T("����������:    %s"),NameA);
	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXGap-iGap;
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK);

	pDC->MoveTo(ibw/2-iGap, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	iCurY += iht+2*iGap;

	cr.top = iCurY+iGap;
	cr.left = ibw/2;
	cr.right = ibw/2+200;
	cstr.Format(_T("���� �"));
	iht = pDC->DrawText(cstr,cr,DT_CENTER);

	cr.left = ibw/2+200+2*iGap;
	cr.right = ibw-iXGap-iGap;
	pDC->DrawText(AccountA,cr,DT_CENTER);

	pDC->MoveTo(iXGap, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2-iGap, iCurY);
	pDC->LineTo(ibw/2-iGap, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2+200+iGap, iCurY);
	pDC->LineTo(ibw/2+200+iGap, iCurY+iht+2*iGap);

	iCurY += iht+2*iGap;

	//BANK A

	cr.left = iXGap+iGap;
	cr.right = ibw - iXGap - iGap;
	cr.top = iCurY + iGap;
	cstr.Format(_T("����-�����������:      %s"), BankNameA);
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK);

	pDC->MoveTo(ibw/2-iGap+150, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	iCurY += iht+2*iGap;
	cr.top = iCurY+iGap;
	cr.left = ibw/2+150;
	cr.right = ibw/2 + 200+150;
	cstr.Format(_T("��� �����"));
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_CENTER);

	cr.left = ibw/2 + 200 + iGap*2+150;
	cr.right = ibw/2 + 800+ iGap*2;
	pDC->DrawText(BankCodeA, cr, DT_CENTER);

	cr.left = ibw/2 + 800 + iGap*4;
	cr.right = ibw - iGap-iXGap;
	pDC->DrawText(BankDivisionA, cr, DT_CENTER);

	pDC->MoveTo(ibw/2-iGap+150, iCurY);
	pDC->LineTo(ibw/2-iGap+150, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2+200+iGap+150, iCurY);
	pDC->LineTo(ibw/2+200+iGap+150, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2+800+3*iGap, iCurY);
	pDC->LineTo(ibw/2+800+3*iGap, iCurY+iht+2*iGap);

	iCurY+=iht+2*iGap;

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXGap, iCurY);

	//������������� �����-����������
	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = iXGap+400;
	cstr.Format(_T("������������� �����-����������:"));
	iht0 = pDC->DrawText(cstr, cr, DT_WORDBREAK);

	cr.left = iXGap+500;
	cr.right = iXGap+700;
	cstr.Format(_T("��� �����"));
	iht = pDC->DrawText(cstr, cr, DT_CENTER);

	cr.left = iXGap+700+2*iGap;
	cr.right = iXGap+1100+2*iGap;
	pDC->DrawText(CodeBankaCor, cr, DT_CENTER);

	cr.left = iXGap+1100+4*iGap;
	cr.right = iXGap+1300+4*iGap;
	cstr.Format(_T("���� �"));
	pDC->DrawText(cstr, cr, DT_CENTER);

	cr.left = iXGap+1300+6*iGap;
	cr.right = ibw-iXGap-iGap;
	pDC->DrawText(SchetBankCor, cr, DT_CENTER);

	pDC->MoveTo(iXGap+500-iGap,iCurY);
	pDC->LineTo(iXGap+500-iGap,iCurY+iht+2*iGap);

	pDC->MoveTo(iXGap+700+iGap,iCurY);
	pDC->LineTo(iXGap+700+iGap,iCurY+iht+2*iGap);

	pDC->MoveTo(iXGap+1100+3*iGap,iCurY);
	pDC->LineTo(iXGap+1100+3*iGap,iCurY+iht+2*iGap);

	pDC->MoveTo(iXGap+1300+5*iGap,iCurY);
	pDC->LineTo(iXGap+1300+5*iGap,iCurY+iht+2*iGap);

	pDC->MoveTo(iXGap+500-iGap,iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap,iCurY+iht+2*iGap);

	cr.top = iCurY+iht0+2*iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXGap-iGap;
	iht = pDC->DrawText(CorrespondentA, cr, DT_WORDBREAK);

	iCurY += iht+iht0+4*iGap;

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXGap, iCurY);

	//BANK B
	cr.left = iXGap+iGap;
	cr.right = ibw - iXGap - iGap;
	cr.top = iCurY + iGap;
	cstr.Format(_T("����-����������:      %s"), BankNameB);
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK);

	pDC->MoveTo(ibw/2-iGap+150, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	iCurY += iht+2*iGap;
	cr.top = iCurY+iGap;
	cr.left = ibw/2+150;
	cr.right = ibw/2 + 200+150;
	cstr.Format(_T("��� �����"));
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_CENTER);

	cr.left = ibw/2 + 200 + iGap*2+150;
	cr.right = ibw/2 + 800+ iGap*2;
	pDC->DrawText(BankCodeB, cr, DT_CENTER);

	cr.left = ibw/2 + 800 + iGap*4;
	cr.right = ibw - iXGap-iGap;
	pDC->DrawText(BankDivisionB, cr, DT_CENTER);

	pDC->MoveTo(ibw/2-iGap+150, iCurY);
	pDC->LineTo(ibw/2-iGap+150, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2+200+iGap+150, iCurY);
	pDC->LineTo(ibw/2+200+iGap+150, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2+800+3*iGap, iCurY);
	pDC->LineTo(ibw/2+800+3*iGap, iCurY+iht+2*iGap);

	iCurY+=iht+2*iGap;

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXGap, iCurY);

	//����������
	cstr.Format(_T("����������:    %s"),NameB);
	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXGap-iGap;
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK);

	pDC->MoveTo(ibw/2-iGap, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	iCurY += iht+2*iGap;

	cr.top = iCurY+iGap;
	cr.left = ibw/2;
	cr.right = ibw/2+200;
	cstr.Format(_T("���� �"));
	iht = pDC->DrawText(cstr,cr,DT_CENTER);

	cstr.Format(_T("%s"),AccountB);

	cr.left = ibw/2+200+2*iGap;
	cr.right = ibw-iXGap-iGap;
	pDC->DrawText(cstr,cr,DT_CENTER);

	pDC->MoveTo(iXGap, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2-iGap, iCurY);
	pDC->LineTo(ibw/2-iGap, iCurY+iht+2*iGap);

	pDC->MoveTo(ibw/2+200+iGap, iCurY);
	pDC->LineTo(ibw/2+200+iGap, iCurY+iht+2*iGap);

	iCurY += iht+2*iGap;

	//PURPOSE
	cstr.Format(_T("���������� �������:       %s"),PaymentPurpose);
	cr.left = iXGap + iGap;
	cr.right = ibw - iXGap - iGap;
	cr.top = iCurY+iGap;
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK);

	iCurY+=2*iGap+(iht<180?180:iht);
	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXGap, iCurY);

	//UNNs

	int iUNNWidth = (ibw-2*iXGap-200-2*iGap)/4-2*iGap;
	cr.left = iXGap+iGap;
	cr.right = iXGap+iGap+iUNNWidth;
	cr.top = iCurY+iGap;
	iht = pDC->DrawText(_T("��� �����������"), cr, DT_CENTER);

	pDC->MoveTo(iXGap, iCurY+iht+2*iGap);
	pDC->LineTo(ibw-iXGap, iCurY+iht+2*iGap);

	cr.top = iCurY+3*iGap+iht;
	iht0 = pDC->DrawText(UnnA, cr, DT_CENTER);
	iht0 = iht>iht0?iht:iht0;

	cr.left = iXGap+3*iGap+iUNNWidth;
	cr.right = iXGap+3*iGap+2*iUNNWidth;
	cr.top = iCurY+iGap;
	pDC->DrawText(_T("��� �����������"), cr, DT_CENTER);
	cr.top = iCurY+3*iGap+iht;
	pDC->DrawText(UnnB, cr, DT_CENTER);

	cr.left = iXGap+5*iGap+2*iUNNWidth;
	cr.right = iXGap+5*iGap+3*iUNNWidth;
	cr.top = iCurY+iGap;
	pDC->DrawText(_T("��� �������� ����"), cr, DT_CENTER);
	cr.top = iCurY+3*iGap+iht;
	pDC->DrawText(UNP_Third, cr, DT_CENTER);

	cr.left = iXGap+7*iGap+3*iUNNWidth;
	cr.right = iXGap+7*iGap+4*iUNNWidth;
	cr.top = iCurY+iGap;
	pDC->DrawText(_T("��� �������"), cr, DT_CENTER);
	cr.top = iCurY+3*iGap+iht;
	pDC->DrawText(BudgetPaymentCode, cr, DT_CENTER);

	cr.left = iXGap+9*iGap+4*iUNNWidth;
	cr.right = iXGap+9*iGap+4*iUNNWidth+200;
	cr.top = iCurY+iGap;
	pDC->DrawText(_T("�������"), cr, DT_CENTER);
	cr.top = iCurY+3*iGap+iht;
	pDC->DrawText(PaymentOrder, cr, DT_CENTER);

	iht+=iht0;
	pDC->MoveTo(iXGap+2*iGap+iUNNWidth, iCurY);
	pDC->LineTo(iXGap+2*iGap+iUNNWidth, iCurY+iht+4*iGap);

	pDC->MoveTo(iXGap+4*iGap+2*iUNNWidth, iCurY);
	pDC->LineTo(iXGap+4*iGap+2*iUNNWidth, iCurY+iht+4*iGap);

	pDC->MoveTo(iXGap+6*iGap+3*iUNNWidth, iCurY);
	pDC->LineTo(iXGap+6*iGap+3*iUNNWidth, iCurY+iht+4*iGap);

	pDC->MoveTo(iXGap+8*iGap+4*iUNNWidth, iCurY);
	pDC->LineTo(iXGap+8*iGap+4*iUNNWidth, iCurY+iht+4*iGap);

	iCurY += iht+4*iGap;

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw - iXGap, iCurY);

	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw/2-iGap;
	iht = pDC->DrawText(_T("������� �����������(����������)"),cr,0);

	cr.top = iCurY+3*iGap+iht+50;
	pDC->DrawText(_T("�.�."),cr,0);

	cr.top = iCurY+iGap;
	cr.left = ibw/2+iGap;
	cr.right = ibw-iXGap-iGap;
	pDC->DrawText(_T("����������� ������-�����������"),cr,0);

	cr.top = iCurY+2*iGap+iht+50;
	pDC->DrawText(_T("���� ������"),cr,0);

	cr.top = iCurY+4*iGap+2*iht+50;
	pDC->DrawText(_T("�������"),cr,0);

	cr.top = iCurY+6*iGap+3*iht+100;
	pDC->DrawText(_T("����� �����"),cr,0);

	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(iXGap,iCurY+8*iGap+4*iht+200);

	pDC->MoveTo(ibw-iXGap,iCurY);
	pDC->LineTo(ibw-iXGap,iCurY+8*iGap+4*iht+200);

	pDC->MoveTo(ibw/2,iCurY);
	pDC->LineTo(ibw/2,iCurY+8*iGap+4*iht+200);

	iCurY+=8*iGap+4*iht+200;

	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXGap,iCurY);

	//����������� ������
	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXGap-iGap;
	iht = pDC->DrawText(_T("����������� ������-������������:"),cr,0);

	iCurY+=2*iGap+iht;

	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXGap,iCurY);

	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = iXGap+iGap+400;
	iht = pDC->DrawText(_T("����� �����"),cr,DT_CENTER);

	pDC->MoveTo(iXGap+2*iGap+400,iCurY);
	pDC->LineTo(iXGap+2*iGap+400,iCurY+4*iGap+2*iht);

	cr.left = iXGap+3*iGap+400;
	cr.right = iXGap+3*iGap+800;
	pDC->DrawText(_T("������ �����"),cr,DT_CENTER);

	pDC->MoveTo(iXGap+4*iGap+800,iCurY);
	pDC->LineTo(iXGap+4*iGap+800,iCurY+4*iGap+2*iht);

	cr.left = iXGap+5*iGap+800;
	cr.right = iXGap+5*iGap+1050;
	pDC->DrawText(_T("��� ������"),cr,DT_CENTER);

	pDC->MoveTo(iXGap+6*iGap+1050,iCurY);
	pDC->LineTo(iXGap+6*iGap+1050,iCurY+4*iGap+2*iht);

	cr.left = iXGap+7*iGap+1050;
	cr.right = iXGap+7*iGap+1400;
	pDC->DrawText(_T("����� ��������"),cr,DT_CENTER);

	pDC->MoveTo(iXGap+8*iGap+1400,iCurY);
	pDC->LineTo(iXGap+8*iGap+1400,iCurY+4*iGap+2*iht);

	cr.left = iXGap+9*iGap+1400;
	cr.right = ibw-iXGap-iGap;
	pDC->DrawText(_T("���������� � ����������� ������"),cr,DT_CENTER);

	pDC->MoveTo(iXGap,iCurY+2*iGap+iht);
	pDC->LineTo(ibw-iXGap, iCurY+2*iGap+iht);

	pDC->MoveTo(iXGap,iCurY+4*iGap+2*iht);
	pDC->LineTo(ibw-iXGap, iCurY+4*iGap+2*iht);

	iCurY+=4*iGap+2*iht;
	pDC->MoveTo(iXGap, iYGap);
	pDC->LineTo(iXGap, iCurY);
	pDC->MoveTo(ibw-iXGap, iYGap);
	pDC->LineTo(ibw-iXGap, iCurY);

	cr.top = iCurY+iGap;
	cr.left = iXGap;
	cr.right = ibw/2-iGap;
	iht = pDC->DrawText(_T("���� �����������"),cr,0);

	cr.top = iCurY+3*iGap+iht;
	pDC->DrawText(_T("���� �������������� �������"),cr,0);

	cr.top = iCurY+5*iGap+2*iht;
	pDC->DrawText(_T("���� ����������"),cr,0);

	cr.top = iCurY+iGap;
	cr.left = ibw/2;
	cr.right = ibw-iXGap;
	pDC->DrawText(_T("������� �����������"),cr,0);

	cr.top = iCurY+2*iGap+iht;
	pDC->DrawText(_T("���� ���������� ������"),cr,0);

	cr.top = iCurY+4*iGap+2*iht;
	pDC->DrawText(_T("����� �����"),cr,0);

	iCurY+=6*iGap+3*iht;

	pPen = pDC->SelectObject(pPen);
	delete pPen;

	pDC->SelectObject(oldFont);


#pragma endregion
	pDC->SelectObject(oldBmp);
	BarcodePainter bp(buf, iXGap, iCurY+iYGap, ibw-2*iXGap, ibh-2*iYGap-iCurY, stride);
	bp.DrawXmlBarcodes(xmlDocument);
	pDC->SelectObject(pBmp);

	return pBmp;
}
#pragma endregion

void PTInit()
{
	Prior2DPTModule::_ptModule = new Prior2DPTModule();
}
void PTFinish()
{
	delete Prior2DPTModule::_ptModule;
}

void PTBuildBarcode(char* sFileName)
{
	int ibw = 2480;
	int ibh = 3508;
	CDC dc;
	CBitmap bmp;
	Prior2DPTModule::_ptModule->BuildBarcode(&dc, &bmp, ibw, ibh);
}


void PTSetAccountA(char* val){Prior2DPTModule::_ptModule->setAccountA(CString(val));}
void PTSetAccountB(char* val){Prior2DPTModule::_ptModule->setAccountB(CString(val));}
void PTSetBankCodeA(char* val){Prior2DPTModule::_ptModule->setBankCodeA(CString(val));}
void PTSetBankCodeB(char* val){Prior2DPTModule::_ptModule->setBankCodeB(CString(val));}
void PTSetBankCBUA(char* val){Prior2DPTModule::_ptModule->setBankCBUA(CString(val));}
void PTSetBankCBUB(char* val){Prior2DPTModule::_ptModule->setBankCBUB(CString(val));}
void PTSetBankNameA(char* val){Prior2DPTModule::_ptModule->setBankNameA(CString(val));}
void PTSetBankNameB(char* val){Prior2DPTModule::_ptModule->setBankNameB(CString(val));}
void PTSetNoAccept(char* val){Prior2DPTModule::_ptModule->setNoAccept(CString(val));}
void PTSetBudgetPaymentCode(char* val){Prior2DPTModule::_ptModule->setBudgetPaymentCode(CString(val));}
void PTSetBankCodeC(char* val){Prior2DPTModule::_ptModule->setBankCodeC(CString(val));}
void PTSetBankNameC(char* val){Prior2DPTModule::_ptModule->setBankNameC(CString(val));}
void PTSetCurrency(char* val){Prior2DPTModule::_ptModule->setCurrency(CString(val));}
void PTSetDate(char* val){Prior2DPTModule::_ptModule->setDate(CString(val));}
void PTSetNameA(char* val){Prior2DPTModule::_ptModule->setNameA(CString(val));}
void PTSetNameB(char* val){Prior2DPTModule::_ptModule->setNameB(CString(val));}
void PTSetNumber(char* val){Prior2DPTModule::_ptModule->setNumber(CString(val));}
void PTSetPaymentOrder(char* val){Prior2DPTModule::_ptModule->setPaymentOrder(CString(val));}
void PTSetPaymentPurpose(char* val){Prior2DPTModule::_ptModule->setPaymentPurpose(CString(val));}
void PTSetDelayedAccept(char* val){Prior2DPTModule::_ptModule->setDelayedAccept(CString(val));}
void PTSetPredAccept(char* val){Prior2DPTModule::_ptModule->setPredAccept(CString(val));}
void PTSetAccountC(char* val){Prior2DPTModule::_ptModule->setAccountC(CString(val));}
void PTSetSumDigits(char* val){Prior2DPTModule::_ptModule->setSumDigits(CString(val));}
void PTSetSumSpelled(char* val){Prior2DPTModule::_ptModule->setSumSpelled(CString(val));}
void PTSetUnpA(char* val){Prior2DPTModule::_ptModule->setUnpA(CString(val));}
void PTSetUnpB(char* val){Prior2DPTModule::_ptModule->setUnpB(CString(val));}
void PTSetUnpThird(char* val){Prior2DPTModule::_ptModule->setUnpThird(CString(val));}
