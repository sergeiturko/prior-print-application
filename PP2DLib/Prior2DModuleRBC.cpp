#pragma once;
#include "stdafx.h"

xParams rbcxp;

Prior2DReceiveBelCashModule* Prior2DReceiveBelCashModule::_rbcModule = NULL;

/**************************************
---===������ ����������� ��������===---
***************************************/

void Prior2DReceiveBelCashModule::setName(CString &val){Name = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setApplicationNumber(CString &val){ApplicationNumber = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setApplicationDate(SYSTEMTIME val){ApplicationDate = val;}
void Prior2DReceiveBelCashModule::setSumDigits(CString &val){SumDigits = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setSumSpelled(CString &val){SumSpelled = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setPriority(CString &val){Priority = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setAccount(CString &val){Account = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setRepresentative(CString &val){Representative = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setDocType(CString &val){DocType = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setDocSeria(CString &val){DocSeria = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setDocNumber(CString &val){DocNumber = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setDocIssueDate(SYSTEMTIME val){DocIssueDate = val;}
void Prior2DReceiveBelCashModule::setDocIssueCompany(CString &val){DocIssueCompany = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setAnnex(CString &val){Annex = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setAnnexPageCount(CString &val){AnnexPageCount = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setPurposesCount(int val){PurposesCount = val;}
//1
void Prior2DReceiveBelCashModule::setCode1(CString &val){Code1 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setPurpose1(CString &val){Purpose1 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setSum1(CString &val){Sum1 = val.Trim(_T(" "));}
//2
void Prior2DReceiveBelCashModule::setCode2(CString &val){Code2 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setPurpose2(CString &val){Purpose2 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setSum2(CString &val){Sum2 = val.Trim(_T(" "));}
//3
void Prior2DReceiveBelCashModule::setCode3(CString &val){Code3 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setPurpose3(CString &val){Purpose3 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setSum3(CString &val){Sum3 = val.Trim(_T(" "));}
//4
void Prior2DReceiveBelCashModule::setCode4(CString &val){Code4 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setPurpose4(CString &val){Purpose4 = val.Trim(_T(" "));}
void Prior2DReceiveBelCashModule::setSum4(CString &val){Sum4 = val.Trim(_T(" "));}

CBitmap* Prior2DReceiveBelCashModule::BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw, int ibh, bool bGlobalPrintBarcode)
{
	char *data = (char*)malloc(2048);
	ZeroMemory(&rbcxp, sizeof(xParams));
	rbcxp.data = data;

	CString applicationDate, docIssueDate;
	applicationDate.Format(_T("%.2i.%.2i.%i"), ApplicationDate.wDay, ApplicationDate.wMonth, ApplicationDate.wYear);
	docIssueDate.Format(_T("%.2i.%.2i.%i"), DocIssueDate.wDay, DocIssueDate.wMonth, DocIssueDate.wYear);

	// bfull = true??
	Utilites::formatXmlElements(rbcxp.data, rbcxp.datalen, true, 0, 
		1 , &ApplicationNumber,							// ����� ��������
		2 , &applicationDate,								// ���� ���������
		3 , &Name,
		4 , &SumDigits,
		5 , &SumSpelled,
		6 , &Priority,
		7 , &Account,
		8 , &Representative,
		9 , &DocType,
		10, &DocSeria,
		11, &DocNumber,
		12, &docIssueDate,
		13, &DocIssueCompany,
		14, &Annex,
		15, &AnnexPageCount,
		//1		
		16, &Purpose1,
		17, &Code1,
		18, &Sum1,
		//2
		19, &Purpose2,
		20, &Code2,
		21, &Sum2,
		//3
		22, &Purpose3,
		23, &Code3,
		24, &Sum3,
		//4
		25, &Purpose4,
		26, &Code4,
		27, &Sum4,
		NULL, NULL
		);

	//����� ��������� ������� ����������������� ���� 
	int symLen = 20;//����� �������
	int symHeight = 42;//������ �������
	int xGap = symLen;		//������������ ��� 
	int yGap = symHeight/2;	//������� MoveTo
	int stride;
	char *buf;

	//�������	
	int topIndent = 0.03*ibh;		//�������
	int leftIndent = 0.04*ibw;		//�����
	int rightIndent = 0.02*ibw;	//������
	int bottomIndent = 0.03*ibh;	//������

	CBitmap *oldBmp = Utilites::InitializeForPrinting(pDC, pBmp, ibw, ibh, &buf, &stride);
	
	CFont simpleFont;
	simpleFont.CreateFontW(42, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	CFont underlinedFont;
	underlinedFont.CreateFontW(42, 0, 0, 0, 500, 0, true, 0, 0, 1, 2, 2, 34,_T("Arial"));
	CFont italicFont;
	italicFont.CreateFontW(38, 0, 0, 0, 500, true, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	CFont miniFont;
	miniFont.CreateFontW(38, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	
	pDC->SelectObject(new CPen(PS_SOLID, 3, RGB(0,0,0)));
	pDC->SelectObject(&simpleFont);

	RECT r; 
	r.top=0; r.left=0; r.right=ibw; r.bottom=ibh;//2480 3558
	CBrush whiteBrush(RGB(255,255,255));
	pDC->FillRect(&r,&whiteBrush);

	//For debugging only: shows indents on printing document
	/*pDC->MoveTo(leftIndent - 10, topIndent - 10);
	pDC->LineTo(ibw-rightIndent + 10, topIndent - 10);
	pDC->LineTo(ibw-rightIndent + 10, ibh-bottomIndent + 10);
	pDC->LineTo(leftIndent - 10, ibh-bottomIndent + 10);
	pDC->LineTo(leftIndent - 10, topIndent - 10);*/
	//******************************************************

	CRect cr = new CRect();
	CString cstr;

	cstr.Format(_T("���������� ���, ��� 749"));
	cr.top = topIndent;
	cr.bottom = topIndent + 2*ibh;
	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP);

	//while(Name.GetLength() < 256)
	//	Name.Insert(Name.GetLength(), ' ');	

	pDC->SelectObject(&underlinedFont);

	//Utilites::MLTextOut(pDC->GetSafeHdc(), leftIndent + 0.25*(ibw-leftIndent-rightIndent), topIndent + 4*yGap, 0.5*(ibw-leftIndent-rightIndent), 6*yGap, Name, 4*yGap);	
	//Utilites::MLTextOutRightAlign(pDC->GetSafeHdc(), leftIndent + 0.45*(ibw-leftIndent-rightIndent), topIndent + 4*yGap, 0.5*(ibw-leftIndent-rightIndent), 6*yGap, Name, 4*yGap);	
	Utilites::TwoLinesTextOut(pDC->GetSafeHdc(), leftIndent + 0.45*(ibw-leftIndent-rightIndent), topIndent + 4*yGap, 0.5*(ibw-leftIndent-rightIndent), 6*yGap, Name, 4*yGap);	

	pDC->SelectObject(&italicFont);
	CString nameText1 = _T("(������������ ������������ ����, �������������,");
	CString nameText2 = _T("�������� � ������� ��������������� ���������������)");

	cr.left = 0.5*(ibw-leftIndent-rightIndent);
	cr.right = ibw - rightIndent;
	cr.top = topIndent + 6*yGap;
	cr.bottom = topIndent + 8*yGap;	
	pDC->DrawText(nameText1, &cr, DT_CENTER);
	cr.top = topIndent + 10*yGap;
	cr.bottom = topIndent + 12*yGap;
	pDC->DrawText(nameText2, &cr, DT_CENTER);

	pDC->SelectObject(&simpleFont);

	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cstr.Format(_T("��������� � %s"), ApplicationNumber);
	cr.top = topIndent + 14*yGap;
	cr.bottom = topIndent + 16*yGap;
	pDC->DrawText(cstr, &cr, DT_CENTER);
	cstr.Format(_T("�� ��������� �������� ����������� ������ � ������� (���������) ���������� ������"));
	cr.top = topIndent + 16*yGap;
	cr.bottom = topIndent + 18*yGap;
	pDC->DrawText(cstr, &cr, DT_CENTER);	
	cstr.Format(_T("�� �%i� %s %i �.*"), ApplicationDate.wDay, Utilites::NumserOfMonthToString(ApplicationDate.wMonth), ApplicationDate.wYear);
	cr.top = topIndent + 18*yGap;
	cr.bottom = topIndent + 20*yGap;
	pDC->DrawText(cstr, &cr, DT_CENTER);
	cr.top = topIndent + 20*yGap;
	cr.bottom = topIndent + 22*yGap;
	cstr.Format(_T("������ ������:"));
	pDC->DrawText(cstr, &cr, DT_LEFT);

	int tableWidth = 0.95*(ibw - leftIndent - rightIndent);
	int tableIndent = leftIndent + xGap;

	//����� �������
	int sumColumn = 0.16*tableWidth;
	pDC->MoveTo(tableIndent, topIndent + 23*yGap);
	pDC->LineTo(tableIndent + tableWidth, topIndent + 23*yGap);
	pDC->LineTo(tableIndent + tableWidth, topIndent + 33*yGap);
	pDC->LineTo(tableIndent, topIndent  + 33*yGap);
	pDC->LineTo(tableIndent,  topIndent + 23*yGap);

	pDC->MoveTo(tableIndent, topIndent + 27*yGap);
	pDC->LineTo(tableIndent + tableWidth, topIndent + 27*yGap);

	pDC->MoveTo(tableIndent + sumColumn, topIndent + 23*yGap);
	pDC->LineTo(tableIndent + sumColumn, topIndent + 33*yGap);
	
	
	cr.left = tableIndent + xGap;
	cr.right = tableIndent + sumColumn;
	cr.top = topIndent + 24*yGap;
	cr.bottom = topIndent + 26*yGap;	
	cstr.Format(_T("����� �������"));
	pDC->DrawText(cstr, &cr, DT_CENTER);
	cr.left = tableIndent + xGap + sumColumn;
	cr.right = tableIndent + tableWidth;
	cstr.Format(_T("����� ��������"));
	pDC->DrawText(cstr, &cr, DT_CENTER);
	cr.left = tableIndent + xGap;
	cr.right = tableIndent + sumColumn;
	cr.top = topIndent + 28*yGap;
	cr.bottom = topIndent + 32*yGap;	
	pDC->DrawText(SumDigits, &cr, DT_CENTER|DT_VCENTER);
	cr.left = tableIndent + xGap + sumColumn;
	cr.right = tableIndent + tableWidth;
	pDC->DrawText(SumSpelled, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	//-------------------------------------

	//��������� �����
	int column1 = 0.2*tableWidth;
	int column2 = 0.8*tableWidth;
	int sumTableIndent = topIndent + 37*yGap;

	//�����	
	pDC->MoveTo(tableIndent, sumTableIndent);
	pDC->LineTo(tableIndent + tableWidth, sumTableIndent);
	pDC->LineTo(tableIndent + tableWidth, sumTableIndent  + 4*yGap);
	pDC->LineTo(tableIndent, sumTableIndent + 4*yGap);
	pDC->LineTo(tableIndent,  sumTableIndent);

	pDC->MoveTo(tableIndent + column1, sumTableIndent);
	pDC->LineTo(tableIndent + column1, sumTableIndent + 4*yGap);

	pDC->MoveTo(tableIndent + column2, sumTableIndent);
	pDC->LineTo(tableIndent + column2, sumTableIndent + 4*yGap);

	cr.left = tableIndent + xGap;
	cr.right = tableIndent + column1;
	cr.top = sumTableIndent + yGap;
	cr.bottom = sumTableIndent + 3*yGap;
	cstr.Format(_T("��� ��������� �������"));
	pDC->DrawText(cstr, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	cr.left = tableIndent + xGap + column1;
	cr.right = tableIndent + column2;
	cstr.Format(_T("���� ������������ ���������� �������� ����������� ������"));
	
	pDC->DrawText(cstr, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	
	cr.left = tableIndent + xGap + column2;
	cr.right = tableIndent + tableWidth;
	cstr.Format(_T("�����"));
	pDC->DrawText(cstr, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	//-------------------------------------

	sumTableIndent += 4*yGap;

	CFont sosoFont;
	sosoFont.CreateFontW(38, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	

	//1
	pDC->MoveTo(tableIndent, sumTableIndent);
	pDC->LineTo(tableIndent, sumTableIndent + 4*yGap);
	pDC->LineTo(tableIndent + tableWidth, sumTableIndent + 4*yGap);
	pDC->LineTo(tableIndent + tableWidth, sumTableIndent);

	pDC->MoveTo(tableIndent + column1, sumTableIndent);
	pDC->LineTo(tableIndent + column1, sumTableIndent + 4*yGap);

	pDC->MoveTo(tableIndent + column2, sumTableIndent);
	pDC->LineTo(tableIndent + column2, sumTableIndent + 4*yGap);

	cr.left = tableIndent + xGap;
	cr.right = tableIndent + column1;
	cr.top = sumTableIndent + yGap;
	cr.bottom = sumTableIndent + 3*yGap;
	cr.bottom += 3*yGap;
	pDC->DrawText(Code1, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	cr.left = tableIndent + xGap + column1;
	cr.right = tableIndent + column2;	
	pDC->SelectObject(sosoFont);
	pDC->DrawText(Purpose1, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	pDC->SelectObject(simpleFont);
	cr.left = tableIndent + xGap + column2;
	cr.right = tableIndent + tableWidth;	
	pDC->DrawText(Sum1, &cr,DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	//-------------------------------------------

	//2
	if (PurposesCount > 1)
	{
		sumTableIndent += 4*yGap;

		pDC->MoveTo(tableIndent, sumTableIndent);
		pDC->LineTo(tableIndent, sumTableIndent + 4*yGap);
		pDC->LineTo(tableIndent + tableWidth, sumTableIndent + 4*yGap);
		pDC->LineTo(tableIndent + tableWidth, sumTableIndent);	
	
		pDC->MoveTo(tableIndent + column1, sumTableIndent);
		pDC->LineTo(tableIndent + column1, sumTableIndent + 4*yGap);

		pDC->MoveTo(tableIndent + column2, sumTableIndent);
		pDC->LineTo(tableIndent + column2, sumTableIndent + 4*yGap);

		cr.left = tableIndent + xGap;
		cr.right = tableIndent + column1;
		cr.top = sumTableIndent + yGap;
		cr.bottom = sumTableIndent + 3*yGap;	
		pDC->DrawText(Code2, &cr, DT_CENTER);
		cr.left = tableIndent + xGap + column1;
		cr.right = tableIndent + column2;	
		pDC->SelectObject(sosoFont);
		pDC->DrawText(Purpose2, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
		pDC->SelectObject(simpleFont);
		cr.left = tableIndent + xGap + column2;
		cr.right = tableIndent + tableWidth;	
		pDC->DrawText(Sum2, &cr, DT_CENTER);	
	}
	//-------------------------------------------

	//3
	if (PurposesCount > 2)
	{
		sumTableIndent += 4*yGap;

		pDC->MoveTo(tableIndent, sumTableIndent);
		pDC->LineTo(tableIndent, sumTableIndent + 4*yGap);
		pDC->LineTo(tableIndent + tableWidth, sumTableIndent + 4*yGap);
		pDC->LineTo(tableIndent + tableWidth, sumTableIndent);	
	
		pDC->MoveTo(tableIndent + column1, sumTableIndent);
		pDC->LineTo(tableIndent + column1, sumTableIndent + 4*yGap);

		pDC->MoveTo(tableIndent + column2, sumTableIndent);
		pDC->LineTo(tableIndent + column2, sumTableIndent + 4*yGap);

		cr.left = tableIndent + xGap;
		cr.right = tableIndent + column1;
		cr.top = sumTableIndent + yGap;
		cr.bottom = sumTableIndent + 3*yGap;	
		pDC->DrawText(Code3, &cr, DT_CENTER);
		cr.left = tableIndent + xGap + column1;
		cr.right = tableIndent + column2;	
		pDC->SelectObject(sosoFont);
		pDC->DrawText(Purpose3, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
		pDC->SelectObject(simpleFont);
		cr.left = tableIndent + xGap + column2;
		cr.right = tableIndent + tableWidth;	
		pDC->DrawText(Sum3, &cr, DT_CENTER);	
	}
	//-------------------------------------------

	//4
	if (PurposesCount > 3)
	{
		sumTableIndent += 4*yGap;

		pDC->MoveTo(tableIndent, sumTableIndent);
		pDC->LineTo(tableIndent, sumTableIndent + 4*yGap);
		pDC->LineTo(tableIndent + tableWidth, sumTableIndent + 4*yGap);
		pDC->LineTo(tableIndent + tableWidth, sumTableIndent);	
	
		pDC->MoveTo(tableIndent + column1, sumTableIndent);
		pDC->LineTo(tableIndent + column1, sumTableIndent + 4*yGap);

		pDC->MoveTo(tableIndent + column2, sumTableIndent);
		pDC->LineTo(tableIndent + column2, sumTableIndent + 4*yGap);

		cr.left = tableIndent + xGap;
		cr.right = tableIndent + column1;
		cr.top = sumTableIndent + yGap;
		cr.bottom = sumTableIndent + 3*yGap;	
		pDC->DrawText(Code4, &cr, DT_CENTER);
		cr.left = tableIndent + xGap + column1;
		cr.right = tableIndent + column2;	
		pDC->SelectObject(sosoFont);
		pDC->DrawText(Purpose4, &cr, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
		pDC->SelectObject(simpleFont);
		cr.left = tableIndent + xGap + column2;
		cr.right = tableIndent + tableWidth;	
		pDC->DrawText(Sum4, &cr, DT_CENTER);	
	}
	//-------------------------------------------

	int undertableIndent = sumTableIndent += 6*yGap;

	//�������
	cr.left = leftIndent;
	cr.right = leftIndent + 20*xGap;
	cr.top = undertableIndent;
	cr.bottom = undertableIndent + 2*yGap;		
	pDC->DrawText(_T("����������� �������"), &cr, DT_LEFT);
	cr.left = leftIndent + 30*xGap;
	cr.right = leftIndent + 72*xGap;
	pDC->DrawText(_T("���������� ����� ������ ������� �� ����� �"), &cr, DT_LEFT);

	cr.left = leftIndent;
	cr.right = leftIndent + 80*xGap;
	cr.top = undertableIndent + 4*yGap;
	cr.bottom = undertableIndent + 6*yGap;
	pDC->SelectObject(&italicFont);
	pDC->DrawText(_T("(������������ ��������� �����)"), &cr, DT_CENTER);

	cr.left = leftIndent;
	cr.right = leftIndent + 80*xGap;
	cr.top = undertableIndent + 7*yGap;
	cr.bottom = undertableIndent + 9*yGap;		
	pDC->SelectObject(&simpleFont);
	pDC->DrawText(_T("�������� �������� �������� �������� ������ �������������:"), &cr, DT_LEFT);
	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = undertableIndent + 11*yGap;
	cr.bottom = undertableIndent + 13*yGap;
	pDC->SelectObject(&italicFont);
	pDC->DrawText(_T("(�������, ���, �������� �������������)"), &cr, DT_CENTER);

	cr.top = undertableIndent + 14*yGap;
	cr.bottom = undertableIndent + 16*yGap;
	pDC->SelectObject(&simpleFont);
	cr.left = leftIndent;
	cr.right = leftIndent + 11*xGap;
	pDC->DrawText(_T("����������"), &cr, DT_LEFT);
	cr.left = leftIndent + 40*xGap;
	cr.right = leftIndent + 46*xGap;
	pDC->DrawText(_T("�����"), &cr, DT_LEFT);
	cr.left = leftIndent + 57*xGap;
	cr.right = leftIndent + 59*xGap;
	pDC->DrawText(_T("�"), &cr, DT_LEFT);

	cr.top = undertableIndent + 16*yGap;
	cr.bottom = undertableIndent + 18*yGap;
	pDC->SelectObject(&italicFont);
	cr.left = leftIndent + 12*xGap;
	cr.right = leftIndent + 36*xGap;
	pDC->DrawText(_T("(��� ���������)"), &cr, DT_CENTER);
	cr.left = leftIndent + 45*xGap;
	cr.right = leftIndent + 56*xGap;
	pDC->DrawText(_T("(��� �������)"), &cr, DT_CENTER);

	cr.top = undertableIndent + 18*yGap;
	cr.bottom = undertableIndent + 20*yGap;
	pDC->SelectObject(&simpleFont);
	cr.left = leftIndent;
	cr.right = leftIndent + 13*xGap;
	pDC->DrawText(_T("���� ������"), &cr, DT_LEFT);
	cr.left = leftIndent + 28*xGap;
	cr.right = leftIndent + 56*xGap;
	pDC->DrawText(_T("�����, �������� ��������"), &cr, DT_LEFT);

	cr.top = undertableIndent + 21*yGap;
	cr.bottom = undertableIndent + 23*yGap;	
	cr.left = leftIndent;
	cr.right = leftIndent + 12*xGap;
	pDC->DrawText(_T("����������"), &cr, DT_LEFT);
	cr.left = leftIndent + 44*xGap;
	cr.right = leftIndent + 46*xGap;
	pDC->DrawText(_T("��"), &cr, DT_LEFT);
	cr.left = leftIndent + 52*xGap;
	cr.right = leftIndent + 60*xGap;
	pDC->DrawText(_T("������."), &cr, DT_LEFT);

	cr.top = undertableIndent + 26*yGap;
	cr.bottom = undertableIndent + 28*yGap;
	cr.left = leftIndent;
	cr.right = leftIndent + 34*xGap;
	pDC->DrawText(_T("������� ���, ������� �����"), &cr, DT_LEFT);
	cr.top = undertableIndent + 28*yGap;
	cr.bottom = undertableIndent + 30*yGap;
	cr.left = leftIndent;
	cr.right = ibw - rightIndent - 20*xGap;
	pDC->DrawText(_T("������������ ������: ____________________________________________________________________________"), &cr, DT_NOCLIP);

	cr.top = undertableIndent + 32*yGap;
	cr.bottom = undertableIndent + 34*yGap;	
	cr.left = leftIndent;
	cr.right = leftIndent + 20*xGap;
	pDC->DrawText(_T("�.�."), &cr, DT_CENTER);

	cr.top = undertableIndent + 40*yGap;
	cr.bottom = undertableIndent + 42*yGap;	
	cr.left = leftIndent;
	cr.right = leftIndent + 20*xGap;
	pDC->SelectObject(&miniFont);
	pDC->DrawText(_T("* ��������� ������������� � ������� ������ ����������� ����, �� ������ ��� ����������"), &cr, DT_NOCLIP);

	pDC->SelectObject(&simpleFont);
	pDC->MoveTo(leftIndent, undertableIndent + 43*yGap);
	pDC->LineTo(ibw - rightIndent, undertableIndent + 43*yGap);

	cr.top = undertableIndent + 48*yGap;
	cr.bottom = undertableIndent + 50*yGap;	
	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	pDC->DrawText(_T("������� �����"), &cr, DT_CENTER);

	cr.top = undertableIndent + 52*yGap;
	cr.bottom = undertableIndent + 54*yGap;	
	cr.left = leftIndent;
	cr.right = leftIndent + 32*xGap;
	pDC->DrawText(_T("�������  �����: _______________"), &cr, DT_LEFT);
	cr.left = leftIndent + 36*xGap;
	cr.right = ibw - rightIndent - 10*xGap;
	pDC->DrawText(_T("���� ��������� �������� ����������� ������: _______________"), &cr, DT_LEFT);

	//������
	pDC->SelectObject(&underlinedFont);

	cr.left = leftIndent + 21*xGap;
	cr.right = leftIndent + 27*xGap;
	cr.top = undertableIndent;
	cr.bottom = undertableIndent + 2*yGap;		
	pDC->DrawText(Priority, &cr, DT_LEFT);
	cr.left = leftIndent + 73*xGap;
	cr.right = leftIndent + 100*xGap;
	pDC->DrawText(Account, &cr, DT_LEFT);

	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = undertableIndent + 2*yGap;
	cr.bottom = undertableIndent + 4*yGap;
	pDC->DrawText(Name, &cr, DT_LEFT);

	cr.left = leftIndent;
	cr.right = ibw - rightIndent;
	cr.top = undertableIndent + 9*yGap;
	cr.bottom = undertableIndent + 11*yGap;	
	while(Representative.GetLength() < 256)
		Representative.Insert(Representative.GetLength(), ' ');	
	pDC->DrawText(Representative, &cr, DT_LEFT);

	cr.top = undertableIndent + 14*yGap;
	cr.bottom = undertableIndent + 16*yGap;	
	cr.left = leftIndent + 12*xGap;
	cr.right = leftIndent + 39*xGap;
	pDC->DrawText(DocType, &cr, DT_LEFT);
	cr.left = leftIndent + 46*xGap;
	cr.right = leftIndent + 58*xGap;
	pDC->DrawText(DocSeria, &cr, DT_LEFT);
	cr.left = leftIndent + 60*xGap;
	cr.right = leftIndent + 80*xGap;
	pDC->DrawText(DocNumber, &cr, DT_LEFT);

	cr.top = undertableIndent + 18*yGap;
	cr.bottom = undertableIndent + 20*yGap;
	cr.left = leftIndent + 12*xGap;
	cr.right = leftIndent + 27*xGap;
	if (DocIssueDate.wYear != 0)
	{
		cstr.Format(_T("%.2i.%.2i.%i"), DocIssueDate.wDay, DocIssueDate.wMonth, DocIssueDate.wYear);
		pDC->DrawText(cstr, &cr, DT_LEFT);
	}
	cr.left = leftIndent + 54*xGap;
	cr.right = ibw - rightIndent;
	pDC->DrawText(DocIssueCompany, &cr, DT_LEFT);

	cr.top = undertableIndent + 21*yGap;
	cr.bottom = undertableIndent + 23*yGap;	
	cr.left = leftIndent + 13*xGap;
	cr.right = leftIndent + 43*xGap;
	pDC->DrawText(Annex, &cr, DT_LEFT);
	cr.left = leftIndent + 47*xGap;
	cr.right = leftIndent + 51*xGap;
	pDC->DrawText(AnnexPageCount, &cr, DT_LEFT);	
	//------------------------------------

	BarcodePainter bp(buf, 0, 0, 0, 0, stride);
	pDC->SelectObject(pBmp);
	RECT ro;
	ZeroMemory(&r, sizeof(RECT));
	r.right = ibw;
	r.bottom = ibh;
	CopyMemory(&ro, &r, sizeof(RECT));
	rbcxp.dataact = rbcxp.datalen;
	rbcxp.yoff = 3;
	rbcxp.error_level = 5;
	if(bGlobalPrintBarcode) {
		bp.DrawBarcode(pDC->m_hDC, &r, &ro, &rbcxp);
	}
	free(data);

	return pBmp;
}