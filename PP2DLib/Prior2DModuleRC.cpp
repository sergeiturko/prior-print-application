#pragma once;
#include "stdafx.h"

Prior2DCheckRegModule* Prior2DCheckRegModule::_rcModule = NULL;

/*********************************
---===���������-������ �����===---
*********************************/

void Prior2DCheckRegModule::setAccountA(CString &val){AccountA = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setAccountB(CString &val){AccountB = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setBankCodeA(CString &val){BankCodeA = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setBankNameA(CString &val){BankNameA = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setBankCodeB(CString &val){BankCodeB = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setBankNameB(CString &val){BankNameB = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setDate(CString &val){Date = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setNameA(CString &val){NameA = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setNameB(CString &val){NameB = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setNumber(CString &val){Number = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCode(CString &val){Code = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setBikA(CString &val){BikA = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setBikB(CString &val){BikB = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassSeria(CString &val){PassSeria = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassNum(CString &val){PassNum = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassIssue(CString &val){PassIssue = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckNum(CString &val){CheckNum = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckSum(CString &val){CheckSum = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCount(int val){Count = val;}
void Prior2DCheckRegModule::setTotalSum(__int64 val){TotalSum = val;}
//1
void Prior2DCheckRegModule::setNameA1(CString &val){NameA1 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setAccountA1(CString &val){AccountA1 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassSeria1(CString &val){PassSeria1 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassNum1(CString &val){PassNum1 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassIssue1(CString &val){PassIssue1 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckNum1(CString &val){CheckNum1 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckSum1(CString &val){CheckSum1 = val.Trim(_T(" "));}
//2
void Prior2DCheckRegModule::setNameA2(CString &val){NameA2 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setAccountA2(CString &val){AccountA2 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassSeria2(CString &val){PassSeria2 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassNum2(CString &val){PassNum2 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassIssue2(CString &val){PassIssue2 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckNum2(CString &val){CheckNum2 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckSum2(CString &val){CheckSum2 = val.Trim(_T(" "));}
//3
void Prior2DCheckRegModule::setNameA3(CString &val){NameA3 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setAccountA3(CString &val){AccountA3 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassSeria3(CString &val){PassSeria3 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassNum3(CString &val){PassNum3 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setPassIssue3(CString &val){PassIssue3 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckNum3(CString &val){CheckNum3 = val.Trim(_T(" "));}
void Prior2DCheckRegModule::setCheckSum3(CString &val){CheckSum3 = val.Trim(_T(" "));}


CString Prior2DCheckRegModule::getAccountA() {return AccountA;}
CString Prior2DCheckRegModule::getAccountB() {return AccountB;}
CString Prior2DCheckRegModule::getBankCodeA() {return BankCodeA;}
CString Prior2DCheckRegModule::getBankNameA() {return BankNameA;}
CString Prior2DCheckRegModule::getBankCodeB() {return BankCodeB;}
CString Prior2DCheckRegModule::getBankNameB() {return BankNameB;}
CString Prior2DCheckRegModule::getDate() {return Date;}
CString Prior2DCheckRegModule::getNameA() {return NameA;}
CString Prior2DCheckRegModule::getNameB() {return NameB;}
CString Prior2DCheckRegModule::getNumber() {return Number;}
CString Prior2DCheckRegModule::getCode() {return Code;}
CString Prior2DCheckRegModule::getBikA() {return BikA;}
CString Prior2DCheckRegModule::getBikB() {return BikB;}
CString Prior2DCheckRegModule::getPassIssue() {return PassIssue;}
CString Prior2DCheckRegModule::getPassNum() {return PassNum;}
CString Prior2DCheckRegModule::getPassSeria() {return PassSeria;}
CString Prior2DCheckRegModule::getCheckNum() {return CheckNum;}
CString Prior2DCheckRegModule::getCheckSum() {return CheckSum;}
int Prior2DCheckRegModule::getCount() {return Count;}
__int64 Prior2DCheckRegModule::getTotalSum() {return TotalSum;}
//1
CString Prior2DCheckRegModule::getNameA1() {return NameA1;}
CString Prior2DCheckRegModule::getAccountA1() {return AccountA1;}
CString Prior2DCheckRegModule::getPassIssue1() {return PassIssue1;}
CString Prior2DCheckRegModule::getPassNum1() {return PassNum1;}
CString Prior2DCheckRegModule::getPassSeria1() {return PassSeria1;}
CString Prior2DCheckRegModule::getCheckNum1() {return CheckNum1;}
CString Prior2DCheckRegModule::getCheckSum1() {return CheckSum1;}
//2
CString Prior2DCheckRegModule::getNameA2() {return NameA2;}
CString Prior2DCheckRegModule::getAccountA2() {return AccountA2;}
CString Prior2DCheckRegModule::getPassIssue2() {return PassIssue2;}
CString Prior2DCheckRegModule::getPassNum2() {return PassNum2;}
CString Prior2DCheckRegModule::getPassSeria2() {return PassSeria2;}
CString Prior2DCheckRegModule::getCheckNum2() {return CheckNum2;}
CString Prior2DCheckRegModule::getCheckSum2() {return CheckSum2;}
//3
CString Prior2DCheckRegModule::getNameA3() {return NameA3;}
CString Prior2DCheckRegModule::getAccountA3() {return AccountA3;}
CString Prior2DCheckRegModule::getPassIssue3() {return PassIssue3;}
CString Prior2DCheckRegModule::getPassNum3() {return PassNum3;}
CString Prior2DCheckRegModule::getPassSeria3() {return PassSeria3;}
CString Prior2DCheckRegModule::getCheckNum3() {return CheckNum3;}
CString Prior2DCheckRegModule::getCheckSum3() {return CheckSum3;}

CBitmap* Prior2DCheckRegModule::BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw, int ibh)
{
	
	//����� ��������� ������� ����������������� ���� 
	int symLen = 24;//����� �������
	int symHeight = 44;//������ �������
	int xGap = symLen;		//������������ ��� 
	int yGap = symHeight/2;	//������� MoveTo
	int stride;
	char *buf;
	//�������
	int topIndent = 0.032*ibh;		//�������
	int leftIndent = 0.119*ibw;		//�����
	int rightIndent = 0.0476*ibw;	//������
	int bottomIndent = 0.032*ibh;	//������

	int firstOffset = topIndent + 9*symHeight; //������ ��� ����� � ����������� ������
	int secondOffset = topIndent + 32*symHeight; //������ ��� ����� � ���������
	
	CBitmap *oldBmp = Utilites::InitializeForPrinting(pDC, pBmp, ibw, ibh, &buf, &stride);
	
	CFont simpleFont;
	simpleFont.CreateFontW(44, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	CFont underlinedFont;
	underlinedFont.CreateFontW(44, 0, 0, 0, 500, 0, true, 0, 0, 1, 2, 2, 34,_T("Arial"));
	
	pDC->SelectObject(new CPen(PS_SOLID, 3, RGB(0,0,0)));
	pDC->SelectObject(&simpleFont);

	RECT r; 
	r.top=0; r.left=0; r.right=ibw; r.bottom=ibh;//2480 3558
	CBrush whiteBrush(RGB(255,255,255));
	pDC->FillRect(&r,&whiteBrush);
/*
	//For debugging only: shows indents on printing document
	pDC->MoveTo(leftIndent - 10, topIndent - 10);
	pDC->LineTo(ibw-rightIndent + 10, topIndent - 10);
	pDC->LineTo(ibw-rightIndent + 10, ibh-bottomIndent + 10);
	pDC->LineTo(leftIndent - 10, ibh-bottomIndent + 10);
	pDC->LineTo(leftIndent - 10, topIndent - 10);
	//******************************************************
*/
	CRect cr = new CRect();
	CString cstr;

	//040171009
	cr.bottom = topIndent + 3*yGap;
	cr.top = topIndent + yGap;
	cr.left = ibw - rightIndent - 15*xGap;
	cr.right = ibw - rightIndent - 2*xGap;
	cstr.Format(_T("040171009"));
	pDC->DrawTextW(cstr, &cr, DT_RIGHT);

	pDC->MoveTo(ibw - rightIndent - 16*xGap, topIndent);
	pDC->LineTo(ibw - rightIndent - xGap, topIndent);
	pDC->LineTo(ibw - rightIndent - xGap, topIndent + 4*yGap);
	pDC->LineTo(ibw - rightIndent - 16*xGap, topIndent + 4*yGap);
	pDC->LineTo(ibw - rightIndent - 16*xGap, topIndent);
	//----------------------------------------------------
	

	//Number
	cr.bottom = topIndent + 7*yGap;
	cr.top = topIndent + 5*yGap;
	cr.left = leftIndent + xGap*25;
	cr.right = leftIndent + xGap*60;
	cstr.Format(_T("���������-������ ����� � %s"), Number);
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP | DT_CENTER);
	//--------------------------------

	//Date
	cr.bottom = topIndent + 9*yGap;
	cr.top = topIndent + 7*yGap;
	cr.left = leftIndent + xGap*25;
	cr.right = leftIndent + xGap*60;
	cstr.Format(Date);
	pDC->DrawTextW(cstr, &cr, DT_NOCLIP | DT_CENTER);
	//----

	//���������� �������� � �����
	cr.left = leftIndent;
	cr.right = leftIndent + 50*xGap;
	cr.top = topIndent + 11*yGap;
	cr.bottom = topIndent + 13*yGap;
	pDC->DrawText(_T("����� ����� ������� �� ������ �����������"), &cr, DT_NOCLIP);

	int additionalOffset = 0;

	if (NameA != L"")
	{
		cr.left = leftIndent;
		cr.right = leftIndent + 80*xGap;
		cr.top = topIndent + 14*yGap;
		cr.bottom = topIndent + 16*yGap;
		pDC->DrawText(NameA, &cr, DT_NOCLIP);
		
		additionalOffset += symHeight;

		if ((PassSeria != L"") && (PassNum != L"") && (PassIssue != L""))
		{
			r.left = leftIndent;
			cr.right = leftIndent + 80*xGap;
			cr.top = topIndent + 14*yGap + additionalOffset;
			cr.bottom = topIndent + 16*yGap + additionalOffset;

			cstr.Format(_T("������� %s%s ����� %s"), PassSeria, PassNum, PassIssue);
			pDC->DrawText(cstr, &cr, DT_NOCLIP);

			additionalOffset += symHeight;
		}
	}
	if (Count > 1)
	{
		if (NameA1 != L"")
		{
			cr.left = leftIndent;
			cr.right = leftIndent + 80*xGap;
			cr.top = topIndent + 14*yGap + additionalOffset;
			cr.bottom = topIndent + 16*yGap + additionalOffset;
			pDC->DrawText(NameA1, &cr, DT_NOCLIP);

			additionalOffset += symHeight;

			if ((PassSeria1 != L"") && (PassNum1 != L"") && (PassIssue1 != L""))
			{
				r.left = leftIndent;
				cr.right = leftIndent + 80*xGap;
				cr.top = topIndent + 14*yGap + additionalOffset;
				cr.bottom = topIndent + 16*yGap + additionalOffset;

				cstr.Format(_T("������� %s%s ����� %s"), PassSeria1, PassNum1, PassIssue1);
				pDC->DrawText(cstr, &cr, DT_NOCLIP);

				additionalOffset += symHeight;
			}
		}
	}
	if (Count > 2)
	{
		if (NameA2 != L"")
		{
			cr.left = leftIndent;
			cr.right = leftIndent + 80*xGap;
			cr.top = topIndent + 14*yGap + additionalOffset;
			cr.bottom = topIndent + 16*yGap + additionalOffset;
			pDC->DrawText(NameA2, &cr, DT_NOCLIP);

			additionalOffset += symHeight;

			if ((PassSeria2 != L"") && (PassNum2 != L"") && (PassIssue2 != L""))
			{
				r.left = leftIndent;
				cr.right = leftIndent + 80*xGap;
				cr.top = topIndent + 14*yGap + additionalOffset;
				cr.bottom = topIndent + 16*yGap + additionalOffset;

				cstr.Format(_T("������� %s%s ����� %s"), PassSeria2, PassNum2, PassIssue2);
				pDC->DrawText(cstr, &cr, DT_NOCLIP);

				additionalOffset += symHeight;
			}
		}
	}
	if (Count > 3)
	{
		if (NameA3 != L"")
		{
			cr.left = leftIndent;
			cr.right = leftIndent + 80*xGap;
			cr.top = topIndent + 14*yGap + additionalOffset;
			cr.bottom = topIndent + 16*yGap + additionalOffset;
			pDC->DrawText(NameA3, &cr, DT_NOCLIP);

			additionalOffset += symHeight;

			if ((PassSeria3 != L"") && (PassNum3 != L"") && (PassIssue3 != L""))
			{
				r.left = leftIndent;
				cr.right = leftIndent + 80*xGap;
				cr.top = topIndent + 14*yGap + additionalOffset;
				cr.bottom = topIndent + 16*yGap + additionalOffset;

				cstr.Format(_T("������� %s%s ����� %s"), PassSeria3, PassNum3, PassIssue3);
				pDC->DrawText(cstr, &cr, DT_NOCLIP);

				additionalOffset += symHeight;
			}
		}
	}
	//--------------------------

	firstOffset += additionalOffset;
	secondOffset += additionalOffset + 2*Count*symHeight;

	//��������� �������� � ������� ������
	cr.left = leftIndent;
	cr.right = leftIndent + 19*xGap;
	cr.top = firstOffset + 4*yGap;
	cr.bottom = firstOffset + 8*yGap;
	pDC->DrawText(_T("������������ ����� ����������"), &cr, DT_WORDBREAK);

	cr.left = leftIndent;
	cr.right = leftIndent + 19*xGap;
	cr.top = firstOffset + 14*yGap;
	cr.bottom = firstOffset + 16*yGap;
	pDC->DrawText(_T("�������������"), &cr, DT_WORDBREAK);

	cr.left = leftIndent;
	cr.right = leftIndent + 19*xGap;
	cr.top = firstOffset + 22*yGap;
	cr.bottom = firstOffset + 26*yGap;
	pDC->DrawText(_T("������������ ����� �������������"), &cr, DT_WORDBREAK);

	cr.left = ibw - rightIndent - 27*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset;
	cr.bottom = firstOffset + 2*yGap;
	pDC->DrawText(_T("�����"), &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 14*xGap;
	cr.right = ibw - rightIndent - xGap;
	cr.top = firstOffset;
	cr.bottom = firstOffset + 2*yGap;
	pDC->DrawText(_T("����� �������"), &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 35*xGap;
	cr.right = ibw - rightIndent - 32*xGap;
	cr.top = firstOffset + 6*yGap;
	cr.bottom = firstOffset + 8*yGap;
	pDC->DrawText(_T("���"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 27*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset + 6*yGap;
	cr.bottom = firstOffset + 8*yGap;
	pDC->DrawText(_T("���� �"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 27*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset + 18*yGap;
	cr.bottom = firstOffset + 20*yGap;
	pDC->DrawText(_T("������"), &cr, DT_CENTER);

	cr.left = ibw - rightIndent - 35*xGap;
	cr.right = ibw - rightIndent - 32*xGap;
	cr.top = firstOffset + 24*yGap;
	cr.bottom = firstOffset + 26*yGap;
	pDC->DrawText(_T("���"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 27*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset + 24*yGap;
	cr.bottom = firstOffset + 26*yGap;
	pDC->DrawText(_T("���� �"), &cr, DT_NOCLIP);

	cr.left = leftIndent;
	cr.right = leftIndent + 23*xGap;
	cr.top = firstOffset + 32*yGap;
	cr.bottom = firstOffset + 36*yGap;
	pDC->DrawText(_T("����� � ������ �����"), &cr, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	cr.left = leftIndent + 24*xGap;
	cr.right = leftIndent + 30*xGap;
	cr.top = firstOffset + 32*yGap;
	cr.bottom = firstOffset + 34*yGap;
	pDC->DrawText(_T("�����"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 24*xGap;
	cr.right = leftIndent + 37*xGap;
	cr.top = firstOffset + 34*yGap;
	cr.bottom = firstOffset + 36*yGap;
	pDC->DrawText(_T("������ ������"), &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 35*xGap;
	cr.right = ibw - rightIndent - 17*xGap;
	cr.top = firstOffset + 32*yGap;
	cr.bottom = firstOffset + 36*yGap;
	pDC->DrawText(_T("����� ����"), &cr, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	cr.left = ibw - rightIndent - 13*xGap;
	cr.right = ibw - rightIndent - xGap;
	cr.top = firstOffset + 32*yGap;
	cr.bottom = firstOffset + 36*yGap;
	cstr.Format(_T("��� ��������� ������� %s"), Code);
	pDC->DrawText(cstr, &cr, DT_WORDBREAK);
	//--------------------------------

	//������
	cr.left = leftIndent + 18*xGap;
	cr.right = ibw - rightIndent - 39*xGap;
	cr.top = firstOffset + 4*yGap;
	cr.bottom = firstOffset + 12*yGap;
	pDC->DrawText(BankNameA, &cr, DT_WORDBREAK);

	cr.left = leftIndent + 18*xGap;
	cr.right = ibw - rightIndent - 39*xGap;
	cr.top = firstOffset + 22*yGap;
	cr.bottom = firstOffset + 30*yGap;
	pDC->DrawText(BankNameB, &cr, DT_WORDBREAK);

	cr.left = leftIndent + 18*xGap;
	cr.right = ibw - rightIndent - 29*xGap;
	cr.top = firstOffset + 14*yGap;
	cr.bottom = firstOffset + 20*yGap;
	pDC->DrawText(NameB, &cr, DT_WORDBREAK);

	cr.left = ibw - rightIndent - 37*xGap;
	cr.right = ibw - rightIndent - 29*xGap;
	cr.top = firstOffset + 8*yGap;
	cr.bottom = firstOffset + 10*yGap;
	pDC->DrawText(BikA, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 37*xGap;
	cr.right = ibw - rightIndent - 29*xGap;
	cr.top = firstOffset + 26*yGap;
	cr.bottom = firstOffset + 28*yGap;
	pDC->DrawText(BikB, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 27*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset + 8*yGap;
	cr.bottom = firstOffset + 10*yGap;
	if (Count == 1)
	{
		pDC->DrawText(AccountA, &cr, DT_NOCLIP);
	}
	else
	{
		pDC->DrawText(L"������", &cr, DT_NOCLIP);
	}

	cr.left = ibw - rightIndent - 27*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset + 26*yGap;
	cr.bottom = firstOffset + 28*yGap;
	pDC->DrawText(AccountB, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 13.5*xGap;
	cr.right = ibw - rightIndent - 1.5*xGap;
	cr.top = firstOffset + 8*yGap;
	cr.bottom = firstOffset + 10*yGap;
	cstr.Format(_T("%I64d="), TotalSum);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);
	//------

	//����� � ������� ������

	//�������������� �����
	pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 3*yGap);
	pDC->LineTo(ibw - rightIndent, firstOffset + 3*yGap);

	pDC->MoveTo(leftIndent, firstOffset + 13*yGap);
	pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 13*yGap);

	pDC->MoveTo(leftIndent, firstOffset + 21*yGap);
	pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 21*yGap);

	pDC->MoveTo(leftIndent, firstOffset + 31*yGap);
	pDC->LineTo(ibw - rightIndent, firstOffset + 31*yGap);

	pDC->MoveTo(leftIndent, firstOffset + 37*yGap);
	pDC->LineTo(ibw - rightIndent, firstOffset + 37*yGap);

	//������������ �����
	pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 3*yGap);
	pDC->LineTo(ibw - rightIndent - 38*xGap, firstOffset + 13*yGap);

	pDC->MoveTo(ibw - rightIndent - 28*xGap, firstOffset + 3*yGap);
	pDC->LineTo(ibw - rightIndent - 28*xGap, firstOffset + 13*yGap);

	pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 21*yGap);
	pDC->LineTo(ibw - rightIndent - 38*xGap, firstOffset + 37*yGap);

	pDC->MoveTo(ibw - rightIndent - 28*xGap, firstOffset + 21*yGap);
	pDC->LineTo(ibw - rightIndent - 28*xGap, firstOffset + 31*yGap);

	pDC->MoveTo(ibw - rightIndent - 14*xGap, firstOffset + 3*yGap);
	pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 37*yGap);

	pDC->MoveTo(ibw - rightIndent, firstOffset + 3*yGap);
	pDC->LineTo(ibw - rightIndent, firstOffset + 37*yGap);

	pDC->MoveTo(leftIndent + 23*xGap, firstOffset + 31*yGap);
	pDC->LineTo(leftIndent + 23*xGap, firstOffset + 37*yGap);
	//----------------------

	//������ �����
	
	//������
	cr.left = leftIndent;
	cr.right = leftIndent + 23*xGap;
	cr.top = firstOffset + 38*yGap;
	cr.bottom = firstOffset + 40*yGap;
	pDC->DrawText(CheckNum, &cr, DT_NOCLIP);

	cr.left = leftIndent + 24*xGap;
	cr.right = leftIndent + 37*xGap;
	cr.top = firstOffset + 38*yGap;
	cr.bottom = firstOffset + 40*yGap;
	pDC->DrawText(AccountA, &cr, DT_NOCLIP);

	cr.left = ibw - rightIndent - 37*xGap;
	cr.right = ibw - rightIndent - 15*xGap;
	cr.top = firstOffset + 38*yGap;
	cr.bottom = firstOffset + 40*yGap;
	pDC->DrawText(CheckSum, &cr, DT_RIGHT);

	//�����
	pDC->MoveTo(leftIndent, firstOffset + 41*yGap);
	pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 41*yGap);

	pDC->MoveTo(leftIndent + 23*xGap, firstOffset + 37*yGap);
	pDC->LineTo(leftIndent + 23*xGap, firstOffset + 41*yGap);

	pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 37*yGap);
	pDC->LineTo(ibw - rightIndent - 38*xGap, firstOffset + 41*yGap);

	pDC->MoveTo(ibw - rightIndent - 14*xGap, firstOffset + 37*yGap);
	pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 41*yGap);
	//------------

	//�������������� ����������
	//1
	if (Count > 1)
	{
		cr.left = leftIndent;
		cr.right = leftIndent + 23*xGap;
		cr.top = firstOffset + 42*yGap;
		cr.bottom = firstOffset + 44*yGap;
		pDC->DrawText(CheckNum1, &cr, DT_NOCLIP);

		cr.left = leftIndent + 24*xGap;
		cr.right = leftIndent + 37*xGap;
		cr.top = firstOffset + 42*yGap;
		cr.bottom = firstOffset + 44*yGap;
		pDC->DrawText(AccountA1, &cr, DT_NOCLIP);

		cr.left = ibw - rightIndent - 37*xGap;
		cr.right = ibw - rightIndent - 15*xGap;
		cr.top = firstOffset + 42*yGap;
		cr.bottom = firstOffset + 44*yGap;
		pDC->DrawText(CheckSum1, &cr, DT_RIGHT);

		pDC->MoveTo(leftIndent, firstOffset + 45*yGap);
		pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 45*yGap);

		pDC->MoveTo(leftIndent + 23*xGap, firstOffset + 41*yGap);
		pDC->LineTo(leftIndent + 23*xGap, firstOffset + 45*yGap);

		pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 41*yGap);
		pDC->LineTo(ibw - rightIndent - 38*xGap, firstOffset + 45*yGap);

		pDC->MoveTo(ibw - rightIndent - 14*xGap, firstOffset + 41*yGap);
		pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 45*yGap);
	}

	//2
	if (Count > 2)
	{
		cr.left = leftIndent;
		cr.right = leftIndent + 23*xGap;
		cr.top = firstOffset + 46*yGap;
		cr.bottom = firstOffset + 48*yGap;
		pDC->DrawText(CheckNum2, &cr, DT_NOCLIP);

		cr.left = leftIndent + 24*xGap;
		cr.right = leftIndent + 37*xGap;
		cr.top = firstOffset + 46*yGap;
		cr.bottom = firstOffset + 48*yGap;
		pDC->DrawText(AccountA2, &cr, DT_NOCLIP);

		cr.left = ibw - rightIndent - 37*xGap;
		cr.right = ibw - rightIndent - 15*xGap;
		cr.top = firstOffset + 46*yGap;
		cr.bottom = firstOffset + 48*yGap;
		pDC->DrawText(CheckSum2, &cr, DT_RIGHT);

		pDC->MoveTo(leftIndent, firstOffset + 49*yGap);
		pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 49*yGap);

		pDC->MoveTo(leftIndent + 23*xGap, firstOffset + 45*yGap);
		pDC->LineTo(leftIndent + 23*xGap, firstOffset + 49*yGap);

		pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 45*yGap);
		pDC->LineTo(ibw - rightIndent - 38*xGap, firstOffset + 49*yGap);

		pDC->MoveTo(ibw - rightIndent - 14*xGap, firstOffset + 45*yGap);
		pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 49*yGap);
	}
	//3
	if (Count > 3)
	{
		cr.left = leftIndent;
		cr.right = leftIndent + 23*xGap;
		cr.top = firstOffset + 50*yGap;
		cr.bottom = firstOffset + 52*yGap;
		pDC->DrawText(CheckNum3, &cr, DT_NOCLIP);

		cr.left = leftIndent + 24*xGap;
		cr.right = leftIndent + 37*xGap;
		cr.top = firstOffset + 50*yGap;
		cr.bottom = firstOffset + 52*yGap;
		pDC->DrawText(AccountA3, &cr, DT_NOCLIP);

		cr.left = ibw - rightIndent - 37*xGap;
		cr.right = ibw - rightIndent - 15*xGap;
		cr.top = firstOffset + 50*yGap;
		cr.bottom = firstOffset + 52*yGap;
		pDC->DrawText(CheckSum3, &cr, DT_RIGHT);

		pDC->MoveTo(leftIndent, firstOffset + 53*yGap);
		pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 53*yGap);

		pDC->MoveTo(leftIndent + 23*xGap, firstOffset + 49*yGap);
		pDC->LineTo(leftIndent + 23*xGap, firstOffset + 53*yGap);

		pDC->MoveTo(ibw - rightIndent - 38*xGap, firstOffset + 49*yGap);
		pDC->LineTo(ibw - rightIndent - 38*xGap, firstOffset + 53*yGap);

		pDC->MoveTo(ibw - rightIndent - 14*xGap, firstOffset + 49*yGap);
		pDC->LineTo(ibw - rightIndent - 14*xGap, firstOffset + 53*yGap);
	}
	//-------------------------

	//������ ������

	//�������
	cr.left = leftIndent;
	cr.right = leftIndent + 5*xGap;
	cr.top = secondOffset + yGap;
	cr.bottom = secondOffset + 3*yGap;
	pDC->DrawText(_T("�.�."), &cr, DT_CENTER);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 64*xGap;
	cr.top = secondOffset + yGap;
	cr.bottom = secondOffset + 3*yGap;
	pDC->DrawText(_T("������� �������������:"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 74*xGap;
	cr.top = secondOffset + 5*yGap;
	cr.bottom = secondOffset + 7*yGap;
	pDC->DrawText(_T("������������ _________________________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 74*xGap;
	cr.top = secondOffset + 7*yGap;
	cr.bottom = secondOffset + 9*yGap;
	pDC->DrawText(_T("������� ��������� ____________________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + xGap;
	cr.right = leftIndent + 30*xGap;
	cr.top = secondOffset + 11*yGap;
	cr.bottom = secondOffset + 13*yGap;
	pDC->DrawText(_T("������� ����� �������������"), &cr, DT_NOCLIP);

	cr.left = leftIndent + xGap;
	cr.right = leftIndent + 30*xGap;
	cr.top = secondOffset + 17*yGap;
	cr.bottom = secondOffset + 19*yGap;
	pDC->DrawText(_T("���� ������ _______________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 64*xGap;
	cr.top = secondOffset + 15*yGap;
	cr.bottom = secondOffset + 17*yGap;
	pDC->DrawText(_T("������� ����������� __________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 64*xGap;
	cr.top = secondOffset + 17*yGap;
	cr.bottom = secondOffset + 19*yGap;
	pDC->DrawText(_T("����� �����"), &cr, DT_NOCLIP);

	cr.left = leftIndent + xGap;
	cr.right = leftIndent + 30*xGap;
	cr.top = secondOffset + 21*yGap;
	cr.bottom = secondOffset + 23*yGap;
	pDC->DrawText(_T("������� ����� ����������"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 64*xGap;
	cr.top = secondOffset + 25*yGap;
	cr.bottom = secondOffset + 27*yGap;
	pDC->DrawText(_T("������� ����������� __________"), &cr, DT_NOCLIP);

	cr.left = leftIndent + 33*xGap;
	cr.right = leftIndent + 64*xGap;
	cr.top = secondOffset + 27*yGap;
	cr.bottom = secondOffset + 29*yGap;
	pDC->DrawText(_T("����� �����"), &cr, DT_NOCLIP);

	cr.left = leftIndent + xGap;
	cr.right = leftIndent + 30*xGap;
	cr.top = secondOffset + 27*yGap;
	cr.bottom = secondOffset + 29*yGap;
	pDC->DrawText(_T("���� ������ _______________"), &cr, DT_NOCLIP);
	//-------
	
	//�������������� �����
	pDC->MoveTo(leftIndent, secondOffset);
	pDC->LineTo(ibw - rightIndent, secondOffset);

	pDC->MoveTo(leftIndent, secondOffset + 10*yGap);
	pDC->LineTo(ibw - rightIndent, secondOffset + 10*yGap);

	pDC->MoveTo(leftIndent, secondOffset + 14*yGap);
	pDC->LineTo(ibw - rightIndent, secondOffset + 14*yGap);

	pDC->MoveTo(leftIndent, secondOffset + 20*yGap);
	pDC->LineTo(ibw - rightIndent, secondOffset + 20*yGap);

	pDC->MoveTo(leftIndent, secondOffset + 24*yGap);
	pDC->LineTo(ibw - rightIndent, secondOffset + 24*yGap);

	pDC->MoveTo(leftIndent, secondOffset + 30*yGap);
	pDC->LineTo(ibw - rightIndent, secondOffset + 30*yGap);
	//--------------------

	//������������ �����
	pDC->MoveTo(leftIndent, secondOffset);
	pDC->LineTo(leftIndent, secondOffset + 30*yGap);

	pDC->MoveTo(ibw - rightIndent, secondOffset);
	pDC->LineTo(ibw - rightIndent, secondOffset + 30*yGap);

	pDC->MoveTo(leftIndent + 32*xGap, secondOffset + 14*yGap);
	pDC->LineTo(leftIndent + 32*xGap, secondOffset + 20*yGap);
	
	pDC->MoveTo(leftIndent + 32*xGap, secondOffset + 24*yGap);
	pDC->LineTo(leftIndent + 32*xGap, secondOffset + 30*yGap);
	//------------------
	return pBmp;

}

void PP2DLIB_API RCInit()
{
	Prior2DCheckRegModule::_rcModule = new Prior2DCheckRegModule();
}
void RCFinish()
{
	delete Prior2DCheckRegModule::_rcModule;
}

void RCBuildBarcode(char* sFileName, bool bFull)
{
	int ibw = 2480;
	int ibh = 3508;
	CDC dc;
	CBitmap bmp;
	Prior2DCheckRegModule::_rcModule->BuildBarcode(&dc, &bmp, ibw, ibh);
}

void RCSetAccountA(char* val) {Prior2DCheckRegModule::_rcModule->setAccountA(CString(val));}
void RCSetAccountB(char* val) {Prior2DCheckRegModule::_rcModule->setAccountB(CString(val));}
void RCSetBankCodeA(char* val) {Prior2DCheckRegModule::_rcModule->setBankCodeA(CString(val));}
void RCSetBankNameA(char* val) {Prior2DCheckRegModule::_rcModule->setBankNameA(CString(val));}
void RCSetBankCodeB(char* val) {Prior2DCheckRegModule::_rcModule->setBankCodeB(CString(val));}
void RCSetBankNameB(char* val) {Prior2DCheckRegModule::_rcModule->setBankNameB(CString(val));}
void RCSetDate(char* val) {Prior2DCheckRegModule::_rcModule->setDate(CString(val));}
void RCSetNameA(char* val) {Prior2DCheckRegModule::_rcModule->setNameA(CString(val));}
void RCSetNameB(char* val) {Prior2DCheckRegModule::_rcModule->setNameB(CString(val));}
void RCSetNumber(char* val) {Prior2DCheckRegModule::_rcModule->setNumber(CString(val));}
void RCSetCode(char* val) {Prior2DCheckRegModule::_rcModule->setCode(CString(val));}
void RCSetBikA(char* val) {Prior2DCheckRegModule::_rcModule->setBikA(CString(val));}
void RCSetBikB(char* val) {Prior2DCheckRegModule::_rcModule->setBikB(CString(val));}
void RCSetPassNum(char* val) {Prior2DCheckRegModule::_rcModule->setPassNum(CString(val));}
void RCSetPassSeria(char* val) {Prior2DCheckRegModule::_rcModule->setPassSeria(CString(val));}
void RCSetPassIssue(char* val) {Prior2DCheckRegModule::_rcModule->setPassIssue(CString(val));}
void RCSetCheckNum(char* val) {Prior2DCheckRegModule::_rcModule->setCheckNum(CString(val));}
void RCSetCheckSum(char* val) {Prior2DCheckRegModule::_rcModule->setCheckSum(CString(val));}
void RCSetCount(int val) {Prior2DCheckRegModule::_rcModule->setCount(val);}
void RCSetTotalSum(__int64 val) {Prior2DCheckRegModule::_rcModule->setTotalSum(val);}
//1
void RCSetNameA1(char* val) {Prior2DCheckRegModule::_rcModule->setNameA1(CString(val));}
void RCSetAccountA1(char* val) {Prior2DCheckRegModule::_rcModule->setAccountA1(CString(val));}
void RCSetPassNum1(char* val) {Prior2DCheckRegModule::_rcModule->setPassNum1(CString(val));}
void RCSetPassSeria1(char* val) {Prior2DCheckRegModule::_rcModule->setPassSeria1(CString(val));}
void RCSetPassIssue1(char* val) {Prior2DCheckRegModule::_rcModule->setPassIssue1(CString(val));}
void RCSetCheckNum1(char* val) {Prior2DCheckRegModule::_rcModule->setCheckNum1(CString(val));}
void RCSetCheckSum1(char* val) {Prior2DCheckRegModule::_rcModule->setCheckSum1(CString(val));}
//2
void RCSetNameA2(char* val) {Prior2DCheckRegModule::_rcModule->setNameA2(CString(val));}
void RCSetAccountA2(char* val) {Prior2DCheckRegModule::_rcModule->setAccountA2(CString(val));}
void RCSetPassNum2(char* val) {Prior2DCheckRegModule::_rcModule->setPassNum2(CString(val));}
void RCSetPassSeria2(char* val) {Prior2DCheckRegModule::_rcModule->setPassSeria2(CString(val));}
void RCSetPassIssue2(char* val) {Prior2DCheckRegModule::_rcModule->setPassIssue2(CString(val));}
void RCSetCheckNum2(char* val) {Prior2DCheckRegModule::_rcModule->setCheckNum2(CString(val));}
void RCSetCheckSum2(char* val) {Prior2DCheckRegModule::_rcModule->setCheckSum2(CString(val));}
//3
void RCSetNameA3(char* val) {Prior2DCheckRegModule::_rcModule->setNameA3(CString(val));}
void RCSetAccountA3(char* val) {Prior2DCheckRegModule::_rcModule->setAccountA3(CString(val));}
void RCSetPassNum3(char* val) {Prior2DCheckRegModule::_rcModule->setPassNum3(CString(val));}
void RCSetPassSeria3(char* val) {Prior2DCheckRegModule::_rcModule->setPassSeria3(CString(val));}
void RCSetPassIssue3(char* val) {Prior2DCheckRegModule::_rcModule->setPassIssue3(CString(val));}
void RCSetCheckNum3(char* val) {Prior2DCheckRegModule::_rcModule->setCheckNum3(CString(val));}
void RCSetCheckSum3(char* val) {Prior2DCheckRegModule::_rcModule->setCheckSum3(CString(val));}