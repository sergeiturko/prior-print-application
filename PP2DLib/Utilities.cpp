#include "stdafx.h"

#pragma setlocale("russian")


char *InsertWord(char * d,int w) {
	LPBYTE p = (LPBYTE)d;
	if (w < 128) {
		*p++ = (BYTE)(w & 0x7F);
	} else {
		w = ((w & 0x7FFF) | 0x8000);
		*p++ = HIBYTE(w);
		*p++ = LOBYTE(w);
	}
	return (char*)p;
}
char *InsertString(char *d,char *s,int w) {
	char *p = InsertWord(d,w);
	if (s == NULL) return p;
	while (w > 0 && *s != 0) *p++ = *s++;
	return p;
}
CString& Utilites::encodeForXml( CString &sSrc, CString &sDst )
{
	sDst = "";
    if (!ENCODEXML) sDst = sSrc;
	else
	for (int i = 0; i < sSrc.GetLength(); i++){
         TCHAR c = sSrc[i];
         switch( c ){
             case '&': sDst += "&amp;"; break;
             case '<': sDst += "&lt;"; break;
             case '>': sDst += "&gt;"; break;
             case '"': sDst += "&quot;"; break;
             case '\'': sDst += "&apos;"; break;

             default:
              if ( c<32){
					sDst.AppendFormat(L"&#%d;", (unsigned int)c);
              }else{
					sDst += c;
              }
         }
    }
    return sDst;
}


void __cdecl Utilites::formatXmlElements(CString *Dst, ...)
{
	CString res;
	va_list argptr;
	va_start(argptr, Dst);
	while (true)
	{
		WCHAR *name = va_arg(argptr, WCHAR*);
		if (name == NULL) break;
		CString *val = va_arg(argptr, CString*);
		encodeForXml(*val, res);
		Dst->AppendFormat(L"<%s>%s</%s>", name, res, name);
	}
	va_end(argptr);
}
void __cdecl Utilites::formatXmlElements(char *data, int &datalen, bool bFull, int args, ...)
{
	if (data == NULL) return;
	char *lpSTR = (char*)malloc(1024), 
		*datap = data;
	CString valp;
	CString res;
	CString *val;

	data = InsertWord(data,0x7f03);
	data = InsertWord(data,0x01);
	if (bFull) {
		data = InsertWord(data,0x61); // 31
	} else {
		data = InsertWord(data,0x62); // 36
	}

	va_list argptr;
	va_start(argptr, args);
	while (true)
	{
		int code = va_arg(argptr, int);
		if (code == 0) break;
		val = va_arg(argptr, CString*);
		if (val->GetLength() > 0) {
			valp = val->GetString();
		} else {
			valp = "";
		}

		int strLen1 = 0;
		LPWSTR lpWSTR = new WCHAR[(strLen1 = valp.GetLength())+4];
		for(int i = 0; i < strLen1; i++) lpWSTR[i] = valp[i];
		lpWSTR[strLen1] = 0;
		lpWSTR[strLen1+1] = 0;

		int strLen2 = WideCharToMultiByte(1251,0,lpWSTR,strLen1,NULL,0,NULL,NULL);
		strLen2 = WideCharToMultiByte(1251,0,lpWSTR,strLen1,lpSTR,strLen2,NULL,NULL);
		lpSTR[strLen2]=0;
		delete lpWSTR;

		data = InsertWord(data, code);
		data = InsertString(data, lpSTR, strLen1);
	}
	va_end(argptr);
	free(lpSTR);
	datalen = data - datap;
}
void __cdecl Utilites::formatXmlElements(char *Dst, ...)
{
	va_list argptr;
	va_start(argptr, Dst);
	while (true)
	{
		char *name = va_arg(argptr, char*);
		if (name == NULL) break;
		char *val = va_arg(argptr, char*);
		//encodeForXml(*val, *res);
		//Dst->AppendFormat(L"<%s>%s</%s>", name, res, name);
	}
	va_end(argptr);
}
CBitmap *Utilites::InitializeForPrinting(CDC *pDC, CBitmap *pBmp, int ibw, int ibh, char **buf, int *stride)
{
	//*stride = ((ibw*3 + 3)/4)*4;
	//???
	*stride = ((ibw + 1)/4)*4;
	int fullsize = *stride*ibh;

	
	LPBITMAPINFO bmi = (LPBITMAPINFO) new BYTE[sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD))];;
	bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi->bmiHeader.biSizeImage = fullsize;
	// Polimorph
	bmi->bmiHeader.biBitCount = 8; //24
	bmi->bmiHeader.biXPelsPerMeter = bmi->bmiHeader.biYPelsPerMeter = bmi->bmiHeader.biClrImportant = bmi->bmiHeader.biClrUsed = bmi->bmiHeader.biCompression = 0;
	bmi->bmiHeader.biHeight = -ibh;
	bmi->bmiHeader.biWidth = ibw;
	bmi->bmiHeader.biPlanes = 1;

	*buf = NULL;
	pDC->CreateCompatibleDC(NULL);
	for (int i = 0; i <= 255; i++)
	{
		bmi->bmiColors[i].rgbRed      = i;
		bmi->bmiColors[i].rgbGreen    = i;
		bmi->bmiColors[i].rgbBlue     = i;
		bmi->bmiColors[i].rgbReserved = 0;
	}
	HBITMAP hbmp = ::CreateDIBSection(*pDC, bmi, DIB_RGB_COLORS, (void**)buf, NULL, 0);
	pBmp->Attach(hbmp);
	delete (bmi);
	return pDC->SelectObject(pBmp);
}

std::string Utilites::NumberToStringS(int n, int&c, bool b1000)
{
	n %= 1000;
	int n1 = n/100;n%=100;
	int	n2 = n/10;n%=10;
	int	n3 = n%10;

	std::string cstr2="";

	if(n1!=0)
	{
		switch(n1)
		{
			case 9: cstr2+=" ���������"; break;
			case 8: cstr2+=" ���������"; break;
			case 7: cstr2+=" �������"; break;
			case 6: cstr2+=" ��������"; break;
			case 5: cstr2+=" �������"; break;
			case 4: cstr2+=" ���������"; break;
			case 3: cstr2+=" ������"; break;
			case 2: cstr2+=" ������"; break;
			case 1: cstr2+=" ���"; break;
		}
	}
	if(n2!=0)
	{
		switch(n2)
		{
			case 9: cstr2+=" ���������"; break;
			case 8: cstr2+=" �����������"; break;
			case 7: cstr2+=" ���������"; break;
			case 6: cstr2+=" ����������"; break;
			case 5: cstr2+=" ���������"; break;
			case 4: cstr2+=" �����"; break;
			case 3: cstr2+=" ��������"; break;
			case 2: cstr2+=" ��������"; break;
			case 1: 
				switch(n3)
				{
					case 9: cstr2+=" ������������"; break;
					case 8: cstr2+=" ������������"; break;
					case 7: cstr2+=" ����������"; break;
					case 6: cstr2+=" �����������"; break;
					case 5: cstr2+=" ����������"; break;
					case 4: cstr2+=" ������������"; break;
					case 3: cstr2+=" ����������"; break;
					case 2: cstr2+=" ����������"; break;
					case 1: cstr2+=" �����������"; break;
					case 0: cstr2 += " ������"; break;
				}
				break;

		}
	}
	c=3;
	if(n2 != 1)
	{
		switch(n3)
		{
			case 9: cstr2+=" ������"; break;
			case 8: cstr2+=" ������"; break;
			case 7: cstr2+=" ����"; break;
			case 6: cstr2+=" �����"; break;
			case 5: cstr2+=" ����"; break;
			case 4: cstr2+=" ������"; c=2; break;
			case 3: cstr2+=" ���"; c=2; break;
			case 2: cstr2+=b1000?" ���":" ���"; c=2; break;
			case 1: cstr2+=b1000?" ����":" ����"; c=1; break;
		}
	}
	return cstr2;
}


wchar_t * Utilites::SummToStringS(__int64 ival, wchar_t* cs)
{
	std::string cstrSumm;
	int n=0;
	if(ival>=1000*1000*1000)
	{
		int c;
		int n = ival/(1000*1000*1000);
		cstrSumm += NumberToStringS(n,c);
		switch(c)
		{
		case 1: cstrSumm+=" ��������"; break;
		case 2: cstrSumm+=" ���������"; break;
		case 3: cstrSumm+=" ����������"; break;
		}
		ival%=(1000*1000*1000);
	}
	if(ival>=1000*1000)
	{
		int c;
		int n = ival/(1000*1000);
		cstrSumm += NumberToStringS(n,c);
		switch(c)
		{
		case 1: cstrSumm+=" �������"; break;
		case 2: cstrSumm+=" ��������"; break;
		case 3: cstrSumm+=" ���������"; break;
		}
		ival%=(1000*1000);
	}
	if(ival>=1000)
	{
		int c;
		int n = ival/1000;
		cstrSumm += NumberToStringS(n,c,true);
		switch(c)
		{
		case 1: cstrSumm+=" ������"; break;
		case 2: cstrSumm+=" ������"; break;
		case 3: cstrSumm+=" �����"; break;
		}
		ival%=(1000);
	}
	if(ival>0)
	{
		int c;
		int n = ival;
		cstrSumm += NumberToStringS(n,c);
		switch(c)
		{
		case 1: cstrSumm+=" ����������� �����"; break;
		case 2: cstrSumm+=" ����������� �����"; break;
		case 3: cstrSumm+=" ����������� ������"; break;
		}
		ival%=(1000);
	}
	else cstrSumm+=" ����������� ������";

	if(cstrSumm[1]>='�'&&cstrSumm[1]<='�')
		cstrSumm[1] = cstrSumm[1]-'�'+'�';
	setlocale(LC_ALL, "rus");
	mbstowcs(cs,cstrSumm.c_str(),1024);
	return cs;
}


wchar_t * Utilites::SummToStringWithoutBelRub(__int64 ival, wchar_t* cs)
{
	
	std::string cstrSumm;
	int n=0;
	if(ival>=1000*1000*1000)
	{
		int c;
		int n = ival/(1000*1000*1000);
		cstrSumm += NumberToStringS(n,c);
		switch(c)
		{
		case 1: cstrSumm+=" ��������"; break;
		case 2: cstrSumm+=" ���������"; break;
		case 3: cstrSumm+=" ����������"; break;
		}
		ival%=(1000*1000*1000);
	}
	if(ival>=1000*1000)
	{
		int c;
		int n = ival/(1000*1000);
		cstrSumm += NumberToStringS(n,c);
		switch(c)
		{
		case 1: cstrSumm+=" �������"; break;
		case 2: cstrSumm+=" ��������"; break;
		case 3: cstrSumm+=" ���������"; break;
		}
		ival%=(1000*1000);
	}
	if(ival>=1000)
	{
		int c;
		int n = ival/1000;
		cstrSumm += NumberToStringS(n,c,true);
		switch(c)
		{
		case 1: cstrSumm+=" ������"; break;
		case 2: cstrSumm+=" ������"; break;
		case 3: cstrSumm+=" �����"; break;
		}
		ival%=(1000);
	}
	if(ival>0)
	{
		int c;
		int n = ival;
		cstrSumm += NumberToStringS(n,c);
		ival%=(1000);
	}
	if(cstrSumm[1]>='�'&&cstrSumm[1]<='�')
		cstrSumm[1] = cstrSumm[1]-'�'+'�';
	setlocale(LC_ALL, "rus");
	mbstowcs(cs,cstrSumm.c_str(),1024);
	return cs;
}


wchar_t * Utilites::NumserOfMonthToString(unsigned short monthNumber)
{
	switch (monthNumber)
	{
	case 1: return _T("������"); break;
	case 2: return _T("�������"); break;
	case 3: return _T("�����"); break;
	case 4: return _T("������"); break;
	case 5: return _T("���"); break;
	case 6: return _T("����"); break;
	case 7: return _T("����"); break;
	case 8: return _T("�������"); break;
	case 9: return _T("��������"); break;
	case 10: return _T("�������"); break;
	case 11: return _T("������"); break;
	case 12: return _T("�������"); break;
	default: return _T("unrecognized"); break;
	}
}

bool Utilites::CheckAccount(CString code, CString bank,CString bankcode)
{
	CString account;
/*
	if (ilen > 3)
	{
		// Substring
		bank = bank.Mid(ilen-3);
	}
	if (ilen < 3)
	{
		CString zero = L"000";
		bank = zero.Mid(3-ilen) + bank;
	}
	account = bank + code + L"";
*/
	if (code.GetLength() < 13)
		return false;
	while(bank.GetLength()>3)
	{
		bank.SetAt(0,' ');
		bank.TrimLeft();
	}
	if (bankcode != "" && bank != bankcode) return false;
	account = bank + code + ' ';
	int res=0;
	try
	{
		for (int i=0;i<15;i++)
		{
			res += ("7133713713713713"[i]-'0')*(account[i]-'0');
		}
		res = (res*3)%10;
		return res == code[12]-'0';
	}
	catch (...) 
	{
		return false;
	}
}

bool Utilites::CheckAccount(CString code, CString bank)
{
	CString account;

	if (code.GetLength() < 13)
		return false;
	while(bank.GetLength()>3)
	{
		bank.SetAt(0,' ');
		bank.TrimLeft();
	}
	account = bank + code + ' ';
	int res=0;
	try
	{
		for (int i=0;i<15;i++)
		{
			res += ("7133713713713713"[i]-'0')*(account[i]-'0');
		}
		res = (res*3)%10;
		return res == code[12]-'0';
	}
	catch (...) 
	{
		return false;
	}
}

bool Utilites::CheckUNN(CString unn)
{
	unn.MakeUpper();
	unn = unn.MakeUpper();
	if(unn.GetLength()<9)return false;
	for(int i = 2; i < 9; i++)
		if(unn.GetAt(i)>'9'||unn.GetAt(i)<'0')return false;
	if(unn.GetAt(0)<='9'&&unn.GetAt(0)>='0' &&
		unn.GetAt(1)<='9'&&unn.GetAt(1)>='0')
	{
		unsigned int uiunn=0;
		uiunn+=(unn.GetAt(0)-'0')*29;
		uiunn+=(unn.GetAt(1)-'0')*23;
		uiunn+=(unn.GetAt(2)-'0')*19;
		uiunn+=(unn.GetAt(3)-'0')*17;
		uiunn+=(unn.GetAt(4)-'0')*13;
		uiunn+=(unn.GetAt(5)-'0')*7;
		uiunn+=(unn.GetAt(6)-'0')*5;
		uiunn+=(unn.GetAt(7)-'0')*3;
		uiunn%=11;
		return uiunn==unn.GetAt(8)-'0';
	}
	else
	{
		if((unn.GetAt(0)=='A' ||
			unn.GetAt(0)=='B' ||
			unn.GetAt(0)=='C' ||
			unn.GetAt(0)=='E' ||
			unn.GetAt(0)=='H' ||
			unn.GetAt(0)=='K' ||
			unn.GetAt(0)=='M')
			&&
		   (unn.GetAt(1)=='A' ||
			unn.GetAt(1)=='B' ||
			unn.GetAt(1)=='C' ||
			unn.GetAt(1)=='E' ||
			unn.GetAt(1)=='H' ||
			unn.GetAt(1)=='K' ||
			unn.GetAt(1)=='M' ||
			unn.GetAt(1)=='O' ||
			unn.GetAt(1)=='P' ||
			unn.GetAt(1)=='T'))
		{
			unsigned int uiunn=0;
			switch(unn.GetAt(0))
			{
			case 'A': uiunn+=10*29; break;
			case 'B': uiunn+=11*29; break;
			case 'C': uiunn+=12*29; break;
			case 'E': uiunn+=14*29; break;
			case 'H': uiunn+=17*29; break;
			case 'K': uiunn+=20*29; break;
			case 'M': uiunn+=22*29; break;
			}
			switch(unn.GetAt(1))
			{
			case 'A': uiunn+=0*23; break;
			case 'B': uiunn+=1*23; break;
			case 'C': uiunn+=2*23; break;
			case 'E': uiunn+=3*23; break;
			case 'H': uiunn+=4*23; break;
			case 'K': uiunn+=5*23; break;
			case 'M': uiunn+=6*23; break;
			case 'O': uiunn+=7*23; break;
			case 'P': uiunn+=8*23; break;
			case 'T': uiunn+=9*23; break;
			}
			uiunn+=(unn.GetAt(2)-'0')*19;
			uiunn+=(unn.GetAt(3)-'0')*17;
			uiunn+=(unn.GetAt(4)-'0')*13;
			uiunn+=(unn.GetAt(5)-'0')*7;
			uiunn+=(unn.GetAt(6)-'0')*5;
			uiunn+=(unn.GetAt(7)-'0')*3;
			return uiunn%11==unn.GetAt(8)-'0';
		}
	}
	return false;
}

bool Utilites::MLTextOut(HDC hDC, int x, int y, int cx, int cy, CString str, int nLineHeight = 0)
{
    if (!hDC || cx <= 0 || cy <= 0)
        return false;

    if (str.IsEmpty() || x + cx <= 0 || y + cy <= 0)
        return true;

    const TCHAR *lpszEnd = (const TCHAR *)str + str.GetLength();
    const TCHAR *p1 = str, *p2, *p3, *p4;
    SIZE sz;
    int yInc = 0, n;
    RECT rc = {x, y, x + cx, y + cy};
    bool bContinue;

    while (true)
    {
        p2 = _tcsstr(p1, _T("\r\n"));
        if (!p2)
            p2 = lpszEnd;

        // check if we're already out of the rect

        if (y + yInc >= rc.bottom)
            break;

        // calculate line length

        GetTextExtentPoint32(hDC, p1, p2 - p1, &sz);

        // if line fits

        if (sz.cx <= cx)
        {
            //TextOut(hDC, x, y + yInc, p1, p2 - p1);
            ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, p1, p2 - p1, NULL);
            yInc += (nLineHeight ? nLineHeight : sz.cy);
        }

        // when line does not fit

        else
        {
            // estimate the line break point in characters

            n = ((p2 - p1) * cx) / sz.cx;
            if (n < 0)
                n = 0;

            // reverse find nearest space

            for (p3 = p1 + n; p3 > p1; p3--)
                if (*p3 == _T(' '))
                    break;

            // if it's one word spanning this line, but it doesn't fit... let's clip it

            if (p3 == p1)
            {
                // find first space on line

                for (p3 = p1; p3 < p2; p3++)
                    if (*p3 == _T(' '))
                        break;

                ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                yInc += (nLineHeight ? nLineHeight : sz.cy);

                p1 = (p3 == p2 ? p2 + 2 : p3 + 1);
                continue;
            }

            // see if p3 as line end fits

            GetTextExtentPoint32(hDC, p1, p3 - p1, &sz);
            if (sz.cx <= cx)
            {
                // try to add another word until it doesn't fit anymore

                p4 = p3;
                do
                {
                    p3 = p4; // save last position that was valid
                    for (p4 = p4+1; p4 < p2; p4++)
                        if (*p4 == _T(' '))
                            break;
                    if (p4 == p2)
                        break;

                    GetTextExtentPoint32(hDC, p1, p4 - p1, &sz);

                } while (sz.cx <= cx);

                ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                yInc += (nLineHeight ? nLineHeight : sz.cy);
                p1 = p3 + 1;
                continue;
            }
            else
            {
                // try to strip another word until it fits

                bContinue = false;

                do
                {
                    for (p4 = p3-1; p4 > p1; p4--)
                        if (*p4 == _T(' '))
                            break;

                    // if it's one word spanning this line, but it doesn't fit... let's clip it

                    if (p4 == p1)
                    {
                        // find first space on line

                        for (p3 = p1; p3 < p2; p3++)
                            if (*p3 == _T(' '))
                                break;

                        ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                        yInc += (nLineHeight ? nLineHeight : sz.cy);

                        p1 = (p3 == p2 ? p2 + 2 : p3 + 1);
                        bContinue = true;
                        break;
                    }
                    p3 = p4;
                    GetTextExtentPoint32(hDC, p1, p3 - p1, &sz);

                } while (sz.cx > cx);

                if (bContinue)
                    continue;

                ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                yInc += (nLineHeight ? nLineHeight : sz.cy);
                p1 = p3 + 1;
                continue;
            }
        }

        if (p2 == lpszEnd)
            break;
        p1 = p2 + 2;
    }
    return true;
}

bool Utilites::MLTextOutRightAlign(HDC hDC, int x, int y, int cx, int cy, CString str, int nLineHeight = 0)
{
    if (!hDC || cx <= 0 || cy <= 0)
        return false;

    if (str.IsEmpty() || x + cx <= 0 || y + cy <= 0)
        return true;

    const TCHAR *lpszEnd = (const TCHAR *)str + str.GetLength();
    const TCHAR *p1 = str, *p2, *p3, *p4;
    SIZE sz;
    int yInc = 0, n;
    RECT rc = {x, y, x + cx, y + cy};
    bool bContinue;

    while (true)
    {
        p2 = _tcsstr(p1, _T("\r\n"));
        if (!p2)
            p2 = lpszEnd;

        // check if we're already out of the rect

        if (y + yInc >= rc.bottom)
            break;

        // calculate line length

        GetTextExtentPoint32(hDC, p1, p2 - p1, &sz);

        // if line fits

        if (sz.cx <= cx)
        {
            //TextOut(hDC, x, y + yInc, p1, p2 - p1);
			ExtTextOut(hDC, x + cx - sz.cx, y + yInc, ETO_CLIPPED, &rc, p1, p2 - p1, NULL);
            yInc += (nLineHeight ? nLineHeight : sz.cy);
        }

        // when line does not fit

        else
        {
            // estimate the line break point in characters

            n = ((p2 - p1) * cx) / sz.cx;
            if (n < 0)
                n = 0;

            // reverse find nearest space

            for (p3 = p1 + n; p3 > p1; p3--)
                if (*p3 == _T(' '))
                    break;

            // if it's one word spanning this line, but it doesn't fit... let's clip it

            if (p3 == p1)
            {
                // find first space on line

                for (p3 = p1; p3 < p2; p3++)
                    if (*p3 == _T(' '))
                        break;

				ExtTextOut(hDC, x + cx - sz.cx, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                yInc += (nLineHeight ? nLineHeight : sz.cy);

                p1 = (p3 == p2 ? p2 + 2 : p3 + 1);
                continue;
            }

            // see if p3 as line end fits

            GetTextExtentPoint32(hDC, p1, p3 - p1, &sz);
            if (sz.cx <= cx)
            {
                // try to add another word until it doesn't fit anymore

                p4 = p3;
                do
                {
                    p3 = p4; // save last position that was valid
                    for (p4 = p4+1; p4 < p2; p4++)
                        if (*p4 == _T(' '))
                            break;
                    if (p4 == p2)
                        break;

                    GetTextExtentPoint32(hDC, p1, p4 - p1, &sz);

                } while (sz.cx <= cx);

                ExtTextOut(hDC, x + cx - sz.cx, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                yInc += (nLineHeight ? nLineHeight : sz.cy);
                p1 = p3 + 1;
                continue;
            }
            else
            {
                // try to strip another word until it fits

                bContinue = false;

                do
                {
                    for (p4 = p3-1; p4 > p1; p4--)
                        if (*p4 == _T(' '))
                            break;

                    // if it's one word spanning this line, but it doesn't fit... let's clip it

                    if (p4 == p1)
                    {
                        // find first space on line

                        for (p3 = p1; p3 < p2; p3++)
                            if (*p3 == _T(' '))
                                break;

                        ExtTextOut(hDC, x + cx - sz.cx, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                        yInc += (nLineHeight ? nLineHeight : sz.cy);

                        p1 = (p3 == p2 ? p2 + 2 : p3 + 1);
                        bContinue = true;
                        break;
                    }
                    p3 = p4;
                    GetTextExtentPoint32(hDC, p1, p3 - p1, &sz);

                } while (sz.cx > cx);

                if (bContinue)
                    continue;

                ExtTextOut(hDC, x + cx - sz.cx, y + yInc, ETO_CLIPPED, &rc, p1, p3 - p1, NULL);
                yInc += (nLineHeight ? nLineHeight : sz.cy);
                p1 = p3 + 1;
                continue;
            }
        }

        if (p2 == lpszEnd)
            break;
        p1 = p2 + 2;
    }
    return true;
}

bool Utilites::TwoLinesTextOut(HDC hDC, int x, int y, int cx, int cy, CString str, int nLineHeight = 0)
{
    if (!hDC || cx <= 0 || cy <= 0)
        return false;

    if (str.IsEmpty() || x + cx <= 0 || y + cy <= 0)
        return true;

    const TCHAR *p1 = str;
    SIZE sz;
    int yInc = 0;
    RECT rc = {x, y, x + cx, y + cy};
    
	// calculate line length

        GetTextExtentPoint32(hDC, str, str.GetLength(), &sz);

        // if line fits

        if (sz.cx <= cx)
        {
            //first line					
			while (sz.cx <= cx)
			{
				str.Insert(str.GetLength(), ' ');
				GetTextExtentPoint32(hDC, str, str.GetLength(), &sz);
			}
            ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, str, str.GetLength(), NULL);
            yInc += (nLineHeight ? nLineHeight : sz.cy);
			
			//secondLine
			CString secondLine = _T(" ");			
			GetTextExtentPoint32(hDC, secondLine, secondLine.GetLength(), &sz);
			while (sz.cx <= cx)
			{
				secondLine.Insert(secondLine.GetLength(), ' ');
				GetTextExtentPoint32(hDC, secondLine, secondLine.GetLength(), &sz);
			}
            ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, secondLine, secondLine.GetLength(), NULL);
        }
		else
		{
			//find max substring that fits
			CString firstLine = _T("");
			GetTextExtentPoint32(hDC, firstLine, firstLine.GetLength(), &sz);
			while (sz.cx <= cx)
			{
				firstLine.Insert(firstLine.GetLength(), str.GetAt(firstLine.GetLength()));
				GetTextExtentPoint32(hDC, firstLine, firstLine.GetLength(), &sz);
			}
			//remove last char
			firstLine = firstLine.Left(firstLine.GetLength() - 1);
			
			//find last space
			int lastSpace = firstLine.ReverseFind(_T(' '));

			int secondLineOffset = lastSpace > 0 ? lastSpace : firstLine.GetLength();

			if (lastSpace == -1)
			{
				//if no spaces - write clipped string
				ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, firstLine, firstLine.GetLength(), NULL);				
			}
			else
			{
				//if space is found - trim to last space
				CString firstLineToWrite = firstLine.Left(lastSpace);
				GetTextExtentPoint32(hDC, firstLineToWrite, firstLineToWrite.GetLength(), &sz);
				while (sz.cx <= cx)
				{
					firstLineToWrite.Insert(firstLineToWrite.GetLength(), ' ');
					GetTextExtentPoint32(hDC, firstLineToWrite, firstLineToWrite.GetLength(), &sz);
				}
				ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, firstLineToWrite, firstLineToWrite.GetLength(), NULL);
			}
			yInc += (nLineHeight ? nLineHeight : sz.cy);	

			CString secondLine = str.Mid(secondLineOffset).Trim();

			GetTextExtentPoint32(hDC, secondLine, secondLine.GetLength(), &sz);
			while (sz.cx <= cx)
			{
				secondLine.Insert(secondLine.GetLength(), ' ');
				GetTextExtentPoint32(hDC, secondLine, secondLine.GetLength(), &sz);
			}
			ExtTextOut(hDC, x, y + yInc, ETO_CLIPPED, &rc, secondLine, secondLine.GetLength(), NULL);
		}
    return true;
}
