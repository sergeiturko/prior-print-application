#pragma once

#include "stdafx.h"
#include "pdf417lib.h"

#ifndef _PRIOR2DMODULE_H_
#define _PRIOR2DMODULE_H_

namespace std_ext
{
	typedef std::basic_string<TCHAR> tstring;
	//typedef std::string<TCHAR> tstring;
}

typedef struct xParams{
	int		index;
	int		page;
	int		formcode;
	bool	Hit;
	char	*text;
	int		textlen;
	int		textact;
	char	*data;
	int		datalen;
	int		dataact;
	int		xoff;
	int		yoff;
	int		lowline;
	int		dpi, olddpi;
	int		lastnotfoundfield;
	int		numofcopies;
	int		numofpage;
	int     error_level;
	DWORD	retcode;
	DWORD	dwErrorFields;
} *pxParams;

void DrawBarcode(HDC hDC, RECT *r, RECT *ro, xParams *p);

typedef struct _STRBLK{
	int		Length;
	int		Type;
	char*	String;
	int		BarcodeWidth[32];
	int		BarcodeHeight[32];
    int     BarcodeCount;
	bool	Encrypt;
} STRBLK, *PSTRBLK;

class PP2DLIB_API Prior2DPPModule
{
public:
	static Prior2DPPModule* _ppModule;
	CBitmap* BuildBarcode(CDC *pDC, CBitmap *pBmp, bool bFull=true, int ibw = 2480, int ibh = 3508, bool bGlobalPrintBarcode = true);

	void setAccountA(CString& val);
	void setAccountB(CString& val);
	void setBankCodeA(CString& val);
	void setBankCodeB(CString& val);
	void setBankDivisionA(CString& val);
	void setBankDivisionB(CString& val);
	void setBankNameA(CString& val);
	void setBankNameB(CString& val);
	void setBN(CString& val);
	void setBudgetPaymentCode(CString& val);
	void setCodeBankaCor(CString& val);
	void setComissiaInvoice(CString& val);
	void setCorrespondentA(CString& val);
	void setCurrency(CString& val);
	void setDataNomerPasport(CString& val);
	void setDate(CString& val);
	void setDetaliPlatezha(CString& val);
	void setNameA(CString& val);
	void setNameB(CString& val);
	void setNumber(CString& val);
	void setOrdinary(CString& val);
	void setPaymentOrder(CString& val);
	void setPaymentPurpose(CString& val);
	void setPaymentType(CString& val);
	void setPL(CString& val);
	void setPL_BN(CString& val);
	void setSchetBankCor(CString& val);
	void setSumDigits(CString& val);
	void setSumSpelled(CString& val);
	void setUnnA(CString& val);
	void setUnnB(CString& val);
	void setUNP_Third(CString& val);
	void setUrgent(CString& val);
	void setCompanyFirstMan(CString& val);
	void setCompanySecondMan(CString& val);
	void setDocCode(CString& val);
	void setSeriaNum(CString& val);
	void setIssueDate(CString& val);
	void setIssueCompany(CString& val);
	void setLiveAdress(CString& val);

	CString getAccountA();
	CString getAccountB();
	CString getBankCodeA();
	CString getBankCodeB();
	CString getBankDivisionA();
	CString getBankDivisionB();
	CString getBankNameA();
	CString getBankNameB();
	CString getBN();
	CString getBudgetPaymentCode();
	CString getCodeBankaCor();
	CString getComissiaInvoice();
	CString getCorrespondentA();
	CString getCurrency();
	CString getDataNomerPasport();
	CString getDate();
	CString getDetaliPlatezha();
	CString getNameA();
	CString getNameB();
	CString getNotPaymentType();
	CString getNumber();
	CString getOrdinary();
	CString getPaymentOrder();
	CString getPaymentPurpose();
	CString getPaymentType();
	CString getPL();
	CString getPL_BN();
	CString getSchetBankCor();
	CString getSumDigits();
	CString getSumSpelled();
	CString getUnnA();
	CString getUnnB();
	CString getUNP_Third();
	CString getUrgent();
	CString getCompanyFirstMan();
	CString getCompanySecondMan();
	CString getDocCode();
	CString getSeriaNum();
	CString getIssueDate();
	CString getIssueCompany();
	CString getLiveAdress();
private:
	CString AccountA;
	CString AccountB;
	CString BankCodeA;
	CString BankCodeB;
	CString BankDivisionA;
	CString BankDivisionB;
	CString BankNameA;
	CString BankNameB;
	CString BN;
	CString BudgetPaymentCode;
	CString CodeBankaCor;
	CString ComissiaInvoice;
	CString CorrespondentA;
	CString CorrespondentBankaOtpravitel;
	CString Currency;
	CString DataNomerPasport;
	CString DataValut;
	CString Date;
	CString DetaliPlatezha;
	CString NameA;
	CString NameB;
	CString NotPaymentType;
	CString Number;
	CString Ordinary;
	CString PaymentOrder;
	CString PaymentPurpose;
	CString PaymentType;
	CString PL;
	CString PL_BN;
	CString SchetBankCor;
	CString SumDigits;
	CString SumSpelled;
	CString UnnA;
	CString UnnB;
	CString UNP_Third;
	CString Urgent;
	CString CompanyFirstMan;
	CString CompanySecondMan;
	CString DocCode;
	CString SeriaNum;
	CString IssueDate;
	CString IssueCompany;
	CString LiveAdress;
};

class PP2DLIB_API Prior2DPTModule
{
public:
	static Prior2DPTModule* _ptModule;
	CBitmap* BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw, int ibh);

	void setAccountA(CString& val);
	void setAccountB(CString& val);
	void setBankCodeA(CString& val);
	void setBankCodeB(CString& val);
	void setBankCBUA(CString& val);
	void setBankCBUB(CString& val);
	void setBankNameA(CString& val);
	void setBankNameB(CString& val);
	void setNoAccept(CString& val);
	void setBudgetPaymentCode(CString& val);
	void setBankCodeC(CString& val);
	void setBankNameC(CString& val);
	void setCurrency(CString& val);
	void setDate(CString& val);
	void setNameA(CString& val);
	void setNameB(CString& val);
	void setNumber(CString& val);
	void setPaymentOrder(CString& val);
	void setPaymentPurpose(CString& val);
	void setDelayedAccept(CString& val);
	void setPredAccept(CString& val);
	void setAccountC(CString& val);
	void setSumDigits(CString& val);
	void setSumSpelled(CString& val);
	void setUnpA(CString& val);
	void setUnpB(CString& val);
	void setUnpThird(CString& val);

	CString getAccountA();
	CString getAccountB();
	CString getBankCodeA();
	CString getBankCodeB();
	CString getBankCBUA();
	CString getBankCBUB();
	CString getBankNameA();
	CString getBankNameB();
	CString getNoAccept();
	CString getBudgetPaymentCode();
	CString getBankCodeC();
	CString getBankNameC();
	CString getCurrency();
	CString getDate();
	CString getNameA();
	CString getNameB();
	CString getNumber();
	CString getPaymentOrder();
	CString getPaymentPurpose();
	CString getDelayedAccept();
	CString getPredAccept();
	CString getAccountC();
	CString getSumDigits();
	CString getSumSpelled();
	CString getUnpA();
	CString getUnpB();
	CString getUnpThird();
private:
	CString AccountA;
	CString AccountB;
	CString BankCodeA;
	CString BankCodeB;
	CString BankDivisionA;
	CString BankDivisionB;
	CString BankNameA;
	CString BankNameB;
	CString NoAccept;
	CString BudgetPaymentCode;
	CString CodeBankaCor;
	CString CorrespondentA;
	CString Currency;
	CString Date;
	CString NameA;
	CString NameB;
	CString Number;
	CString PaymentOrder;
	CString PaymentPurpose;
	CString DelayedAccept;
	CString PredAccept;
	CString SchetBankCor;
	CString SumDigits;
	CString SumSpelled;
	CString UnnA;
	CString UnnB;
	CString UNP_Third;
};


//21.09.2011
class PP2DLIB_API Prior2DFeeCashModule
{
public:
	static Prior2DFeeCashModule* _fcModule;
	CBitmap* BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw = 2480, int ibh = 3508);

	void setAccountB(CString& val);
	void setBankCodeB(CString& val);
	void setBankNameB(CString& val);
	void setDate(CString& val);
	void setNameA(CString& val);
	void setNameB(CString& val);
	void setNumber(CString& val);
	void setPaymentPurpose(CString& val);
	void setSumDigits(CString& val);
	void setSumSpelled(CString& val);
	void setReportCode(CString& val);
	void setUnp(CString& val);
	void setAdditionalInfo(CString& val);

	CString getAccountB();
	CString getBankCodeB();
	CString getBankNameB();
	CString getDate();
	CString getNameA();
	CString getNameB();
	CString getNumber();
	CString getPaymentPurpose();
	CString getSumDigits();
	CString getSumSpelled();
	CString getReportCode();
	CString getUnp();
	CString getAdditionalInfo();

private:
	CString AccountB;
	CString BankCodeB;
	CString BankNameB;
	CString Date;
	CString NameA;
	CString NameB;
	CString Number;
	CString PaymentPurpose;
	CString SumDigits;
	CString SumSpelled;
	CString ReportCode;
	CString Unp;
	CString AdditionalInfo;
};

//16.11.2011
class PP2DLIB_API Prior2DCheckRegModule
{
public:
	static Prior2DCheckRegModule* _rcModule;
	CBitmap* BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw = 2480, int ibh = 3508);

	void setAccountA(CString& val);
	void setAccountB(CString& val);
	void setBankCodeA(CString& val);
	void setBankNameA(CString& val);
	void setBankCodeB(CString& val);
	void setBankNameB(CString& val);
	void setDate(CString& val);
	void setNameA(CString& val);
	void setNameB(CString& val);
	void setNumber(CString& val);
	void setCode(CString& val);
	void setBikA(CString& val);
	void setBikB(CString& val);
	void setPassSeria(CString& val);
	void setPassNum(CString& val);
	void setPassIssue(CString& val);
	void setCheckNum(CString& val);
	void setCheckSum(CString& val);
	void setCount(int val);
	void setTotalSum(__int64 val);
	//1
	void setNameA1(CString& val);
	void setAccountA1(CString& val);
	void setPassSeria1(CString& val);
	void setPassNum1(CString& val);
	void setPassIssue1(CString& val);
	void setCheckNum1(CString& val);
	void setCheckSum1(CString& val);
	//2
	void setNameA2(CString& val);
	void setAccountA2(CString& val);
	void setPassSeria2(CString& val);
	void setPassNum2(CString& val);
	void setPassIssue2(CString& val);
	void setCheckNum2(CString& val);
	void setCheckSum2(CString& val);
	//3
	void setNameA3(CString& val);
	void setAccountA3(CString& val);
	void setPassSeria3(CString& val);
	void setPassNum3(CString& val);
	void setPassIssue3(CString& val);
	void setCheckNum3(CString& val);
	void setCheckSum3(CString& val);

	CString getAccountA();
	CString getAccountB();
	CString getBankCodeA();
	CString getBankNameA();
	CString getBankCodeB();
	CString getBankNameB();
	CString getDate();
	CString getNameA();
	CString getNameB();
	CString getNumber();
	CString getCode();
	CString getBikA();
	CString getBikB();
	CString getPassSeria();
	CString getPassNum();
	CString getPassIssue();
	CString getCheckNum();
	CString getCheckSum();
	int getCount();
	__int64 getTotalSum();
	//1
	CString getNameA1();
	CString getAccountA1();
	CString getPassSeria1();
	CString getPassNum1();
	CString getPassIssue1();
	CString getCheckNum1();
	CString getCheckSum1();
	//2
	CString getNameA2();
	CString getAccountA2();
	CString getPassSeria2();
	CString getPassNum2();
	CString getPassIssue2();
	CString getCheckNum2();
	CString getCheckSum2();
	//3
	CString getNameA3();
	CString getAccountA3();
	CString getPassSeria3();
	CString getPassNum3();
	CString getPassIssue3();
	CString getCheckNum3();
	CString getCheckSum3();

private:	
	CString AccountA;
	CString AccountB;
	CString BankCodeA;
	CString BankNameA;
	CString BankCodeB;
	CString BankNameB;
	CString Date;
	CString NameA;
	CString NameB;
	CString Number;
	CString Code;
	CString BikA;
	CString BikB;
	CString PassSeria;
	CString PassNum;
	CString PassIssue;
	CString CheckNum;
	CString CheckSum;
	int Count;
	__int64 TotalSum;
	//1
	CString NameA1;
	CString AccountA1;
	CString PassSeria1;
	CString PassNum1;
	CString PassIssue1;
	CString CheckNum1;
	CString CheckSum1;
	//2
	CString NameA2;
	CString AccountA2;
	CString PassSeria2;
	CString PassNum2;
	CString PassIssue2;
	CString CheckNum2;
	CString CheckSum2;
	//3
	CString NameA3;
	CString AccountA3;
	CString PassSeria3;
	CString PassNum3;
	CString PassIssue3;
	CString CheckNum3;
	CString CheckSum3;
};

class PP2DLIB_API Prior2DReceiveBelCashModule
{
public:
	static Prior2DReceiveBelCashModule* _rbcModule;
	CBitmap* BuildBarcode(CDC *pDC, CBitmap *pBmp, int ibw = 2480, int ibh = 3508, bool bGlobalPrintBarcode = true);

	void setName(CString& val);
	void setApplicationNumber(CString& val);
	void setApplicationDate(SYSTEMTIME val);
	void setSumDigits(CString& val);
	void setSumSpelled(CString& val);
	void setPriority(CString& val);
	void setAccount(CString& val);
	void setRepresentative(CString& val);
	void setDocType(CString& val);
	void setDocSeria(CString& val);
	void setDocNumber(CString& val);
	void setDocIssueDate(SYSTEMTIME val);
	void setDocIssueCompany(CString& val);
	void setAnnex(CString& val);
	void setAnnexPageCount(CString& val);
	void setPurposesCount(int val);
	//1
	void setCode1(CString& val);
	void setPurpose1(CString& val);
	void setSum1(CString& val);
	//2
	void setCode2(CString& val);
	void setPurpose2(CString& val);
	void setSum2(CString& val);
	//3
	void setCode3(CString& val);
	void setPurpose3(CString& val);
	void setSum3(CString& val);
	//4
	void setCode4(CString& val);
	void setPurpose4(CString& val);
	void setSum4(CString& val);

	//Do we need getters?

	private:	
	CString Name;
	CString ApplicationNumber;	
	SYSTEMTIME ApplicationDate;
	CString SumDigits;
	CString SumSpelled;
	CString Priority;
	CString Account;
	CString Representative;
	CString DocType;
	CString DocSeria;
	CString DocNumber;
	SYSTEMTIME DocIssueDate;
	CString DocIssueCompany;
	CString Annex;
	CString AnnexPageCount;	
	int PurposesCount;
	//1
	CString Code1;
	CString Purpose1;
	CString Sum1;	
	//2
	CString Code2;
	CString Purpose2;
	CString Sum2;
	//3
	CString Code3;
	CString Purpose3;
	CString Sum3;
	//4
	CString Code4;
	CString Purpose4;
	CString Sum4;	
};

void PP2DLIB_API PPInit();
void PP2DLIB_API PPFinish();

PP2DLIB_API void PPBuildBarcode(char* sFileName, bool bFull);
void PP2DLIB_API PPSetAccountA(char* val);
void PP2DLIB_API PPSetAccountB(char* val);
void PP2DLIB_API PPSetBankCodeA(char*  val);
void PP2DLIB_API PPSetBankCodeB(char*  val);
void PP2DLIB_API PPSetBankCBUA(char*  val);
void PP2DLIB_API PPSetBankCBUB(char*  val);
void PP2DLIB_API PPSetBankNameA(char*  val);
void PP2DLIB_API PPSetBankNameB(char*  val);
void PP2DLIB_API PPSetBN(char*  val);
void PP2DLIB_API PPSetBudgetPaymentCode(char*  val);
void PP2DLIB_API PPSetBankCodeC(char*  val);
void PP2DLIB_API PPSetComissiaInvoice(char*  val);
void PP2DLIB_API PPSetBankNameC(char*  val);
void PP2DLIB_API PPSetCurrency(char*  val);
void PP2DLIB_API PPSetDataNomerPasport(char*  val);
void PP2DLIB_API PPSetDate(char*  val);
void PP2DLIB_API PPSetPaymentDetailes(char*  val);
void PP2DLIB_API PPSetNameA(char*  val);
void PP2DLIB_API PPSetNameB(char*  val);
void PP2DLIB_API PPSetNumber(char*  val);
void PP2DLIB_API PPSetOrdinary(char*  val);
void PP2DLIB_API PPSetPaymentOrder(char*  val);
void PP2DLIB_API PPSetPaymentPurpose(char*  val);
void PP2DLIB_API PPSetPaymentType(char*  val);
void PP2DLIB_API PPSetPL(char*  val);
void PP2DLIB_API PPSetPL_BN(char*  val);
void PP2DLIB_API PPSetAccountC(char*  val);
void PP2DLIB_API PPSetSumDigits(char*  val);
void PP2DLIB_API PPSetSumSpelled(char*  val);
void PP2DLIB_API PPSetUnpA(char*  val);
void PP2DLIB_API PPSetUnpB(char*  val);
void PP2DLIB_API PPSetUnpThird(char*  val);
void PP2DLIB_API PPSetUrgent(char*  val);
void PP2DLIB_API PPSetCompanyFirstMan(char*  val);
void PP2DLIB_API PPSetCompanySecondMan(char*  val);
void PP2DLIB_API PPSetDocCode(char*  val);
void PP2DLIB_API PPSetSeriaNum(char*  val);
void PP2DLIB_API PPSetIssueDate(char*  val);
void PP2DLIB_API PPSetIssueCompany(char*  val);
void PP2DLIB_API PPSetLiveAdress(char*  val);

void PP2DLIB_API PTBuildBarcode(char* sFileName);
void PP2DLIB_API PTInit();
void PP2DLIB_API PTFinish();
void PP2DLIB_API PTSetAccountA(char* val);
void PP2DLIB_API PTSetAccountB(char* val);
void PP2DLIB_API PTSetBankCodeA(char* val);
void PP2DLIB_API PTSetBankCodeB(char* val);
void PP2DLIB_API PTSetBankCBUA(char* val);
void PP2DLIB_API PTSetBankCBUB(char* val);
void PP2DLIB_API PTSetBankNameA(char* val);
void PP2DLIB_API PTSetBankNameB(char* val);
void PP2DLIB_API PTSetNoAccept(char* val);
void PP2DLIB_API PTSetBudgetPaymentCode(char* val);
void PP2DLIB_API PTSetBankCodeC(char* val);
void PP2DLIB_API PTSetBankNameC(char* val);
void PP2DLIB_API PTSetCurrency(char* val);
void PP2DLIB_API PTSetDate(char* val);
void PP2DLIB_API PTSetNameA(char* val);
void PP2DLIB_API PTSetNameB(char* val);
void PP2DLIB_API PTSetNumber(char* val);
void PP2DLIB_API PTSetPaymentOrder(char* val);
void PP2DLIB_API PTSetPaymentPurpose(char* val);
void PP2DLIB_API PTSetDelayedAccept(char* val);
void PP2DLIB_API PTSetPredAccept(char* val);
void PP2DLIB_API PTSetAccountC(char* val);
void PP2DLIB_API PTSetSumDigits(char* val);
void PP2DLIB_API PTSetSumSpelled(char* val);
void PP2DLIB_API PTSetUnpA(char* val);
void PP2DLIB_API PTSetUnpB(char* val);
void PP2DLIB_API PTSetUnpThird(char* val);


//21.09.2011
void PP2DLIB_API FCBuildBarcode(char* sFileName);
void PP2DLIB_API FCInit();
void PP2DLIB_API FCFinish();
void PP2DLIB_API FCSetAccountB(char* val);
void PP2DLIB_API FCSetBankCodeB(char* val);
void PP2DLIB_API FCSetBankNameB(char* val);
void PP2DLIB_API FCSetDate(char* val);
void PP2DLIB_API FCSetNameA(char* val);
void PP2DLIB_API FCSetNameB(char* val);
void PP2DLIB_API FCSetNumber(char* val);
void PP2DLIB_API FCSetPaymentPurpose(char* val);
void PP2DLIB_API FCSetSumDigits(char* val);
void PP2DLIB_API FCSetSumSpelled(char* val);
void PP2DLIB_API FCSetReportCode(char* val);
void PP2DLIB_API FCSetUnp(char* val);
void PP2DLIB_API FCSetAdditionalInfo(char* val);

//16.11.2011
void PP2DLIB_API RCBuildBarcode(char* sFileName);
void PP2DLIB_API RCInit();
void PP2DLIB_API RCFinish();
void PP2DLIB_API RCSetAccountA(char* val);
void PP2DLIB_API RCSetAccountB(char* val);
void PP2DLIB_API RCSetBankCodeA(char* val);
void PP2DLIB_API RCSetBankNameA(char* val);
void PP2DLIB_API RCSetBankCodeB(char* val);
void PP2DLIB_API RCSetBankNameB(char* val);
void PP2DLIB_API RCSetDate(char* val);
void PP2DLIB_API RCSetNameA(char* val);
void PP2DLIB_API RCSetNameB(char* val);
void PP2DLIB_API RCSetNumber(char* val);
void PP2DLIB_API RCSetCode(char* val);
void PP2DLIB_API RCSetBikA(char* val);
void PP2DLIB_API RCSetBikB(char* val);
void PP2DLIB_API RCSetPassSeria(char* val);
void PP2DLIB_API RCSetPassNum(char* val);
void PP2DLIB_API RCSetPassIssue(char* val);
void PP2DLIB_API RCSetCheckNum(char* val);
void PP2DLIB_API RCSetCheckSum(char* val);
void PP2DLIB_API RCSetCount(int val);
void PP2DLIB_API RCTotalSum(__int64 val);
//1
void PP2DLIB_API RCSetNameA1(char* val);
void PP2DLIB_API RCSetAccountA1(char* val);
void PP2DLIB_API RCSetPassSeria1(char* val);
void PP2DLIB_API RCSetPassNum1(char* val);
void PP2DLIB_API RCSetPassIssue1(char* val);
void PP2DLIB_API RCSetCheckNum1(char* val);
void PP2DLIB_API RCSetCheckSum1(char* val);
//2
void PP2DLIB_API RCSetNameA2(char* val);
void PP2DLIB_API RCSetAccountA2(char* val);
void PP2DLIB_API RCSetPassSeria2(char* val);
void PP2DLIB_API RCSetPassNum2(char* val);
void PP2DLIB_API RCSetPassIssue2(char* val);
void PP2DLIB_API RCSetCheckNum2(char* val);
void PP2DLIB_API RCSetCheckSum2(char* val);
//3
void PP2DLIB_API RCSetNameA3(char* val);
void PP2DLIB_API RCSetAccountA3(char* val);
void PP2DLIB_API RCSetPassSeria3(char* val);
void PP2DLIB_API RCSetPassNum3(char* val);
void PP2DLIB_API RCSetPassIssue3(char* val);
void PP2DLIB_API RCSetCheckNum3(char* val);
void PP2DLIB_API RCSetCheckSum3(char* val);

//ReceiveBelCash
void PP2DLIB_API RBCBuildBarcode(char* sFileName);
void PP2DLIB_API RBCInit();
void PP2DLIB_API RBCFinish();

void PP2DLIB_API RBCSetName(CString& val);
void PP2DLIB_API RBCSetApplicationNumber(CString& val);
void PP2DLIB_API RBCSetApplicationDate(SYSTEMTIME val);
void PP2DLIB_API RBCSetSumDigits(CString& val);
void PP2DLIB_API RBCSetSumSpelled(CString& val);
void PP2DLIB_API RBCSetPriority(CString& val);
void PP2DLIB_API RBCSetAccount(CString& val);
void PP2DLIB_API RBCSetRepresentative(CString& val);
void PP2DLIB_API RBCSetDocType(CString& val);
void PP2DLIB_API RBCSetDocSeria(CString& val);
void PP2DLIB_API RBCSetDocNumber(CString& val);
void PP2DLIB_API RBCSetDocIssueDate(SYSTEMTIME val);
void PP2DLIB_API RBCSetDocIssueCompany(CString& val);
void PP2DLIB_API RBCSetAnnex(CString& val);
void PP2DLIB_API RBCSetAnnexPageCount(CString& val);
void PP2DLIB_API RBCSetPurposesCount(int val);
//1
void PP2DLIB_API RBCSetCode1(CString& val);
void PP2DLIB_API RBCSetPurpose1(CString& val);
void PP2DLIB_API RBCSetSum1(CString& val);
//2
void PP2DLIB_API RBCSetCode2(CString& val);
void PP2DLIB_API RBCSetPurpose2(CString& val);
void PP2DLIB_API RBCSetSum2(CString& val);
//3
void PP2DLIB_API RBCSetCode3(CString& val);
void PP2DLIB_API RBCSetPurpose3(CString& val);
void PP2DLIB_API RBCSetSum3(CString& val);
//4
void PP2DLIB_API RBCSetCode4(CString& val);
void PP2DLIB_API RBCSetPurpose4(CString& val);
void PP2DLIB_API RBCSetSum4(CString& val);

class PP2DLIB_API Utilites
{
public:
	static std::string NumberToStringS(int n, int&c, bool b1000=false);
	static wchar_t * SummToStringS(__int64 n, wchar_t *cs);
	static bool CheckAccount(CString account, CString code,CString bankcode);
	static bool CheckAccount(CString account, CString code);
	static bool CheckUNN(CString unn);
	static CString& encodeForXml( CString &sSrc, CString &sDst );
	static void __cdecl formatXmlElements(CString *Dst, ...);
	static void __cdecl formatXmlElements(char *Dst, ...);
	static void __cdecl formatXmlElements(char *data, int &datalen, bool bFull, int args, ...);
	static CBitmap *InitializeForPrinting(CDC *pDC, CBitmap *pBmp, int ibw, int ibh, char **buf, int *stride);
	//21.09.2011
	static wchar_t * NumserOfMonthToString (unsigned short monthNumber);
	static wchar_t * SummToStringWithoutBelRub(__int64 n, wchar_t * cs);

	static bool MLTextOut(HDC hDC, int x, int y, int cx, int cy, CString str, int nLineHeight);
	static bool MLTextOutRightAlign(HDC hDC, int x, int y, int cx, int cy, CString str, int nLineHeight);
	static bool TwoLinesTextOut(HDC hDC, int x, int y, int cx, int cy, CString str, int nLineHeight);
};

class PP2DLIB_API CBarcode{
	int m_x,m_y,m_Width,m_Height,m_Stride;
	byte *m_Buf;
	int m_Angle,m_ErrorLevel,m_BitColumns,m_FullSize;
	bool m_ForceBinary;
	int m_XGap,m_YGap;
	int m_BarcodesCount;
	int m_BarcodesRows;
	int m_BarcodesColumns;
	HDC m_DC;
	HGDIOBJ m_OldO;
	int iGapX,iGapY;
	HBITMAP m_hBMP;
public:
	CBarcode(void):m_DC(0),m_x(0),m_y(0),m_Width(0),m_Height(0),m_Stride(0),
		m_Buf(0),m_Angle(0),m_ErrorLevel(7),
		m_BitColumns(222),m_ForceBinary(false),
		iGapX(150),iGapY(100),m_BarcodesCount(-1),
		m_BarcodesRows(-1),m_BarcodesColumns(-1){};
	CBarcode(BYTE*,int,int,int,int,int);
	~CBarcode();
	HDC get_DC(){return m_DC;};
	__declspec(property(get = get_DC)) HDC hDC;
	void Init(int,int);
	void Draw(PSTRBLK,BYTE*,int,BYTE,int,int,int,int,unsigned char,int);
	void DrawXML(PSTRBLK);
	// bool PlayEnhMetaFileAtOriginalSize(SDrawXML*);
	void SetErrorLevel(int i){m_ErrorLevel = i;};
};

class PP2DLIB_API BarcodePainter
{
protected:
	char *imgBuf;
	int imgX, imgY, imgWidth, imgHeight, imgStride;

	int imgXGap, imgYGap;
	int barcodesCount; // autodetect
	int barcodesRows, barcodesColumns;
	int angle; // 0, 90, 180, 270	

	bool forceBinary;
	int errorLevel, bitColumns;

public:
	BarcodePainter(char *imgBuf, int imgX, int imgY, int imgWidth, int imgHeight, int imgStride);

	void DrawBarcode(HDC hDC, RECT *r, RECT *ro, xParams *p);
	void DrawPlainBarcode(char *data, int len, char index, int x, int y, int width, int height);

	void DrawXmlBarcodes(HDC hDC, int w, int h, CString &xmlDocument);
};

#endif