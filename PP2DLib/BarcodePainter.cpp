#include "stdafx.h"

//#define BARCODEDEBUG

#define KEYBUFLENGTH 1024

extern bool g_Encrypt;

int error_handler(const int code, const char *mess) {
	char err_txt[1024];
	memset(err_txt, 0, 1024);
	
	StringCchPrintfA(err_txt, 1024, "��� ������ GetLastError=%ld ������:%s", GetLastError(), mess);
	StringCchPrintfA(err_txt, 1024, "������:%s � �����:%08X", mess, code);
	MessageBoxA(NULL, err_txt, "������", MB_OK | MB_ICONERROR);
	return 1;
}
BOOL SaveToFile(wchar_t *fn, LPBYTE szDeflateTemp, DWORD iTotal)
{
	DWORD dwWritten;
	HANDLE h = CreateFile(fn, FILE_ALL_ACCESS, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(h != INVALID_HANDLE_VALUE)
	{
		WriteFile(h, szDeflateTemp, iTotal, &dwWritten, NULL);
		CloseHandle(h);
		return TRUE;
	}
	return FALSE;
}
int filter(unsigned int code, struct _EXCEPTION_POINTERS *ep)
{
	TCHAR sE[MAX_PATH];
	if (code == EXCEPTION_ACCESS_VIOLATION)
	{
		MessageBox(NULL, L"EXCEPTION_EXECUTE_HANDLER", L"EXCEPTION_ACCESS_VIOLATION", MB_OK);
		return EXCEPTION_EXECUTE_HANDLER;

   }
   else
   {
		MessageBox(NULL, L"EXCEPTION_CONTINUE_SEARCH", L"EXCEPTION_ACCESS_VIOLATION", MB_OK);
		return EXCEPTION_CONTINUE_SEARCH;

   };

}

BarcodePainter::BarcodePainter(char *imgBuf, int imgX, int imgY, int imgWidth, int imgHeight, int imgStride)
{
#ifdef OLDMETHOD
	this->imgBuf = imgBuf;
	this->imgX = imgX;
	this->imgY = imgY;
	this->imgWidth = imgWidth;
	this->imgHeight = imgHeight;
	this->imgStride = imgStride;

	imgXGap = 260; imgYGap = 100;
	barcodesCount = -1; // autodetect
	barcodesRows = -1; barcodesColumns = -1;
	angle = 0; // 0, 90, 180, 270	
	errorLevel = 7;
	bitColumns = 222;
	forceBinary = true;
#endif
}
//void BarcodePainter::DrawBarcode(char *data, int len, char index, char *buf, int x, int y, int width, int height, int stride)
void BarcodePainter::DrawPlainBarcode(char *data, int len, char index, int x, int y, int width, int height) {
#ifdef OLDMETHOD
	// prepare barcode buffer
	char *outData = new char[len+2];
	outData[0] = 0x81;
	outData[1] = index;
	memcpy(outData+2, data, len);


	// initialize barcode params
	pdf417param p1;
	memset(&p1,0,sizeof(pdf417param));
	pdf417init(&p1);

	// setup params
	p1.text = outData;
	p1.lenText = len+2;
	if (errorLevel != -1)
		p1.errorLevel = errorLevel;
	if (bitColumns != -1)
		p1.bitColumns = bitColumns;
//	p1.options = ((errorLevel != -1)?PDF417_USE_ERROR_LEVEL:PDF417_AUTO_ERROR_LEVEL)|PDF417_INVERT_BITMAP|PDF417_FIXED_RECTANGLE|(forceBinary?PDF417_FORCE_BINARY:0);
	p1.options = ((errorLevel != -1)?PDF417_USE_ERROR_LEVEL:PDF417_AUTO_ERROR_LEVEL)|PDF417_INVERT_BITMAP|PDF417_FIXED_COLUMNS|(forceBinary?PDF417_FORCE_BINARY:0);
		
#ifdef BARCODEDEBUG
	DWORD dw;
	CString debugname;
	debugname.AppendFormat(_TEXT("C:\\data_%d.bin"), index);		
	HANDLE h = CreateFile(debugname, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, NULL, NULL);
	WriteFile(h, p1.text, p1.lenText, &dw, NULL);
	CloseHandle(h);
#endif
	paintCode(&p1);

	int wPDF4171 = p1.bitColumns/8+(p1.bitColumns%8>0?1:0);
	int height1 = p1.codeRows;
	int width1 = p1.bitColumns;
	int sibw = width;
	int sibh = height;//-iCurY;
	int scale1 = 0;
	if(sibw/width1>sibh/height1)
	{
		scale1 = (sibh/height1);
		width1 *= (int)(sibh/height1);
		height1 = sibh-sibh%height1;
	}
	else
	{
		scale1 = (sibw/width1);
		height1 *= (int)(sibw/width1);
		width1 = sibw-sibw%width1;
	}
	// get pointer to bitmap bits data
	/*		
	for(int ix = 0 ; ix < 512; ix++){
		((char*)imgBuf)[imgStride*(ix*2)+(ix*2)] = ix;
		((char*)imgBuf)[imgStride*(ix*2)+(ix*2+1)] = ix;
		((char*)imgBuf)[imgStride*(ix*2+1)+(ix*2)] = ix;
		((char*)imgBuf)[imgStride*(ix*2+1)+(ix*2+1)] = ix;
	}
	*/
	bool bGood = true;
	for(int iy = 0 ; iy < height1 && bGood; iy++)
	{
		for(int ix = 0 ; ix < width1 && bGood; ix++)
		{
			__try{
			//char cValue = ((p1.outBits[wPDF4171*(iy/scale1)+(ix/scale1)/8]>>(7-(ix/scale1)%8))&0x01)*0x0FF;
				char cValue = (p1.outBits[wPDF4171*(iy/scale1)+(ix/scale1)/8]>>(7-(ix/scale1)%8))&0x01;
				if (cValue == 0) cValue = 0;
				else cValue = 255;
				((char*)imgBuf)[imgStride*(iy + y - (height1 / 4)) + (ix + x)] = cValue;
			//((char*)imgBuf)[imgStride*(iy+y)+3*(ix+x)] = cValue;
			//((char*)imgBuf)[imgStride*(iy+y)+3*(ix+x)+1] = cValue;
			//((char*)imgBuf)[imgStride*(iy+y)+3*(ix+x)+2] = cValue;
			}__except(filter(GetExceptionCode(), GetExceptionInformation())){
				bGood = false;
				break;
			}
		}
	}

	//bmp->UnlockBits(&bData1);
	pdf417free(&p1);
	delete outData;
#endif // OLDMETHOD
}

#pragma region CRYPTO
BYTE cryptPublicKey[] = 
{
	0x06, 0x02, 0x00, 0x00, 0x00, 0xA4, 0x00, 0x00, 0x52, 0x53, 0x41, 0x31, 0x00, 0x04, 0x00, 0x00, 
	0x01, 0x00, 0x01, 0x00, 0x6B, 0xB5, 0x12, 0x7A, 0xCA, 0xD3, 0x4F, 0x8F, 0x1E, 0x10, 0x3F, 0x03, 
	0x58, 0x8C, 0x01, 0x54, 0x38, 0x66, 0x6E, 0xD4, 0xF2, 0x2E, 0x95, 0xE6, 0xF2, 0xDF, 0xE4, 0xDD, 
	0x19, 0x90, 0xFA, 0x64, 0xA4, 0x2B, 0x7C, 0x82, 0x8D, 0xED, 0x50, 0xB1, 0xBC, 0xC6, 0x31, 0x56, 
	0x54, 0x0F, 0xD9, 0xFE, 0x9A, 0x40, 0xA9, 0x2B, 0xA7, 0x43, 0x30, 0x2E, 0x08, 0x86, 0xD7, 0x57, 
	0x1B, 0xDE, 0xC6, 0x58, 0xE7, 0x06, 0x1D, 0x5F, 0xE9, 0x5F, 0x62, 0x33, 0xB7, 0x4E, 0xAA, 0xBD, 
	0x97, 0xC0, 0xDC, 0xAC, 0x8B, 0x05, 0xD6, 0xD5, 0xD9, 0x2E, 0xAD, 0x44, 0xBF, 0x31, 0x4A, 0xFB, 
	0x27, 0x5B, 0x21, 0xFA, 0xD1, 0xDF, 0xD9, 0x88, 0x79, 0x6E, 0xB7, 0x84, 0x0B, 0x4D, 0x4A, 0x2F, 
	0xEF, 0xCD, 0xFF, 0x3B, 0xAE, 0x88, 0xDB, 0x13, 0x31, 0xC6, 0x57, 0x25, 0x6B, 0x40, 0xF8, 0x59, 
	0x59, 0x58, 0x87, 0xBF, };

bool RSAEncrypt(char *data, DWORD len, char** odata, DWORD* olen)
{
	HCRYPTPROV hCryptProv;
	HCRYPTKEY hKey,hSesKey;
	DWORD dwSesKeyLen = KEYBUFLENGTH;
	DWORD dwDataLen = len, dwOO, dwIO;
	BYTE *obuf,*skey;
    BOOL bRes,bFinal;
	*odata  = NULL;
    *olen = NULL;
	bRes = CryptAcquireContext(&hCryptProv,NULL,
		MS_STRONG_PROV,
		PROV_RSA_FULL,
		CRYPT_VERIFYCONTEXT);
	if (!bRes){
		return false;
	}
	bRes = CryptGenKey(hCryptProv, CALG_3DES, (192<<16)|CRYPT_EXPORTABLE, &hSesKey);
	if (!bRes){
		return false;
	}
	if (!CryptImportKey(hCryptProv, cryptPublicKey, sizeof(cryptPublicKey), 0, 0, &hKey)){
		return false;
	}
	bRes = CryptExportKey(hSesKey, hKey, SIMPLEBLOB, 0, 0, &dwSesKeyLen);
	if (!bRes){
		return false;
	}
	bRes = CryptEncrypt(hSesKey, 0, TRUE, 0, NULL, &dwDataLen, KEYBUFLENGTH);
	if (!bRes){
		return false;
	}
	skey = (BYTE*) malloc(dwSesKeyLen);
	bRes = CryptExportKey (hSesKey, hKey, SIMPLEBLOB, 0, (BYTE *)skey, &dwSesKeyLen);
	if (!bRes){
		free(skey);
		return false;
	}
	obuf = (BYTE*) malloc(dwSesKeyLen + dwDataLen + 64);
	if (obuf){
		*((WORD*)obuf) = (WORD)0x6666;
		*((WORD*)obuf + 1) = (WORD)dwSesKeyLen;
		memcpy(obuf + 4,skey,dwSesKeyLen);
		dwOO = 4 + dwSesKeyLen;
	
		DWORD dwTI = dwDataLen;
		bFinal = FALSE;
	    dwIO = 0;
		while (dwTI > 0){
			DWORD dwLR;
			if (dwTI < KEYBUFLENGTH){
				dwLR = dwTI;
				dwTI = 0;
			}else{
				dwTI -= KEYBUFLENGTH;
				dwLR = KEYBUFLENGTH;
			}
			DWORD dwLW = dwLR;
			if (dwLR != KEYBUFLENGTH) bFinal = TRUE;
			memcpy(obuf + dwOO,data + dwIO,dwLR);
			bRes = CryptEncrypt (hSesKey, 0, bFinal, 0, (BYTE *)obuf + dwOO, &dwLW, KEYBUFLENGTH);
			if (!bRes){
			}
			dwIO += dwLR;
			dwOO += dwLW;
		}
		if(hKey){
			if(!(CryptDestroyKey(hKey))){
			}
		}
		if(hSesKey){
			if(!(CryptDestroyKey(hSesKey))){
			}
		}
		if(hCryptProv){
			if(!(CryptReleaseContext(hCryptProv,0))){
			}
		} 
	}
	free(skey);
	*odata = (char*)obuf;
	*olen = dwOO;
	return true;
}
#ifdef OLMETHOD
bool RSAEncrypt(char *data, DWORD len, char **outData, DWORD *outLen) {
		HCRYPTPROV hCryptProv;        // handle for the 
                              //  cryptographic provider context
	HCRYPTKEY hKey, hSessionKey;               // public/private key handle
	WORD header;
	DWORD alg, bits;

	// try to encrypt with AES
	if (
		CryptAcquireContext(
		   &hCryptProv,               // handle to the CSP
		   NULL,                  // container name 
		   NULL,//MS_ENH_RSA_AES_PROV,                      // use the default provider
		   PROV_RSA_AES,           // provider type
		   0))
	{
		header = 0x6266;
		alg = CALG_AES_256;
		bits = 256;
	} else
	{
		// try to use 3DES then
		if (!
		CryptAcquireContext(
		   &hCryptProv,               // handle to the CSP
		   NULL,                  // container name 
		   MS_ENHANCED_PROV,//MS_ENH_RSA_AES_PROV,                      // use the default provider
		   PROV_RSA_FULL,           // provider type
		   0))
		{
			// encryption failed, provide plain data
			*outData = data;
			*outLen = len;
			return false;
		}
		header = 0x6166;
		alg = CALG_3DES;
		bits = 168;
	}
   CryptImportKey(hCryptProv, cryptPublicKey, sizeof(cryptPublicKey), 0, CRYPT_NO_SALT, &hKey);

   // generate session key
   CryptGenKey(hCryptProv, alg, (bits << 16) | CRYPT_EXPORTABLE | CRYPT_NO_SALT, &hSessionKey);

   // determine session key blob length
   DWORD keysize;
   CryptExportKey(hSessionKey, hKey, SIMPLEBLOB, 0, NULL, &keysize);

   // determine encrypted data length
   int res, gle;
   DWORD buflen = *outLen = len;
   CryptEncrypt(hSessionKey, 0, TRUE, 0, NULL, &buflen, len);

   // get total size and allocate buffer
   DWORD totalsize = keysize + buflen+2;
   *outData = new char[totalsize];
   *((WORD*)*outData) = header;
   // export session key
   CryptExportKey(hSessionKey, hKey, SIMPLEBLOB, 0, ((BYTE*)*outData)+2, &keysize);

   // prepare data and encrypt
   memcpy((*outData) + keysize + 2, data, len);
   CryptEncrypt(hSessionKey, 0, TRUE, 0, ((BYTE*)*outData) + keysize + 2, outLen, buflen);
   *outLen = 2 + keysize + *outLen;

   CryptDestroyKey(hKey);
   CryptDestroyKey(hSessionKey);
   CryptReleaseContext(hCryptProv, 0);
   return true;
}
#endif
#pragma endregion
CBarcode::CBarcode(BYTE* b,int x,int y,int w,int h,int s){
	m_x = x;
	m_y = y;
	m_Width = w;
	m_Height = h;
	m_Stride = s;
	m_Buf = b;
}
CBarcode::~CBarcode(){
	SelectObject(m_DC,m_OldO);
	DeleteObject(m_hBMP);
	DeleteDC(m_DC);
}
void CBarcode::Init(int ibw, int ibh){
	byte *buf(0);
	RECT r;
	m_Stride = ((ibw + 1) / 4) * 4;
	m_FullSize = m_Stride * ibh;
	m_Width = m_Stride;
	m_Height = ibh;
	LPBITMAPINFO bmi = (LPBITMAPINFO) malloc (sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD)));
	bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi->bmiHeader.biSizeImage = m_FullSize;
	bmi->bmiHeader.biBitCount = 8;
	bmi->bmiHeader.biXPelsPerMeter = bmi->bmiHeader.biYPelsPerMeter = bmi->bmiHeader.biClrImportant = bmi->bmiHeader.biClrUsed = bmi->bmiHeader.biCompression = 0;
	bmi->bmiHeader.biHeight = -m_Height;
	bmi->bmiHeader.biWidth = m_Width;
	bmi->bmiHeader.biPlanes = 1;
	if (m_DC = CreateCompatibleDC(NULL)){
		// DBG_OUT(0,"'CBarcode::Init' CreateCompatibleDC:(%x)",m_DC);
		bmi->bmiColors[0].rgbRed     = 0;
		bmi->bmiColors[0].rgbGreen    = 0;
		bmi->bmiColors[0].rgbBlue    = 0;
		bmi->bmiColors[0].rgbReserved = 0;
		bmi->bmiColors[1].rgbRed     = 254;
		bmi->bmiColors[1].rgbGreen    = 254;
		bmi->bmiColors[1].rgbBlue    = 254;
		bmi->bmiColors[1].rgbReserved = 0;
		bmi->bmiColors[2].rgbRed     = 255;
		bmi->bmiColors[2].rgbGreen    = 255;
		bmi->bmiColors[2].rgbBlue    = 255;
		bmi->bmiColors[2].rgbReserved = 0;
		m_hBMP = CreateDIBSection(m_DC,bmi,DIB_RGB_COLORS,(void**)&buf,NULL,0);
	}
	m_Buf = (byte*)buf;
	if (m_Buf) ZeroMemory(m_Buf,m_FullSize);
	m_OldO = SelectObject(m_DC,m_hBMP);
	r.left = r.top = 0;
	r.right = m_Width;
	r.bottom = m_Height;
    HGDIOBJ hbrush,hbrushOld;
	hbrush = GetStockObject(WHITE_BRUSH);
    hbrushOld = SelectObject(m_DC,hbrush);
	FillRect(m_DC,&r,(HBRUSH)GetStockObject(DC_BRUSH));
    SelectObject(m_DC, hbrushOld);
	DeleteObject(hbrush);
	free (bmi);
	return;
}
void CBarcode::Draw(PSTRBLK psb,BYTE *b,int iLen,BYTE ind,int XGap,int YGap,int width,int height,unsigned char code,int i){
	byte *p = (byte*)malloc(iLen + 2);
	p[0] = code;
	p[1] = ind;
	memcpy(p + 2, b, iLen);

	pdf417param p1;
	memset(&p1,0,sizeof(pdf417param));
	pdf417init(&p1);

	p1.text = (char*)p;
	p1.lenText = iLen + 2;
	if (m_ErrorLevel != -1) p1.errorLevel = m_ErrorLevel;
	if (m_BitColumns != -1) p1.bitColumns = m_BitColumns;
	m_ForceBinary = 1;
	p1.options = p1.options | PDF417_USE_ASPECT_RATIO;
	p1.options = ((m_ErrorLevel != -1)?PDF417_USE_ERROR_LEVEL:PDF417_AUTO_ERROR_LEVEL)|PDF417_INVERT_BITMAP|PDF417_FIXED_RECTANGLE|(m_ForceBinary?PDF417_FORCE_BINARY:0);
	paintCode(&p1);
	int wPDF4171 = p1.bitColumns/8+(p1.bitColumns%8>0?1:0);
	int height1 = p1.codeRows;
	int width1 = p1.bitColumns;
	int sibw = width / i;
	int sibh = height - YGap;
	int scale1 = 0;
	int offset = (width / i) * (ind - 1);
	if(sibw/width1>sibh/height1){
		scale1 = (sibh/height1);
		width1 *= (int)(sibh/height1);
		height1 = sibh-sibh%height1;
	}else{
		scale1 = (sibw/width1);
		height1 *= (int)(sibw/width1);
		width1 = sibw-sibw%width1;
	}
	psb->BarcodeWidth[ind - 1] = width1;
	psb->BarcodeHeight[ind - 1] = height1;
	for(int iy = 0 ; iy < height1; iy++){
		for(int ix = 0 ; ix < width1; ix++){
			char cValue = (p1.outBits[wPDF4171*(iy/scale1)+(ix/scale1)/8]>>(7-(ix/scale1)%8))&0x01;
			cValue = cValue?2:0;
			((char*)m_Buf)[m_Stride * (iy + YGap) + (offset + ix + XGap)] = cValue;
		}
	}
	pdf417free(&p1);
	free(p);
}
#ifdef OLDMETHOD
void CBarcode::DrawXML(PSTRBLK psb){
	byte *szXML(0),*szDeflateTemp(0);
	int iLen,iLen2;
	int iTotal(0);
	iLen = psb->Length;
	// vector<bool> p;
	if (iLen > 0){
		szXML = (byte*)psb->String;

		SaveToFile(L"c:\\VirtualPrinter\\dump.xml",szXML,iLen);

		//deflate compress
		unsigned have;
		byte *szDeflate = (byte*)malloc(iLen);
		long buf_pointer = 0;
		char* szDefBuf = NULL;

		/* allocate deflate state */
		z_stream strm;
		memset(&strm,0,sizeof(z_stream));
		int iRes = deflateInit(&strm, Z_DEFAULT_COMPRESSION);

		/* compress until end of file */
		strm.avail_in = iLen;
		strm.next_in = szXML;
		do{
			strm.avail_out = iLen;
			strm.next_out = (BYTE*)szDeflate;
			iRes = deflate(&strm, Z_FINISH);
			have = iLen - strm.avail_out;
			if(szDeflateTemp) free(szDeflateTemp);
			szDeflateTemp = (byte*) malloc(have + iTotal);
			memcpy(szDeflateTemp + iTotal,szDeflate,have);
			iTotal += have;
		} while (strm.avail_out == 0);
		free(szDeflate);
		/* clean up */
		(void)deflateEnd(&strm);
		DWORD EncLen;
		char *EncData = NULL;

		SaveToFile(L"c:\\VirtualPrinter\\dump.bin",szDeflateTemp,iTotal);

		if (!psb->Encrypt || !RSAEncrypt((char*)szDeflateTemp,iTotal,&EncData,&EncLen)){
			if (EncData)
			{
				free(EncData); EncData = NULL;
			}
			EncData = (char*)szDeflateTemp; szDeflateTemp = NULL;
			EncLen = iTotal;
		}
		
		SaveToFile(L"c:\\VirtualPrinter\\dump.enc",(LPBYTE)EncData,EncLen);
		
		unsigned char c = psb->Type == 1?0x82:0x81;
		for (int i = 2; i < 6; i++){
		    if((EncLen + (i * 2)) < (i * 670)){
			    int iLen = EncLen / i; // 700 / 2 = 350
				int ii = 0,iAdd = 0;
				while (ii < i && EncLen > 0){
					int off = ii * iLen;
				    Draw(psb,(BYTE*)EncData + off,(iLen + iAdd),(ii + 1),0,0,m_Width,m_Height,c,i);
					EncLen -= iLen + iAdd;
					ii++;
					if (EncLen < (iLen * (ii + 1))) iAdd = EncLen - (iLen * ii);
				}
				psb->BarcodeCount = i;
				break;
			}
		}
		if (EncData) free(EncData);
		if (szDeflateTemp) free(szDeflateTemp);
	}
}
#endif
void CBarcode::DrawXML(PSTRBLK psb) {
	byte *szXML(0),*szDeflateTemp(0);
	int iLen, iLeno;
	int iTotal(0);
	if(psb == NULL) return;
	try {
		iLen = psb->Length;
		iLeno = iLen * 2;
		//vector<bool> p;
		if (iLen > 0) {
			szXML = (byte*)psb->String;
			SaveToFile(L"c:\\VirtualPrinter\\dump.xml",szXML,iLen);
	
			//deflate compress
			unsigned have;
			LPBYTE szDeflate = (LPBYTE)malloc(iLeno);
			long buf_pointer = 0;
			char* szDefBuf = NULL;
	
			/* allocate deflate state */
			z_stream strm;
			memset(&strm,0,sizeof(z_stream));
			int iRes = deflateInit(&strm, Z_DEFAULT_COMPRESSION);
	
			/* compress until end of file */
			strm.avail_in = iLen;
			strm.next_in = szXML;
			do {
				strm.avail_out = iLeno;
				strm.next_out = (BYTE*)szDeflate;
				iRes = deflate(&strm, Z_FINISH);
				have = iLeno - strm.avail_out;
				if(szDeflateTemp) free(szDeflateTemp);
				szDeflateTemp = (byte*) malloc(have + iTotal);
				memcpy(szDeflateTemp + iTotal,szDeflate,have);
				iTotal += have;
			} while (strm.avail_out == 0);
			free(szDeflate);
			/* clean up */
			(void)deflateEnd(&strm);
			DWORD EncLen = 0;
			char *EncData = NULL;
	
			SaveToFile(L"c:\\VirtualPrinter\\dump.bin",szDeflateTemp,iTotal);
	
			if (psb->Type == 0 || !RSAEncrypt((char*)szDeflateTemp,iTotal,&EncData,&EncLen)) {
				free(EncData);
				EncData = (char*)szDeflateTemp; szDeflateTemp = NULL;
				EncLen = iTotal;
			}
			SaveToFile(L"c:\\VirtualPrinter\\dump.enc",(LPBYTE)EncData,EncLen);
			unsigned char c = psb->Type == 1?0x82:0x81;
			for (int i = 2; i < 6; i++) {
			    if((EncLen + (i * 2)) < (DWORD)(i * 670)) {
					int iLen = EncLen / i; // 700 / 2 = 350
					int ii = 0,iAdd = 0;
					while (ii < i && EncLen > 0) {
						int off = ii * iLen;
						Draw(psb,(BYTE*)EncData + off,(iLen + iAdd),(ii + 1),0,0,m_Width,m_Height,0x82,i);
						EncLen -= iLen + iAdd;
						ii++;
						if (EncLen < (DWORD)(iLen * (ii + 1))) iAdd = EncLen - (iLen * ii);
					}
					psb->BarcodeCount = i;
					break;
				}
			}
			if(EncData) {
				free(EncData);
				free(szDeflateTemp);
			} else {
				free(szDeflateTemp);
			}
		}
	} catch(...) {
		error_handler(0, "CBarcode::DrawXML");
	}
}
void BarcodePainter::DrawXmlBarcodes(HDC hDC, int w, int h, CString &xmlDocument/*, char *buf, int x, int y, int width, int height, int gap, int stride*/)
{
	int strLen1 = 0;
	LPWSTR lpWSTR = new WCHAR[(strLen1=xmlDocument.GetLength())+4];
	for(int i = 0; i < strLen1; i++) lpWSTR[i] = xmlDocument[i];
	lpWSTR[strLen1] = 0;
	lpWSTR[strLen1+1] = 0;

	int strLen2 = WideCharToMultiByte(1251,0,lpWSTR,strLen1,NULL,0,NULL,NULL);
	LPSTR lpSTR = new CHAR[strLen2+1];
	strLen2 = WideCharToMultiByte(1251,0,lpWSTR,strLen1,lpSTR,strLen2,NULL,NULL);
	lpSTR[strLen2]=0;
	delete []lpWSTR;

/*
#pragma region Barcode
	//deflate

	unsigned have;
	unsigned char *out = new unsigned char[strLen2];
	int level = Z_DEFAULT_COMPRESSION;
	long buf_pointer = 0;
	int data_len = 0;
	char* outData = NULL;

	z_stream strm;
	memset(&strm,0,sizeof(z_stream));
	int ret = deflateInit(&strm, level);

//#ifdef BARCODEDEBUG
	DWORD dw;
	HANDLE h = CreateFile(L"C:\\data.xml", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, NULL, NULL);
	WriteFile(h, lpSTR, strLen2, &dw, NULL);
	CloseHandle(h);
//#endif

	strm.avail_in = strLen2;
	strm.next_in = (unsigned char*)lpSTR;
	// run deflate() on input until output buffer not full, finish	compression if all of source has been read in 
	do
	{
		strm.avail_out = strLen2;
		strm.next_out = out;
		ret = deflate(&strm, Z_FINISH);    // no bad return value

		have = strLen2 - strm.avail_out;
		if(outData) delete[]outData;
		outData = new char[have+data_len];
		memcpy(outData+data_len,out,have);
		data_len += have;
	} while (strm.avail_out == 0);
	delete[] out;

	(void)deflateEnd(&strm);

//#ifdef BARCODEDEBUG
	h = CreateFile(L"C:\\data.bin", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, NULL, NULL);
	WriteFile(h, outData, data_len, &dw, NULL);
	CloseHandle(h);
//#endif

	// encode?
#ifdef RSAENCRYPT
	DWORD encLen;
	char *encData;
	RSAEncrypt(outData, data_len, &encData, &encLen);
	delete outData;
	data_len = encLen;
	outData = encData;
#endif

//#ifdef BARCODEDEBUG
	h = CreateFile(L"C:\\data.enc", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, NULL, NULL);
	WriteFile(h, outData, data_len, &dw, NULL);
	CloseHandle(h);
//#endif
*/
	//deflate
	int MaxX = w;
	int MaxY = h;

	CBarcode *Barcode = new CBarcode();
	int BarCodeWidth = MaxX;
	int BarCodeHeight = (MaxY / 100) * 10;
	Barcode->Init(BarCodeWidth, BarCodeHeight);
	Barcode->SetErrorLevel(5);
	STRBLK StrBlk = {0};
	StrBlk.String = lpSTR;
	StrBlk.Length = strLen2;
	StrBlk.Encrypt = false;


	Barcode->DrawXML(&StrBlk);

	int cury = MaxY - (((MaxY / 100) * 10) + ((MaxY / 100) * 2));
	for (int i = 0; i <= StrBlk.BarcodeCount; i++)
	{
		int pixinper = (MaxX / 100);
		int xgapminus = i * (pixinper * 10);
		int baroff = (BarCodeWidth / StrBlk.BarcodeCount) * i;
		double xreg = 100 + (StrBlk.BarcodeWidth[i] * 15e-1 + 30) * StrBlk.BarcodeCount;
		double xoff = (StrBlk.BarcodeWidth[i] * 15e-1 + 30) * i;
		int offset = MaxX - xreg + xoff;
		int barpag = MaxX / StrBlk.BarcodeCount;
		double mulw = 15e-1;
		double mulh = 14e-1;
		double tw = StrBlk.BarcodeWidth[i];
		double th = StrBlk.BarcodeHeight[i];
		double bw = tw * mulw;
		double bh = th * mulh;
		//char *ss = (char*)malloc(1024);
		//sprintf(ss, "mulw:%f mulh:%f tw:%f th:%f bw:%f bh:%f", mulw, mulh, tw, th, bw, bh);
		//MessageBoxA(NULL, ss, "DEBUG", MB_OK);
		//free(ss);
		StretchBlt(
			hDC,
			offset /*(XOff + offset)*/,
			cury /*MaxY - YOff - BarCodeHeight*/,
			bw,
			bh,
			Barcode->hDC,
			baroff,
			0,
			StrBlk.BarcodeWidth[i],
			StrBlk.BarcodeHeight[i],
			SRCCOPY);
	}
	/*
	if(data_len+4<2*670)
	{
		int sibw = (imgWidth - imgXGap)/2;
		int sibh = imgHeight;//-iCurY;
		int len1 = data_len/2;
		DrawPlainBarcode(outData, len1, 1, imgX + imgXGap, imgY, sibw, sibh);
		DrawPlainBarcode(outData + len1, data_len - len1, 2, imgX + sibw + imgXGap, imgY, sibw, sibh);
	}
	*/
	delete[] lpSTR;
#pragma endregion
}
void BarcodePainter::DrawBarcode(HDC hDC, RECT *r, RECT *ro, xParams *p) {
	CBarcode *Barcode;
	int MaxX = r->right, MaxY = r->bottom;
	float ycoef = 1;
	char szID[MAX_PATH];
	float rob = ro->bottom;
	float rrb = r->bottom;
	DWORD XOff, YOff;
	DWORD dwReads = 0;
	int gOffY = 0;

	ycoef = rrb / rob;

	int minx = p->xoff * (MaxX / 100), maxy = MaxY - (p->xoff * (MaxY / 100)), curx(0), cury(0);

	if (p->dwErrorFields == NULL) {
		maxy = MaxY;
		Barcode = new CBarcode();
		if (Barcode) {
            int LowLine = p->lowline + 2;
			int BarCodeWidth = MaxX;
			int BarCodeHeight = (MaxY / 100) * 10;
			Barcode->Init(BarCodeWidth, BarCodeHeight);
			STRBLK StrBlk = {0};

			if (p->dataact > 0)	{
				StrBlk.Type = 1;
				StrBlk.String = p->data;
				StrBlk.Length = p->dataact;
				SaveToFile(L"c:\\VirtualPrinter\\barcode.bin", (LPBYTE)p->data, p->dataact);
/*
				HGDIOBJ hPenOld,hPen = CreatePen(PS_SOLID, 3, RGB(255,255,255));
				HGDIOBJ hBOld,hB = CreateSolidBrush(RGB(255,255,255));
				hPenOld = SelectObject(hDC, hPen);
				hBOld = SelectObject(hDC, hB);
				if (p->formcode != 401600036 && p->formcode > 400000000) { // clear the block after table
					int x1, y1, x2, y2;
					x1 = 0; x2 = MaxX;
					y1 = LowLine + gOffY; y2 = MaxY;
				    //Rectangle(hDC, x1, y1, x2, y2);
				}
				SelectObject(hDC, hPenOld);
				SelectObject(hDC, hBOld);
				DeleteObject(hPen);DeleteObject(hB); */
				if(p->error_level != -1) Barcode->SetErrorLevel(p->error_level);
				Barcode->DrawXML(&StrBlk);
			}
			int PointSize = 10;
			int logpixy = GetDeviceCaps(hDC, LOGPIXELSY);
			int nHeight = 50;
			int yoff = 70;
			curx = minx;
			float ycrd;
            ycrd = LowLine;
			cury = (int)(ycrd) + gOffY;

			LOGFONT lf;
			lf.lfHeight = nHeight;
    		lf.lfWidth = 0;
    		lf.lfEscapement = 0;
    		lf.lfOrientation = 0;
    		lf.lfWeight = FW_MEDIUM;
			lf.lfItalic = 0;
    		lf.lfUnderline = 0;
    		lf.lfStrikeOut = 0;
    		lf.lfCharSet = RUSSIAN_CHARSET;
    		lf.lfOutPrecision = OUT_STRING_PRECIS;
    		lf.lfClipPrecision = CLIP_STROKE_PRECIS;
    		lf.lfQuality = PROOF_QUALITY;
    		lf.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
    		wcscpy(lf.lfFaceName, L"Times New Roman");
			HGDIOBJ hFontOld,hFont,hFont2;
			hFont = CreateFontIndirect(&lf);
			lf.lfHeight = nHeight + (nHeight >> 1);
    		wcscpy(lf.lfFaceName, L"Courier New");
			lf.lfWeight = FW_BOLD;
			hFont2 = CreateFontIndirect(&lf);
			HGDIOBJ hPenOld,hPen = CreatePen(PS_SOLID, 3, RGB(255,255,255));
			hPenOld = SelectObject(hDC, hPen);
			int TextX = MaxX / 100 * 70;
			int TextY = MaxY / 100 * 1;
			memset(&szID, 0, MAX_PATH);
			sprintf((char*)&szID, "1030 9020 0000 2752");

			hFontOld = SelectObject(hDC, hFont);
			if (p->formcode == 401600036)
			{
				curx = MaxX / 2 + minx;
				szID[5] = L'9';
			}
			else if (p->formcode == 401890033)
			{
				szID[5] = L'8';
				curx = minx;
				//TextOut(hDC, curx,cury,L"���� �����������",16);
				curx = MaxX / 2 + minx;
				//TextOut(hDC, curx,cury,L"������� �����������",19);
				cury += yoff;
				curx = minx;
				//TextOut(hDC, curx,cury,L"���� ������������� �������",26);
				curx = MaxX / 2 + minx;
				//TextOut(hDC, curx,cury,L"���� ���������� ������",22);
				cury += yoff;
				curx = minx;
				//TextOut(hDC, curx,cury,L"���� ����������",15);
				curx = MaxX / 2 + minx;
				//TextOut(hDC, curx,cury,L"����� �����",11);
				cury += yoff;
			}
			else if (p->formcode == 401600031)
			{
				szID[5] = L'7';
				//TextOut(hDC, curx,cury,L"������� �����������",19);
				curx = MaxX / 2 + minx;
				//TextOut(hDC, curx,cury,L"������� �����������",19);
				cury += yoff;
				curx = minx;
				curx = MaxX / 2 + minx;
				//TextOut(hDC, curx,cury,L"���� ���������� ������",22);
				cury += yoff;
				curx = minx;
				//TextOut(hDC, curx,cury,L"�.�.",4);
				curx = MaxX / 2 + minx;
				//TextOut(hDC, curx,cury,L"����� �����",11);
				cury += yoff;
			}
            XOff = (MaxX / 100) * p->xoff;
			YOff = (MaxY / 100) * p->yoff;
			cury = MaxY - YOff - BarCodeHeight - yoff;
			RECT rec;
			rec.left = ((MaxX / 100) * 50); rec.top = cury - 10; rec.right = ((MaxX / 100) * 97); rec.bottom = cury + yoff - 10;
			SelectObject(hDC, hFont2);
			DrawTextA(hDC, (LPCSTR)&szID, 19, &rec, DT_LEFT); 
			int i = 0;
			cury = MaxY - YOff - BarCodeHeight;
			double xoff = (double)MaxX;
			for (int i = 0; i <= StrBlk.BarcodeCount; i++) {
				int baroff = (BarCodeWidth / StrBlk.BarcodeCount) * i;
				double tw = StrBlk.BarcodeWidth[i];
				double th = StrBlk.BarcodeHeight[i];
				double bw = tw * 15e-1;
				double bh = th * 15e-1;
				double xbarw = bw;
				double xbargap = (double)MaxX / (double)100;
				xoff = xoff - xbarw - (xbargap * 5);
				int offset = xoff;

				StretchBlt(
					hDC,
					offset,
					cury,
					bw,
					bh,
					Barcode->hDC,
					baroff,
					0,
					StrBlk.BarcodeWidth[i],
					StrBlk.BarcodeHeight[i],
					SRCCOPY);
			}
			SelectObject(hDC, hFontOld);
			SelectObject(hDC, hPenOld);
			DeleteObject(hFont);
			DeleteObject(hPen);
		}
   		if(Barcode) delete(Barcode);
	}
}
