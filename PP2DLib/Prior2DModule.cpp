#include "stdafx.h"

xParams xp;

Prior2DPPModule* Prior2DPPModule::_ppModule=NULL;

void Prior2DPPModule::setAccountA(CString& val){AccountA=val.Trim(_T(" "));}
void Prior2DPPModule::setAccountB(CString& val){AccountB=val.Trim(_T(" "));}
void Prior2DPPModule::setBankCodeA(CString& val){BankCodeA=val.Trim(_T(" "));}
void Prior2DPPModule::setBankCodeB(CString& val){BankCodeB=val.Trim(_T(" "));}
void Prior2DPPModule::setBankDivisionA(CString& val){BankDivisionA=val.Trim(_T(" "));}
void Prior2DPPModule::setBankDivisionB(CString& val){BankDivisionB=val.Trim(_T(" "));}
void Prior2DPPModule::setBankNameA(CString& val){BankNameA=val.Trim(_T(" "));}
void Prior2DPPModule::setBankNameB(CString& val){BankNameB=val.Trim(_T(" "));}
void Prior2DPPModule::setBN(CString& val){BN=val.Trim(_T(" "));}
void Prior2DPPModule::setBudgetPaymentCode(CString& val){BudgetPaymentCode=val.Trim(_T(" "));}
void Prior2DPPModule::setCodeBankaCor(CString& val){CodeBankaCor=val.Trim(_T(" "));}
void Prior2DPPModule::setComissiaInvoice(CString& val){ComissiaInvoice=val.Trim(_T(" "));}
void Prior2DPPModule::setCorrespondentA(CString& val){CorrespondentA=val.Trim(_T(" "));}
void Prior2DPPModule::setCurrency(CString& val){Currency=val.Trim(_T(" "));}
void Prior2DPPModule::setDataNomerPasport(CString& val){DataNomerPasport=val.Trim(_T(" "));}
void Prior2DPPModule::setDate(CString& val){Date=val.Trim(_T(" "));}
void Prior2DPPModule::setDetaliPlatezha(CString& val){DetaliPlatezha=val.Trim(_T(" "));}
void Prior2DPPModule::setNameA(CString& val){NameA=val.Trim(_T(" "));}
void Prior2DPPModule::setNameB(CString& val){NameB=val.Trim(_T(" "));}
void Prior2DPPModule::setNumber(CString& val){Number=val.Trim(_T(" "));}
void Prior2DPPModule::setOrdinary(CString& val){Ordinary=val.Trim(_T(" "));}
void Prior2DPPModule::setPaymentOrder(CString& val){PaymentOrder=val.Trim(_T(" "));}
void Prior2DPPModule::setPaymentPurpose(CString& val)
{
	val.Replace(_T("\n\r"),_T(" "));
	val.Replace(_T("\r\n"),_T(" "));
	val.Replace(_T("\n"),_T(" "));
	val.Replace(_T("\r"),_T(" "));
	PaymentPurpose=val.Trim(_T(" "));
}
void Prior2DPPModule::setPaymentType(CString& val){PaymentType=val.Trim(_T(" "));}
void Prior2DPPModule::setPL(CString& val){PL=val.Trim(_T(" "));}
void Prior2DPPModule::setPL_BN(CString& val){PL_BN=val.Trim(_T(" "));}
void Prior2DPPModule::setSchetBankCor(CString& val){SchetBankCor=val.Trim(_T(" "));}
void Prior2DPPModule::setSumDigits(CString& val){SumDigits=val.Trim(_T(" "));}
void Prior2DPPModule::setSumSpelled(CString& val){SumSpelled=val.Trim(_T(" "));}
void Prior2DPPModule::setUnnA(CString& val){UnnA=val.Trim(_T(" "));}
void Prior2DPPModule::setUnnB(CString& val){UnnB=val.Trim(_T(" "));}
void Prior2DPPModule::setUNP_Third(CString& val){UNP_Third=val.Trim(_T(" "));}
void Prior2DPPModule::setUrgent(CString& val){Urgent=val.Trim(_T(" "));}
void Prior2DPPModule::setCompanyFirstMan(CString& val){CompanyFirstMan=val.Trim(_T(" "));}
void Prior2DPPModule::setCompanySecondMan(CString& val){CompanySecondMan=val.Trim(_T(" "));}
void Prior2DPPModule::setDocCode(CString& val){DocCode=val.Trim(_T(" "));}
void Prior2DPPModule::setSeriaNum(CString& val){SeriaNum=val.Trim(_T(" "));}
void Prior2DPPModule::setLiveAdress(CString& val){LiveAdress=val.Trim(_T(" "));}
void Prior2DPPModule::setIssueDate(CString& val){IssueDate=val.Trim(_T(" "));}
void Prior2DPPModule::setIssueCompany(CString& val){IssueCompany=val.Trim(_T(" "));}

CString Prior2DPPModule::getAccountA(){return AccountA;}
CString Prior2DPPModule::getAccountB(){return AccountB;}
CString Prior2DPPModule::getBankCodeA(){return BankCodeA;}
CString Prior2DPPModule::getBankCodeB(){return BankCodeB;}
CString Prior2DPPModule::getBankDivisionA(){return BankDivisionA;}
CString Prior2DPPModule::getBankDivisionB(){return BankDivisionB;}
CString Prior2DPPModule::getBankNameA(){return BankNameA;}
CString Prior2DPPModule::getBankNameB(){return BankNameB;}
CString Prior2DPPModule::getBN(){return BN;}
CString Prior2DPPModule::getBudgetPaymentCode(){return BudgetPaymentCode;}
CString Prior2DPPModule::getCodeBankaCor(){return CodeBankaCor;}
CString Prior2DPPModule::getComissiaInvoice(){return ComissiaInvoice;}
CString Prior2DPPModule::getCorrespondentA(){return CorrespondentA;}
CString Prior2DPPModule::getCurrency(){return Currency;}
CString Prior2DPPModule::getDataNomerPasport(){return DataNomerPasport;}
CString Prior2DPPModule::getDate(){return Date;}
CString Prior2DPPModule::getDetaliPlatezha(){return DetaliPlatezha;}
CString Prior2DPPModule::getNameA(){return NameA;}
CString Prior2DPPModule::getNameB(){return NameB;}
CString Prior2DPPModule::getNotPaymentType(){return NotPaymentType;}
CString Prior2DPPModule::getNumber(){return Number;}
CString Prior2DPPModule::getOrdinary(){return Ordinary;}
CString Prior2DPPModule::getPaymentOrder(){return PaymentOrder;}
CString Prior2DPPModule::getPaymentPurpose(){return PaymentPurpose;}
CString Prior2DPPModule::getPaymentType(){return PaymentType;}
CString Prior2DPPModule::getPL(){return PL;}
CString Prior2DPPModule::getPL_BN(){return PL_BN;}
CString Prior2DPPModule::getSchetBankCor(){return SchetBankCor;}
CString Prior2DPPModule::getSumDigits(){return SumDigits;}
CString Prior2DPPModule::getSumSpelled(){return SumSpelled;}
CString Prior2DPPModule::getUnnA(){return UnnA;}
CString Prior2DPPModule::getUnnB(){return UnnB;}
CString Prior2DPPModule::getUNP_Third(){return UNP_Third;}
CString Prior2DPPModule::getUrgent(){return Urgent;}
CString Prior2DPPModule::getCompanyFirstMan(){return CompanyFirstMan;}
CString Prior2DPPModule::getCompanySecondMan(){return CompanySecondMan;}
CString Prior2DPPModule::getDocCode(){return DocCode;}
CString Prior2DPPModule::getSeriaNum(){return SeriaNum;}
CString Prior2DPPModule::getLiveAdress(){return LiveAdress;}
CString Prior2DPPModule::getIssueDate(){return IssueDate;}
CString Prior2DPPModule::getIssueCompany(){return IssueCompany;}



#pragma region Prior2DPPModule::BuildBarcode(bool bFull)
CBitmap* Prior2DPPModule::BuildBarcode(CDC *pDC,CBitmap *pBmp,bool bFull, int ibw, int ibh, bool bGlobalPrintBarcode)
{
	char *data = (char*)malloc(2048);
	ZeroMemory(&xp, sizeof(xParams));
	xp.data = data;

#pragma region XML
	
	CString DocCodeNum;
	if(bFull) {
		xp.formcode = 401600031;
		DocCodeNum.Format(_T("0401600031"));
	} else {
		xp.formcode = 401600036;
		DocCodeNum.Format(_T("0401600036"));
	}
	Utilites::formatXmlElements(xp.data, xp.datalen, bFull, 0, 
		1 , &Number,							// ����� ��������
		2 , &Date,								// ���� ���������
		3 , &Urgent,							// �������
		4 , &Ordinary,							// ���������
		5 , &DocCodeNum,						// ��� ���������
		6 , &SumSpelled,						// ����� ��������
		7 , &Currency,							// ��� ������
		8 , &SumDigits,							// ����� �������
		9 , &NameA,								// ������������ ����������
		10, &AccountA,							// ���� �����������
		11, &BankNameA,							// ���� �����������
		12, &BankCodeA,							// ��� ����� �����������
		13, &BankDivisionA,						// ��� �������. ����� �����������
		14, &BankNameB,							// ���� ����������
		15, &BankCodeB,							// ��� ����� ����������
		16, &BankDivisionB,						// ��� �������. ����� ����������
		17, &NameB,								// ����������
		18, &AccountB,							// ����� ����� �����������
		19, &PaymentPurpose,					// ���������� �������
		20, &UnnA,								// ��� ����� ����� ������
		21, &UnnB,								// ��� �����������
		22, &UNP_Third,							// ��� �������� ����
		23, &BudgetPaymentCode,					// ��� �������
		24, &PaymentOrder,						// �������
		25, &CorrespondentA,					// �������. ����� ����������
		26, &CodeBankaCor,						// ��� ����� �������������
		27, &SchetBankCor,						// ���� ����� ��������������
		28, &PL,								// ��
		29, &BN,								// ��
		30, &PL_BN,								// �� ��
		31, &ComissiaInvoice,					// ������. ������� �� �����
		32, &DataNomerPasport,					// ��������������� ����� ������
		33, &DetaliPlatezha,					// ������ �������
		34, &CompanyFirstMan,					// ������� � �������� ������� ���� �����������
		35, &CompanySecondMan,					// ������� � �������� ������� ���� �����������
		36, &DocCode,							// ��� ���������
		37, &SeriaNum,							// ����� � �����
		38, &LiveAdress,						// ����� ����� ���������� 
		39, &IssueDate,							// ���� ������
		40, &IssueCompany,						// �����, �������� ��������
		NULL, NULL
		);
#pragma endregion
#pragma region Draw
// !!!!!
	double widthmm = (double)(ibw)/(double)210; // �������������� ����� � 1�� 	
	double heightmm = (double)(ibh)/(double)297; // ������������ ����� � 1 ��
	int iGap = 9, 
		vGap = 3,				
		iXGap = widthmm*20,		
		iXRightGap = widthmm*10,		
		iYGap = heightmm*20,
		iYstep = 20; // ������ � ��
	int iCurY = 20*heightmm;
	int stride;
	char *buf;
	CBitmap *oldBmp = Utilites::InitializeForPrinting(pDC, pBmp, ibw, ibh, &buf, &stride);
	CString DocumentType;
	CString cstr;
	
	CFont font;	
	CFont titlefont;
	CFont middlefont;

	font.CreateFontW(56, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Times New Roman"));
	titlefont.CreateFontW(42, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Times New Roman"));
	middlefont.CreateFontW(48, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Times New Roman"));	
	CFont littleFont;
	littleFont.CreateFontW(36, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Times New Roman"));

	CPen* pPen = new CPen(PS_SOLID, 3, RGB(0,0,0));

	pPen = pDC->SelectObject(pPen);
	CFont* oldFont = pDC->SelectObject(&font);

	RECT r; r.top=0; r.left=0; r.right=ibw; r.bottom=ibh;
	CBrush whiteBrush(RGB(255,255,255));
	pDC->FillRect(&r,&whiteBrush);	

	/*pDC->MoveTo(0,0);
	pDC->LineTo(ibw, 0);
	pDC->MoveTo(0,ibh);
	pDC->LineTo(ibw, ibh);
	pDC->MoveTo(ibw,0);
	pDC->LineTo(ibw, ibh);
	pDC->MoveTo(0,0);
	pDC->LineTo(0, ibh);*/

	int iht0=0, iht=0;
	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	CRect cr(iXGap+iGap,iYGap+iGap,iXGap+iGap,iYGap+iGap);
	cstr.Format(_T("��������� ���������   �  %s"),Number);
	iht = pDC->DrawText(cstr, &cr, DT_NOCLIP);

	iYstep += 7;
	iCurY = iYstep*heightmm;
	int curX = iXGap;
	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	curX += 92*widthmm; //������ ���� 92 
	pDC->MoveTo(curX,iYGap);
	pDC->LineTo(curX,iCurY);

	cr.left = curX + iGap;
	cr.right = cr.left + 12*30;
	cstr.Format(_T("����  %s"), Date);
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	curX += 35*widthmm; //������ ���� 35
	pDC->MoveTo(curX,iYGap);
	pDC->LineTo(curX,iCurY);

	CFont* oldLittleFont = pDC->SelectObject(&littleFont);

	cr.left = curX + iGap;
	cr.right = cr.left + 13*widthmm; // ������ ���� 13
	cstr.Format(_T("�������"));
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	curX += 13*widthmm;
	pDC->MoveTo(curX,iYGap);
	pDC->LineTo(curX,iCurY);

	cr.left = curX + iGap;
	cr.right = curX - iGap + 5*widthmm; //������ ���� 5
	pDC->DrawText(Urgent, &cr, DT_NOCLIP|DT_CENTER);

	curX += 5*widthmm;
	pDC->MoveTo(curX,iYGap);
	pDC->LineTo(curX,iCurY);

	cr.left = curX + iGap;
	cr.right = cr.left + 15*widthmm; //������ ���� 15
	cstr.Format(_T("���������"));
	pDC->DrawText(cstr, &cr, DT_NOCLIP);

	curX += 15*widthmm; // ������ ���� 15
	pDC->MoveTo(curX,iYGap);
	pDC->LineTo(curX,iCurY);

	cr.left = curX + iGap;
	cr.right = curX - iGap + 5*widthmm; //������ ���� 5
	pDC->DrawText(Ordinary, &cr, DT_NOCLIP|DT_CENTER);

	curX += 5*widthmm;
	pDC->MoveTo(curX,iYGap);
	pDC->LineTo(curX,iCurY);

	CFont f2;
	f2.CreateFontW(32, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	pDC->SelectObject(&f2);
	

	cr.left = curX + iGap;
	cr.right = cr.left + 15*widthmm;
	DocumentType = DocCodeNum;
	pDC->DrawText(DocCodeNum, &cr, DT_NOCLIP);

	pDC->SelectObject(&font);

	cr.left = iXGap+iGap;
	cr.right = ibw-iXRightGap - iGap;	
	cr.top = iCurY + iGap;
	cr.bottom = ibh-iYGap;

	cstr.Format(_T("����� � ������:  %s"), SumSpelled);
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK|DT_EDITCONTROL);

	iYstep += 22;
	iCurY = iYstep*heightmm;

	curX = ibw - 87*widthmm - iXRightGap; 
	cr.left = curX + iGap;
	cr.right = cr.left + 13*widthmm;
	cr.top = vGap+(iYstep-7)*heightmm;
	pDC->SelectObject(&titlefont);
	cstr.Format(_T("��� ������"));
	pDC->DrawText(cstr,cr,DT_WORDBREAK);

	curX += 13*widthmm;
	pDC->MoveTo(curX, iCurY);
	pDC->LineTo(curX, (iYstep-7)*heightmm);


	cr.left = curX + iGap;
	cr.right = curX + 13*widthmm - iGap;
	cr.top = (iYstep-7)*heightmm+iGap;
	cr.bottom = iCurY;
	pDC->SelectObject(&font);
	pDC->DrawText(Currency,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	curX += 13*widthmm;
	pDC->MoveTo(curX, iCurY);
	pDC->LineTo(curX, (iYstep-7)*heightmm);

	cr.left = curX + iGap;
	cr.right = curX + 16*widthmm;
	cr.top = vGap+(iYstep-7)*heightmm;
	pDC->SelectObject(&titlefont);
	cstr.Format(_T("����� �������"));
	pDC->DrawText(cstr,cr,DT_WORDBREAK);

	curX += 16*widthmm;
	pDC->MoveTo(curX, iCurY);
	pDC->LineTo(curX, (iYstep-7)*heightmm);

	cr.left = ibw-iXRightGap-45*widthmm+iGap;
	cr.right = ibw-iXRightGap;
	cr.top = (iYstep-7)*heightmm+iGap;
	pDC->SelectObject(&font);
	pDC->DrawText(SumDigits,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	pDC->MoveTo(ibw-iXRightGap-87*widthmm,(iYstep-7)*heightmm);
	pDC->LineTo(ibw-iXRightGap, (iYstep-7)*heightmm);

	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	pDC->MoveTo(ibw-iXRightGap-87*widthmm, iCurY);
	pDC->LineTo(ibw-iXRightGap-87*widthmm, (iYstep-7)*heightmm);

	//����������	

	/*CString dCode = DocCode + _T("-");
	if (DocCode == _T("01"))
		dCode += _T("����. ��.���� 1974�.");		
	else if (DocCode == _T("02"))		
		dCode += _T("����. ��. �� 1993�.");
	else if (DocCode == _T("03"))
		dCode += _T("����. ��. ��");
	else if (DocCode == _T("04"))
		dCode += _T("���. �.� 9");
	else if (DocCode == _T("05"))
		dCode += _T("��. � ������.");
	else if (DocCode == _T("06"))
		dCode += _T("��� �� �-��");
	else if (DocCode == _T("07"))
		dCode += _T("�����. �������");
	else if (DocCode == _T("08"))
		dCode += _T("��. � �������. ��. �������");
	else if (DocCode == _T("09"))
		dCode += _T("������. ���. ����. ��. ��. �-��");
	else if (DocCode == _T("10"))
		dCode += _T("���, �������. ����. ���� ��� ��.");
	else if (DocCode == _T("11"))
		dCode += _T("�����. ����� �����. ��. ������");
	else if (DocCode == _T("12"))
		dCode += _T("���. �����������.");
	else if (DocCode == _T("13"))
		dCode += _T("����. ������ (��. ����. ������)");
	else if (DocCode == _T("14"))
		dCode += _T("��. � �������. ���. ������ � ��");
	else if (DocCode == _T("99"))
		dCode += _T("���� ��������");
	else	
		dCode = DocCode;*/

	CString dCode = DocCode + _T("-");
	if (DocCode == _T("03"))
		dCode += _T("�������");
	else if (DocCode == _T("06"))
		dCode += _T("��� �� �");	
	else	
		dCode = DocCode;

	cstr.Format(_T("����������: %s"),NameA + _T(" ") + LiveAdress + _T(" ") + 
		dCode + _T(" ") + SeriaNum + _T(" ") + IssueDate + _T(" ") + IssueCompany);
	cr.top = iCurY+vGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXRightGap-iGap;
	cr.bottom = (iYstep+17)*heightmm ;
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK|DT_EDITCONTROL);

	iYstep += 17;
	iCurY = iYstep*heightmm;
	
	if(bFull)
	{
		pDC->MoveTo(ibw - iXRightGap-86*widthmm, (iYstep-7)*heightmm);
	}
	else
	{
		pDC->MoveTo(ibw - iXRightGap - 58*widthmm, (iYstep-7)*heightmm);
	}	
	pDC->LineTo(ibw-iXRightGap, (iYstep-7)*heightmm);

	cr.top = iGap+(iYstep-7)*heightmm;
	
	if(bFull)
	{
		cr.left = ibw - iXRightGap-86*widthmm+iGap;
		cr.right = ibw - iXRightGap-73*widthmm-iGap;
	}
	else
	{
		cr.left = ibw - iXRightGap-58*widthmm+iGap;
		cr.right = ibw - iXRightGap-45*widthmm-iGap;
	}		
	cr.bottom = iCurY;
	pDC->SelectObject(&titlefont);
	cstr.Format(_T("���� �"));
	iht = pDC->DrawText(cstr,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	if(bFull)
	{
		cr.left = ibw - iXRightGap-73*widthmm+iGap;
	}
	else
	{
		cr.left = ibw - iXRightGap-45*widthmm+iGap;
	}	
	cr.right = ibw-iXRightGap-iGap;
	pDC->SelectObject(&font);
	pDC->DrawText(AccountA,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	if(bFull)
	{
	pDC->MoveTo(ibw - iXRightGap-86*widthmm, iCurY);
	pDC->LineTo(ibw - iXRightGap-86*widthmm, (iYstep-7)*heightmm);

	pDC->MoveTo(ibw - iXRightGap-73*widthmm, iCurY);
	pDC->LineTo(ibw - iXRightGap-73*widthmm, (iYstep-7)*heightmm);
	}
	else
	{
		pDC->MoveTo(ibw - iXRightGap-58*widthmm, iCurY);
		pDC->LineTo(ibw - iXRightGap-58*widthmm, (iYstep-7)*heightmm);

		pDC->MoveTo(ibw - iXRightGap-45*widthmm, iCurY);
		pDC->LineTo(ibw - iXRightGap-45*widthmm, (iYstep-7)*heightmm);
	}

	//BANK A

	cr.left = iXGap+iGap;
	cr.right = ibw - iXRightGap - iGap;
	cr.top = iCurY + iGap;
	cr.bottom = (iYstep+13)*heightmm;
	cstr.Format(_T("����-�����������:  %s"), BankNameA);
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_EDITCONTROL);

	iYstep += 13;
	iCurY = iYstep*heightmm;

	pDC->MoveTo(ibw - iXRightGap - 58*widthmm, (iYstep-7)*heightmm);
	pDC->LineTo(ibw-iXRightGap, (iYstep-7)*heightmm);
	cr.top = vGap+(iYstep-7)*heightmm;
	cr.bottom = iCurY;
		curX = ibw-iXRightGap-58*widthmm;
		cr.left = curX + iGap;
		cr.right = curX +13*widthmm - iGap;
		pDC->SelectObject(&titlefont);
		cstr.Format(_T("��� �����"));
		iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_CENTER);
		
		pDC->MoveTo(curX, iCurY);
		pDC->LineTo(curX, (iYstep-7)*heightmm);

		curX += 13*widthmm;		
		pDC->MoveTo(curX, iCurY);
		pDC->LineTo(curX, (iYstep-7)*heightmm);

		cr.left = curX + iGap;
		cr.right = curX + 32*widthmm - iGap;
		pDC->SelectObject(&font);
		pDC->DrawText(BankCodeA, cr, DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		curX += 32*widthmm;
		pDC->MoveTo(curX, iCurY);
		pDC->LineTo(curX, (iYstep-7)*heightmm);		

		cr.left = curX + iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->DrawText(BankDivisionA, cr, DT_CENTER|DT_VCENTER|DT_SINGLELINE);		
	
	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);


	//BANK B
	cr.left = iXGap+iGap;
	//cr.right = ibw - iXRightGap - iGap- 58*widthmm;
	cr.right = ibw - iXRightGap - iGap;
	cr.top = iCurY + iGap;
	iYstep += 13;
	iCurY = iYstep*heightmm;
	cr.bottom = iCurY;
	cstr.Format(_T("����-����������:  %s"), BankNameB);
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_EDITCONTROL);

	

	pDC->MoveTo(ibw - iXRightGap - 58*widthmm, (iYstep-7)*heightmm);
	pDC->LineTo(ibw-iXRightGap, (iYstep-7)*heightmm);
	cr.top = vGap+(iYstep-7)*heightmm;

		curX = ibw-iXRightGap-58*widthmm;
		cr.left = curX + iGap;
		cr.right = curX +13*widthmm - iGap;
		pDC->SelectObject(&titlefont);
		cstr.Format(_T("��� �����"));
		iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_CENTER);
		
		pDC->MoveTo(curX, iCurY);
		pDC->LineTo(curX, (iYstep-7)*heightmm);

		curX += 13*widthmm;		
		pDC->MoveTo(curX, iCurY);
		pDC->LineTo(curX, (iYstep-7)*heightmm);

		cr.left = curX + iGap;
		cr.right = curX + 32*widthmm - iGap;
		pDC->SelectObject(&font);
		pDC->DrawText(BankCodeB, cr, DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		curX += 32*widthmm;
		pDC->MoveTo(curX, iCurY);
		pDC->LineTo(curX, (iYstep-7)*heightmm);		

		cr.left = curX + iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->DrawText(BankDivisionB, cr, DT_CENTER|DT_VCENTER|DT_SINGLELINE);		
	
	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	//����������
	cstr.Format(_T("����������:  %s"),NameB);
	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXRightGap-iGap;
	iYstep += 17;
	iCurY = iYstep*heightmm;
	cr.bottom = iCurY;
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK|DT_EDITCONTROL);	

	if(bFull)
	{
		pDC->MoveTo(ibw-iXRightGap-83*widthmm, (iYstep-7)*heightmm);
	}
	else
	{
		pDC->MoveTo(ibw-iXRightGap-58*widthmm, (iYstep-7)*heightmm);
	}		
	pDC->LineTo(ibw-iXRightGap, (iYstep-7)*heightmm);

	cr.top = (iYstep-7)*heightmm+vGap;
	if(bFull)
	{
		cr.left = ibw-iXRightGap-83*widthmm+iGap;
		cr.right = ibw-iXRightGap-73*widthmm-iGap;
	}
	else
	{
		cr.left = ibw-iXRightGap-58*widthmm+iGap;
		cr.right = ibw-iXRightGap-45*widthmm-iGap;
	}
		
	pDC->SelectObject(&titlefont);
	cstr.Format(_T("���� �"));
	iht = pDC->DrawText(cstr,cr,DT_WORDBREAK|DT_CENTER);

	pDC->SelectObject(&font);
	cstr.Format(_T("%s"),AccountB);

	if(bFull)
	{
		cr.left = ibw-iXRightGap-73*widthmm+iGap;
	}
	else
	{
		cr.left = ibw-iXRightGap-45*widthmm+iGap;
	}
	cr.right = ibw-iXRightGap-iGap;
	pDC->DrawText(cstr,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	if(bFull)
	{
		pDC->MoveTo(ibw-iXRightGap-83*widthmm, iCurY);
		pDC->LineTo(ibw-iXRightGap-83*widthmm, (iYstep-7)*heightmm);

		pDC->MoveTo(ibw-iXRightGap-73*widthmm, iCurY);
		pDC->LineTo(ibw-iXRightGap-73*widthmm, (iYstep-7)*heightmm);
	}
	else
	{
		pDC->MoveTo(ibw-iXRightGap-58*widthmm, iCurY);	
		pDC->LineTo(ibw-iXRightGap-58*widthmm, (iYstep-7)*heightmm);

		pDC->MoveTo(ibw-iXRightGap-45*widthmm, iCurY);	
		pDC->LineTo(ibw-iXRightGap-45*widthmm, (iYstep-7)*heightmm);
	}

	//PURPOSE
	
	cstr.Format(_T("���������� �������:   %s"),PaymentPurpose);
	cr.left = iXGap + iGap;
	cr.right = ibw - iXRightGap - iGap;
	cr.top = iCurY+iGap;
	iYstep += 25;
	iCurY = iYstep*heightmm;
	cr.bottom = iCurY;
	iht = pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_EDITCONTROL);
	
	

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	//UNNs

	pDC->MoveTo(iXGap, (iYstep+5)*heightmm);
	pDC->LineTo(ibw-iXRightGap, (iYstep+5)*heightmm);

	int iUNNWidth = 41*widthmm;
	cr.left = iXGap+iGap;
	cr.right = iXGap-iGap+iUNNWidth;
	cr.top = iCurY+iGap;
	cr.bottom = (iYstep+10)*heightmm - iGap;
	pDC->SelectObject(&titlefont);
	iht = pDC->DrawText(_T("��� �����������"), cr, DT_CENTER);

	cr.top = vGap+(iYstep+5)*heightmm;
	pDC->SelectObject(&font);
	pDC->DrawText(UnnA, cr, DT_CENTER);

	cr.left = iXGap+iUNNWidth+iGap;
	cr.right = iXGap-iGap+2*iUNNWidth;
	cr.top = iCurY+iGap;
	pDC->SelectObject(&titlefont);
	pDC->DrawText(_T("��� �����������"), cr, DT_CENTER);

	cr.top = vGap+(iYstep+5)*heightmm;
	pDC->SelectObject(&font);
	pDC->DrawText(UnnB, cr, DT_CENTER);


	cr.left = iXGap+iGap+2*iUNNWidth;
	cr.right = iXGap-iGap+3*iUNNWidth;
	cr.top = iCurY+iGap;
	pDC->SelectObject(&titlefont);
	pDC->DrawText(_T("��� �������� ����"), cr, DT_CENTER);

	cr.top = vGap+(iYstep+5)*heightmm;
	pDC->SelectObject(&font);
	pDC->DrawText(UNP_Third, cr, DT_CENTER);

	cr.left = iXGap+iGap+3*iUNNWidth;
	cr.right = iXGap-iGap+4*iUNNWidth;
	cr.top = iCurY+iGap;
	pDC->SelectObject(&titlefont);
	pDC->DrawText(_T("��� �������"), cr, DT_CENTER);
	cr.top = vGap+(iYstep+5)*heightmm;
	pDC->SelectObject(&font);
	pDC->DrawText(BudgetPaymentCode, cr, DT_CENTER);

	cr.left = iXGap+iGap+4*iUNNWidth;
	cr.right = ibw-iXRightGap-iGap;
	cr.top = iCurY+iGap;
	pDC->SelectObject(&titlefont);
	pDC->DrawText(_T("�������"), cr, DT_CENTER);
	cr.top = vGap+(iYstep+5)*heightmm;
	pDC->SelectObject(&font);
	pDC->DrawText(PaymentOrder, cr, DT_CENTER);

	pDC->MoveTo(iXGap+iUNNWidth, iCurY);
	pDC->LineTo(iXGap+iUNNWidth, (iYstep+10)*heightmm);

	pDC->MoveTo(iXGap+2*iUNNWidth, iCurY);
	pDC->LineTo(iXGap+2*iUNNWidth, (iYstep+10)*heightmm);

	pDC->MoveTo(iXGap+3*iUNNWidth, iCurY);
	pDC->LineTo(iXGap+3*iUNNWidth, (iYstep+10)*heightmm);

	pDC->MoveTo(iXGap+4*iUNNWidth, iCurY);
	pDC->LineTo(iXGap+4*iUNNWidth, (iYstep+10)*heightmm);

	iYstep += 10;
	iCurY = iYstep*heightmm;

	pDC->MoveTo(iXGap, iCurY);
	pDC->LineTo(ibw - iXRightGap, iCurY);

	if(bFull)
	{
		curX = ibw - iXRightGap - 131*widthmm;

		//������������� �����-����������
		cr.top = iCurY+vGap;
		cr.left = iXGap+iGap;
		cr.right = curX-iGap;
		cr.bottom = (iYstep+13)*heightmm;
		pDC->SelectObject(&middlefont);
		cstr.Format(_T("������������� �����-����������:"));
		pDC->DrawText(cstr, cr, DT_WORDBREAK);

		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+7)*heightmm);

		

		cr.left = curX + iGap;
		cr.right = curX + 13*widthmm - iGap;
		cr.bottom = (iYstep+7)*heightmm - vGap;
		pDC->SelectObject(&titlefont);
		cstr.Format(_T("��� �����"));
		iht = pDC->DrawText(cstr, cr, DT_WORDBREAK);
		
		curX += 13*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+7)*heightmm);

		cr.left = curX + iGap;
		cr.right = curX + 35*widthmm - iGap;
		pDC->SelectObject(&font);
		pDC->DrawText(CodeBankaCor, cr, DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		curX += 35*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+7)*heightmm);

		cr.left = curX + iGap;
		cr.right = curX + 10*widthmm - iGap;
		pDC->SelectObject(&titlefont);
		cstr.Format(_T("���� �"));
		pDC->DrawText(cstr, cr, DT_WORDBREAK|DT_CENTER);

		curX += 10*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+7)*heightmm);

		cr.left = curX + iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->SelectObject(&font);
		pDC->DrawText(SchetBankCor, cr, DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		cr.top = (iYstep+7)*heightmm+iGap;
		cr.left = iXGap+iGap;
		cr.right = ibw-iXRightGap-iGap;
		cr.bottom = (iYstep+13)*heightmm;
		//pDC->DrawText(CorrespondentA, cr, DT_WORDBREAK|DT_EDITCONTROL);
		pDC->DrawText(CorrespondentA, cr, DT_WORDBREAK);

		pDC->MoveTo(ibw - iXRightGap - 131*widthmm,(iYstep+7)*heightmm);
		pDC->LineTo(ibw-iXRightGap,(iYstep+7)*heightmm);

		iYstep += 13;
		iCurY = iYstep*heightmm;

		pDC->MoveTo(iXGap, iCurY);
		pDC->LineTo(ibw-iXRightGap, iCurY);

		// ������� �� ��������
		cr.bottom = (iYstep+5)*heightmm - vGap;

		cr.top = iCurY+iGap;
		cr.left = iXGap + iGap;
		cr.right = cr.left + 35*widthmm - iGap;
		pDC->SelectObject(&titlefont);
		cstr.Format(_T("������� �� ��������"));
		iht = pDC->DrawText(cstr,cr,DT_CENTER);

		curX = iXGap + 35*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);

		pDC->SelectObject(&littleFont);

		cr.left = curX + iGap;
		cr.right = curX + 5*widthmm - iGap;
		cstr.Format(_T("��"));		
		pDC->DrawText(cstr,cr,DT_CENTER);

		curX += 5*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);

		pDC->SelectObject(&font);

		cr.left = curX + iGap;
		cr.right = curX - iGap + 5*widthmm;
		pDC->DrawText(PL,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		curX += 5*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);
		
		pDC->SelectObject(&littleFont);

		cr.left = curX + iGap;
		cr.right = curX - iGap + 5*widthmm;
		cstr.Format(_T("��"));
		pDC->DrawText(cstr,cr,DT_CENTER);

		curX += 5*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);

		pDC->SelectObject(&font);
		
		cr.left = curX + iGap;
		cr.right = curX - iGap + 5*widthmm;
		pDC->DrawText(BN,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		curX += 5*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);		

		pDC->SelectObject(&littleFont);

		cr.left = curX + iGap;
		cr.right = curX - iGap + 10*widthmm; //������ ���� 10
		cstr.Format(_T("��/��"));
		pDC->DrawText(cstr,cr,DT_CENTER);

		curX += 10*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);

		pDC->SelectObject(&font);

		cr.left = curX + iGap; 
		cr.right = curX - iGap + 5*widthmm;
		pDC->DrawText(PL_BN,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		curX += 5*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);		

		cr.left = curX + iGap;
		cr.right = curX - iGap + 45*widthmm;
		pDC->SelectObject(&titlefont);
		cstr.Format(_T("�������� ������� �� ����� �"));
		pDC->DrawText(cstr,cr,DT_CENTER);

		curX += 45*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+5)*heightmm);

		cr.left = curX + iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->SelectObject(&font);
		pDC->DrawText(ComissiaInvoice,cr,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		iYstep += 5;
		iCurY = iYstep*heightmm;

		pDC->MoveTo(iXGap,iCurY);
		pDC->LineTo(ibw-iXRightGap,iCurY);

		//���� � ����� �������� ������
		cr.bottom = ibh;

		cr.top = iCurY+iGap;
		cr.left = iXGap+iGap;
		cr.right = ibw-iXRightGap - iGap;
		pDC->SelectObject(&font);
		cstr.Format(_T("��������������� ����� ������: %s"),DataNomerPasport);
		iht = pDC->DrawText(cstr,cr,DT_WORDBREAK|DT_EDITCONTROL);

		iYstep += 7;
		iCurY = iYstep*heightmm;

		pDC->MoveTo(iXGap,iCurY);
		pDC->LineTo(ibw-iXRightGap,iCurY);

		//������ �������
		cr.top = iCurY+vGap;
		cr.left = iXGap+iGap;
		cr.right = iXGap - iGap + 16*widthmm;
		pDC->SelectObject(&titlefont);
		iht = pDC->DrawText(_T("������ �������"),cr,DT_WORDBREAK);

		curX = iXGap + 16*widthmm;
		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+7)*heightmm);
		
		cr.left = curX + iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->SelectObject(&font);
		pDC->DrawText(DetaliPlatezha,cr,DT_WORDBREAK|DT_EDITCONTROL);

		iYstep += 7;
		iCurY= iYstep*heightmm;

		pDC->MoveTo(iXGap,iCurY);
		pDC->LineTo(ibw-iXRightGap,iCurY);
	}

	cr.bottom = ibh;

	//����������� ������
	cr.top = iCurY+iGap;
	cr.left = iXGap+iGap;
	cr.right = ibw-iXRightGap-iGap;
	pDC->SelectObject(&middlefont);
	iht = pDC->DrawText(_T("����������� ������:"),cr,0);

	iYstep += 5;
	iCurY = iYstep*heightmm;

	pDC->MoveTo(iXGap,iCurY);
	pDC->LineTo(ibw-iXRightGap,iCurY);

	if(bFull)
	{
		cr.top = iCurY+iGap;
		cr.left = iXGap+iGap;
		cr.right = ibw-iXRightGap-iGap;
		iht = pDC->DrawText(_T("����� � ������������/��������:"),cr,0);

		iYstep += 10;
		iCurY = iYstep*heightmm;

		pDC->MoveTo(iXGap,iCurY);
		pDC->LineTo(ibw-iXRightGap,iCurY);

		cr.top = iCurY+iGap;
		cr.left = iXGap+iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->DrawText(_T("������������� �����-�����������:"),cr,0);
		cr.top = (iYstep+10)*heightmm;
		pDC->DrawText(_T("���� �������������"),cr,0);
		cr.left = ibw - 60*widthmm - iXRightGap;
		pDC->DrawText(_T("�������"),cr,0);

		iYstep += 15;
		iCurY = iYstep*heightmm;

		pDC->MoveTo(iXGap,iCurY);
		pDC->LineTo(ibw-iXRightGap,iCurY);
	}

	cr.top = iCurY+vGap;
	cr.left = iXGap+iGap;
	cr.right = iXGap-iGap+37*widthmm;
	iht = pDC->DrawText(_T("����� �����"),cr,DT_CENTER);

	curX = iXGap + 37*widthmm;

	pDC->MoveTo(curX,iCurY);
	pDC->LineTo(curX,(iYstep+12)*heightmm);

	cr.left = curX+iGap;
	cr.right = curX-iGap+37*widthmm;
	pDC->DrawText(_T("������ �����"),cr,DT_CENTER);

	curX += 37*widthmm;

	pDC->MoveTo(curX,iCurY);
	pDC->LineTo(curX,(iYstep+12)*heightmm);

	cr.left = curX + iGap;
	cr.right = curX - iGap + 13*widthmm;
	pDC->SelectObject(&titlefont);
	pDC->DrawText(_T("��� ������"),cr,DT_WORDBREAK);

	curX+=13*widthmm;

	pDC->MoveTo(curX,iCurY);
	pDC->LineTo(curX,(iYstep+12)*heightmm);

	pDC->SelectObject(&middlefont);

	if(bFull)
	{		
		cr.left = curX + iGap;
		cr.right = curX - iGap + 41*widthmm;
		pDC->DrawText(_T("����� ��������"),cr,DT_CENTER);

		curX += 41*widthmm;

		pDC->MoveTo(curX,iCurY);
		pDC->LineTo(curX,(iYstep+12)*heightmm);

		cr.left = curX + iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->DrawText(_T("���������� � ����������� ������"),cr,DT_CENTER);

		pDC->MoveTo(iXGap,(iYstep+7)*heightmm);
		pDC->LineTo(ibw-iXRightGap, (iYstep+7)*heightmm);

		pDC->MoveTo(iXGap,(iYstep+12)*heightmm);
		pDC->LineTo(ibw-iXRightGap, (iYstep+12)*heightmm);
	}
	else
	{
		cr.left = curX+iGap;
		cr.right = ibw-iXRightGap-iGap;
		pDC->DrawText(_T("����� ��������"),cr,DT_CENTER);

		pDC->MoveTo(iXGap,(iYstep+7)*heightmm);
		pDC->LineTo(ibw-iXRightGap, (iYstep+7)*heightmm);

		pDC->MoveTo(iXGap,(iYstep+12)*heightmm);
		pDC->LineTo(ibw-iXRightGap, (iYstep+12)*heightmm);
	}

	iYstep += 12;
	iCurY = iYstep*heightmm;
	pDC->MoveTo(iXGap,iYGap);
	pDC->LineTo(iXGap, iCurY);

	pDC->MoveTo(ibw-iXRightGap,iYGap);
	pDC->LineTo(ibw-iXRightGap, iCurY);

	iYstep += 2;
	iCurY = iYstep*heightmm;

	cr.top = iCurY+iGap;
	cr.left = iXGap;
	cr.right = ibw/2-iGap;
	pDC->SelectObject(&middlefont);
	iht = pDC->DrawText(_T("������� �����������"),cr,0);

	cr.top = cr.top+iht+2*iGap;
	cr.left = iXGap;
	cr.right = ibw/2-iGap;
	pDC->SelectObject(&font);
	iht = pDC->DrawText(CompanyFirstMan,cr,DT_WORDBREAK);

	cr.top = cr.top + iht + 2*iGap;
	cr.left = iXGap;
	cr.right = ibw/2-iGap;
	iht = pDC->DrawText(CompanySecondMan,cr,DT_WORDBREAK);

	cr.top = cr.top+iht + 2*iGap;
	pDC->SelectObject(&middlefont);
	pDC->DrawText(_T("�.�."),cr,0);

	cr.top = iCurY+iGap;
	cr.left = ibw/2;
	cr.right = ibw-iXGap;
	iht = pDC->DrawText(_T("���� �����������"),cr,0);

	cr.top = iCurY+3*iGap+iht;
	pDC->DrawText(_T("������� �������������� �����������"),cr,0);

	cr.top = iCurY+5*iGap+2*iht;
	pDC->DrawText(_T("���� ���������� ������"),cr,0);

	cr.top = iCurY+7*iGap+3*iht;
	pDC->DrawText(_T("����� �����"),cr,0);

	DWORD dwReads;
	char szCode[32];
	CString strCode;
	CString strFile;

	DWORD sDirLen = ::GetCurrentDirectory(0, NULL)+1;
	TCHAR *sDir = new TCHAR[sDirLen];
	::GetCurrentDirectory(sDirLen, sDir);
	sDir[sDirLen - 1]=0;
	strFile.Format(_T("%s\\id.txt"), CString(sDir));
	delete sDir;

	TCHAR *p = strFile.GetBuffer(sDirLen);

	if(DocumentType == L"0401600036") {
		xp.formcode = 401600036;
	}
	if(DocumentType == L"0401600031") {
		xp.formcode = 401600031;
	}
	font.DeleteObject();
#ifdef OLDMETHOD
	font.CreateFontW(70, 0, 0, 0, 500, 0, 0, 0, 0, 1, 2, 2, 34,_T("Arial"));
	pDC->SelectObject(&font);
	cr.top = ibh - 490;
	pDC->DrawText(strCode, cr, 0);

	iCurY+=6*iGap+3*iht - 12*11;
	//////////////////

	pPen = pDC->SelectObject(pPen);
	delete pPen;
#endif
	pDC->SelectObject(oldFont);
#pragma endregion 

	pDC->SelectObject(oldBmp);
	BarcodePainter bp(buf, iXGap, ibh - 400, ibw - 2 * iXGap, (ibh*2) - 2 * iYGap/*-iCurY*/, stride);
	pDC->SelectObject(pBmp);
	RECT ro;
	ZeroMemory(&r, sizeof(RECT));
	r.right = ibw;
	r.bottom = ibh;
	CopyMemory(&ro, &r, sizeof(RECT));
	xp.dataact = xp.datalen;
	xp.yoff = 3;
	xp.error_level = 5;
	if(bGlobalPrintBarcode) {
		bp.DrawBarcode(pDC->m_hDC, &r, &ro, &xp);
	}
	free(data);

	return pBmp;
}
#pragma endregion


void PP2DLIB_API PPInit()
{
	Prior2DPPModule::_ppModule = new Prior2DPPModule();
}
void PPFinish()
{
	delete Prior2DPPModule::_ppModule;
}

void PPBuildBarcode(char* sFileName, bool bFull)
{
	int ibw = 2480;	
	int ibh = 3508;
	CDC dc;
	CBitmap bmp;
	Prior2DPPModule::_ppModule->BuildBarcode(&dc, &bmp, bFull, ibw, ibh);
}


void PPSetAccountA(char* val){Prior2DPPModule::_ppModule->setAccountA(CString(val));}
void PPSetAccountB(char*  val){Prior2DPPModule::_ppModule->setAccountB(CString(val));}
void PPSetBankCodeA(char*  val){Prior2DPPModule::_ppModule->setBankCodeA(CString(val));}
void PPSetBankCodeB(char*  val){Prior2DPPModule::_ppModule->setBankCodeB(CString(val));}
void PPSetBankCBUA(char*  val){Prior2DPPModule::_ppModule->setBankDivisionA(CString(val));}
void PPSetBankCBUB(char*  val){Prior2DPPModule::_ppModule->setBankDivisionB(CString(val));}
void PPSetBankNameA(char*  val){Prior2DPPModule::_ppModule->setBankNameA(CString(val));}
void PPSetBankNameB(char*  val){Prior2DPPModule::_ppModule->setBankNameB(CString(val));}
void PPSetBN(char*  val){Prior2DPPModule::_ppModule->setBN(CString(val));}
void PPSetBudgetPaymentCode(char*  val){Prior2DPPModule::_ppModule->setBudgetPaymentCode(CString(val));}
void PPSetBankCodeC(char*  val){Prior2DPPModule::_ppModule->setCodeBankaCor(CString(val));}
void PPSetComissiaInvoice(char*  val){Prior2DPPModule::_ppModule->setComissiaInvoice(CString(val));}
void PPSetBankNameC(char*  val){Prior2DPPModule::_ppModule->setCorrespondentA(CString(val));}
void PPSetCurrency(char*  val){Prior2DPPModule::_ppModule->setCurrency(CString(val));}
void PPSetDataNomerPasport(char*  val){Prior2DPPModule::_ppModule->setDataNomerPasport(CString(val));}
void PPSetDate(char*  val){Prior2DPPModule::_ppModule->setDate(CString(val));}
void PPSetPaymentDetailes(char*  val){Prior2DPPModule::_ppModule->setDetaliPlatezha(CString(val));}
void PPSetNameA(char*  val){Prior2DPPModule::_ppModule->setNameA(CString(val));}
void PPSetNameB(char*  val){Prior2DPPModule::_ppModule->setNameB(CString(val));}
void PPSetNumber(char*  val){Prior2DPPModule::_ppModule->setNumber(CString(val));}
void PPSetOrdinary(char*  val){Prior2DPPModule::_ppModule->setOrdinary(CString(val));}
void PPSetPaymentOrder(char*  val){Prior2DPPModule::_ppModule->setPaymentOrder(CString(val));}
void PPSetPaymentPurpose(char*  val){Prior2DPPModule::_ppModule->setPaymentPurpose(CString(val));}
void PPSetPaymentType(char*  val){Prior2DPPModule::_ppModule->setPaymentType(CString(val));}
void PPSetPL(char*  val){Prior2DPPModule::_ppModule->setPL(CString(val));}
void PPSetPL_BN(char*  val){Prior2DPPModule::_ppModule->setPL_BN(CString(val));}
void PPSetAccountC(char*  val){Prior2DPPModule::_ppModule->setSchetBankCor(CString(val));}
void PPSetSumDigits(char*  val){Prior2DPPModule::_ppModule->setSumDigits(CString(val));}
void PPSetSumSpelled(char*  val){Prior2DPPModule::_ppModule->setSumSpelled(CString(val));}
void PPSetUnpA(char*  val){Prior2DPPModule::_ppModule->setUnnA(CString(val));}
void PPSetUnpB(char*  val){Prior2DPPModule::_ppModule->setUnnB(CString(val));}
void PPSetUnpThird(char*  val){Prior2DPPModule::_ppModule->setUNP_Third(CString(val));}
void PPSetUrgent(char*  val){Prior2DPPModule::_ppModule->setUrgent(CString(val));}
void PPSetCompanyFirstMan(char*  val){Prior2DPPModule::_ppModule->setCompanyFirstMan(CString(val));}
void PPSetCompanySecondMan(char*  val){Prior2DPPModule::_ppModule->setCompanySecondMan(CString(val));};
void PPSetDocCode(char*  val){Prior2DPPModule::_ppModule->setDocCode(CString(val));};
void PPSetSeriaNum(char*  val){Prior2DPPModule::_ppModule->setSeriaNum(CString(val));};
void PPSetIssueDate(char*  val){Prior2DPPModule::_ppModule->setIssueDate(CString(val));};
void PPSetIssueCompany(char*  val){Prior2DPPModule::_ppModule->setIssueCompany(CString(val));};
void PPSetLiveAdress(char*  val){Prior2DPPModule::_ppModule->setLiveAdress(CString(val));};

